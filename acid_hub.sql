-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2019 at 04:20 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `acid_hub`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `birth_date` date NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `additional_massage` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_blog`
--

CREATE TABLE `tbl_admin_blog` (
  `blog_id` int(10) NOT NULL,
  `admin_id` int(10) DEFAULT NULL,
  `blog_title` varchar(150) NOT NULL,
  `blog_content` text NOT NULL,
  `blog_image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

CREATE TABLE `tbl_blog` (
  `blog_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `blog_author_name` varchar(255) NOT NULL,
  `blog_title` varchar(255) NOT NULL,
  `blog_description` text NOT NULL,
  `created_date` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog_image`
--

CREATE TABLE `tbl_blog_image` (
  `blog_mult_img` varchar(255) NOT NULL,
  `blog_img_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

CREATE TABLE `tbl_faq` (
  `faq_id` int(10) NOT NULL,
  `admin_id` varchar(10) DEFAULT NULL,
  `question` varchar(100) NOT NULL,
  `answer` text NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_interior_resume`
--

CREATE TABLE `tbl_interior_resume` (
  `job_resume_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `locality` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `cvfile` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job`
--

CREATE TABLE `tbl_job` (
  `job_id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `job_experiance` varchar(255) NOT NULL,
  `no_of_vacancy` varchar(255) NOT NULL,
  `package` varchar(255) NOT NULL,
  `job_type` varchar(255) NOT NULL,
  `min_qualification` varchar(255) NOT NULL,
  `skills` varchar(255) NOT NULL,
  `job_position` varchar(255) NOT NULL,
  `created_date` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `job_description` text NOT NULL,
  `gender` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project`
--

CREATE TABLE `tbl_project` (
  `project_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_location` varchar(255) NOT NULL,
  `project_description` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `sub_region` varchar(255) NOT NULL,
  `project_title` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_project`
--

INSERT INTO `tbl_project` (`project_id`, `project_name`, `project_location`, `project_description`, `user_id`, `created_date`, `updated_date`, `sub_region`, `project_title`, `status`) VALUES
(40, '', 'Mumbai', 'cfvd ghyt gh', 65, '2019-01-15', '0000-00-00', 'Parel', 'Eye clinic mngt', 'open');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project_img`
--

CREATE TABLE `tbl_project_img` (
  `project_img_id` int(11) NOT NULL,
  `user_file` varchar(255) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_project_img`
--

INSERT INTO `tbl_project_img` (`project_img_id`, `user_file`, `project_id`) VALUES
(92, 'uploads/images_(6)2.jpg', 40),
(91, 'uploads/images_(4)3.jpg', 40),
(90, 'uploads/images_(3)_-_Copy.jpg', 40);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `student_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `confirmpassword` varchar(255) NOT NULL,
  `bday` date NOT NULL,
  `gender` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `image_file` varchar(255) NOT NULL,
  `resume_upload` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_education`
--

CREATE TABLE `tbl_student_education` (
  `education_id` int(11) NOT NULL,
  `my_university` varchar(255) NOT NULL,
  `education_from` varchar(255) NOT NULL,
  `education_to` varchar(255) NOT NULL,
  `graduate` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `experience_from` varchar(255) NOT NULL,
  `experience_to` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `content` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_job_apply`
--

CREATE TABLE `tbl_student_job_apply` (
  `student_job_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `status` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_professional`
--

CREATE TABLE `tbl_student_professional` (
  `professional_id` int(11) NOT NULL,
  `total_experience` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `key_skill` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_resume`
--

CREATE TABLE `tbl_student_resume` (
  `student_resume_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `personal_objectivities` text NOT NULL,
  `career_objectivities` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_date` date NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `hobby` varchar(255) NOT NULL,
  `student_address` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `skills` varchar(255) NOT NULL,
  `upload_image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_resume_skill`
--

CREATE TABLE `tbl_student_resume_skill` (
  `skills` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `student_resume_id` int(11) NOT NULL,
  `resume_id` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_details`
--

CREATE TABLE `tbl_users_details` (
  `user_details_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `bday` date NOT NULL,
  `contact` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `firm_firstname` varchar(255) DEFAULT NULL,
  `firm_contact` varchar(255) DEFAULT NULL,
  `firm_address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `website` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `confirmpassword` varchar(255) NOT NULL,
  `user_file` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `user_unique_id` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_admin_blog`
--
ALTER TABLE `tbl_admin_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `tbl_blog_image`
--
ALTER TABLE `tbl_blog_image`
  ADD PRIMARY KEY (`blog_img_id`);

--
-- Indexes for table `tbl_faq`
--
ALTER TABLE `tbl_faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `tbl_interior_resume`
--
ALTER TABLE `tbl_interior_resume`
  ADD PRIMARY KEY (`job_resume_id`);

--
-- Indexes for table `tbl_job`
--
ALTER TABLE `tbl_job`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `tbl_project`
--
ALTER TABLE `tbl_project`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `tbl_project_img`
--
ALTER TABLE `tbl_project_img`
  ADD PRIMARY KEY (`project_img_id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `tbl_student_education`
--
ALTER TABLE `tbl_student_education`
  ADD PRIMARY KEY (`education_id`);

--
-- Indexes for table `tbl_student_job_apply`
--
ALTER TABLE `tbl_student_job_apply`
  ADD PRIMARY KEY (`student_job_id`);

--
-- Indexes for table `tbl_student_professional`
--
ALTER TABLE `tbl_student_professional`
  ADD PRIMARY KEY (`professional_id`);

--
-- Indexes for table `tbl_student_resume`
--
ALTER TABLE `tbl_student_resume`
  ADD PRIMARY KEY (`student_resume_id`);

--
-- Indexes for table `tbl_student_resume_skill`
--
ALTER TABLE `tbl_student_resume_skill`
  ADD PRIMARY KEY (`resume_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_users_details`
--
ALTER TABLE `tbl_users_details`
  ADD PRIMARY KEY (`user_details_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_admin_blog`
--
ALTER TABLE `tbl_admin_blog`
  MODIFY `blog_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_blog_image`
--
ALTER TABLE `tbl_blog_image`
  MODIFY `blog_img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `tbl_faq`
--
ALTER TABLE `tbl_faq`
  MODIFY `faq_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_interior_resume`
--
ALTER TABLE `tbl_interior_resume`
  MODIFY `job_resume_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_job`
--
ALTER TABLE `tbl_job`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_project`
--
ALTER TABLE `tbl_project`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `tbl_project_img`
--
ALTER TABLE `tbl_project_img`
  MODIFY `project_img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_student_education`
--
ALTER TABLE `tbl_student_education`
  MODIFY `education_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_student_job_apply`
--
ALTER TABLE `tbl_student_job_apply`
  MODIFY `student_job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_student_professional`
--
ALTER TABLE `tbl_student_professional`
  MODIFY `professional_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_student_resume`
--
ALTER TABLE `tbl_student_resume`
  MODIFY `student_resume_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `tbl_student_resume_skill`
--
ALTER TABLE `tbl_student_resume_skill`
  MODIFY `resume_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `tbl_users_details`
--
ALTER TABLE `tbl_users_details`
  MODIFY `user_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
