<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JobController extends CI_Controller
 {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function create_job()
	{
	    if(!($this->session->userdata('user_id')))
	    {
	      redirect('signup','refresh');
	    }
	    else
	    {
			$this->load->view('backend/includes/header');
			$this->load->view('backend/job/create-job');
			$this->load->view('backend/includes/footer');
		}
	}//public function create_project()

	public function job_insert()
	{   
		$this->load->library('form_validation');
		$this->form_validation->set_rules('company_name','company_name','required|trim')
		;
		$this->form_validation->set_rules('job_title','job_title','required|trim');
		$this->form_validation->set_rules('job_position','job_position','required|trim');
		$this->form_validation->set_rules('job_experiance','job_experiance','required|trim');
		$this->form_validation->set_rules('no_of_vacancy','no_of_vacancy','required');
		$this->form_validation->set_rules('package','package','required|trim');
		$this->form_validation->set_rules('job_type','job_type','required|trim');
		$this->form_validation->set_rules('gender','gender','required|trim');
		$this->form_validation->set_rules('min_qualification','min_qualification','required|trim');

		$this->form_validation->set_rules('skills','skills','required|trim');
		$this->form_validation->set_rules('job_description','job_description','required|trim');
		if($this->form_validation->run() == true)
         {
		$user_id=$this->session->userdata('user_id');
		$data=array
		  (
   			"company_name"=>ucfirst($this->input->post("company_name")),
			"job_title"=>ucfirst($this->input->post("job_title")),
         	"job_position"=>$this->input->post("job_position"),
            "job_experiance"=>$this->input->post("job_experiance"),
            "no_of_vacancy "=>$this->input->post("no_of_vacancy"), 
            "package"=>$this->input->post("package"),
            "job_type"=>$this->input->post("job_type"),
            "gender"=>$this->input->post("gender"),
            "min_qualification"=>$this->input->post("min_qualification"),
            "skills"=>$this->input->post("skills"),
            "job_description"=>$this->input->post("job_description"),
            "created_date"=>date('Y-m-d'),
              "status"=>"open",
            "user_id"=>$user_id
          );
	
             $this->load->model("JobModel");
          	 $result=$this->JobModel->jobaddcreate($data);
			 if($result)
			 { 
			    $this->session->set_flashdata('Feedback',"Thank You Job Is Inserted.");
			    return redirect("job-dashboard",'refresh');
			  }
			else
			  {
			  	$this->session->set_flashdata('Feedback',"Job Information Not inserted successfully.");
				echo "Record Not Inserted";
			  }
		}//if($this->form_validation->run() == true)

			else
              {
                    echo validation_errors('<div class="errors">', '</div>');
                        redirect("create-job");
              }//else
			
    }//public function job_insert()

    public function job_dashboard()
		{
		    if(!($this->session->userdata('user_id')))
		    {
		      redirect('signup','refresh');
		    }
		    else
		    {
		    	$this->load->view('backend/includes/header');
			    $id=$this->session->userdata("user_id");
			    $this->load->model("JobModel");
			    $result["ans"]=$this->JobModel->searchjob($id);
			 /*   print_r($result);
			    exit();*/
				$this->load->view('backend/job/job-dashboard',$result);
				$this->load->view('backend/includes/footer');
		    }//else(session)
		}//public function job_dashboard()

	public function edit_job($job_id)
          {   
	          if(!($this->session->userdata('user_id')))
			    {
			      redirect('signup','refresh');
			    }
			    else
			    {   
		            $this->load->view('backend/includes/header');
		            $this->load->model("JobModel");
		          	$data['ans'] = $this->JobModel->find_job($job_id);
		            $this->load->view("backend/job/edit-job",$data);
		        }//else
          }//public function edit_job($job_id)

    public function delete_job($job_id)
    {
                          
	    $this->load->model("JobModel");
	    if($this->JobModel->deletejob($job_id)) 
	      {
         	 $this->session->set_flashdata('Delete',"Job  deleted Successfully");
             return redirect('job-dashboard');
          }
        else
          {    
             $this->session->set_flashdata('error',"Job  failed to delete, please try again");
             return redirect('job-dashboard');
          }
    }// public function delete_job($job_id)


    public function update_job($id)
    {	 
		 $user_id=$this->session->userdata('user_id');
		 $config['upload_path']          = './uploads/';
         $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
         $config['max_size']             = 100000;
         $config['max_width']            = 1024;
         $config['max_height']           = 768;
		  
		$data=array
		(
  			"company_name"=>ucfirst($this->input->post("company_name")),
			"job_title"=>ucfirst($this->input->post("job_title")),
	        "job_position"=>$this->input->post("job_position"),
	        "job_experiance"=>$this->input->post("job_experiance"),
            "no_of_vacancy "=>$this->input->post("no_of_vacancy"), 
            "package"=>$this->input->post("package"),
            "job_type"=>$this->input->post("job_type"),
            "gender"=>$this->input->post("gender"),
            "min_qualification"=>$this->input->post("min_qualification"),
            "skills"=>$this->input->post("skills"),
            "job_description"=>$this->input->post("job_description"),
            "created_date"=>date('Y-m-d'),
            "status"=>"updated",
            "user_id"=>$user_id
	    );
		                
		   $this->load->model("JobModel");
		   if($this->JobModel->updateq($id,$data))
		  	{
		  	
		  		$this->session->set_flashdata('Feedback',"Job Information Updated successfully.");
		  		redirect ("job-dashboard",'refresh');
		  	}
				  	else{
				  		$this->session->set_flashdata('Feedback',"Job Information Not Updated successfully.");
				  		echo"error";
				  	}
    }//public function update_job($id)

    public function job_details($job_id)
	{
			if(!($this->session->userdata('user_id')))
		{
			redirect('signup','refresh');
		}
		$this->load->view('backend/includes/header');
		$id=$this->session->userdata("user_id");
		$this->load->model("JobModel");
		$result["ans"]=$this->JobModel->searchjobdetails($id,$job_id);
		$result["job"] = $this->JobModel->jobapplydetails($job_id);
		$this->load->view('backend/job/job-details',$result);
		$this->load->view('backend/includes/footer');
	}// public function job_details($job_id)


	       /* **************** manage resume ********************/

	public function manage_resume()
	{
	    if(!($this->session->userdata('user_id')))
	    {
	      redirect('signup','refresh');
	    }
	    else
	    {
			$this->load->view('backend/includes/header');
			$this->load->view('backend/job/manage-resume');
			$this->load->view('backend/includes/footer');
		}
	}//public function create_project()
	public function alljob()
	{
     	if(!($this->session->userdata('user_id')))
	    {
	      redirect('signup','refresh');
	    }
	    else
	    {
			$this->load->view('backend/includes/header');
			$this->load->model("JobModel");
	    	$result["ans"]=$this->JobModel->alljobspecific();
			$this->load->view('backend/job/myjob',$result);
			$this->load->view('backend/includes/footer');
		}
    }//public function alljob()
    public function acidjobdetails($job_id)
	{
		$this->load->view('backend/includes/header');
		$this->load->model("JobModel");
		$result['ans']=$this->JobModel->allselectjob($job_id);
		$result["studentjob"] = $this->JobModel->studentjobapplydetails($job_id);
		$this->load->view('backend/job/alljobdetails',$result);
		$this->load->view('backend/includes/footer');
	}//public function jobdetails($job_id)
	
	/*public function send_mail()
  {

  	  $this->load->library('email');
  	   $config = Array(
	  $config['protocol'] => 'smtp',
	  $config['smtp_host'] => 'XXX',
	  $config['smtp_user'] => 'xxx', // change it to yours
	  $config['smtp_pass'] => 'xxx', // change it to yours
	  $config['smtp_port']=> 25;
  $this->email->initialize('$config');
		$this->email->from('harshadakh9503@gmail.com', 'harshada');
		$this->email->to('someone@example.com');
		$this->email->cc('another@another-example.com');
		$this->email->bcc('them@their-example.com');
		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');
		$this->email->send();
  }*/
  

}//class JobController extends CI_Controller
