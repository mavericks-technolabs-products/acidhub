<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller
{
	public function adminsignup()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->view('backend/admin/adminsignup');
		$this->load->view('backend/admin/includes/footer');
	}

	public function admin_signup()
	{
		$config=[
               		'upload_path'=>'./uploads/',
                	'allowed_types'=>'jpg|gif|png|jpeg',
                ];
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->load->helper('form');

		$firstname=$this->input->post("firstname");
	    $lastname=$this->input->post("lastname");
	    $email=$this->input->post("email");
	    $password=md5("password");
	    /*$reg_password=$this->input->post("password");*/
	    $address=$this->input->post("address");
	    $city=$this->input->post("city");
	    $state=$this->input->post("state");
	    $country=$this->input->post("country");
	    $optradio=$this->input->post("optradio");
	    $birth_date=$this->input->post("birth_date");
	    $contact=$this->input->post("contact");
	    $additional_massage=$this->input->post("additional_massage");

		if($this->upload->do_upload('userfile'))
		{
			$data=$this->upload->data();
	        $image_path=$data["raw_name"].$data['file_ext'];

	        $insert=['firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'password'=>$password,/*'reg_password'=>$reg_password,*/'address'=>$address,'city'=>$city,'state'=>$state,'country'=>$country,'gender'=>$optradio,'birth_date'=>$birth_date,'contact'=>$contact,'additional_massage'=>$additional_massage,'user_image'=>$image_path];

	        $this->load->model('AdminModel');

	        if($this->AdminModel->insertadmin($insert))
	        {
	            $error="Add Admin Successful.";
	            $this->session->set_flashdata('error',$error);
	            $this->session->set_flashdata('status','btn-success');
	            redirect(base_url('dashboard'));
	       	}
	        else
	        {
	            redirect(base_url('adminsignup'));                         
	        }
		}
		else
		{
	        $insert=['firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'password'=>$password,'reg_password'=>$reg_password,'address'=>$address,'city'=>$city,'state'=>$state,'country'=>$country,'gender'=>$optradio,'birth_date'=>$birth_date,'contact'=>$contact,'additional_massage'=>$additional_massage];

	        $this->load->model('AdminModel');

	        if($this->AdminModel->insertadmin($insert))
	        {
	            $error="Add Admin Successful.";
	            $this->session->set_flashdata('error',$error);
	            $this->session->set_flashdata('status','btn-success');
	            redirect(base_url('dashboard'));
	       	}
	        else
	        {
	            redirect(base_url('adminsignup'));                         
	        }
		}
		
	}

	public function cheq_number()
	{
		$number=$_POST['number'];
        $this->load->model('AdminModel');
        if(!empty($number))
        {
        	$find_number=$this->AdminModel->findcontact($number);
        	if($find_number)
	        {
	        	echo json_encode($data['json']="Contact Number Already Exist.");
	        }
	        else
	        {
	        	echo json_encode($data['json']="Success.");
	        }
        }
        else
        {
        	echo json_encode($data['json']="");
        }
	}

	public function cheq_email()
	{
		$email=$_POST['email'];
        $this->load->model('AdminModel');
        if(!empty($email))
        {
        	$find_email=$this->AdminModel->findemail($email);
        	if($find_email)
	        {
	        	echo json_encode($data['json']="Email Already Exist.");
	        }
	        else
	        {
	        	echo json_encode($data['json']="Success.");
	        }
        }
        else
        {
        	echo json_encode($data['json']="");
        }
	}

	public function dashboard()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->view('backend/admin/dashboard');
		$this->load->view('backend/admin/includes/footer');
	}

	public function insert_blog()
	{
		$config=[
               		'upload_path'=>'./uploads/admin/blogs/',
                	'allowed_types'=>'jpg|gif|png|jpeg',
                ];
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->load->helper('form');

        if($this->upload->do_upload('userfile'))
		{
			$blog_title=$this->input->post("blogtitle");
	    	$blog_content=$this->input->post("blog_content");
	    	$admin_id=$this->session->userdata('user_id');

	    	$data=$this->upload->data();
	        $image_path=$data["raw_name"].$data['file_ext'];

	        $insert=['blog_title'=>$blog_title,'blog_content'=>$blog_content,'admin_id'=>$admin_id,'blog_image'=>$image_path];

	        $this->load->model('AdminModel');
	        if($this->AdminModel->insertblog($insert))
	        {
	            $error="Add Blog Successful.";
	            $this->session->set_flashdata('error',$error);
	            $this->session->set_flashdata('status','btn-success');
	            redirect(base_url('admin_blog'));
	       	}
	        else
	        {
	            redirect(base_url('add_blog'));                         
	        }
	    }
	    else
	    {
	    	return redirect(base_url('add_blog'));
	    }
	}

	public function blog_edit($blog_id)
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$blog = $this->AdminModel->editblog($blog_id);
		$this->load->view('backend/admin/blog_edit',['blog'=>$blog]);
		$this->load->view('backend/admin/includes/footer');
	}

	public function edit_insert_blog()
	{
		$config=[
               		'upload_path'=>'./uploads/admin/blogs/',
                	'allowed_types'=>'jpg|gif|png|jpeg',
                ];
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->load->helper('form');

        if($this->upload->do_upload('userfile'))
		{
			$blog_title=$this->input->post("blogtitle");
	    	$blog_content=$this->input->post("blog_content");
	    	$admin_id=$this->session->userdata('user_id');
	    	$blog_id=$this->input->post("blogid");

	    	$old_image_path = $this->input->post("blog_image");
	    	unlink("uploads/admin/blogs/".$old_image_path);

	    	$fdata=$this->upload->data();
	        $image_path=$fdata["raw_name"].$fdata['file_ext'];

	        $insert=['blog_title'=>$blog_title,'blog_content'=>$blog_content,'admin_id'=>$admin_id,'blog_image'=>$image_path];

	        $this->load->model('AdminModel');
	        if($this->AdminModel->editinsertblog($insert,$blog_id))
	        {
	            $error="Edit Blog Successful.";
	            $this->session->set_flashdata('error',$error);
	            $this->session->set_flashdata('status','btn-success');
	            redirect(base_url('admin_blog'));
	       	}
	        else
	        {
	            redirect(base_url('blog_edit'));                         
	        }
	    }
	    else
	    {
	    	$blog_title=$this->input->post("blogtitle");
	    	$blog_content=$this->input->post("blog_content");
	    	$blog_image=$this->input->post("blog_image");
	    	$admin_id=$this->session->userdata('user_id');
	    	$blog_id=$this->input->post("blogid");

	        $insert=['blog_title'=>$blog_title,'blog_content'=>$blog_content,'admin_id'=>$admin_id,'blog_image'=>$blog_image];

	        $this->load->model('AdminModel');
	        if($this->AdminModel->editinsertblog($insert,$blog_id))
	        {
	            $error="Edit Blog Successful.";
	            $this->session->set_flashdata('error',$error);
	            $this->session->set_flashdata('status','btn-success');
	            redirect(base_url('admin_blog'));
	       	}
	        else
	        {
	            redirect(base_url('blog_edit'));
	        }
	    }
	}

	public function deleteblog($blog_id,$image)
	{
		$this->load->model('AdminModel');                    
        $this->load->helper("file");

        if($this->AdminModel->delete_blog($blog_id))
        {
        		unlink("uploads/admin/blogs/".$image);
            	redirect(base_url('admin_blog'));
        }
        else
        {
            echo "Delete Failed";   	
            redirect(base_url('admin_blog'));
        }
	}

	public function deletefaq($faq_id)
	{
		$this->load->model('AdminModel');
        if($this->AdminModel->delete_faq($faq_id))
        {
            redirect(base_url('faq'));
        }
	}

	public function blogstatus()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$blog['user_blog_status'] = $this->AdminModel->requestbloglist();
		$this->load->view('backend/admin/blogstatus',$blog);
		$this->load->view('backend/admin/includes/footer');
	}

	public function changeblogstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$blogstatus = $this->AdminModel->change_blogs_status($id,$type);
		if($blogstatus)
		{
			return redirect(base_url('blogstatus'));
		}
	}

	public function activeblog()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$blog['active_blog'] = $this->AdminModel->activebloglist();
		$this->load->view('backend/admin/activeblog',$blog);
		$this->load->view('backend/admin/includes/footer');
	}

	public function admin_change_blog_status($id,$type)
	{
		$this->load->model('AdminModel');
		$blogstatus = $this->AdminModel->change_blogs_status($id,$type);

		if($blogstatus)
		{
			return redirect(base_url('activeblogs'));
		}
	}

	public function edit_blog()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$blog['updated_blog'] = $this->AdminModel->editedbloglist();
		$this->load->view('backend/admin/editblog',$blog);
		$this->load->view('backend/admin/includes/footer');
	}

	public function edit_blog_status($id,$type)
	{
		$this->load->model('AdminModel');
		$blogstatus = $this->AdminModel->change_blogs_status($id,$type);

		if($blogstatus)
		{
			return redirect(base_url('editedblogs'));
		}
	}

	public function declinedblogs()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$blog['declined_blog'] = $this->AdminModel->declinedbloglist();
		$this->load->view('backend/admin/declinedblog',$blog);
		$this->load->view('backend/admin/includes/footer');
	}

	public function changedeclinedblogs($id,$type)
	{
		$this->load->model('AdminModel');
		$blogstatus = $this->AdminModel->change_blogs_status($id,$type);

		if($blogstatus)
		{
			return redirect(base_url('declinedblogs'));
		}
	}

	/*******FAQ's********/
	public function insert_faq()
	{
        $this->load->library('form_validation');
		$this->form_validation->set_rules('question','question','required');
		$this->form_validation->set_rules('answer','answer','required');

		$this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
        if($this->form_validation->run())
		{
			$question=$this->input->post("question");
	    	$answer=$this->input->post("answer");
	    	$admin_id=$this->session->userdata('user_id');

	        $insert=['question'=>$question,'answer'=>$answer,'admin_id'=>$admin_id];

	        $this->load->model('AdminModel');
	        if($this->AdminModel->insertfaq($insert))
	        {
	            $error="Add FAQ Successful.";
	            $this->session->set_flashdata('error',$error);
	            $this->session->set_flashdata('status','btn-success');
	            redirect(base_url('faq'));
	       	}
	        else
	        {
	            redirect(base_url('add_faq'));                         
	        }
	    }
	    else
	    {
	    	$this->load->view('backend/admin/includes/header');
			$this->load->view('backend/admin/add_faq');
			$this->load->view('backend/admin/includes/footer');
	    }
	}

	public function faq_edit($faq_id)
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$faq['edit_faq'] = $this->AdminModel->faqedit($faq_id);
		$this->load->view('backend/admin/edit_faq',$faq);
		$this->load->view('backend/admin/includes/footer');
	}

	public function edit_faq()
	{
        $this->load->library('form_validation');
		$this->form_validation->set_rules('question','question','required');
		$this->form_validation->set_rules('answer','answer','required');

		$this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
        if($this->form_validation->run())
		{
			$question=$this->input->post("question");
	    	$answer=$this->input->post("answer");
	    	$admin_id=$this->session->userdata('user_id');
	    	$faq_id=$this->input->post('faqid');

	        $insert=['question'=>$question,'answer'=>$answer,'admin_id'=>$admin_id];

	        $this->load->model('AdminModel');
	        if($this->AdminModel->faq_edit($insert,$faq_id))
	        {
	            $error="Edit FAQ Successful.";
	            $this->session->set_flashdata('error',$error);
	            $this->session->set_flashdata('status','btn-success');
	            redirect(base_url('faq'));
	       	}
	        else
	        {
	            redirect(base_url('edit_faq'));                         
	        }
	    }
	    else
	    {
	    	$this->load->view('backend/admin/includes/header');
			$this->load->view('backend/admin/edit_faq');
			$this->load->view('backend/admin/includes/footer');
	    }
	}

	/***********Job Status************/

	public function jobstatus()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$job['job_status'] = $this->AdminModel->requestjoblist();
		$this->load->view('backend/admin/jobstatus',$job);
		$this->load->view('backend/admin/includes/footer');
	}

	public function changejobstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$jobtatus = $this->AdminModel->change_jobs_status($id,$type);
		if($jobtatus)
		{
			return redirect(base_url('jobstatus'));
		}
	}

	public function activejob()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$job['active_job'] = $this->AdminModel->activejoblist();
		$this->load->view('backend/admin/activejobs',$job);
		$this->load->view('backend/admin/includes/footer');
	}

	public function adminchangejobstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$jobtatus = $this->AdminModel->change_jobs_status($id,$type);
		if($jobtatus)
		{
			return redirect(base_url('activejobs'));
		}
	}

	public function editrequestjoblist()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$job['edit_job'] = $this->AdminModel->editrequestjoblist();
		$this->load->view('backend/admin/jobedit',$job);
		$this->load->view('backend/admin/includes/footer');
	}

	public function editjobstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$jobtatus = $this->AdminModel->change_jobs_status($id,$type);
		if($jobtatus)
		{
			return redirect(base_url('editededucation'));
		}
	}

	public function declinedjoblist()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$job['delined_job'] = $this->AdminModel->declinedjoblist();
		$this->load->view('backend/admin/declinedjobs.php',$job);
		$this->load->view('backend/admin/includes/footer');
	}

	public function declinedjobstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$jobtatus = $this->AdminModel->change_jobs_status($id,$type);
		if($jobtatus)
		{
			return redirect(base_url('declinedjobs'));
		}
	}


	/***********End Job Status************/

	/**************Project Status**************/

	public function activeproject()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$project['active_project'] = $this->AdminModel->activeprojectlist();
		$this->load->view('backend/admin/activeproject',$project);
		$this->load->view('backend/admin/includes/footer');
	}

	public function adminchangeprojectstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$projectstatus = $this->AdminModel->change_projects_status($id,$type);
		if($projectstatus)
		{
			return redirect(base_url('activeprojects'));
		}
	}

	public function projectstatus()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$project['project_status'] = $this->AdminModel->requestprojectlist();
		$this->load->view('backend/admin/projectstatus',$project);
		$this->load->view('backend/admin/includes/footer');
	}

	public function changeprojectstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$projectstatus = $this->AdminModel->change_projects_status($id,$type);
		if($projectstatus)
		{
			return redirect(base_url('projectstatus'));
		}
	}

	public function editprojectlist()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$project['edit_project'] = $this->AdminModel->editprojectslist();
		$this->load->view('backend/admin/projectedit',$project);
		$this->load->view('backend/admin/includes/footer');
	}

	public function editprojectstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$projectstatus = $this->AdminModel->change_projects_status($id,$type);
		if($projectstatus)
		{
			return redirect(base_url('editedprojects'));
		}
	}

	public function declinedprojectlist()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$project['declined_project'] = $this->AdminModel->declineprojectslist();
		$this->load->view('backend/admin/projectdecline',$project);
		$this->load->view('backend/admin/includes/footer');
	}

	public function declinedprojectstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$projectstatus = $this->AdminModel->change_projects_status($id,$type);
		if($projectstatus)
		{
			return redirect(base_url('declinedprojects'));
		}
	}

	/*public function activejob()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$job['active_job'] = $this->AdminModel->activejoblist();
		$this->load->view('backend/admin/activejobs',$job);
		$this->load->view('backend/admin/includes/footer');
	}

	public function adminchangejobstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$jobtatus = $this->AdminModel->change_jobs_status($id,$type);
		if($jobtatus)
		{
			return redirect(base_url('activejobs'));
		}
	}

	public function editrequestjoblist()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$job['edit_job'] = $this->AdminModel->editrequestjoblist();
		$this->load->view('backend/admin/jobedit',$job);
		$this->load->view('backend/admin/includes/footer');
	}

	public function editjobstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$jobtatus = $this->AdminModel->change_jobs_status($id,$type);
		if($jobtatus)
		{
			return redirect(base_url('editedjobs'));
		}
	}

	public function declinedjoblist()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$job['delined_job'] = $this->AdminModel->declinedjoblist();
		$this->load->view('backend/admin/declinedjobs.php',$job);
		$this->load->view('backend/admin/includes/footer');
	}

	public function declinedjobstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$jobtatus = $this->AdminModel->change_jobs_status($id,$type);
		if($jobtatus)
		{
			return redirect(base_url('declinedjobs'));
		}
	}*/


	/***********End Job Status************/
	/************** education status *************/


	public function educationstatus()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$education['education_status'] = $this->AdminModel->requesteducationlist();
		$this->load->view('backend/admin/educationstatus',$education);
		$this->load->view('backend/admin/includes/footer');
	}

	public function changeeducationstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$educationtatus = $this->AdminModel->change_educations_status($id,$type);
		if($educationtatus)
		{
			return redirect(base_url('educationstatus'));
		}
	}

	public function activeeducation()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$education['active_education'] = $this->AdminModel->activeeducationlist();
		$this->load->view('backend/admin/activeeducation',$education);
		$this->load->view('backend/admin/includes/footer');
	}
	
	public function adminchangeeducationstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$educationtatus = $this->AdminModel->change_educations_status($id,$type);
		if($educationtatus)
		{
			return redirect(base_url('activeeducation'));
		}
	}
	public function editeducationlist()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$education['edit_education'] = $this->AdminModel->editeducationslist();
		$this->load->view('backend/admin/editeducation',$education);
		$this->load->view('backend/admin/includes/footer');
	}
	
	public function editeducationstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$educationtatus = $this->AdminModel->change_educations_status($id,$type);
		if($educationtatus)
		{
			return redirect(base_url('editededucation'));
		}
	}
	public function declinededucationlist()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$education['delined_education'] = $this->AdminModel->declinededucationlist();
		$this->load->view('backend/admin/declinededucation.php',$education);
		$this->load->view('backend/admin/includes/footer');
	}
	public function declinededucationstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$educationtatus = $this->AdminModel->change_educations_status($id,$type);
		if($educationtatus)
		{
			return redirect(base_url('declinededucation'));
		}
	}


	/* ******************* end education *****************/
	/******************** education profession **********************/
	public function professionalstatus()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$professional['professional_status'] = $this->AdminModel->requestprofessionallist();
		/*print_r($professional['professional_status']);
		exit();*/
		$this->load->view('backend/admin/professionalstatus',$professional);
		$this->load->view('backend/admin/includes/footer');
	}
	public function changeprofessionalstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$professionalstatus = $this->AdminModel->change_professional_status($id,$type);
		if($professionalstatus)
		{
			return redirect(base_url('professionalstatus'));
		}
	}
	public function activeprofessional()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$professional['active_professional'] = $this->AdminModel->activeprofessionallist();
		$this->load->view('backend/admin/activeprofessional',$professional);
		$this->load->view('backend/admin/includes/footer');
	}

	public function adminchangeprofessionalstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$professionalstatus = $this->AdminModel->change_professional_status($id,$type);
		if($professionalstatus)
		{
			return redirect(base_url('activeprofessional'));
		}
	}
	
	
	public function editprofessionallist()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$professional['edit_professional'] = $this->AdminModel->editprofessionalslist();
		$this->load->view('backend/admin/professionaledit',$professional);
		$this->load->view('backend/admin/includes/footer');
	}
	public function editprofessionalstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$professionalstatus = $this->AdminModel->change_professional_status($id,$type);
		if($professionalstatus)
		{
			return redirect(base_url('editedprofessional'));
		}
	}

	public function declinedprofessionallist()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('AdminModel');
		$professional['declined_professional'] = $this->AdminModel->declineprojectslist();
		$this->load->view('backend/admin/professionaldecline',$professional);
		$this->load->view('backend/admin/includes/footer');
	}

	public function declinedprofessionalstatus($id,$type)
	{
		$this->load->model('AdminModel');
		$professionalstatus = $this->AdminModel->change_professional_status($id,$type);
		if($professionalstatus)
		{
			return redirect(base_url('declinedprofessional'));
		}
	}
	/*********************** resume ***************************/

	
	/**************** End education status *************/

	public function __construct()
	{
		parent::__construct();			
		$this->load->helper('form');
		$this->load->library('session');
        if($this->session->userdata('user_id') == NULL || $this->session->userdata('user_id') == 0)
        {
            return redirect(base_url('adminlogin'));
        }
    }
}
	 