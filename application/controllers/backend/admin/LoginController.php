<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller
{
	public function adminlogin()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->view('backend/admin/adminlogin');
		$this->load->view('backend/admin/includes/footer');
	}

	public function adminvalid_login()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('email','email','required');
		$this->form_validation->set_rules('password','password','required');

		$this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
		if ( $this->form_validation->run() )
		{//if validation passess

			$email = $this->input->post('email');
			$password= $this->input->post('password');

			$this->load->model('AdminLoginModel');

			$login_id = $this->AdminLoginModel->login_valid( $email, $password);
			if ( $login_id )
			{
				$this->session->set_userdata('user_id', $login_id);
				$this->load->model('AdminLoginModel');
				$welcome="Welcome to ACID.";
                $this->session->set_flashdata('welcome',$welcome);
                $this->session->set_flashdata('status','btn-success');
				redirect(base_url('dashboard'));				
			}
			else
			{
				
	        	$error="Please enter valid email and password.";
	            $this->session->set_flashdata('error',$error);
				redirect(base_url('adminlogin'));
			}
		}
		else
		{

	        $error="Please enter valid email and password.";
	        $this->session->set_flashdata('error',$error);
	        $this->session->set_flashdata('status','btn-success');
			redirect(base_url('adminlogin'));
		}
	}

	
}
	 