<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends CI_Controller
{
	public function dashboard()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->view('backend/admin/dashboard');
		$this->load->view('backend/admin/includes/footer');
	}

	public function userstatus()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('DashboardModel');
		$interior = $this->DashboardModel->user_list();
		$this->load->view('backend/admin/interiorstatus',['interior'=>$interior]);
		$this->load->view('backend/admin/includes/footer');
	}

	public function activeusers()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('DashboardModel');
		$user = $this->DashboardModel->active_user_list();
		$this->load->view('backend/admin/activeinteriorsusers',['user'=>$user]);
		$this->load->view('backend/admin/includes/footer');
	}

	public function changeinteriorstatus($id,$type)
	{
		$this->load->model('DashboardModel');
		$interiorstatus = $this->DashboardModel->change_interior_status($id,$type);
		if($interiorstatus)
		{

			return redirect(base_url('userstatus'));
		}
	}

	public function admin_change_interior_status($id,$type)
	{
		$this->load->model('DashboardModel');
		$interiorstatus = $this->DashboardModel->admin_change_interior_status($id,$type);
		if($interiorstatus)
		{
			return redirect(base_url('activeusers'));
		}
	}

	public function architecturestatus()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('DashboardModel');
		$architecture = $this->DashboardModel->architecture_list();
		$this->load->view('backend/admin/architecturestatus',['architecture'=>$architecture]);
		$this->load->view('backend/admin/includes/footer');
	}

	public function changearchitecturestatus($id,$type)
	{
		$this->load->model('DashboardModel');
		$interiorstatus = $this->DashboardModel->change_architecture_status($id,$type);
		if($interiorstatus)
		{
			return redirect(base_url('architecturestatus'));
		}
	}

	public function architecturesusers()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('DashboardModel');
		$architecture = $this->DashboardModel->active_architecture_list();
		$this->load->view('backend/admin/activearchitecturesusers',['architecture'=>$architecture]);
		$this->load->view('backend/admin/includes/footer');
	}

	public function admin_change_architecture_status($id,$type)
	{
		$this->load->model('DashboardModel');
		$interiorstatus = $this->DashboardModel->admin_change_architecture_status($id,$type);
		if($interiorstatus)
		{
			return redirect(base_url('activearchitecturesusers'));
		}
	}

	public function declinedusers()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('DashboardModel');
		$interior['decline_interior'] = $this->DashboardModel->decline_interior_list();
		$this->load->view('backend/admin/declinedusers',$interior);
		$this->load->view('backend/admin/includes/footer');
	}

	public function admin_change_decline_interior_status($id,$type)
	{
		$this->load->model('DashboardModel');
		$interiorstatus = $this->DashboardModel->admin_change_decline_interior_status($id,$type);
		if($interiorstatus)
		{
			return redirect(base_url('declinedusers'));
		}
	}

	public function admin_change_decline_architecture_status($id,$type)
	{
		$this->load->model('DashboardModel');
		$interiorstatus = $this->DashboardModel->admin_change_decline_architecture_status($id,$type);
		if($interiorstatus)
		{
			return redirect(base_url('declinedusers'));
		}
	}

	public function alladmin()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('DashboardModel');
		$admin = $this->DashboardModel->adminlist();
		$this->load->view('backend/admin/alladmin',['admin'=>$admin]);
		$this->load->view('backend/admin/includes/footer');
	}

	public function allblog()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('DashboardModel');
		$blog['admin_blog'] = $this->DashboardModel->bloglist();
		$this->load->view('backend/admin/blog',$blog);
		$this->load->view('backend/admin/includes/footer');
	}

	public function add_faq()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->view('backend/admin/add_faq');
		$this->load->view('backend/admin/includes/footer');
	}

	public function faq()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->model('DashboardModel');
		$user['faq'] = $this->DashboardModel->allfaq();
		$this->load->view('backend/admin/faq',$user);
		$this->load->view('backend/admin/includes/footer');
	}

	public function add_blog()
	{
		$this->load->view('backend/admin/includes/header');
		$this->load->view('backend/admin/add_blog');
		$this->load->view('backend/admin/includes/footer');
	}

	/***********construct***********/
	public function __construct()
	{
		parent::__construct();			
		$this->load->helper('form');
		$this->load->library('session');
        if($this->session->userdata('user_id') == NULL || $this->session->userdata('user_id') == 0)
        {
            return redirect(base_url('adminlogin'));
        }
    }
    /***********end construct***********/
}
	 