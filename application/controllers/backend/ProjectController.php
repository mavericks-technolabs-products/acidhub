<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProjectController extends CI_Controller 
{
	public function create_project()
	{
	    if(!($this->session->userdata('user_id')))
	    {
	      redirect('signup','refresh');
	    }
	    else
	    {
			$this->load->view('backend/includes/header');
			$this->load->view('backend/project/create-project');
			$this->load->view('backend/includes/footer');
		}
	}//public function create_project()

	public function project_insert()
	{  
		/*$this->load->library('email');
		$this->email->from('harshadakh9503@gmail.com', 'harshada');
		$this->email->to('someone@example.com');
		$this->email->cc('another@another-example.com');
		$this->email->bcc('them@their-example.com');
		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');
		$this->email->send();*/

		/*************** email***************/
		$post = $this->input->post();
		//echo "<pre>"; print_r($post); die;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('project_title','project_title','required|trim');
		$this->form_validation->set_rules('project_location','project_location','required|trim');
		$this->form_validation->set_rules('sub_region','sub_region','required|trim');
		$this->form_validation->set_rules('project_description','project_description','required|trim');
		if(empty($_FILES['user_file']['name']))
		{
			$this->form_validation->set_rules('user_file','Image File','trim|required');
		}
		if($this->form_validation->run() == true)
     	{
        	$uploadPath = './uploads/';
	        $config['upload_path'] = $uploadPath;
	        $config['allowed_types'] = 'jpg|jpeg|png|gif|jpe';
	        $this->load->library('upload');
	        $this->upload->initialize($config);
	        $user_id=$this->session->userdata('user_id');
            $data=array
          	( 
              "project_title"=>ucfirst($this->input->post("project_title")),
	          "project_location"=>ucfirst($this->input->post("project_location")),
              "sub_region"=>ucfirst($this->input->post("sub_region")),
              "project_description"=>$this->input->post("project_description"), 
              "created_date"=>date('Y-m-d'),
              "status"=>"open",
			  "user_id"=>$user_id
           	);
           /*	print_r($data);
           	exit();*/
            $this->load->model("ProjectModel");
	          $result=$this->ProjectModel->projectaddcreate($data);
	          if($result)
		          { 
		                /* echo "Success";*/
		//-------------------------Get Serial No--------------------------------------  
		            $rest=$this->ProjectModel->get_recent_project_id();
		            $project_id=$rest->project_id;/*
		//--------------------------Upload Data--------------------------------------------------
		            $data = array();
		          /* print_r($_FILES['user_file']);
		            exit();*/
		            $filesCount = count($_FILES['user_file']['name']);
		            for($i = 0; $i < $filesCount; $i++)
		              {
		                $_FILES['file']['name']     = $_FILES['user_file']['name'][$i];
		                $_FILES['file']['type']     = $_FILES['user_file']['type'][$i];
		                $_FILES['file']['tmp_name'] = $_FILES['user_file']['tmp_name'][$i];
		                $_FILES['file']['error']     = $_FILES['user_file']['error'][$i];
		                $_FILES['file']['size']     = $_FILES['user_file']['size'][$i];

		            // Upload file to server
		              if($this->upload->do_upload('file'))
		                 {
			                $fileData = $this->upload->data();
			                $uploadData[$i]['user_file'] = "uploads/".$fileData["raw_name"].$fileData['file_ext'];
			                $uploadData[$i]['project_id'] = $project_id;
			             }
			                else
			                {
			             	$this->load->view('backend/includes/header');
							$this->load->view('backend/project/create-project');
							$this->load->view('backend/includes/footer');
			                	print_r($this->upload->display_errors());
			                }
		        }//for($i = 0; $i < $filesCount; $i++)

			            if(!empty($uploadData))
			               {
			                  $this->load->model("ProjectModel");
			                  $result=$this->ProjectModel->insert_multi_img($uploadData);
			                  if($result)
			                  {
			                    $this->session->set_flashdata('Feedback',"Thank You Project Is Inserted.");
			                    redirect("project-dashboard", 'refresh');
			                  }
			               }
			            else
			             {
			             	$error="Please select image.";
		       				$this->session->set_flashdata('error',$error);
		       				$this->session->set_flashdata('status','btn-success');
		       				redirect('create-project');
			             }
			             
		             }//if($result)
		         }//if($this->form_validation->run() == true)


		         	else
                        {
                        	/*print_r(validation_errors());
                        	exit();*/
                        	//echo validation_errors('<div class="errors">', '</div>');
                        	$error="Please select image.";
		       				$this->session->set_flashdata('error',$error);
		       				$this->session->set_flashdata('status','btn-success');
                        	$this->load->view('backend/includes/header');
							$this->load->view('backend/project/create-project');
							$this->load->view('backend/includes/footer');
                        	//redirect("create-project");
                        }//else
                       

     }//public function project_insert()

	public function project_dashboard()
	{
		    if(!($this->session->userdata('user_id')))
		    {
		      redirect('signup','refresh');
		    }
		    else
		    {
		    	$this->load->view('backend/includes/header');
			    $id=$this->session->userdata("user_id");
			    $this->load->model("ProjectModel");
			    $result["ans"]=$this->ProjectModel->searchproject($id);
			    $this->load->view('backend/project/project-dashboard',$result);
				$this->load->view('backend/includes/footer');
		    }//else(session)
	}//public function project_dashboard()

	public function edit_project($project_id)
        {   
	          if(!($this->session->userdata('user_id')))
			    {
			      redirect('signup','refresh');
			    }
			    else
			    {   
		            $this->load->view('backend/includes/header');
		            $this->load->model("ProjectModel");
		            $ans = $this->ProjectModel->find_project_one($project_id);
		            $user_file=$this->ProjectModel->project_get_multi_images($project_id);
		            $this->load->view("backend/project/edit-project",compact('ans','user_file'));
		        }//else
      }//public function edit_project($project_id)

    public function update_project($id)
       {  
              $uploadPath = 'uploads/';
              $config['upload_path'] = $uploadPath;
              $config['allowed_types'] = 'jpg|jpeg|png|gif';
              $this->load->library('upload');
              $this->upload->initialize($config);
              if (!empty($_FILES['user_file']['name']))
              {
	              $data=array
	              (
	                "project_title"=>ucfirst($this->input->post("project_title")),
	                "project_location"=>ucfirst($this->input->post("project_location")),
	                "sub_region"=>ucfirst($this->input->post("sub_region")),
	                "project_description"=>$this->input->post("project_description"),
	                "status"=>"updated",
	                "created_date"=>date('Y-m-d')
	              );
        
                $this->load->model('ProjectModel');
                if($this->ProjectModel->update_project($id,$data))
                  {
                    $data = array();
                    $filesCount = count($_FILES['user_file']['name']);
                    for($i = 0; $i < $filesCount; $i++)
                    {
                        $_FILES['file']['name']     = $_FILES['user_file']['name'][$i];
                        $_FILES['file']['type']     = $_FILES['user_file']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['user_file']['tmp_name'][$i];
                        $_FILES['file']['error']     = $_FILES['user_file']['error'][$i];
                        $_FILES['file']['size']     = $_FILES['user_file']['size'][$i];
                    if($this->upload->do_upload('file'))
                      {
                           $fileData = $this->upload->data();
                           $uploadData['user_file'] = "uploads/".$fileData["raw_name"].$fileData['file_ext'];
                           $uploadData['project_id'] = $id;
                        if(!empty($uploadData))
                          {                      
                            $this->load->model('ProjectModel','file');
                            $this->ProjectModel->update_project_multi_img($uploadData);                     
                          }//if(!empty($uploadData))
                        else
                          {
                            $this->session->set_flashdata('Feedback',"Project Information Updated Successfully.");
                            $this->session->set_flashdata('Feedback_class','alert-success');
                            redirect('project-dashboard','refresh');
                          }//else
                      }//if($this->upload->do_upload('file'))
                        else
                        {
                          redirect('project-dashboard','refresh');
                        }
                   }// for($i = 0; $i < $filesCount; $i++)
                }//if($this->ProjectModel->update_project($id,$data))
                      else
                      {
                        $this->session->set_flashdata('error',"Project Information Not Updated successfully.");
                        $this->session->set_flashdata('project_dashboard','refresh');
                      } 
                        redirect('project-dashboard','refresh');
              }//if (!empty($_FILES['user_file']['name']))
                      else
                      {
                        echo "not inserted";
                      }
      }//public function update_project($id)

    public function delete_image($project_id,$project_img_id)
          {
          	 if(!($this->session->userdata('user_id')))
			    {
			      redirect('signup','refresh');
			    }
			    else
			    { 
		          # Get image path
		            $this->load->model("ProjectModel");
		            $ans=$this->ProjectModel->project_get_image($project_img_id);
		            $user_file = "./".$ans->user_file;
		                  # Unlink Image
		            $this->load->helper("file");
		              unlink($user_file); 
		                  # delete Image Record from Database
		            $this->ProjectModel->project_delete_image($project_img_id);
		            return redirect('edit-project/'.$project_id);
		        }
          }//public function delete_image($project_id,$project_img_id)

    public function delete_project($project_id)
          {
            	if(!($this->session->userdata('user_id')))
			    {
			      redirect('signup','refresh');
			    }
			    else
			    { 
	              $this->load->model("ProjectModel");
	              if($this->ProjectModel->deleteproject($project_id)) 
	                {
	                  $this->session->set_flashdata('Delete',"Interior project  deleted Successfully");
	                  return redirect('project-dashboard','refresh');
	                }
	              else
	                {    
	                  $this->session->set_flashdata('error'," Interior project  failed to delete, please try again");
	                  return redirect('project-dashboard','refresh');
	                }//else
	            }//else
          }//public function delete_project($project_id)

     public function project_details($project_id)
			{
				if(!($this->session->userdata('user_id')))
				{
					redirect('signup','refresh');
				}
				else
				{
				    $this->load->view('backend/includes/header');
				    $id=$this->session->userdata("user_id");
		            $this->load->model("ProjectModel");
		            $result['ans']=$this->ProjectModel->selectproject($id,$project_id);
		            $this->load->view('backend/project/project-details',$result);
		    		$this->load->view('backend/includes/footer');
		    	}
			}//public function project_details($project_id)
	  public function allproject()
		{
     	if(!($this->session->userdata('user_id')))
	    {
	      redirect('signup','refresh');
	    }
	    else
	    {
			$this->load->view('backend/includes/header');
			$this->load->model("ProjectModel");
	    	$result["ans"]=$this->ProjectModel->allinteriorsearchproject();
	    	$this->load->view('backend/project/myproject',$result);
			$this->load->view('backend/includes/footer');
		}
    }//public function allproject()

      public function acidprojectdetails($project_id)
	{
		$this->load->view('backend/includes/header');
		$this->load->model("ProjectModel");
		$result['ans']=$this->ProjectModel->acidselectproject($project_id);
		$this->load->view('backend/project/allprojectdetails',$result);
		$this->load->view('backend/includes/footer');
	}


	/**************** mail msg ******************/

	function __construct() 
	{
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
    }
   
    public function send_mail() 
    {
        
        //Send mail
    }// public function send_mail() 
}//class ProjectController extends CI_Controller 