<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentController extends CI_Controller 
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


			/*	starts with backend code*********************************** starts with backend code*/

	public function studentprofile()
		{
			$this->load->view('backend/student/includes/header');
			$user_id=$this->session->userdata('user_id');
			$this->load->model("StudentModel");
			$student['student_details'] = $this->StudentModel->find_student($user_id);
			$this->load->view('backend/employers/studentprofile',$student);
			$this->load->view('backend/student/includes/footer');
		}
	public function student_information()
		{
			$user_id=$this->session->userdata('user_id');
			$this->load->model("StudentModel");
			$student['student_details'] = $this->StudentModel->find_student($user_id);
			$type=$this->session->userdata('type');
			$this->load->view('frontend/login',$tbl_users_details);
		}

	public function store_article()
		{
			$config = [
				'upload_path'   =>    './uploads',
		         ];
			$this->load->library('upload');
		}
	public function studentcheq_number()
		{
			$number=$_POST['number'];
	        $this->load->model('StudentModel');
	        if(!empty($number))
	        {
	        	$find_number=$this->StudentModel->studentfindcontact($number);
	        	if($find_number)
		        {
		        	echo json_encode($data['json']="Contact Number Already Exist.");
		        }//if($find_number)
		        else
		        {
		        	echo json_encode($data['json']="Success.");
		        }//else
	        }//if(!empty($number))
	        else
	        {
	        	echo json_encode($data['json']="");
	        }// else
		}//public function studentcheq_number()

	public function studentupdateprofile($id)
		{	 
	 
		    $this->load->library('form_validation');
			$this->form_validation->set_rules('firstname','firstname','required|trim');
			$this->form_validation->set_rules('lastname','lastname','required|trim');
			$this->form_validation->set_rules('email','email','required|trim|valid_email');
			$this->form_validation->set_rules('contact', 'contact', 'required');
			if(empty($_FILES['user_file']['name']))
			{
			$this->form_validation->set_rules('user_file','Image File','trim|required');
			}
			if($this->form_validation->run() == true)
			    {
			       	$history_date = date_parse ($this->input->post("bday"));
			        $current_date= date_parse(date('Y-m-d'));
					if( $history_date > $current_date)
				   	{
				        $this->session->set_flashdata('date',"please select correct date!");
			           	redirect('signup','refresh');
				   	}//if($history_date< $current_date)
                    else
                    {
					  $config['upload_path']          = './uploads/student/';
					  $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf|docs';
					  $config['max_size']             = '100000';
					  $config['max_width']            = "1024";
					  $config['max_height']           = '768';
					      
					 $this->load->library('upload');
				     $this->upload->initialize($config);
				     if($this->upload->do_upload('user_file'))
				     {
				     $date=$this->input->post("bday");
	             	 $data = $this->upload->data();
	             	 $user_file = "uploads/".$data["raw_name"].$data['file_ext'];
			         $data=array
					(
						   "firstname"=>$this->input->post("firstname"),
						   "lastname"=>$this->input->post("lastname"),
						   "email"=>$this->input->post("email"),
						   "contact"=>$this->input->post("contact"),
						   "bday"=>date("Y-m-d", strtotime($date)),
						   "gender"=>$this->input->post("optradio"),
						   "user_file"=>$_FILES['user_file']['name']
				    );
				    $this->load->model("StudentModel");
                    if($this->StudentModel->updatestudentprofile($id,$data))
                    {
               			redirect ("education-dashboard",'refresh');
                    }//if
               		else
               		{
               				echo"error";
              		}//else
              	}//if end
              		else
              		{
              			$old_image_path=$this->input->post('user_file');
              			$old_resume_upload=$this->input->post('resume_upload');
              			$data=array(
				  	    "firstname"=>$this->input->post("firstname"),
						"lastname"=>$this->input->post("lastname"),
						"email"=>$this->input->post("email"),
						"contact"=>$this->input->post("contact"),
			            "gender"=>$this->input->post("optradio"),
						"user_file"=>$old_image_path,
						
	 				);
              		}//end
              		$this->load->model("StudentModel");
		            if($this->StudentModel->updatestudentprofile($id,$data))
		               {
		                   redirect ("education-dashboard",'refresh');
		               }//if
		               else
		               {
		               	echo "error";
		               }//else

              		$this->load->model("StudentModel");
		            if($this->StudentModel->updatestudentprofile($id,$data))
		               {
		                   redirect ("education-dashboard",'refresh');
		               }//if
		               else
		               {
		               	echo"error";
		               }//else
		           }
		       }//if($this->form_validation->run() == true)
		      
		       	 else
			    	{
		    	
                    	$error="Please select image.";
	       				$this->session->set_flashdata('error',$error);
	       				$this->session->set_flashdata('status','btn-success');
                    	redirect(base_url('studentprofile'));
						
                    }//else
			}//end of function



	public function delete()
		{
           	$id=$this->input->post('id');
			$this->load->model('LoginModel');
           	$post=$this->LoginModel->delete($id);
           	if (array_key_exists('user_file',$_POST))
           	{
           		$store = $_POST['user_file'];
                if (file_exists("./uploads/".$store))
           		{
           			unlink("uploads/".$store);
           			echo  'profile deleted successfully';
           			$this->session->set_flashdata('feedback', ' deleted successfully');

           		}//if (file_exists("./uploads/".$store))
           		else
           		{
           			$this->session->set_flashdata('feedback', ' deleted unsuccessfully');
           			redirect('employerprofile');
           		}//else	
           	}//	if (array_key_exists('user_file',$_POST))
           	else
           	{
           		$this->session->set_flashdata('feedback', '  not deleted successfully');
           		redirect('employerjobs','refresh');
           	}//	else
        }//public function delete()

	public function updatechangepassword_query()
		{	  
	           	$password=$this->input->post("newpassword");
	           		$data=array
	           		(
						"password"=>$password
	                );
		                

	            	$id=$this->session->userdata('interior_id');
				    $this->load->model("LoginModel");
				    if($this->LoginModel->updatechangepassword($id,$data))
				     {
				     	echo"updated";
				  		// exit();
				     	redirect ("login",'refresh');
				     }//if
				     else
				     {
				     	echo"error";
				     }//else
		}//public function updatechangepassword_query()


                      /* *************** student education ********************/
	public function studentadd_education()
		{
			if(!($this->session->userdata('user_id')))
	    {
	      redirect('signup','refresh');
	    }
	    else
	    {
			$this->load->view('backend/student/includes/header');
			$this->load->view('backend/student/studenteducation/studentadd-education');
			$this->load->view('backend/student/includes/footer');
		}
		}//public function studentadd_education()	


	public function educationinsert()
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('my_university','my_university','required|trim');
			$this->form_validation->set_rules('education_from','education_from','required');
			$this->form_validation->set_rules('education_to','education_to','required');
			$this->form_validation->set_rules('graduate','graduate','required|trim');
			$this->form_validation->set_rules('gender','gender','required|trim');
			$content=$this->input->post("work_experience");
			if($content == 'experience')
			{
			$this->form_validation->set_rules('company','company','required|trim');
			$this->form_validation->set_rules('designation','designation','required|trim');
			$this->form_validation->set_rules('education_from','education_from','required');
			$this->form_validation->set_rules('education_to', 'education_to', 'required');
			}
			//$this->form_validation->set_rules('optradio','optradio','required|alpha|trim');
			if($this->form_validation->run() == true)
         		{
         			$user_id=$this->session->userdata('user_id');
		      		$data=array
		            (
						"my_university"=>$this->input->post("my_university"),
			         	"education_from"=>$this->input->post("education_from"),
			            "education_to"=>$this->input->post("education_to"),
	                    "graduate "=>$this->input->post("graduate"), 
	                    "gender"=>$this->input->post("gender"),
	                    "content"=>$this->input->post("content"),
						"company"=>$this->input->post("company"),
	                    "designation"=>$this->input->post("designation"),
	                    "experience_from"=>$this->input->post("experience_from"),
	                    "experience_to"=>$this->input->post("experience_to"),
	                    "city"=>$this->input->post("city"),
	                    "state"=>$this->input->post("state"),
	                    "country"=>$this->input->post("country"),
	                     "created_date"=>date('Y-m-d'),
	                     "status"=>"open",
                        "user_id"=>$user_id
	                 );
		      
						 $this->load->model("StudentModel");
			          	 $result=$this->StudentModel->studenteducationcreate($data);
						 if($result)
						  { 
						  	$this->session->set_flashdata('Feedback',"Thank You Education Is Inserted.");
						     return redirect("education-dashboard",'refresh');
						  }//if
						else
						  {
						  	$this->session->set_flashdata('Feedback',"Education Information Not inserted successfully.");
							echo "Record Not Inserted";
						  }//else
         		}
         		else
         		{
         			
         			print_r(validation_errors());
				}
			
   		 }//public function educationinsert()

	public function education_dashboard()
		{
			if(!($this->session->userdata('user_id')))
			{
				redirect('signup','refresh');
			}//if(!($this->session->userdata('user_id')))
			    
			    $this->load->view('backend/student/includes/header');
			    $id=$this->session->userdata("user_id");
				$this->load->model("StudentModel");
				$result["ans"]=$this->StudentModel->searcheducation($id);
				 $this->load->view('backend/student/studenteducation/education-dashboard',$result);
			    $this->load->view('backend/student/includes/footer');
		}//public function education_dashboard()


	public function edit_education($education_id)
		{      
	        if(!($this->session->userdata('user_id')))
			   {
			      redirect('signup','refresh');
			   }//	if(!($this->session->userdata('user_id')))
			else
			   {   
	         	    $this->load->view('backend/student/includes/header');
					$this->load->model("StudentModel");
					$data['ans'] = $this->StudentModel->find_education($education_id);
	                $this->load->view('backend/student/studenteducation/edit-education',$data);
	                 //$this->load->view('backend/student/includes/footer');
	           }//else
		}//public function edit_education($education_id)


	public function delete_education($education_id)
	    {
	        $this->load->model("StudentModel");
		    if($this->StudentModel->deleteeducation($education_id)) 
		      {
	         	 $this->session->set_flashdata('Delete',"Student Education  deleted Successfully");
	             return redirect('education-dashboard');
	          }
	        else
	          {    
	             $this->session->set_flashdata('error',"Student Education failed to delete, please try again");
	             return redirect('education-dashboard');
	          }
		}// public function delete_education($education_id)

 		public function update_education($id)
    	{	 
		       $user_id=$this->session->userdata('user_id');
		  		$config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
                $config['max_size']             = 100000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;
		  
		  	$data=array
		  			(
					   "my_university"=>$this->input->post("my_university"),
			           "education_from"=>$this->input->post("education_from"),
			           "education_to"=>$this->input->post("education_to"),
	                   "graduate "=>$this->input->post("graduate"), 
	                    "gender"=>$this->input->post("gender"),
	                    "content"=>$this->input->post("content"),
						"company"=>$this->input->post("company"),
	                    "designation"=>$this->input->post("designation"),
	                    "experience_from"=>$this->input->post("experience_from"),
	                    "experience_to"=>$this->input->post("experience_to"),
	                    "city"=>$this->input->post("city"),
	                    "state"=>$this->input->post("state"),
	                    "country"=>$this->input->post("country"),
	                     "created_date"=>date('Y-m-d'),
	                    "status"=>"updated",
	                    "user_id"=>$user_id
                     );
		  	  
					$this->load->model("StudentModel");
				  	if($this->StudentModel->updateedu($id,$data))
				  	{
				  		redirect ("education-dashboard",'refresh');
				  	}//if($this->StudentModel->updateedu($id,$data))
				  	else
				  	{
				  		echo"error";
				  	}//else
		}//public function update_education($id)
	public function education_details($education_id)
		{
				if(!($this->session->userdata('user_id')))
				{
				redirect('signup','refresh');
				}
			  	$this->load->view('backend/student/includes/header');
				$id=$this->session->userdata("user_id");
				$this->load->model("StudentModel");
				$result["ans"]=$this->StudentModel->searcheducationdetails($id,$education_id);
				$this->load->view('backend/student/studenteducation/education-details',$result);
			  	$this->load->view('backend/student/includes/footer');
		}//public function education_details($education_id)

		public function myeducation_dashboard()
		{
			if(!($this->session->userdata('user_id')))
			{
				redirect('signup','refresh');
			}//if(!($this->session->userdata('user_id')))
			    
			    $this->load->view('backend/student/includes/header');
			    $id=$this->session->userdata("user_id");
				$this->load->model("StudentModel");
				$result["ans"]=$this->StudentModel->mysearcheducation($id);
				 $this->load->view('backend/student/studenteducation/myeducation',$result);
			    $this->load->view('backend/student/includes/footer');
		}//public function education_dashboard()

		public function studenteducation_details($education_id)
		{
			$this->load->view('backend/student/includes/header');
			$id=$this->session->userdata("user_id");
			$this->load->model("StudentModel");
			$result["ans"]=$this->StudentModel->studentsearcheducationdetails($id,$education_id);
			$this->load->view('backend/student/studenteducation/alleducation',$result);
		  	$this->load->view('backend/student/includes/footer');
		}//public function education_details($education_id)


	            /* ************* professional ****************************/

	public function studentadd_professional()
		{
			$this->load->view('backend/student/includes/header');
			$this->load->view('backend/student/studentprofessional/studentadd-professional');
			$this->load->view('backend/student/includes/footer');
		}//public function studentadd_professional()

	
	public function professional_insert()
		{
	 		$user_id=$this->session->userdata('user_id');
		    $data1=array
		     (
				"total_experience"=>$this->input->post("total_experience"),
	         	"branch"=>$this->input->post("branch"),
	         	"key_skill"=>$this->input->post("key_skill"),
	         	"location"=>$this->input->post("location"),
	            "professional_id "=>$this->input->post("professional_id"), 
	            "status"=>"open",
             	"user_id"=>$user_id
             );
				  $this->load->model("StudentModel");
	          	  $result=$this->StudentModel->studentprofessionalcreate($data1);
				if($result)
			  	{ 
			  	$this->session->set_flashdata('feedback',"Thank You Profession Is Inserted.");
			      redirect("professional-dashboard",'refresh');
			  	}//if
				else
			  	{
			  	$this->session->set_flashdata('error',"Profession Information Not inserted successfully.");
				echo "Record Not Inserted";
			  	}//else
			
		}//public function professional_insert()

    public function professional_dashboard()
	   {
				if(!($this->session->userdata('user_id')))
				{
					redirect('signup','refresh');
				}
				    $this->load->view('backend/student/includes/header');
				    $id=$this->session->userdata("user_id");
					$this->load->model("StudentModel");
					$result["ans"]=$this->StudentModel->searchprofessional($id);
					$this->load->view('backend/student/studentprofessional/professional-dashboard',$result);
				    $this->load->view('backend/student/includes/footer');
	    }//public function professional_dashboard()


    public function edit_professional($professional_id)
	    {      
	         	if(!($this->session->userdata('user_id')))
		          {
			         redirect('signup','refresh');
		          }
	         	   $this->load->view('backend/student/includes/header');
				   $this->load->model("StudentModel");
				   $data['ans'] = $this->StudentModel->find_professional($professional_id);
	               $this->load->view("backend/student/studentprofessional/edit-professional",$data);
		}// public function edit_professional($professional_id)

    public function delete_professional($professional_id)
       {
                $this->load->model("StudentModel");
	            if($this->StudentModel->deleteprofessional($professional_id)) 
	              {
         		    $this->session->set_flashdata('Delete',"Student Professional  deleted Successfully");
         		    return redirect('professional-dashboard');
         		  }
         	    else
         	      {    
         		    $this->session->set_flashdata('error'," Student Professional  failed to delete, please try again");
         		    return redirect('professional-dashboard');
                  }
        }//public function delete_professional($professional_id)
   

    public function update_professional($id)
    {	 
		 
		   	 	$user_id=$this->session->userdata('user_id');
		  		$config['upload_path']          = './uploads/student/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
                $config['max_size']             = 100000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;
				$data=array
		  				(
		  			   		"total_experience"=>$this->input->post("total_experience"),
			         		"branch"=>$this->input->post("branch"),
			         		"key_skill"=>$this->input->post("key_skill"),
			         	 	"location"=>$this->input->post("location"),
			         	  	"status"=>"updated",
			            	"user_id"=>$user_id
	                  	);
						  	
		                	$this->load->model("StudentModel");
						  	if($this->StudentModel->updateprofession($id,$data))
						  		{
						  			$this->session->set_flashdata('Feedback',"Professional Information Updated successfully.");
						  			redirect ('professional-dashboard');
						  		}
						  	else
						  	{
						  		$this->session->set_flashdata('Feedback',"Professional Information Not Updated successfully.");
						  		echo"error";
						  	}

	}//public function update_professional($id)
    public function professional_details($professional_id)
		{
				if(!($this->session->userdata('user_id')))
					{
						redirect('signup','refresh');
					}
					    $this->load->view('backend/student/includes/header');
						$id=$this->session->userdata("user_id");
						$this->load->model("StudentModel");
						$result["ans"]=$this->StudentModel->searchstudeprofessional($id,$professional_id);
						$this->load->view('backend/student/studentprofessional/professional-details',$result);
					    $this->load->view('backend/student/includes/footer');
		}//public function professional_details($professional_id)

	public function studentprofessional_dashboard()
	   {
				if(!($this->session->userdata('user_id')))
				{
					redirect('signup','refresh');
				}
				    $this->load->view('backend/student/includes/header');
				    $id=$this->session->userdata("user_id");
					$this->load->model("StudentModel");
					$result["ans"]=$this->StudentModel->mysearchprofessional($id);
					$this->load->view('backend/student/studentprofessional/myprofessional',$result);
				    $this->load->view('backend/student/includes/footer');
	    }//public function studentprofessional_dashboard()	

	public function allprofessional_details($professional_id)
		{
				if(!($this->session->userdata('user_id')))
					{
						redirect('signup','refresh');
					}
					    $this->load->view('backend/student/includes/header');
						$id=$this->session->userdata("user_id");
						$this->load->model("StudentModel");
						$result["ans"]=$this->StudentModel->searchstudeprofessional($id,$professional_id);
						$this->load->view('backend/student/studentprofessional/allprofesionaldetails',$result);
					    $this->load->view('backend/student/includes/footer');
		}//public function allprofessional_details($professional_id)

	/********************** student resume *********************/
    public function student_resume()
	{
	$this->load->view('backend/student/includes/header');
	$user_id=$this->session->userdata('user_id');
	$this->load->model("StudentModel");
	$result["resume"]=$this->StudentModel->find_resume($user_id);
	$this->load->view('backend/student/student-resume',$result);
	$this->load->view('backend/student/includes/footer');
	}// public function student_resume()

	  				/*new resume code */
	public function studentadd_resume()
	{
		  /* print_r('hi');
		   exit();*/
	 		$uploadPath = './uploads/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            
            // Load and initialize upload library
             $this->load->library('upload', $config);
             $this->upload->initialize($config);
             $user_id=$this->session->userdata('user_id');
	         $this->load->library('form_validation');
			 $this->form_validation->set_rules('firstname','firstname','required|trim');
		  	 $this->form_validation->set_rules('lastname','lastname','required|trim');
		 	 $this->form_validation->set_rules('email','email','required|trim|valid_email');
		     $this->form_validation->set_rules('contact', 'contact', 'required');
		     $this->form_validation->set_rules('gender','gender','required|trim');
		     $this->form_validation->set_rules('skills','skills','required|trim');
		     $this->form_validation->set_rules('content','content','required|trim');
		     $this->form_validation->set_rules('hobby','hobby','required|trim');
		     $this->form_validation->set_rules('student_address','student_address','required|trim');
		     $this->form_validation->set_rules('personal_objectivities','personal_objectivities','required|trim');
		     $this->form_validation->set_rules('career_objectivities','career_objectivities','required|trim');
		     $this->form_validation->set_rules('city','city','required|trim');
		     $this->form_validation->set_rules('state','state','required|trim');
		     $this->form_validation->set_rules('country','country','required|trim');
		  	 if(is_array($_FILES))
           		{
	            	$filesCount = count($_FILES['upload_image']['name']);

		            for($i = 0; $i < $filesCount; $i++)
		              {
		                $_FILES['file']['name']     = $_FILES['upload_image']['name'][$i];
		                $_FILES['file']['type']     = $_FILES['upload_image']['type'][$i];
		                $_FILES['file']['tmp_name'] = $_FILES['upload_image']['tmp_name'][$i];
		                $_FILES['file']['error']     = $_FILES['upload_image']['error'][$i];
		                $_FILES['file']['size']     = $_FILES['upload_image']['size'][$i];

		            	if ($this->upload->do_upload('file'))
	           			{
			               $data=array
					        (

								"firstname"=>$this->input->post("firstname"),
					         	"lastname"=>$this->input->post("lastname"),
					            "email"=>$this->input->post("email"),
			                    "contact "=>$this->input->post("contact"), 
			                    "gender"=>$this->input->post("gender"),
			                    "skills"=>$this->input->post("skills"),
			                    "qualification"=>$this->input->post("qualification"),
			                    "content"=>$this->input->post("content"),
			                    "hobby"=>$this->input->post("hobby"),
			                    "student_address"=>$this->input->post("student_address"),
			                    "personal_objectivities"=>$this->input->post("personal_objectivities"),
			                    "career_objectivities"=>$this->input->post("career_objectivities"),
			                 	"city"=>$this->input->post("city"),
			                    "state"=>$this->input->post("state"),
			                    "country"=>$this->input->post("country"),
			                    "created_date"=>date('Y-m-d'),
			                    "status"=>"open",
			                    'upload_image'=>$_FILES["file"]["name"],
			                    "user_id"=>$this->session->userdata('user_id')
				            );
					    		$this->load->model("StudentModel");
								//last id of reume id
						        $resume_id=$this->StudentModel->studentresumecreate($data);
						       
					   }//if ($this->upload->do_upload('upload_image'))
					 
			}// for($i = 0; $i < $filesCount; $i++)
			 		if($resume_id)
								{ 
									$this->session->set_flashdata('Feedback',"Please contact to admin department for activation of your Resume.");
									return redirect("student-toolsresume/".$resume_id);
								}//if
								else
								{
									$this->session->set_flashdata('Feedback',"Resume Information Not inserted successfully.");
									echo "Record Not Inserted";
								}//else

				
                }// if(is_array($_FILES))
	              	else
	                    {
	                        	$error="Please select image.";
			       				$this->session->set_flashdata('error',$error);
			       				$this->session->set_flashdata('status','btn-success');
	                        	$this->load->view('backend/includes/header');
								$this->load->view('backend/student/student-resume');
								$this->load->view('backend/includes/footer');
	                    }
                
            }
    
	  				/*end resume code */

	 /*public function studentadd_resume()
	{
		 $this->load->library('form_validation');
	 	 $this->form_validation->set_rules('firstname','firstname','required|trim');
	  	 $this->form_validation->set_rules('lastname','lastname','required|trim');
	 	 $this->form_validation->set_rules('email','email','required|trim|valid_email');
	     $this->form_validation->set_rules('contact', 'contact', 'required');
	     $this->form_validation->set_rules('gender','gender','required|trim');
	     $this->form_validation->set_rules('skills','skills','required|trim');
	     $this->form_validation->set_rules('content','content','required|trim');
	     $this->form_validation->set_rules('hobby','hobby','required|trim');
	     $this->form_validation->set_rules('student_address','student_address','required|trim');
	     $this->form_validation->set_rules('personal_objectivities','personal_objectivities','required|trim');
	     $this->form_validation->set_rules('career_objectivities','career_objectivities','required|trim');
	     $this->form_validation->set_rules('city','city','required|trim');
	     $this->form_validation->set_rules('state','state','required|trim');
	     $this->form_validation->set_rules('country','country','required|trim');
	     if(empty($_FILES['upload_image']['name']))
		{
			$this->form_validation->set_rules('upload_image','Image File','trim|required');
		}
		if($this->form_validation->run() == true)
     	{
        	$uploadPath = './uploads/';
	        $config['upload_path'] = $uploadPath;
	        $config['allowed_types'] = 'jpg|jpeg|png|gif|jpe';
	        $this->load->library('upload');
	        $this->upload->initialize($config);
		 	$user_id=$this->session->userdata('user_id');
		 	$data=array
		       (
					"firstname"=>$this->input->post("firstname"),
		         	"lastname"=>$this->input->post("lastname"),
		            "email"=>$this->input->post("email"),
                    "contact "=>$this->input->post("contact"), 
                    "gender"=>$this->input->post("gender"),
                    "skills"=>$this->input->post("skills"),
                    "qualification"=>$this->input->post("qualification"),
                    "content"=>$this->input->post("content"),
                    "hobby"=>$this->input->post("hobby"),
                    "student_address"=>$this->input->post("student_address"),
                    "personal_objectivities"=>$this->input->post("personal_objectivities"),
                    "career_objectivities"=>$this->input->post("career_objectivities"),
                 	"city"=>$this->input->post("city"),
                    "state"=>$this->input->post("state"),
                    "country"=>$this->input->post("country"),
                    "created_date"=>date('Y-m-d'),
                    "status"=>"open",
                    "user_id"=>$this->session->userdata('user_id')
	            );
					   
					$this->load->model("StudentModel");
					//last id of reume id
			        $resume_id=$this->StudentModel->studentresumecreate($data);
					if($resume_id)
					{ 
						$this->session->set_flashdata('Feedback',"Please contact to admin department for activation of your Job.");
						return redirect("student-toolsresume/".$resume_id);
					}//if
					else
					{
						$this->session->set_flashdata('Feedback',"Job Information Not inserted successfully.");
						echo "Record Not Inserted";
					}//else
				}//if($this->form_validation->run() == true)
				else
                        {
                        	$error="Please select image.";
		       				$this->session->set_flashdata('error',$error);
		       				$this->session->set_flashdata('status','btn-success');
                        	$this->load->view('backend/includes/header');
							$this->load->view('backend/student/student-resume');
							$this->load->view('backend/includes/footer');
                        	//redirect("create-project");
                        }//else
			
    }//public function studentadd_resume()*/

    public function studentresume_dashboard()
	{
		if(!($this->session->userdata('user_id')))
		{
			redirect('signup','refresh');
		}
		    $this->load->view('backend/student/includes/header');
		    $user_id=$this->session->userdata("user_id");
			$this->load->model("StudentModel");
			$result["ans"]=$this->StudentModel->searchstudentresume($user_id);
			$result["resume_education"]=$this->StudentModel->searchstudentresumeeducation($user_id);
			$result["resume_workeducation"]=$this->StudentModel->searchstudentresumeworkeducation($user_id);
			$this->load->view('backend/student/studentresume-dashboard',$result);
		    $this->load->view('backend/student/includes/footer');
	}//public function studentresume_dashboard()
		
	public function editstudent_resume()
	         {      
	         	if(!($this->session->userdata('user_id')))
		          {
			         redirect('signup','refresh');
		          }
	         	    $this->load->view('backend/student/includes/header');
	         	    $user_id=$this->session->userdata("user_id");
				    $this->load->model("StudentModel");
				    $data['ans'] = $this->StudentModel->find_studentresume($user_id);
				    /*echo"<pre>";
				    print_r($data);
				    exit();*/
				   $this->load->view("backend/student/edit-resume",$data);
		     }//public function editstudent_resume()

    public function studentupdate_resume($id)
    {	 
		      /*  ***************** new code **********************/

		      $uploadPath = 'uploads/';
              $config['upload_path'] = $uploadPath;
              $config['allowed_types'] = 'jpg|jpeg|png|gif';
              $this->load->library('upload');
              $this->upload->initialize($config);
              $user_id=$this->session->userdata('user_id');
              if (!empty($_FILES['upload_image']['name']))
              { 
              
              	 $data=array
		  	    	(
					    "firstname"=>$this->input->post("firstname"),
			         	"lastname"=>$this->input->post("lastname"),
			            "email"=>$this->input->post("email"),
	                    "contact "=>$this->input->post("contact"), 
	                    "gender"=>$this->input->post("gender"),
	                    "skills"=>$this->input->post("skills"),
	                    "qualification"=>$this->input->post("qualification"),
	                    "content"=>$this->input->post("content"),
	                    "hobby"=>$this->input->post("hobby"),
	                    "student_address"=>$this->input->post("student_address"),
	                    "personal_objectivities"=>$this->input->post("personal_objectivities"),
	                    "career_objectivities"=>$this->input->post("career_objectivities"),
	              		"city"=>$this->input->post("city"),
	                    "state"=>$this->input->post("state"),
	                    "country"=>$this->input->post("country"),
	                    "created_date"=>date('Y-m-d'),
	                    "status"=>"updated",
	                    'upload_image'=>$_FILES["file"]["name"],
	                    "user_id"=>$user_id
                	);
                	
				 $this->load->model("StudentModel");
				 if($this->StudentModel->updateresume($id,$data))
				  	 {
                    $data = array();
                    $filesCount = count($_FILES['upload_image']['name']);
                    for($i = 0; $i < $filesCount; $i++)
                    {
                        $_FILES['file']['name']     = $_FILES['upload_image']['name'][$i];
                        $_FILES['file']['type']     = $_FILES['upload_image']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['upload_image']['tmp_name'][$i];
                        $_FILES['file']['error']     = $_FILES['upload_image']['error'][$i];
                        $_FILES['file']['size']     = $_FILES['upload_image']['size'][$i];
                    if($this->upload->do_upload('file'))
                      {
                           $fileData = $this->upload->data();
                           $uploadData['upload_image'] = "uploads/".$fileData["raw_name"].$fileData['file_ext'];
                           $uploadData['resume_id'] = $resume_id;
                        if(!empty($uploadData))
                          {                      
                            $this->load->model('StudentModel','file');
                            $this->StudentModel->update_resume_multi_img($uploadData);                     
                          }//if(!empty($uploadData))
                        else
                          {
                            $this->session->set_flashdata('Feedback',"Resume Information Updated Successfully.");
                            $this->session->set_flashdata('Feedback_class','alert-success');
                            redirect('studentresume-dashboard','refresh');
                          }//else
                      }//if($this->upload->do_upload('file'))
                        else
                        {
                          redirect('studentresume-dashboard','refresh');
                        }//else
                   }// for($i = 0; $i < $filesCount; $i++)
                }// if($this->StudentModel->updateresume($id,$data))
                      else
                      {
                        $this->session->set_flashdata('error',"Resume Information Not Updated successfully.");
                        $this->session->set_flashdata('studentresume_dashboard','refresh');
                      } 
                        redirect('studentresume-dashboard','refresh');
              }//if (!empty($_FILES['user_file']['name']))
                      echo validation_errors('<div class="errors">', '</div>');
                        redirect("studentresume-dashboard");
      }// public function studentupdate_resume($id)
		        /*  ***************** end new code **********************/

		   	 	/*$user_id=$this->session->userdata('user_id');
		  		$config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
                $config['max_size']             = 100000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;
		  
		  	$data=array
		  	    (
					    "firstname"=>$this->input->post("firstname"),
			         	"lastname"=>$this->input->post("lastname"),
			            "email"=>$this->input->post("email"),
	                    "contact "=>$this->input->post("contact"), 
	                    "gender"=>$this->input->post("gender"),
	                    "skills"=>$this->input->post("skills"),
	                    "qualification"=>$this->input->post("qualification"),
	                    "content"=>$this->input->post("content"),
	                    "hobby"=>$this->input->post("hobby"),
	                    "student_address"=>$this->input->post("student_address"),
	                    "personal_objectivities"=>$this->input->post("personal_objectivities"),
	                    "career_objectivities"=>$this->input->post("career_objectivities"),
	              		"city"=>$this->input->post("city"),
	                    "state"=>$this->input->post("state"),
	                    "country"=>$this->input->post("country"),
	                    "created_date"=>date('Y-m-d'),
	                    "status"=>"updated",
	                    'upload_image'=>$_FILES["file"]["name"],
	                    "user_id"=>$user_id
                );

		  	  
					 $this->load->model("StudentModel");
				     if($this->StudentModel->updateresume($id,$data))
				  	 {
				  		redirect ("studentresume-dashboard",'refresh');
				  	 }
					 else
					 {
					  		echo"error";
					 }
	}// public function studentupdate_resume($id)*/

	  public function delete_resume($student_resume_id)
    	{
                $this->load->model("StudentModel");
	            if($this->StudentModel->deleteresume($student_resume_id)) 
	              {
         		    $this->session->set_flashdata('Delete',"Student resume  deleted Successfully");
         		    return redirect('studentresume-dashboard');
         		  }
         	    else
         	      {    
         		    $this->session->set_flashdata('error'," Student resume  failed to delete, please try again");
         		    return redirect('studentresume-dashboard');
                  }
    	}//public function delete_resume($student_resume_id)

/*
*************** new resume 14 nov********************/


 	public function student_toolsresume($student_resume_id)
	{
    $this->load->view('backend/student/includes/header');
	$this->load->model("StudentModel");
	$ans=$this->StudentModel->studentresumeskill($student_resume_id);
	$this->load->view('backend/student/student-toolsresume',compact('ans','student_resume_id'));
	$this->load->view('backend/student/includes/footer');
	}// public function student_toolsresume($resume_id)

	public function skillsadd_resume()
	{
		 $data=array
		       (
		       	    "skills"=>$this->input->post("skills"),
		         	"version"=>$this->input->post("version"),
		            "student_resume_id"=>$this->input->post('resume_id')
               );
					$this->load->model("StudentModel");
			        $result=$this->StudentModel->studentskillcreate($data);
			      	if($result)
						  { 
						     return redirect("student-toolsresume/".$data['student_resume_id']);
						  }//if($result)
						else
						  {
							echo "Record Not Inserted";
						  }//else
			
    }//public function skillsadd_resume()

 	public function deleteresume($resume_id,$student_resume_id)
    {
			$this->load->model("StudentModel");
	      	if($this->StudentModel->deletestudentresumeskill($resume_id))
        {
	       	$this->session->set_flashdata('delete','Student Resume Deleted successfully');
	        redirect('student-toolsresume/'.$student_resume_id);
        }
        else
       	{    
             $this->session->set_flashdata('error',"  Student Resume  failed to delete, please try again");
               return redirect("student-toolsresume/".$student_resume_id);
        }
        
    }//public function deleteresume($resume_id)

   /* ********************* student job************************/
   public function studentalljob()
	{
     	if(!($this->session->userdata('user_id')))
	    {
	      redirect('signup','refresh');
	    }
	    else
	    {
			$this->load->view('backend/student/includes/header');
			$this->load->model("StudentModel");
	    	$result["ans"]=$this->StudentModel->studentalljobspecific();
			$this->load->view('backend/student/studentjob/studentmyjob',$result);
			$this->load->view('backend/student/includes/footer');
		}
	}// public function studentalljob()
	public function allstudentjobdetails($job_id)
	{
		$this->load->view('backend/student/includes/header');
		$this->load->model("StudentModel");
		$result['ans']=$this->StudentModel->studentallselectjob($job_id);
		$this->load->view('backend/student/studentjob/studentjobdetails',$result);
		$this->load->view('backend/student/includes/footer');
	}//public function allstudentjobdetails($job_id)
	public function studentapplyjob($job_id)
    {
    	$user_id = $this->session->userdata('user_id');
    	$this->load->model("StudentModel");
    	$ans=$this->StudentModel->studentallselectjob1($job_id,$user_id);
    	if(!($ans == NULL))
		{
				$this->session->set_flashdata('feedback',"Alredy applied for the job.Thank you for your interest!");
			  	  redirect('allstudentjobdetails/'.$job_id);
		}
		else
		{
			$user = $this->session->userdata('user_id');
			$current_date= date_parse(date('Y-m-d'));
				$data=array
						(
							"created_date"=>date('Y-m-d'),
							"job_id"=>$job_id, 
							"status"=>"open",
							"user_id"=>$user
						);
				$this->load->model("StudentModel");
				$result=$this->StudentModel->studentjobresumecreate($data);
				if($result)
				{ 
					$this->session->set_flashdata('feedback',"Appiled for job successfully.Thank You!");
					redirect('allstudentjobdetails/'.$job_id);
				}//if
		}
			
    }
    public function studentdatainformation($user_id)
	{
			$this->load->view('backend/includes/header');
			$this->load->model("StudentModel");
			$result["ans"]=$this->StudentModel->personalstudentprofile($user_id);
			$result["resume"]=$this->StudentModel->viewstudentresume($user_id);
			$this->load->view('backend/student/studentallinformation',$result);
			$this->load->view('backend/student/includes/footer');
	}
	
    //public function studentapplyjob()
   /* ********************* end student job************************/
    public function studentcheqmail()
	{
		$email=$_POST['email'];
        $this->load->model('StudentModel');
        if(!empty($email))
        {
        	$find_email=$this->StudentModel->studentsearchemail($email);
        	if($find_email)
	        {
	        	echo json_encode($data['json']="Email Already Exist.");
	        }
	        else
	        {
	        	echo json_encode($data['json']="Success.");
	        }
        }
        else
        {
        	echo json_encode($data['json']="");
        }
	}//public function studentcheqmail()

}//class StudentController extends CI_Controller 
