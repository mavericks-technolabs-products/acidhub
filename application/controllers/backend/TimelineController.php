<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TimelineController extends CI_Controller
 {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html*/

	public function employer_timeline()
	{
	    if(!($this->session->userdata('user_id')))
	    {
	      redirect('signup','refresh');
	    }
	    else
	    {
			$this->load->view('backend/includes/header');
			$id=$this->session->userdata("user_id");
			$this->load->model("TimelineModel");
			$result['ans']=$this->TimelineModel->searchtimeline($id);
			$result['project']=$this->TimelineModel->timelinesearchproject($id);
			$result['blog']=$this->TimelineModel->timelinesearchblog($id);
			$result['job']=$this->TimelineModel->timelinesearchjob($id);
		    $this->load->view('backend/employers/employertimeline',$result);
			$this->load->view('backend/includes/footer');
		}
	}//public function employer_timeline()
}
