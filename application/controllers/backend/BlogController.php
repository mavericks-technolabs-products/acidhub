<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BlogController extends CI_Controller 
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function create_blog()
	{
     	if(!($this->session->userdata('user_id')))
	    {
	      redirect('signup','refresh');
	    }
	    else
	    {
			$this->load->view('backend/includes/header');
			$this->load->view('backend/blog/create-blog');
			$this->load->view('backend/includes/footer');
		}
    }//public function create_blog()

    public function blog_insert()
	{ 
		$this->form_validation->set_rules('blog_author_name','blog_author_name','required|trim');
		$this->form_validation->set_rules('blog_title','blog_title','required|trim');
		$this->form_validation->set_rules('blog_description','blog_description','required|trim');
		if($this->form_validation->run() == true)
         {
		  $uploadPath = 'uploads/blogs/';
	      $config['upload_path'] = $uploadPath;
	      $config['allowed_types'] = 'jpg|jpeg|png|gif';
	      $this->load->library('upload');
	      $this->upload->initialize($config);
		  $user_id=$this->session->userdata('user_id');
		        	$data=array
                    (
                       "blog_author_name"=>ucfirst($this->input->post("blog_author_name")),
	                   "blog_title"=>ucfirst($this->input->post("blog_title")),
			           "blog_description"=>ucfirst($this->input->post("blog_description")),
			           "created_date"=>date('Y-m-d'),
			           "status"=>"open",
	                   "user_id"=>$user_id
	                );
		
			        $this->load->model("BlogModel");
		              $result=$this->BlogModel->blogaddcreate($data);
                      if($result)
                        { 
                          $rest=$this->BlogModel->get_recent_blog_id();
                          $blog_id=$rest->blog_id;
    //--------------------------Upload Data--------------------------------------------------
                          $data = array();
                          $filesCount = count($_FILES['blog_mult_img']['name']);
                          for($i = 0; $i < $filesCount; $i++)
                             {
                                $_FILES['file']['name']     = $_FILES['blog_mult_img']['name'][$i];
                                $_FILES['file']['type']     = $_FILES['blog_mult_img']['type'][$i];
                                $_FILES['file']['tmp_name'] = $_FILES['blog_mult_img']['tmp_name'][$i];
                                $_FILES['file']['error']     = $_FILES['blog_mult_img']['error'][$i];
                                $_FILES['file']['size']     = $_FILES['blog_mult_img']['size'][$i];
                    						// Upload file to server
                      	    if($this->upload->do_upload('file'))
                        	  {
                         		$fileData = $this->upload->data();
                   				$uploadData[$i]['blog_mult_img'] = $fileData["raw_name"].$fileData['file_ext'];
                                $uploadData[$i]['blog_id'] = $blog_id;
                              }
                              else
                              {
                              	$this->load->view('backend/includes/header');
								$this->load->view('backend/blog/create-blog');
								$this->load->view('backend/includes/footer');
                        	    print_r($this->upload->display_errors());
                              }
                             }//for($i = 0; $i < $filesCount; $i++)

                      	  if(!empty($uploadData))
                       		{
	                          $this->load->model("BlogModel");
	                          $result=$this->BlogModel->insert_blog_multi_img($uploadData);
	                          if($result)
                          		{
		                            $this->session->set_flashdata('Feedback',"Thank You Blog Is Inserted");
		                            redirect("blog-dashboard", 'refresh');
                          		}
                       		}//if(!empty($uploadData))
                      	  else
                       		{ 
		                        $error="Please select image.";
		       				    $this->session->set_flashdata('error',$error);
		       				   	$this->session->set_flashdata('status','btn-success');
		       				   	redirect('create-blog');
                      	    }
                        }//if($result)
                    }//if($this->form_validation->run() == true)
		                  else
                        {
                        	echo validation_errors('<div class="errors">', '</div>');
                        	redirect("create-blog");
                        }//else
    }// public function blog_insert()

    public function blog_dashboard()
	{
		    if(!($this->session->userdata('user_id')))
		    {
		      redirect('signup','refresh');
		    }
		    else
		    {
		    	$this->load->view('backend/includes/header');
			    $id=$this->session->userdata("user_id");
			    $this->load->model("BlogModel");
			    $data["ans"]=$this->BlogModel->searchblog($id);
				$this->load->view('backend/blog/blog-dashboard',$data);
				$this->load->view('backend/includes/footer');
		    }//else(session)
	}//public function blog_dashboard()

	public function edit_blog($blog_id)
    {   
	          if(!($this->session->userdata('user_id')))
			    {
			      redirect('signup','refresh');
			    }
			    else
			    {   
		            $this->load->view('backend/includes/header');
		            $this->load->model("BlogModel");
		            $ans = $this->BlogModel->find_blog_one($blog_id);
		            $blog_mult_img=$this->BlogModel->blog_get_multi_images($blog_id);
		            $this->load->view("backend/blog/edit-blog",compact('ans','blog_mult_img'));
		        }//else
    }//public function edit_project($project_id)

    public function update_blog($id)
    {  
        $uploadPath = 'uploads/blogs/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload');
        $this->upload->initialize($config);
        if(!empty($_FILES['blog_mult_img']['name']))
          {
             $data=array
              (
	                "blog_author_name"=>ucfirst($this->input->post("blog_author_name")),
	                "blog_title"=>ucfirst($this->input->post("blog_title")),
	                "blog_description"=>ucfirst($this->input->post("blog_description")),
	                 "status"=>"updated",
	                "created_date"=>date('Y-m-d'),
              );
    
           
          $this->load->model('BlogModel');
          if($this->BlogModel->update_blog($id,$data))
            {
                 $data = array();
                 $filesCount = count($_FILES['blog_mult_img']['name']);
                 for($i = 0; $i < $filesCount; $i++)
                   {
                        $_FILES['file']['name']     = $_FILES['blog_mult_img']['name'][$i];
                        $_FILES['file']['type']     = $_FILES['blog_mult_img']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['blog_mult_img']['tmp_name'][$i];
                        $_FILES['file']['error']     = $_FILES['blog_mult_img']['error'][$i];
                        $_FILES['file']['size']     = $_FILES['blog_mult_img']['size'][$i];

                      if($this->upload->do_upload('file'))
                        {
                            $fileData = $this->upload->data();
                            $uploadData['blog_mult_img'] = $fileData["raw_name"].$fileData['file_ext'];
                            $uploadData['blog_id'] = $id;
                        if(!empty($uploadData))
                          {                      
                             $this->load->model('BlogModel','file');
                             $this->BlogModel->update_blog_multi_img($uploadData);                     
                          }//if(!empty($uploadData))
                        else
                          {
                            $this->session->set_flashdata('Feedback',"Blog Information Updated successfully.");
                             $this->session->set_flashdata('Feedback_class','alert-success');
                             redirect('blog-dashboard','refresh');
                          }     
                       }// if($this->upload->do_upload('file'))
                       
                        else
                         {
                               redirect('blog-dashboard','refresh');
                         }
					}// for($i = 0; $i < $filesCount; $i++)
			}// if($this->BlogModel->update_blog($id,$data))
				        else
				         {
				            $this->session->set_flashdata('error',"BLog Information Not Updated successfully.");
				            $this->session->set_flashdata('project_dashboard','refresh');
				         } 
                           redirect('blog-dashboard','refresh');
          }// if(!empty($_FILES['blog_mult_img']['name']))
					    else
					    {
					      echo "not inserted";
					    }
	}//public function update_blog($id)

    public function blog_delete_image($blog_id,$blog_img_id)
    {
    	if(!($this->session->userdata('user_id')))
			    {
			      redirect('signup','refresh');
			    }
			    else
			    { 
		          # Get image path
		            $this->load->model("BlogModel");
		            $ans=$this->BlogModel->blog_get_image($blog_img_id);
		            $blog_mult_img = "./".$ans->blog_mult_img;
		                  # Unlink Image
		            $this->load->helper("file");
		              unlink($user_file); 
		                  # delete Image Record from Database
		            $this->BlogModel->blogdeleteimage($blog_img_id);
		            return redirect('edit-blog/'.$blog_id);
		        }
    }// public function blog_delete_image($blog_id,$blog_img_id)

     public function delete_blog($blog_id)
      {
          if(!($this->session->userdata('user_id')))
			{
			      redirect('signup','refresh');
			}
			else
			  {               
	          $this->load->model("BlogModel");
	          if($this->BlogModel->deleteblog($blog_id)) 
	            {
	              $this->session->set_flashdata('Delete'," blog  deleted Successfully");
	               return redirect('blog-dashboard');
	            }
           else
            {    
            $this->session->set_flashdata('error',"  blog  failed to delete, please try again");
               return redirect('blog-dashboard');
            }
             }
       }//public function delete_blog($blog_id)

      public function blog_details($blog_id)
			{
				if(!($this->session->userdata('user_id')))
				{
					redirect('signup','refresh');
				}
				else
				{
				    $this->load->view('backend/includes/header');
				    $id=$this->session->userdata("user_id");
		            $this->load->model("BlogModel");
		            $result['ans']=$this->BlogModel->selectblog($id,$blog_id);
		            $this->load->view('backend/blog/blog-details',$result);
		    		$this->load->view('backend/includes/footer');
		    	}
			}//public function project_details($project_id)

	 public function edit_blog_form($value,$error)
    	{
        # code...
		      if(!($this->session->userdata('user_id')))
		      {
		      redirect('signup','refresh');
		      }
		      $this->load->model("BlogModel");
		      $ans=$this->BlogModel->blog_get_article($value);
		      $blog_mult_img=$this->BlogModel->blog_get_multi_images($value);
		      $this->load->view('backend/blog/edit-blog',['ans'=>$ans,'blog_mult_img'=>$error,'blog_mult_img'=>$blog_mult_img]);
        }// public function edit_blog_form($value,$error)

     public function allblog()
	{
     	if(!($this->session->userdata('user_id')))
	    {
	      redirect('signup','refresh');
	    }
	    else
	    {
			$this->load->view('backend/includes/header');
			$this->load->model("BlogModel");
	    	$result["ans"]=$this->BlogModel->allblogspecific();
			$this->load->view('backend/blog/myblog',$result);
			$this->load->view('backend/includes/footer');
		}
    }//public function create_blog()
    public function acidblogdetails($blog_id)
	{
		
		$this->load->view('backend/includes/header');
		$this->load->model("BlogModel");
		$result['ans']=$this->BlogModel->allselectionblog($blog_id);
		$this->load->view('backend/blog/allblogdetails',$result);
		$this->load->view('backend/includes/footer');
	}

	/*public function send_mail()
  {

  	  $this->load->library('email');
  	   $config = Array(
	  $config['protocol'] => 'smtp',
	  $config['smtp_host'] => 'XXX',
	  $config['smtp_user'] => 'xxx', // change it to yours
	  $config['smtp_pass'] => 'xxx', // change it to yours
	  $config['smtp_port']=> 25;
  $this->email->initialize('$config');
		$this->email->from('harshadakh9503@gmail.com', 'harshada');
		$this->email->to('someone@example.com');
		$this->email->cc('another@another-example.com');
		$this->email->bcc('them@their-example.com');
		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');
		$this->email->send();
  }*/
  

 }//class BlogController extends CI_Controller 
