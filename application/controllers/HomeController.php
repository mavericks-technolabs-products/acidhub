<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function home()
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
	    $result["ans"]=$this->AcidModel->homejobspecific();
	   /* print_r($result);
	    exit();*/
		$this->load->view('frontend/home',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function about()
	{
		$this->load->view('frontend/includes/header');
		$this->load->view('frontend/about');
		$this->load->view('frontend/includes/footer');
	}
	public function professional()
	{
		$this->load->view('frontend/includes/header');
		$this->load->view('frontend/professional');
		$this->load->view('frontend/includes/footer');
	}
	
	public function blog()
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
	    $result["ans"]=$this->AcidModel->blogspecific();
		$this->load->view('frontend/blog',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function blogdetails($blog_id)
	{
		
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result['ans']=$this->AcidModel->selectionblog($blog_id);
		$this->load->view('frontend/blogdetails',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function job()
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
	    $result["ans"]=$this->AcidModel->jobspecific();
		$this->load->view('frontend/job',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function jobdetails($job_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result['ans']=$this->AcidModel->selectjob($job_id);
		$this->load->view('frontend/jobdetails',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function contact()
	{
		$this->load->view('frontend/includes/header');
		$this->load->view('frontend/contact');
		$this->load->view('frontend/includes/footer');
	}
	public function interior()
	{
	
    	$this->load->view('frontend/includes/header');
	    $this->load->model("AcidModel");
	    $result["ans"]=$this->AcidModel->searchprofile();
		$this->load->view('frontend/interior',$result);
		$this->load->view('frontend/includes/footer');
		   
	}
	public function interiordetails($user_deatils_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result['ans']=$this->AcidModel->selectprofile($user_deatils_id);
		$this->load->view('frontend/interiordetails',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function interiorproject($user_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result["ans"]=$this->AcidModel->interiorsearchproject($user_id);
		$this->load->view('frontend/interior-project',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function allprojectdetails($project_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result['ans']=$this->AcidModel->allselectproject($project_id);
		$this->load->view('frontend/interior_project_details',$result);
		$this->load->view('frontend/includes/footer');
	}
	
	public function architecture()
	{
		$this->load->view('frontend/includes/header');
	    $this->load->model("AcidModel");
		$result["ans"]=$this->AcidModel->archiprofile();
		$this->load->view('frontend/architecture',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function architecturedetails($user_deatils_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result['ans']=$this->AcidModel->archiselectprofile($user_deatils_id);
		$this->load->view('frontend/architecturedetails',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function architectureproject($user_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result["ans"]=$this->AcidModel->architecturesearchproject($user_id);
		$this->load->view('frontend/architecture-project',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function allarchitectureprojectdetails($project_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result['ans']=$this->AcidModel->allarchiselectproject($project_id);
		$this->load->view('frontend/architecture_project_details',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function civil()
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result["ans"]=$this->AcidModel->civilprofile();
		$this->load->view('frontend/civil',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function civildetails($user_deatils_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result['ans']=$this->AcidModel->civilselectprofile($user_deatils_id);
		$this->load->view('frontend/civildetails',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function civilproject($user_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result["ans"]=$this->AcidModel->civilsearchproject($user_id);
		$this->load->view('frontend/civil-project',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function allcivilprojectdetails($project_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result['ans']=$this->AcidModel->allcivilselectproject($project_id);
		$this->load->view('frontend/civil_project_details',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function developer()
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result["ans"]=$this->AcidModel->developerprofile();
		$this->load->view('frontend/developer',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function developerdetails($user_deatils_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result['ans']=$this->AcidModel->developerselectprofile($user_deatils_id);
		$this->load->view('frontend/developerdetails',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function developerproject($user_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result["ans"]=$this->AcidModel->developersearchproject($user_id);
		$this->load->view('frontend/developer-project',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function alldevoprojectdetails($project_id)
	{
		$this->load->view('frontend/includes/header');
		$this->load->model("AcidModel");
		$result['ans']=$this->AcidModel->alldevoselectproject($project_id);
		$this->load->view('frontend/developer_project_details',$result);
		$this->load->view('frontend/includes/footer');
	}
	public function signup()
	{
		$this->load->view('frontend/includes/header');
		$this->load->view('frontend/signup');
		$this->load->view('frontend/includes/footer');
	}
	public function login()
	{
		$this->load->view('frontend/includes/header');
		$this->load->view('frontend/login');
		$this->load->view('frontend/includes/footer');
	}
	public function forget()
	{
		$this->load->view('frontend/includes/header');
		$this->load->view('frontend/changepassword');
		$this->load->view('frontend/includes/footer');
	}
	public function homedetails()
	{
		$this->load->view('frontend/includes/header');
		$this->load->view('frontend/homejob-details');
		$this->load->view('frontend/includes/footer');
	}
	

	  /****************** pagignation *******************/

	/*public function __construct() 
	{
 	   parent:: __construct();
 
       $this->load->helper("url");
 
       $this->load->model("AcidModel");
 
       $this->load->library("pagination");
    }
    public function blog_dashboard() {
 
       $config = array();
 
       $config["base_url"] = base_url() . "blog-dashboard";
 
       $config["total_rows"] = $this->AcidModel->record_count();
 
       $config["per_page"] = 5;
 
       $config["uri_segment"] = 3;
 
       $this->pagination->initialize($config);
 
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
 
       $data["results"] = $this->AcidModel->
 
           fetch_departments($config["per_page"], $page);
 
       $data["links"] = $this->pagination->create_links();
 
       $this->load->view("blog-dashboard", $data);
   }*/
}
