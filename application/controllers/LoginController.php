<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller 
{
public function signup()
{
	$this->load->view('frontend/includes/header');
	$this->load->view('frontend/signup');
	$this->load->view('frontend/includes/footer');
}
public function aboutall()
{
	$this->load->view('frontend/includes/header');
	$this->load->view('frontend/acidabout');
	$this->load->view('frontend/includes/footer');
}
public function interior_information()
{
	$user_id=$this->session->userdata('user_id');
	$this->load->model("LoginModel");
	$interior['interior_details'] = $this->LoginModel->find_interior($user_id);
	$type=$this->session->userdata('type');
	$this->load->view('frontend/login',$tbl_users_details);
}
public function student_information()
{
	$user_id=$this->session->userdata('user_id');
	$this->load->model("StudentModel");
	$student['student_details'] = $this->StudentModel->find_student($user_id);
	$type=$this->session->userdata('type');
	$this->load->view('frontend/login',$tbl_student);
}
public function architecture_information()
{
	$user_id=$this->session->userdata('user_id');
	$this->load->model("ArchitectureModel");
	$student['architecture_details'] = $this->ArchitectureModel->find_architecture($user_id);
	$type=$this->session->userdata('type');
	$this->load->view('frontend/login',$tbl_users_details);
}

public function store_article()
{
	$config = [
		'upload_path'   =>    './uploads',

	];
	$this->load->library('upload');
}

public function cheq_number()
	{
		$number=$_POST['number'];
        $this->load->model('LoginModel');
        if(!empty($number))
        {
        	$find_number=$this->LoginModel->findcontact($number);
        	if($find_number)
	        {
	        	echo json_encode($data['json']="Contact Number Already Exist.");
	        }
	        else
	        {
	        	echo json_encode($data['json']="Success.");
	        }
        }
        else
        {
        	echo json_encode($data['json']="");
        }
	}
public function cheqmail()
	{
		$email=$_POST['email'];
        $this->load->model('LoginModel');
        if(!empty($email))
        {
        	$find_email=$this->LoginModel->searchemail($email);
        	if($find_email)
	        {
	        	echo json_encode($data['json']="Email Already Exist.");
	        }
	        else
	        {
	        	echo json_encode($data['json']="Success.");
	        }
        }
        else
        {
        	echo json_encode($data['json']="");
        }
	}

public function Addlist_insert()
{
	
	$this->load->library('form_validation');
	$this->form_validation->set_rules('firstname','firstname','required|trim');
	$this->form_validation->set_rules('lastname','lastname','required|trim');
	$this->form_validation->set_rules('email','email','required|trim|valid_email');
	$this->form_validation->set_rules('password', 'Password','required|max_length[15]|min_length[6]|alpha_numeric');
	$this->form_validation->set_rules('password', 'Password', 'required');
	$this->form_validation->set_rules('contact', 'contact', 'required');
	
	$type=$this->input->post("content");
	if($type == 'firm')
	{
		$this->form_validation->set_rules('firm_firstname','firm_firstname','required|trim');
		$this->form_validation->set_rules('firm_address','firm_address','required|trim');
		$this->form_validation->set_rules('firm_contact', 'firm_contact', 'required');
		$this->form_validation->set_rules('website','website','required|trim');
	}
	$this->form_validation->set_rules('optradio','optradio','required|alpha|trim');
		
	if($this->form_validation->run() == true)
    {
       	$history_date = date_parse ($this->input->post("bday"));
        $current_date= date_parse(date('Y-m-d'));
		if( $history_date > $current_date)
	   	{
	        $this->session->set_flashdata('date',"please select correct date!");
           	redirect('signup','refresh');
	   	}//if($history_date< $current_date)
		else
		{
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
			$config['max_size']             = 100000;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;

			$date=$this->input->post("bday");              
			$data=array
			(
				"firstname"=>ucfirst($this->input->post("firstname")),
				"lastname"=>ucfirst($this->input->post("lastname")),
				"email"=>$this->input->post("email"),
				"password"=>md5($this->input->post("password")),
				"confirmpassword"=>$this->input->post("confirmpassword"),
				"contact"=>$this->input->post("contact"),
				"type"=>$this->input->post("type"),
				"bday"=>date("Y-m-d", strtotime($date)),
				"gender"=>$this->input->post("optradio"),
				"firm_firstname"=>ucfirst($this->input->post("firm_firstname")),
				"firm_address"=>ucfirst($this->input->post("firm_address")),
				"firm_contact"=>$this->input->post("firm_contact"),
				"website"=>$this->input->post("website"),
				"city"=>ucfirst($this->input->post("city")),
				"state"=>ucfirst($this->input->post("state")),
				"country"=>$this->input->post("country"),
			);//array

			$data1=array
			(
				"firstname"=>$this->input->post("firstname"),
				"lastname"=>$this->input->post("lastname"),
				"email"=>$this->input->post("email"),
				"password"=>md5($this->input->post("password")),
				"contact"=>$this->input->post("contact"),
				"status"=>"open",
				"type"=>$this->input->post("type")
			);//data1 array

			$this->load->model("LoginModel");
			if($this->LoginModel->insert_data($data,$data1))
			{
				$this->load->library('email');
				$this->email->from('harshadakh9503@gmail.com', 'harshada');
				$this->email->to('someone@example.com');
				$this->email->cc('another@another-example.com');
				$this->email->bcc('them@their-example.com');
				$this->email->subject('Email Test');
				$this->email->message('Testing the email class.');
				$this->email->send();
				$this->session->set_flashdata('Feed', "Success!Your Account Has Been  Created Successfully!");
				return redirect(base_url('aboutall'));
			}//if
			else
			{
				$this->session->set_flashdata('error', "Your Account Is Not Created!");
				echo "Record Not Inserted";
			}//else
		
		}//else	
	}
	else
	{
		$this->session->set_flashdata('error', "Success!Your Account Has Been Not Successfully.");
		echo "Record Not Inserted";
						
	}//else	
}//public function Addlist_insert()

public function login()
{
	$this->load->view('frontend/includes/header');
	$this->load->view('frontend/login');
	$this->load->view('frontend/includes/footer');
}


public function destroy_session() 
{
	$session=$this->session->sess_destroy();
  	redirect('signup','refresh');
}


public function login_valid()
{   
			$this->load->library('form_validation');
        	$this->form_validation->set_rules('email','email','required');
        	$this->form_validation->set_rules('password','password','required');
			if($this->form_validation->run())
			{
          			$email=$this->input->post("email");
          			$password=md5($this->input->post("password"));
          			$this->load->model('LoginModel');
          			$check_id = $this->LoginModel->login($email,$password);
          			
        if($check_id)
        {   
			if(!($check_id[0]->status=="open"))
	        {
	        	if(!($check_id[0]->status=="deactivate"))
				{
					$this->load->library('session');
					foreach($check_id as $ans)
					{
					$data=array
						(
							'user_id'=>$ans->user_id,
							'firstname'=>$ans->firstname,
							'email'=>$ans->email,
							'password'=>$ans->password,
							'contact'=>$ans->contact,
							"status"=>$ans->status,
							'type'=>$ans->type
						);
					}//foreach($check_id as $ans)

					$this->load->library('session');
					$this->session->set_userdata('temp',$data);
					$this->session->set_userdata('user_id',$data['user_id']);
					$this->session->set_userdata('interior_id',$data['interior_id']);
					$this->session->set_userdata('student_id',$data['student_id']);
					$this->session->set_userdata('architecture_id',$data['architecture_id']);
					$this->session->set_userdata('civil_id',$data['civil_id']);
					$this->session->set_userdata('developer_id',$data['developer_id']);
							       
					$temp=$this->session->userdata('temp');
					$type=$temp['type'];
					if($type =="Student")
					{

					   	$Welcome="Welcome To Your Profile Page.";
						$this->session->set_flashdata('Welcome',$Welcome);
						$this->session->set_flashdata('status','btn-success');
						redirect('studentprofile');
					}//if($type =="Student")
					elseif($type =="Interior")
					{
						$Welcome="Welcome To Your Profile Page.";
						$this->session->set_flashdata('Welcome',$Welcome);
						$this->session->set_flashdata('status','btn-success');
						redirect('employerprofile');
					}// elseif($type =="Interior")
					elseif($type =="Architecture")
					{
						$Welcome="Welcome To Your Profile Page.";
						$this->session->set_flashdata('Welcome',$Welcome);
						$this->session->set_flashdata('status','btn-success');
						redirect('employerprofile');
					}//elseif($type =="Architecture")
					elseif($type =="Civil")
					{
						$Welcome="Welcome To Your Profile Page.";
						$this->session->set_flashdata('Welcome',$Welcome);
						$this->session->set_flashdata('status','btn-success');
						redirect('employerprofile');
					}//elseif($type =="Architecture")
					elseif($type =="Developer")
					{
						$Welcome="Welcome To Your Profile Page.";
						$this->session->set_flashdata('Welcome',$Welcome);
						$this->session->set_flashdata('status','btn-success');
						redirect('employerprofile');
					}//elseif($type =="Architecture")}
		    	}//if($check_id)deactivate
		    	else
		    	{
		    		$notice="Please contact to admin department your account has been deactivated.";
		       		$this->session->set_flashdata('notice',$notice);
		       		$this->session->set_flashdata('status','btn-success');
		       		redirect('login');
		    	}
			}//if($check_id)open
		    else
		    {
		       	$notice="Please contact to admin department for activation of your account.";
	       		$this->session->set_flashdata('notice',$notice);
	       		$this->session->set_flashdata('status','btn-success');
	       		redirect('login');
		    }//else check id
		}
		else
		{
			$this->session->set_flashdata('login_failed','Invalid Email Or Password');
			   		redirect("login","refresh");
		}
		}//if($this->form_validation->run())
			   else
			   {
       				$this->session->set_flashdata('login_failed','Invalid Email Or Password');
			   		redirect("login","refresh");
			   }//else
			}//public function login_valid()


			/*	starts with backend code*********************************** starts with backend code*/

public function update_profile($id)
{	 
	  /*print_r("hello") ;
	  exit();*/
	  $this->load->library('form_validation');
	  $this->form_validation->set_rules('firstname','firstname','required|trim');
	  $this->form_validation->set_rules('lastname','lastname','required|trim');
	  $this->form_validation->set_rules('email','email','required|trim|valid_email');
	  $this->form_validation->set_rules('contact', 'contact', 'required');
	  //$this->form_validation->set_rules('type','type','required|alpha|trim');
		$type=$this->input->post("content");
		if($type == 'firm')
		{
		$this->form_validation->set_rules('firm_firstname','firm_firstname','required|trim');
		$this->form_validation->set_rules('firm_address','firm_address','required|trim');
		$this->form_validation->set_rules('firm_contact', 'firm_contact', 'required');
		$this->form_validation->set_rules('website','website','required|trim');
		}
		// $this->form_validation->set_rules('optradio','optradio','required|trim');
		if(empty($_FILES['user_file']['name']))
		{
			$this->form_validation->set_rules('user_file','Image File','trim|required');
		}
		
		if($this->form_validation->run() == true)
        {

        	    $history_date = date_parse ($this->input->post("bday"));
                $current_date= date_parse(date('Y-m-d'));
                $date= $this->input->post("bday");
          			
          			$user_id=$this->session->userdata('user_id');
          			$this->load->model('LoginModel');
          	        if( $history_date > $current_date)
	                {
	                  $this->session->set_flashdata('edate',"please select correct date!");
            			redirect('employerprofile','refresh');
	                  }//if( $history_date< $current_dat
	                  else
	                  {
							  $config['upload_path']          = './uploads/';
							  $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
							  $config['max_size']             = '100000';
							  $config['max_width']            = "1024";
							  $config['max_height']           = '768';
				              $this->load->library('upload');
				              $this->upload->initialize($config);
				              if($this->upload->do_upload('user_file'))
				              {
								$fileData = $this->upload->data();
				             	$uploadData['user_file'] = "uploads/".$fileData["raw_name"].$fileData['file_ext'];
								$data=array
				             	(
							  	"firstname"=>$this->input->post("firstname"),
							  	"lastname"=>$this->input->post("lastname"),
							  	"email"=>$this->input->post("email"),
							  	"contact"=>$this->input->post("contact"),
							  	"bday"=>date("Y-m-d", strtotime($date)),
								"city"=>$this->input->post("city"),
							  	"state"=>$this->input->post("state"),
							  	"country"=>$this->input->post("country"),
							  	"firm_firstname"=>$this->input->post("firm_firstname"),
							  	"firm_address"=>$this->input->post("firm_address"),
							  	"firm_contact"=>$this->input->post("firm_contact"),
							  	"website"=>$this->input->post("website"),
							  	"user_file"=>$_FILES['user_file']['name']
								);
				             	$this->load->model("LoginModel");
			               	    if($this->LoginModel->updateprofile($id,$data))
			                 	{
			               			redirect ("project-dashboard",'refresh');
			                 	}
			               	  	else
			                 	{
			               	     echo"error";
			                 	}
			                  }

                		else
                		{
			                    $old_image_path=$this->input->post('user_file');
			                    $data=array(
							  	"firstname"=>$this->input->post("firstname"),
							  	"lastname"=>$this->input->post("lastname"),
							  	"email"=>$this->input->post("email"),
							  	"contact"=>$this->input->post("contact"),
							  	"bday"=>date("Y-m-d", strtotime($date)),
								"city"=>$this->input->post("city"),
							  	"state"=>$this->input->post("state"),
							  	"country"=>$this->input->post("country"),
							  	"firm_firstname"=>$this->input->post("firm_firstname"),
							  	"firm_address"=>$this->input->post("firm_address"),
							  	"firm_contact"=>$this->input->post("firm_contact"),
							  	"website"=>$this->input->post("website"),
							    "user_file"=>$old_image_path
			                   );

			                  $this->load->model("LoginModel");
			               	  if($this->LoginModel->updateprofile($id,$data))
			               		{
			               		redirect ("project-dashboard",'refresh');
			               		}
				              else
				               	{
				               		echo"error";
				                }
							}
			               $this->load->model("LoginModel");
			               if($this->LoginModel->updateprofile($id,$data))
			               {
			               
			               	redirect ("project-dashboard",'refresh');
			               }
			               else
			               {
			               	echo"error";
			               }
						}
				}
			    else
			    {
			    	$error="Please select image.";
		       		$this->session->set_flashdata('error',$error);
		       		$this->session->set_flashdata('status','btn-success');
                    redirect ("employerprofile");
				}//else
		 }

           public function delete()
           {
           	$id=$this->input->post('id');

           	$this->load->model('LoginModel');
           	$post=$this->LoginModel->delete($id);
           	if (array_key_exists('user_file',$_POST))
           	{
           		$store = $_POST['user_file'];


           		if (file_exists("./uploads/".$store))
           		{
           			unlink("uploads/".$store);
           			echo  'profile deleted successfully';
           			$this->session->set_flashdata('feedback', ' deleted successfully');

           		}
           		else
           		{
           			$this->session->set_flashdata('feedback', ' deleted unsuccessfully');
           			redirect('employerproject');
           		}	
           	}
           	else
           	{
           		$this->session->set_flashdata('feedback', '  not deleted successfully');
           		redirect('employerproject','refresh');
           	}
           }

           public function forget()
			{
				$this->load->view('frontend/includes/header');
				$this->load->view('frontend/changepassword');
				$this->load->view('frontend/includes/footer');
			}

           public function updatechangepassword_query()
           {
            $user_id=$this->session->userdata('user_id');	  
           	$password=$this->input->post("newpassword");
			$data=array
           	(
				"password"=>$password
			);
	               

          
           	$this->load->view('frontend/includes/header');
			$this->load->model("LoginModel");
			if($this->LoginModel->updatechangepassword($id,$data))
			  {

			     	redirect ("login",'refresh');
			  }
			     else{
			     	echo"error";
			     }
			 }

			}	