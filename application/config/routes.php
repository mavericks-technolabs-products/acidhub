<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'HomeController/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
  
  /********* Admin route***************/
$route['adminsignup']='backend/admin/AdminController/adminsignup';
$route['adminlogin']='backend/admin/LoginController/adminlogin';
$route['dashboard']='backend/admin/DashboardController/dashboard';
$route['userstatus']='backend/admin/DashboardController/userstatus';
$route['changeinteriorstatus/(:num)/(:any)']='backend/admin/DashboardController/changeinteriorstatus/$1/$2';
$route['activeusers']='backend/admin/DashboardController/activeusers';
$route['admin_change_interior_status/(:num)/(:any)']='backend/admin/DashboardController/admin_change_interior_status/$1/$2';
$route['architecturestatus']='backend/admin/DashboardController/architecturestatus';
$route['changearchitecturestatus/(:num)/(:any)']='backend/admin/DashboardController/changearchitecturestatus/$1/$2';
$route['activearchitecturesusers']='backend/admin/DashboardController/architecturesusers';
$route['admin_change_architecture_status/(:num)/(:any)']='backend/admin/DashboardController/admin_change_architecture_status/$1/$2';
$route['declinedusers']='backend/admin/DashboardController/declinedusers';
$route['admin_change_decline_interior_status/(:num)/(:any)']='backend/admin/DashboardController/admin_change_decline_interior_status/$1/$2';
$route['admin_change_decline_architecture_status/(:num)/(:any)']='backend/admin/DashboardController/admin_change_decline_architecture_status/$1/$2';
$route['alladmin']='backend/admin/DashboardController/alladmin';
$route['admin_blog']='backend/admin/DashboardController/allblog';
$route['faq']='backend/admin/DashboardController/faq';
$route['deleteuserblog/(:num)']='backend/admin/AdminController/deleteuserblog/$1';
$route['add_blog']='backend/admin/DashboardController/add_blog';
$route['blog_edit']='backend/admin/AdminController/blog_edit';
$route['blog_edit/(:num)']='backend/admin/AdminController/blog_edit/$1';
$route['deleteblog/(:num)/(:any)']='backend/admin/AdminController/deleteblog/$1/$2';
$route['blogstatus']='backend/admin/AdminController/blogstatus';
$route['changeblogstatus/(:num)/(:any)']='backend/admin/AdminController/changeblogstatus/$1/$2';
$route['add_faq']='backend/admin/DashboardController/add_faq';
$route['faq_edit/(:any)']='backend/admin/AdminController/faq_edit/$1';
$route['deletefaq/(:any)']='backend/admin/AdminController/deletefaq/$1';

$route['admin_information']='backend/admin/AdminController/admin_information';

$route['adminregistration_insert']='backend/admin/AdminController/adminregistration_insert';


/********Blog Routes*********/


$route['activeblogs']='backend/admin/AdminController/activeblog';
$route['admin_change_blog_status/(:num)/(:any)']='backend/admin/AdminController/admin_change_blog_status/$1/$2';
$route['editedblogs']='backend/admin/AdminController/edit_blog';
$route['edit_blog_status/(:num)/(:any)']='backend/admin/AdminController/edit_blog_status/$1/$2';
$route['declinedblogs']='backend/admin/AdminController/declinedblogs';
$route['changedeclinedblogs/(:num)/(:any)']='backend/admin/AdminController/changedeclinedblogs/$1/$2';


/*********End Blog Status************/

/***********Job Routes***************/


$route['jobstatus']='backend/admin/AdminController/jobstatus';
$route['changejobstatus/(:num)/(:any)']='backend/admin/AdminController/changejobstatus/$1/$2';
$route['activejobs']='backend/admin/AdminController/activejob';
$route['adminchangejobstatus/(:num)/(:any)']='backend/admin/AdminController/adminchangejobstatus/$1/$2';
$route['editedjobs']='backend/admin/AdminController/editrequestjoblist';
$route['editjobstatus/(:num)/(:any)']='backend/admin/AdminController/editjobstatus/$1/$2';
$route['declinedjobs']='backend/admin/AdminController/declinedjoblist';
$route['declinedjobstatus/(:num)/(:any)']='backend/admin/AdminController/declinedjobstatus/$1/$2';


/***********End Job Status*************/

/*************Project Routes*************/

$route['activeprojects']='backend/admin/AdminController/activeproject';
$route['projectstatus']='backend/admin/AdminController/projectstatus';
//$route['activeeducation']='backend/admin/AdminController/activeeducation';
$route['changeprojectstatus/(:num)/(:any)']='backend/admin/AdminController/changeprojectstatus/$1/$2';
$route['adminchangeprojectstatus/(:num)/(:any)']='backend/admin/AdminController/adminchangeprojectstatus/$1/$2';
$route['editedprojects']='backend/admin/AdminController/editprojectlist';
$route['editprojectstatus/(:num)/(:any)']='backend/admin/AdminController/editprojectstatus/$1/$2';
$route['declinedprojects']='backend/admin/AdminController/declinedprojectlist';
$route['declinedprojectstatus/(:num)/(:any)']='backend/admin/AdminController/declinedprojectstatus/$1/$2';

/************** education route**************/
$route['educationstatus']='backend/admin/AdminController/educationstatus';
$route['changeeducationstatus/(:num)/(:any)']='backend/admin/AdminController/changeeducationstatus/$1/$2';
$route['activeeducations']='backend/admin/AdminController/activeeducation';
$route['adminchangeeducationstatus/(:num)/(:any)']='backend/admin/AdminController/adminchangeeducationstatus/$1/$2';
$route['editededucation']='backend/admin/AdminController/editeducationlist';

$route['editeducationslist']='backend/admin/AdminController/editeducationslist';
$route['editeducationstatus/(:num)/(:any)']='backend/admin/AdminController/editeducationstatus/$1/$2';
$route['declinededucation']='backend/admin/AdminController/declinededucationlist';
$route['declinededucationstatus/(:num)/(:any)']='backend/admin/AdminController/declinededucationstatus/$1/$2';

/************** End education route**************/

/**************** education professional*****************/
$route['activeprofessional']='backend/admin/AdminController/activeprofessional';
$route['professionalstatus']='backend/admin/AdminController/professionalstatus';
$route['changeprofessionalstatus/(:num)/(:any)']='backend/admin/AdminController/changeprofessionalstatus/$1/$2';
$route['adminchangeprofessionalstatus/(:num)/(:any)']='backend/admin/AdminController/adminchangeprofessionalstatus/$1/$2';
$route['editedprofessional']='backend/admin/AdminController/editprofessionallist';
$route['editprofessionalstatus/(:num)/(:any)']='backend/admin/AdminController/editprofessionalstatus/$1/$2';
$route['declinedprofessional']='backend/admin/AdminController/declinedprofessionallist';
$route['declinedprofessionalstatus/(:num)/(:any)']='backend/admin/AdminController/declinedprofessionalstatus/$1/$2';

/**************** End education professional*****************/
 /* ************* Admin end route ********************/

/*Frontend Routes*/
$route['home']='HomeController/home';
$route['homedetails']='HomeController/homedetails';

$route['about']='HomeController/about';
$route['professional']='HomeController/professional';
$route['interior']='HomeController/interior';
$route['interiordetails']='HomeController/interiordetails';
$route['allprojectdetails']='HomeController/allprojectdetails';
$route['allprojectdetails/(:any)']='HomeController/allprojectdetails/$1';

$route['interiordetails/(:any)']='HomeController/interiordetails/$1';
$route['interior-project']='HomeController/interiorproject';
$route['interior-project/(:any)']='HomeController/interiorproject/$1';

//$route['interiorproject-details/(:any)']='HomeController/interiorprojectdetails/$1';

$route['architecture']='HomeController/architecture';
$route['architecturedetails']='HomeController/architecturedetails';
$route['allarchitectureprojectdetails/(:any)']='HomeController/allarchitectureprojectdetails/$1';
$route['architecturedetails/(:any)']='HomeController/architecturedetails/$1';
$route['architecture-project']='HomeController/architectureproject';
$route['architecture-project/(:any)']='HomeController/architectureproject/$1';

$route['civil']='HomeController/civil';
$route['civildetails']='HomeController/civildetails';
$route['allcivilprojectdetails/(:any)']='HomeController/allcivilprojectdetails/$1';

$route['civildetails/(:any)']='HomeController/civildetails/$1';
$route['civil-project']='HomeController/civilproject';
$route['civil-project/(:any)']='HomeController/civilproject/$1';

$route['developer']='HomeController/developer';
$route['developerdetails']='HomeController/developerdetails';
$route['alldevoprojectdetails/(:any)']='HomeController/alldevoprojectdetails/$1';
$route['developerdetails/(:any)']='HomeController/developerdetails/$1';
$route['developer-project']='HomeController/developerproject';
$route['developer-project/(:any)']='HomeController/developerproject/$1';

$route['blog']='HomeController/blog';
$route['blogdetails']='HomeController/blogdetails';
$route['blogdetails/(:any)']='HomeController/blogdetails/$1';
$route['job']='HomeController/job';
$route['jobdetails']='HomeController/jobdetails';
$route['jobdetails/(:any)']='HomeController/jobdetails/$1';
$route['contact']='HomeController/contact';
$route['signup']='HomeController/signup';
$route['login']='HomeController/login';
$route['forget']='HomeController/forget';
$route['aboutall']='LoginController/aboutall';

$route['login_valid']='LoginController/login_valid';
$route['Addlist_insert']='LoginController/Addlist_insert';
$route['update_profile']='LoginController/update_profile';
$route['destroy_session']='LoginController/destroy_session';
$route['updatechangepassword_query']='LoginController/updatechangepassword_query';


/*Frontend Routes*/
$route['employerprofile']='EmployerController/employerprofile';
//$route['employertimeline']='EmployerController/employertimeline';


/*Backend Routes*/
$route['create-project']='backend/ProjectController/create_project';
$route['insert-project']='backend/ProjectController/project_insert';
$route['project-dashboard']='backend/ProjectController/project_dashboard';
$route['edit-project/(:any)']='backend/ProjectController/edit_project/$1';
$route['delete-image/(:any)/(:any)']='backend/ProjectController/delete_image/$1/$2';
$route['delete_project/(:any)']='backend/ProjectController/delete_project/$1';
$route['project-details/(:any)']='backend/ProjectController/project_details/$1';
$route['send_mail']='backend/ProjectController/send_mail';
$route['allproject']='backend/ProjectController/allproject';
$route['acidprojectdetails']='backend/BlogController/acidprojectdetails';
$route['acidprojectdetails/(:any)']='backend/ProjectController/acidprojectdetails/$1';
             /* **************** Blog *******************/
$route['create-blog']='backend/BlogController/create_blog';
$route['blog_insert']='backend/BlogController/blog_insert';
$route['blog-dashboard']='backend/BlogController/blog_dashboard';
$route['edit-blog/(:any)']='backend/BlogController/edit_blog/$1';
$route['blog-delete-image/(:any)/(:any)']='backend/BlogController/blog_delete_image/$1/$2';
$route['delete_blog/(:any)']='backend/BlogController/delete_blog/$1';
$route['blog-details/(:any)']='backend/BlogController/blog_details/$1';
$route['allblog']='backend/BlogController/allblog';
$route['acidblogdetails']='backend/BlogController/acidblogdetails';
$route['acidblogdetails/(:any)']='backend/BlogController/acidblogdetails/$1';

             /* **************** Job******************/
$route['create-job']='backend/JobController/create_job';
$route['insert-job']='backend/JobController/job_insert';
$route['job-dashboard']='backend/JobController/job_dashboard';
$route['edit-job/(:any)']='backend/JobController/edit_job/$1';
$route['update_job']='backend/job/JobController/update_job';
$route['delete_job/(:any)']='backend/JobController/delete_job/$1';
$route['job-details/(:any)']='backend/JobController/job_details/$1';
$route['manage-resume']='backend/JobController/manage_resume';
$route['alljob']='backend/JobController/alljob';
$route['acidjobdetails']='backend/JobController/acidjobdetails';
$route['acidjobdetails/(:any)']='backend/JobController/acidjobdetails/$1';
		/*	***************** student *****************/
$route['studentprofile']='backend/student/StudentController/studentprofile';
$route['studentupdateprofile']='backend/student/StudentController/studentupdateprofile';
$route['studentadd-education']='backend/student/StudentController/studentadd_education';
$route['education-dashboard']='backend/student/StudentController/education_dashboard';
$route['education-details/(:any)']='backend/student/StudentController/education_details/$1';
$route['edit-education/(:any)']='backend/student/StudentController/edit_education/$1';
$route['delete-education/(:any)']='backend/student/StudentController/delete_education/$1';
$route['update-education']='backend/student/StudentController/update_education';
$route['myeducation_dashboard']='backend/student/StudentController/myeducation_dashboard';
$route['studenteducation_details']='backend/student/StudentController/studenteducation_details';
$route['studenteducation_details/(:any)']='backend/student/StudentController/studenteducation_details/$1';

 /* **************** student professional ***************/

$route['studentadd-professional']='backend/student/StudentController/studentadd_professional';
$route['professional-dashboard']='backend/student/StudentController/professional_dashboard';
$route['professional-insert']='backend/student/StudentController/professional_insert';
$route['edit-professional/(:any)']='backend/student/StudentController/edit_professional/$1';
$route['delete-professional/(:any)']='backend/student/StudentController/delete_professional/$1';
$route['professional-details/(:any)']='backend/student/StudentController/professional_details/$1';
$route['studentprofessional_dashboard']='backend/student/StudentController/studentprofessional_dashboard';
$route['allprofessional_details']='backend/student/StudentController/allprofessional_details';
$route['allprofessional_details/(:any)']='backend/student/StudentController/allprofessional_details/$1';

       /* ******************** student resume ****************/
$route['student-resume']='backend/student/StudentController/student_resume';
$route['studentresume-dashboard']='backend/student/StudentController/studentresume_dashboard';
$route['studentadd_resume']='backend/student/StudentController/studentadd_resume';
$route['edit-resume']='backend/student/StudentController/editstudent_resume';
$route['edit-resume/(:any)']='backend/student/StudentController/editstudent_resume/$1';

$route['studentupdate_resume']='backend/student/StudentController/studentupdate_resume';
$route['deleteresume/(:any)/(:any)']='backend/student/StudentController/deleteresume/$1/$2';
   /*  ********************* student job***********************/
$route['studentalljob']='backend/student/StudentController/studentalljob';
$route['allstudentjobdetails']='backend/student/StudentController/allstudentjobdetails';
$route['allstudentjobdetails/(:any)']='backend/student/StudentController/allstudentjobdetails/$1';
$route['studentapplyjob/(:any)']='backend/student/StudentController/studentapplyjob/$1';
	/*  ********************* end student job***********************/
  /* *********************** stud new resume14 nov **************/
  $route['student-toolsresume']='backend/student/StudentController/student_toolsresume';
$route['student-toolsresume/(:any)']='backend/student/StudentController/student_toolsresume/$1';
$route['skillsadd_resume']='backend/student/StudentController/skillsadd_resume';

/*Backend Routes*/
$route['studentdatainformation']='backend/student/StudentController/studentdatainformation';
$route['studentdatainformation/(:any)']='backend/student/StudentController/studentdatainformation/$1';


