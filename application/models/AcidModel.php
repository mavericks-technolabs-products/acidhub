 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AcidModel extends CI_Model
  {
    public function createprofile($data)
	 	{          
            return $this->db->insert("tbl_users_details",$data);
	  }

	  public function searchprofile()
    {
             $q=$this->db->select("*")
						->from('tbl_user')
			            ->join('tbl_users_details','tbl_users_details.user_id=tbl_user.user_id')
			            ->where('tbl_user.type','Interior');
			             $q=$this->db->get();
			             $result=$q->result();
			             return $result;
    }// public function searchprofile($id)

    public function selectprofile($user_deatils_id)
    {
			 $q=$this->db->select("*")
						->from('tbl_users_details')
			            ->where('tbl_users_details.user_details_id',$user_deatils_id)
			            ->join('tbl_user','tbl_user.user_id=tbl_users_details.user_id')
			            ->where('tbl_user.type','Interior');
			             $q=$this->db->get();
			             $result=$q->row();
			             return $result;
    }// public function selectprofile($user_deatils_id)

    public function archiprofile()
    {
             $q=$this->db->select("*")
						->from('tbl_user')
			            ->join('tbl_users_details','tbl_users_details.user_id=tbl_user.user_id')
			            ->where('tbl_user.type','Architecture');
			             $q=$this->db->get();
			             $result=$q->result();
			             return $result;
    }// public function searchprofile($id)

    public function archiselectprofile($user_deatils_id)
    {
			 $q=$this->db->select("*")
						->from('tbl_users_details')
			            ->where('tbl_users_details.user_details_id',$user_deatils_id)
			            ->join('tbl_user','tbl_user.user_id=tbl_users_details.user_id')
			            ->where('tbl_user.type','Architecture');
			             $q=$this->db->get();
			             $result=$q->row();
			            return $result;
    }//  public function archiselectprofile($user_deatils_id)

    public function civilprofile()
    {
             $q=$this->db->select("*")
						->from('tbl_user')
			            ->join('tbl_users_details','tbl_users_details.user_id=tbl_user.user_id')
			            ->where('tbl_user.type','Civil');
			             $q=$this->db->get();
			             $result=$q->result();
			           return $result;
    }//public function civilprofile()

    public function civilselectprofile($user_deatils_id)
    {
			 $q=$this->db->select("*")
						->from('tbl_users_details')
			            ->where('tbl_users_details.user_details_id',$user_deatils_id)
			            ->join('tbl_user','tbl_user.user_id=tbl_users_details.user_id')
			            ->where('tbl_user.type','Civil');
			             $q=$this->db->get();
			             $result=$q->row();
			            return $result;
    }//public function civilselectprofile($user_deatils_id)


    public function developerprofile()
    {
             $q=$this->db->select("*")
						->from('tbl_user')
			            ->join('tbl_users_details','tbl_users_details.user_id=tbl_user.user_id')
			            ->where('tbl_user.type','Developer');
			             $q=$this->db->get();
			             $result=$q->result();
			            return $result;
    }//  public function developerprofile()

    public function developerselectprofile($user_deatils_id)
    {
			 $q=$this->db->select("*")
						->from('tbl_users_details')
			            ->where('tbl_users_details.user_details_id',$user_deatils_id)
			            ->join('tbl_user','tbl_user.user_id=tbl_users_details.user_id')
			            ->where('tbl_user.type','Developer');
			             $q=$this->db->get();
			             $result=$q->row();
			            return $result;
    }//public function civilselectprofile($user_deatils_id)
   		
   		 /***************** project ******************/

   	public function interiorsearchproject($user_id)
    {
               /*$user_id = $this->session->userdata("user_id");*/
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')
                            ->where('tbl_project.user_id',$user_id)
                            // ->where('tbl_project.status','active')
                             ->group_by('tbl_project.project_id');
                             $q=$this->db->get();
                              $result=$q->result();
                            return $result;
    }// public function interiorsearchproject()
    public function allselectproject($project_id)
      {
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')                ->where("tbl_project.project_id",$project_id)
                              ->get();
                             return $q->result();
       }


    public function architecturesearchproject($user_id)
    {
               /*$user_id = $this->session->userdata("user_id");*/
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')
                            ->where('tbl_project.user_id',$user_id)
                             //->where('tbl_project.status','active')
                             ->group_by('tbl_project.project_id');
                             $q=$this->db->get();
                             $result=$q->result();
                             return $result;
    }// public function architecturesearchproject()

    public function allarchiselectproject($project_id)
      {
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')                ->where("tbl_project.project_id",$project_id)
                              ->get();
                             return $q->result();
       }

    public function civilsearchproject($user_id)
    {
               /*$user_id = $this->session->userdata("user_id");*/
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')
                            ->where('tbl_project.user_id',$user_id)
                            // ->where('tbl_project.status','active')
                             ->group_by('tbl_project.project_id');
                             $q=$this->db->get();
                             $result=$q->result();
                             return $result;
    }//   public function civilsearchproject()
    public function allcivilselectproject($project_id)
      {
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')                ->where("tbl_project.project_id",$project_id)
                              ->get();
                            return $q->result();
       }

    public function developersearchproject($user_id)
    {
              /* $user_id = $this->session->userdata("user_id");*/
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')
                            ->where('tbl_project.user_id',$user_id)
                             //->where('tbl_project.status','active')
                             ->group_by('tbl_project.project_id');
                             $q=$this->db->get();
                             $result=$q->result();
                             return $result;
    }//   public function civilsearchproject()
    public function alldevoselectproject($project_id)
      {
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')                ->where("tbl_project.project_id",$project_id)
                              ->get();
                             return $q->result();
       }

        					/************* Blog ************/

    public function createblog($data)
	 	{          
            return $this->db->insert("tbl_blog",$data);
	 	}//public function createblog($data)

	 	public function blogspecific()
    {
          $q=$this->db->select("*")
                      ->from('tbl_blog')
			                ->join('tbl_users_details','tbl_blog.user_id=tbl_users_details.user_id')
                      ->join('tbl_blog_image','tbl_blog.blog_id=tbl_blog_image.blog_id')
                      ->group_by('tbl_blog.blog_id')
                      ->get();
                       $result=$q->result();
                       return $result;
    }//	public function blogspecific()

    public function selectionblog($blog_id)
    {
			       $q=$this->db->select("*")
						            ->from('tbl_blog')
			                  ->join('tbl_users_details','tbl_users_details.user_id=tbl_blog.user_id')
                        ->join('tbl_blog_image','tbl_blog.blog_id=tbl_blog_image.blog_id')
                       ->where('tbl_blog.blog_id',$blog_id)
			                  ->group_by('tbl_blog.blog_id')
			                 ->get();
			                   $result=$q->row();
			                return $result;
    }// public function selectblog()

       /* ************** Job********************/

    public function jobcreate($data)
    {          
           return $this->db->insert("tbl_job",$data);
    }//public function jobcreate($data)

    public function jobspecific()
    {
             $q=$this->db->select("*")
                         ->from('tbl_job')
			                   ->join('tbl_users_details','tbl_job.user_id=tbl_users_details.user_id')
                         ->group_by('tbl_job.job_id')
                         ->get();
                          $result=$q->result();
                          return $result;
    }// public function jobspecific()

    public function selectjob($job_id)
    {
            // $user_id = $this->session->userdata("id");
                $q=$this->db->select("*")
                                ->from('tbl_job')
                                ->join('tbl_users_details','tbl_users_details.user_id=tbl_job.user_id')
                                ->where('tbl_job.job_id',$job_id)
                                ->group_by('tbl_job.job_id')
                                  ->get();
                                  $result=$q->result();
                                  return $result;
    }// public function selectjob()
    public function createhomejob($data)
    {          
            return $this->db->insert("tbl_users_details",$data);
    }//public function createblog($data)

    public function homejobspecific()
    {
          $q=$this->db->select("*")
                      ->from('tbl_users_details')
                      ->join('tbl_job','tbl_users_details.user_id=tbl_job.user_id')
                     /* ->where('tbl_job.user_id')*/
                      ->group_by('tbl_job.job_id')
                      ->get();
                      $result=$q->result();
                       return $result;
    }// public function blogspecific()

/********************Count methods**********************/

	public function job_count($id)
	{
		$query = $this->db
		            ->select('*')
		            ->where('user_id',$id)
		            ->from('tbl_job')
		            ->get();
		return $query->num_rows();
	}

	public function active_job_count($id)
	{
		$query = $this->db
		            ->select('*')
		            ->where('user_id',$id)
		            ->where('status','active')
		            ->from('tbl_job')
		            ->get();
		return $query->num_rows();
	}

}

/********************End count methods**********************/

    

         /* ************* pagignation ***********************/

        /*public function __construct() 
        {
 			parent::__construct();
 		}
 
		public function record_count() 
		{
		  return $this->db->count_all("Departments");
		}
		public function fetch_departments($limit, $start) 
		{
 
		       $this->db->limit($limit, $start);
				$query = $this->db->get("AcidModel");
		 		if ($query->num_rows() > 0) 
		 		{
 					foreach ($query->result() as $row) 
 					{
					$data[] = $row;
				    }
 
           				return $data;
 				}
 
        			return false;
 
   		}*/
	
