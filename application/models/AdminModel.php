<?php
class AdminModel extends CI_Model
{
	public function insertadmin($insert)
	{
		return $this->db->insert('tbl_admin', $insert);
	}

	public function findcontact($contact)
    {
        $this->db->select('*');
        $this->db->from ("tbl_admin");
        $this->db->where('contact', $contact);
            $q=$this->db->get();
        return $q->result();
    }

    public function findemail($email)
    {
        $this->db->select('*');
        $this->db->from ("tbl_admin");
        $this->db->where('email', $email);
            $q=$this->db->get();
        return $q->result();
    }

    public function insertblog($insert)
    {
        return $this->db->insert('tbl_admin_blog', $insert);
    }

    public function insertfaq($insert)
    {
        return $this->db->insert('tbl_faq', $insert);
    }

    public function faq_edit($insert,$faq_id)
    {
        return $this->db->where('faq_id',$faq_id)
                        ->update('tbl_faq', $insert);

                        
    }

    public function editblog($blog_id)
    {
        $query = $this->db
              ->select("*")
              ->where('blog_id',$blog_id)
              ->from('tbl_admin_blog')
              ->get();
    return $query->row();
    }

    public function editinsertblog($insert,$blog_id)
    {
        return $this->db->where('blog_id',$blog_id)
                        ->update('tbl_admin_blog', $insert);
    }

    public function delete_blog($blog_id)
    {
        return $this->db->where('blog_id',$blog_id)
                        ->delete('tbl_admin_blog');
    }

    public function delete_faq($faq_id)
    {
        return $this->db->where('faq_id',$faq_id)
                        ->delete('tbl_faq');
    }

    public function get_blog_images($blog_id)
    {
        $q = $this->db->where('blog_id', $blog_id)
                    ->get("tbl_blog_image");

        return $q->result();
    }

    public function requestbloglist()
    {
        $query = $this->db->select("*")
                            ->where('status','open')
                            ->from('tbl_blog')
                            ->get();
        return $query->result();
    }

    public function editedbloglist()
    {
        $query = $this->db->select("tbl_blog.status,tbl_blog.blog_author_name,tbl_blog.blog_title,tbl_blog.blog_id,tbl_blog.user_id,tbl_user.type")
                            ->where('tbl_blog.status','updated')
                            ->from('tbl_blog')
                            ->join('tbl_user', 'tbl_blog.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function declinedbloglist()
    {
        $query = $this->db->select("tbl_blog.status,tbl_blog.blog_author_name,tbl_blog.blog_title,tbl_blog.blog_id,tbl_blog.user_id,tbl_user.type")
                            ->where('tbl_blog.status','decline')
                            ->from('tbl_blog')
                            ->join('tbl_user', 'tbl_blog.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function change_blogs_status($id,$type)
    {
        return $this->db->set('status', $type)
                    ->where('blog_id', $id)
                    ->update('tbl_blog');
    }

    public function activebloglist()
    {
        $query = $this->db->select("tbl_blog.status,tbl_blog.blog_author_name,tbl_blog.blog_title,tbl_blog.blog_id,tbl_blog.user_id,tbl_user.type")
                            ->where('tbl_blog.status !=','open')
                            ->where('tbl_blog.status !=','updated')                            
                            ->where('tbl_blog.status !=','decline')
                            ->from('tbl_blog')
                            ->join('tbl_user', 'tbl_blog.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function faqedit($faq_id)
    {
        $query = $this->db
              ->select("*")
              ->where('faq_id',$faq_id)
              ->from('tbl_faq')
              ->get();
    return $query->row();
    }

    /***********Job****************/

    public function requestjoblist()
    {
        $query = $this->db->select("tbl_job.status,tbl_job.job_type,tbl_job.job_title,tbl_job.company_name,tbl_job.job_id,tbl_job.user_id,tbl_user.type")
                            ->where('tbl_job.status','open')
                            ->from('tbl_job')
                            ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function change_jobs_status($id,$type)
    {
        return $this->db->set('status', $type)
                    ->where('job_id', $id)
                    ->update('tbl_job');
    }

    public function activejoblist()
    {
        $query = $this->db->select("tbl_job.status,tbl_job.job_type,tbl_job.job_title,tbl_job.company_name,tbl_job.job_id,tbl_job.user_id,tbl_user.type")
                            ->where('tbl_job.status !=','open')
                            ->where('tbl_job.status !=','updated')                            
                            ->where('tbl_job.status !=','decline')
                            ->from('tbl_job')
                            ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function editrequestjoblist()
    {
        $query = $this->db->select("tbl_job.status,tbl_job.job_type,tbl_job.job_title,tbl_job.company_name,tbl_job.job_id,tbl_job.user_id,tbl_user.type")
                            ->where('tbl_job.status','updated')
                            ->from('tbl_job')
                            ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function declinedjoblist()
    {
        $query = $this->db->select("tbl_job.status,tbl_job.job_type,tbl_job.job_title,tbl_job.company_name,tbl_job.job_id,tbl_job.user_id,tbl_user.type")
                            ->where('tbl_job.status','decline')
                            ->from('tbl_job')
                            ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    /***********End Job************/


    /**************Project****************/

    public function activeprojectlist()
    {
        $query = $this->db->select("tbl_project.status,tbl_project.project_name,tbl_project.project_title,tbl_project.project_location,tbl_project.project_id,tbl_project.user_id,tbl_user.type")
                            ->where('tbl_project.status !=','open')
                            ->where('tbl_project.status !=','updated')                            
                            ->where('tbl_project.status !=','decline')
                            ->from('tbl_project')
                            ->join('tbl_user', 'tbl_project.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }


    public function requestprojectlist()
    {
        $query = $this->db->select("tbl_project.status,tbl_project.project_name,tbl_project.project_title,tbl_project.project_location,tbl_project.project_id,tbl_project.user_id,tbl_user.type")
                             ->where('tbl_project.status','open')
                            ->from('tbl_project')
                            ->join('tbl_user', 'tbl_project.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function change_projects_status($id,$type)
    {
        return $this->db->set('status', $type)
                    ->where('project_id', $id)
                    ->update('tbl_project');
    }

    public function editprojectslist()
    {
        $query = $this->db->select("tbl_project.status,tbl_project.project_name,tbl_project.project_title,tbl_project.project_location,tbl_project.project_id,tbl_project.user_id,tbl_user.type")
                            ->where('tbl_project.status','updated')
                            ->from('tbl_project')
                            ->join('tbl_user', 'tbl_project.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function declineprojectslist()
    {
        $query = $this->db->select("tbl_project.status,tbl_project.project_name,tbl_project.project_title,tbl_project.project_location,tbl_project.project_id,tbl_project.user_id,tbl_user.type")
                            ->where('tbl_project.status','decline')
                            ->from('tbl_project')
                            ->join('tbl_user', 'tbl_project.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    /*public function change_jobs_status($id,$type)
    {
        return $this->db->set('status', $type)
                    ->where('project_id', $id)
                    ->update('tbl_project');
    }

    public function activejoblist()
    {
        $query = $this->db->select("tbl_job.status,tbl_job.job_type,tbl_job.job_title,tbl_job.company_name,tbl_job.job_id,tbl_job.user_id,tbl_user.type")
                            ->where('tbl_job.status !=','open')
                            ->where('tbl_job.status !=','updated')                            
                            ->where('tbl_job.status !=','decline')
                            ->from('tbl_job')
                            ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function editrequestjoblist()
    {
        $query = $this->db->select("tbl_job.status,tbl_job.job_type,tbl_job.job_title,tbl_job.company_name,tbl_job.job_id,tbl_job.user_id,tbl_user.type")
                            ->where('tbl_job.status','updated')
                            ->from('tbl_job')
                            ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function declinedjoblist()
    {
        $query = $this->db->select("tbl_job.status,tbl_job.job_type,tbl_job.job_title,tbl_job.company_name,tbl_job.job_id,tbl_job.user_id,tbl_user.type")
                            ->where('tbl_job.status','decline')
                            ->from('tbl_job')
                            ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }*/

    /***********End Job************/

    /**************** education *************/

    public function requesteducationlist()
    {
        $query = $this->db->select("tbl_student_education.status,tbl_student_education.my_university,tbl_student_education.graduate,tbl_student_education.education_id,tbl_student_education.user_id,tbl_user.type")
                            ->where('tbl_student_education.status','open')
                            ->from('tbl_student_education')
                            ->join('tbl_user', 'tbl_student_education.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function change_educations_status($id,$type)
    {
        return $this->db->set('status', $type)
                    ->where('education_id', $id)
                    ->update('tbl_student_education');
    }

    public function activeeducationlist()
    {
        $query = $this->db->select("tbl_student_education.status,tbl_student_education.my_university,tbl_student_education.graduate,tbl_student_education.education_id,tbl_student_education.user_id,tbl_user.type")
                            ->where('tbl_student_education.status !=','open')
                            ->where('tbl_student_education.status !=','updated')                            
                            ->where('tbl_student_education.status !=','decline')
                            ->from('tbl_student_education')
                            ->join('tbl_user', 'tbl_student_education.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }
    public function editeducationslist()
    {
       $query = $this->db->select("tbl_student_education.status,tbl_student_education.my_university,tbl_student_education.graduate,tbl_student_education.education_id,tbl_student_education.user_id,tbl_user.type")
                            ->where('tbl_student_education.status','updated')
                            ->from('tbl_student_education')
                            ->join('tbl_user', 'tbl_student_education.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function editrequesteducationlist()
    {
        $query = $this->db->select("tbl_student_education.status,tbl_student_education.my_university,tbl_student_education.graduate,tbl_student_education.education_id,tbl_student_education.user_id,tbl_user.type")
                            ->where('tbl_student_education.status','updated')
                            ->from('tbl_student_education')
                            ->join('tbl_user', 'tbl_student_education.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    public function declinededucationlist()
    {
        $query = $this->db->select("tbl_student_education.status,tbl_student_education.my_university,tbl_student_education.graduate,tbl_student_education.education_id,tbl_student_education.user_id,tbl_user.type")
                            ->where('tbl_student_education.status','decline')
                            ->from('tbl_student_education')
                            ->join('tbl_user', 'tbl_student_education.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

    /**************** End education *************/

   /* ************************ professional ********************/
   public function activeprofessionallist()
    {
        $query = $this->db->select("tbl_student_professional.status,tbl_student_professional.total_experience,tbl_student_professional.branch,tbl_student_professional.key_skill,tbl_student_professional.location,tbl_student_professional.professional_id,tbl_student_professional.user_id,tbl_user.type")
                            ->where('tbl_student_professional.status !=','open')
                            ->where('tbl_student_professional.status !=','updated')                            
                            ->where('tbl_student_professional.status !=','decline')
                            ->from('tbl_student_professional')
                            ->join('tbl_user', 'tbl_student_professional.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }
    public function change_professional_status($id,$type)
    {
        return $this->db->set('status', $type)
                    ->where('professional_id', $id)
                    ->update('tbl_student_professional');
    }
    public function requestprofessionallist()
    {
        $query = $this->db->select("tbl_student_professional.status,tbl_student_professional.total_experience,tbl_student_professional.branch,,tbl_student_professional.key_skill,tbl_student_professional.location,tbl_student_professional.professional_id,tbl_student_professional.user_id,tbl_user.type")
                             ->where('tbl_student_professional.status','open')
                            ->from('tbl_student_professional')
                            ->join('tbl_user', 'tbl_student_professional.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }

     public function editprofessionalslist()
    {
        $query = $this->db->select("tbl_student_professional.status,tbl_student_professional.total_experience,tbl_student_professional.branch,tbl_student_professional.key_skill,tbl_student_professional.location,tbl_student_professional.professional_id,tbl_student_professional.user_id,tbl_user.type")
                            ->where('tbl_student_professional.status','updated')
                            ->from('tbl_student_professional')
                            ->join('tbl_user', 'tbl_student_professional.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }
    public function declineprofessionallist()
    {
         $query = $this->db->select("tbl_student_professional.status,tbl_student_professional.total_experience,tbl_student_professional.branch,tbl_student_professional.key_skill,tbl_student_professional.location,tbl_student_professional.professional_id,tbl_student_professional.user_id,tbl_user.type")
                            ->where('tbl_student_professional.status','decline')
                            ->from('tbl_student_professional')
                            ->join('tbl_user', 'tbl_student_professional.user_id = tbl_user.user_id')
                            ->get();
        return $query->result();
    }


}