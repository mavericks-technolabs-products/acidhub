<?php

class AdminLoginModel extends CI_Model
{
	public function login_valid( $email, $password)
	{
		$q = $this->db->where(['email'=>$email,'password'=>$password])
						->get('tbl_admin');

		if ( $q->num_rows() )
		{
			return $q->row()->admin_id;				
		}
		else
		{
			return FALSE;				
		}
	}

	public function num_rows()
	{
		$query = $this->db->query('SELECT * FROM tbl_admin');
		return $query->num_rows();
	}
}