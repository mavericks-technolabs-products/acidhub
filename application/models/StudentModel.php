<?php defined('BASEPATH') OR exit('No direct script access allowed');
class StudentModel extends CI_Model
  {

	 public function student_information($data,$abc)
   {          
       			 
                $q=$this->db->insert("tbl_student",$data);
                $id=$this->db->insert_id();
                $abc['user_id']=$id;
                $q3=$this->db->insert('tbl_user',$abc);
                return $q;
    }// public function student_information($data,$abc)
    public function studentfindcontact($contact)
    {
        $this->db->select('*');
        $this->db->from ("tbl_users_details");
        $this->db->where('contact', $contact);
            $q=$this->db->get();
        return $q->result();
    }// public function studentfindcontact($contact)
    public function find_student($user_id)
    {
                $query = $this->db->get_where('tbl_users_details', array('user_id' => $user_id));
                return $query->row();
    }//public function find_student($user_id)
       
    public function find_education($education_id)
    {
   
               $q=$this->db->select("*")
              ->where("education_id",$education_id)
              ->get("tbl_student_education");
              return $q->result();
    }// public function find_education($education_id)
            
    public function student_login($email,$password)
    {
                $this->db->select('*');
                $this->db->from ('tbl_student');
                $this->db->where('email',$email);
                $this->db->where('password',$password);
             
                $q=$this->db->get();
               /* print_r($q);
                exit();*/
                return $q->result();
    }// public function student_login($email,$password)
    public function updatestudentprofile($id,$data)
    {
                     $q= $this->db
                    ->where("user_id",$id)
                    ->update("tbl_users_details",$data);
                     // print_r($this->db->last_query());
                    return $q;
    }//  public function updatestudentprofile($id,$data)

              /* ************ education *********************/
    public function studenteducationcreate($data)
    {          
            return $this->db->insert("tbl_student_education",$data);
            
    }//public function studenteducationcreate($data)
        
    public function searcheducation($user_id)
    {
            // $user_id = $this->session->userdata("id");
            $q=$this->db->select("*")
                        ->from('tbl_users_details')
                        ->join('tbl_student_education','tbl_users_details.user_id=tbl_student_education.user_id')
                         ->where("tbl_student_education.user_id",$user_id)
                        //->where('tbl_student_education.status','active')
                         ->group_by('tbl_student_education.education_id');
                           $q=$this->db->get();
                         $result=$q->result();
                         return $result;
    }//public function searcheducation($user_id)
       

    public function deleteeducation($education_id)
    {
             $q= $this->db->delete('tbl_student_education',['education_id'=>$education_id]);
                  return $q;
  
    }//public function deleteeducation($education_id)
    public function updateedu($id,$data)
    {
            $q= $this->db
                     ->where("education_id",$id)
                     ->update("tbl_student_education",$data);
                      return $q;
         }// public function updateedu($id,$data)
    public function searcheducationdetails($user_id,$education_id)
    {
            // $user_id = $this->session->userdata("id");
            $q=$this->db->select("*")
                        ->from('tbl_users_details')
                        ->join('tbl_student_education','tbl_users_details.user_id=tbl_student_education.user_id')
                        ->where("education_id",$education_id);
                              $q=$this->db->get();
                              $result=$q->result();
                              return $result;
    }//public function searcheducationdetails($user_id,$education_id)

     public function mysearcheducation($user_id)
    {
            // $user_id = $this->session->userdata("id");
               $q=$this->db->select("*")
                          ->from('tbl_users_details')
                          ->join('tbl_student_education','tbl_users_details.user_id=tbl_student_education.user_id')
                                //->where('tbl_student_education.status','active')
                          ->get();
                            $result=$q->result();
                            return $result;
    }//public function searcheducation($user_id)
    public function studentsearcheducationdetails($user_id,$education_id)
    {
            // $user_id = $this->session->userdata("id");
                $q=$this->db->select("*")
                            ->from('tbl_users_details')
                            ->join('tbl_student_education','tbl_users_details.user_id=tbl_student_education.user_id')
                            ->where("tbl_student_education.education_id",$education_id);
                                  $q=$this->db->get();
                                  $result=$q->result();
                                  return $result;
    }//public function searcheducationdetails($user_id,$education_id)
       

   /*   ************************** student professional ************************/
    public function studentprofessionalcreate($data1)
    {          
        
          return $this->db->insert("tbl_student_professional",$data1);
          print_r($this->db->last_query());
          exit();
             
    }// public function studentprofessionalcreate($data1)
    public function searchprofessional($user_id)
    {
            // $user_id = $this->session->userdata("id");
                $q=$this->db->select("*")
                            ->from('tbl_users_details')
                            ->join('tbl_student_professional','tbl_users_details.user_id=tbl_student_professional.user_id')
                              ->where("tbl_student_professional.user_id",$user_id);
                            //->where('tbl_student_professional.status','active');
                             $q=$this->db->get();
                             $result=$q->result();
                              return $result;
   }// public function searchprofessional($user_id)
    public function find_professional($professional_id)
    {
    
        $q=$this->db->select("*")
                    ->where("professional_id",$professional_id)
                    ->get("tbl_student_professional");
                   return $q->result();
    }//  public function find_professional($professional_id)
    public function deleteprofessional($professional_id)
    {
        $q= $this->db->delete('tbl_student_professional',['professional_id'=>$professional_id]);
        return $q;
    }//public function deleteprofessional($professional_id)
    public function updateprofession($id,$data)
    {
       $q= $this->db
                ->where("professional_id",$id)
                ->update("tbl_student_professional",$data);
                return $q;
    }//public function updateprofession($id,$data)
    public function searchstudeprofessional($user_id)
    {
            // $user_id = $this->session->userdata("id");
                $q=$this->db->select("*")
                            ->from('tbl_users_details')
                            ->join('tbl_student_professional','tbl_users_details.user_id=tbl_student_professional.user_id')
                            ->where('tbl_student_professional.user_id',$user_id);
                             //->group_by('blog.blog_id');
                             // $this->db->where_in('blog_image.blog_img_id',$id);
                       $q=$this->db->get();
                       $result=$q->result();
                       return $result;
    }// public function searchstudeprofessional($user_id)

     public function mysearchprofessional($user_id)
    {
            // $user_id = $this->session->userdata("id");
                $q=$this->db->select("*")
                            ->from('tbl_users_details')
                            ->join('tbl_student_professional','tbl_users_details.user_id=tbl_student_professional.user_id');
                              $q=$this->db->get();
                              $result=$q->result();
                              return $result;
   }// public function searchprofessional($user_id)
  /**************** student resume ****************/
    
    public function studentresumecreate($data)
        {          
            $this->db->insert("tbl_student_resume",$data);
            //       print_r($this->db->last_query());
            // exit();

             return $this->db->insert_id();
       
        }//public function studentresumecreate($data)
         public function get_recent_resume_id()
      {
         $q=$this->db->select_max('student_resume_id')
                  ->get('tbl_student_resume');
          return $q->row();
      }//public function get_recent_project_id()
        public function insert_multiresume_img($data = array())
      {
              $insert = $this->db->insert_batch('tbl_student_resume',$data);
              return $insert?true:false;
      }
         public function find_resume($user_id)
    {
    
        $q=$this->db->select("*")
                    ->where("user_id",$user_id)
                    ->get("tbl_student_resume");
                    return $q->result();
    }// public function find_resume($user_id)
    public function studentskillcreate($data)
        {          
           return $this->db->insert("tbl_student_resume_skill",$data);
        }//public function studentresumecreate($data)
        
    public function studentsearchemail($email)
    {
        $this->db->select('*')
                 ->from("tbl_users_details")
                 ->where('email', $email);
                  $q=$this->db->get();
                   return $q->result();
    }//  public function searchemail($email)
    public function studentresumeskill($student_resume_id)
    {
        $q=$this->db->select("*")
                    ->where("student_resume_id",$student_resume_id)
                    ->get("tbl_student_resume_skill");
          return $q->result();
    }

    public function deletestudentresumeskill($resume_id)
    {
      return $this->db->delete('tbl_student_resume_skill',['resume_id'=>$resume_id]);
    }

    public function find_studentresume($user_id)
    {
        $q=$this->db->select("*")
                  ->where("user_id",$user_id)
                    ->get("tbl_student_resume");
                    /*echo $this->db->last_query();
            exit();*/
                    return $q->result();
    }//  public function find_professional($professional_id)

    public function updateresume($id,$data)
         {
          
            $q= $this->db
                     ->where("student_resume_id",$id)
                     ->update("tbl_student_resume",$data);
                        return $q;
         }// public function updateedu($id,$data)
    public function update_resume_multi_img($data)
      {
          $insert = $this->db->insert('tbl_student_resume',$data);
          return true;
      }

    public function deleteresume($student_resume_id)
     {
          //print_r($this->input->post());
           $q= $this->db->delete('tbl_student_resume',['student_resume_id'=>$student_resume_id]);
           /*echo $this->db->last_query();
            exit();*/
           return $q;
      }//public function deleteprofessional($professional_id)
    public function searchstudentresume($user_id)
    {
            // $user_id = $this->session->userdata("id");
                     $q=$this->db->select("*")
                                ->from('tbl_users_details')
                                ->join('tbl_student_resume','tbl_users_details.user_id=tbl_student_resume.user_id')
                                ->where('tbl_student_resume.user_id',$user_id)
                                
                             ->group_by('tbl_student_resume.user_id');
                             // $this->db->where_in('blog_image.blog_img_id',$id);
                       $q=$this->db->get();
                      /* print_r($this->db->last_query());
                          exit();*/
                       $result=$q->result();
                      return $result;
    }// public function searchprofessional($user_id)
    public function searchstudentresumeeducation($user_id)
    {
            // $user_id = $this->session->userdata("id");
                $q=$this->db->select("*")
                            ->from('tbl_student_education')
                            ->join('tbl_student_resume','tbl_student_education.user_id=tbl_student_resume.user_id')
                              ->where('tbl_student_resume.user_id',$user_id)
                             ->group_by('tbl_student_resume.user_id');
                             // $this->db->where_in('blog_image.blog_img_id',$id);
                         $q=$this->db->get();
                        $result=$q->result();
                        return $result;
    }// public function searchstudentresumeeducation($user_id)
     public function searchstudentresumeworkeducation($user_id)
    {
            // $user_id = $this->session->userdata("id");
                $q=$this->db->select("*")
                            ->from('tbl_student_education')
                            ->join('tbl_student_resume','tbl_student_education.user_id=tbl_student_resume.user_id')
                              ->where('tbl_student_resume.user_id',$user_id)
                             ->group_by('tbl_student_resume.user_id');
                             // $this->db->where_in('blog_image.blog_img_id',$id);
                         $q=$this->db->get();
                         $result=$q->result();
                        return $result;
    }// public function searchstudentresumeeducation($user_id)
    public function searchspecificresume($student_resume_id)
        {
            $user_id = $this->session->userdata("user_id");
              $q=$this->db->select("*")
                          ->from('tbl_student_resume')
                          ->join('tbl_student_resume_skill','tbl_student_resume.student_resume_id=tbl_student_resume_skill.student_resume_id')
                         /* ->join('tbl_blog_image','tbl_blog.blog_id=tbl_blog_image.blog_id')*/
                          ->where('tbl_student_resume.user_id',$user_id)
                          ->group_by('tbl_student_resume.student_resume_id')
                          ->get();
                         $result=$q->result();
                          return $result;
        }//public function searchblog()
    /***************** student job***********************/
      public function studentalljobspecific()
    {
             $q=$this->db->select("*")
                         ->from('tbl_job')
                         ->join('tbl_users_details','tbl_job.user_id=tbl_users_details.user_id')
                         ->group_by('tbl_job.job_id')
                         ->get();
                          $result=$q->result();
                          return $result;
    }// public function studentalljobspecific()  
    public function studentallselectjob($job_id)
    {
            // $user_id = $this->session->userdata("id");
                $q=$this->db->select("*")
                                ->from('tbl_job')
                                ->join('tbl_users_details','tbl_users_details.user_id=tbl_job.user_id')
                                ->where('tbl_job.job_id',$job_id)
                                ->group_by('tbl_job.job_id')
                                  ->get();
                                  $result=$q->result();
                                  return $result;
    }// public function studentallselectjob()
    public function studentallselectjob1($job_id,$user_id)
    {
                $q=$this->db->select("*")
                                ->from('tbl_student_job_apply')
                                ->where('job_id',$job_id)
                                ->where('user_id',$user_id)
                                ->get();
                                  $result=$q->row();
                                  return $result;
    }// public function studentallselectjob()
     public function applyjob($user_id,$job_id)
    {
            $q = $this->db->where('job_id', $job_id)
                          ->get("tbl_student_job_apply");
                         return $q->row();
    }
    public function studentjobresumecreate($data)
    {          
        
          return $this->db->insert("tbl_student_job_apply",$data);
             
    }// public function studentprofessionalcreate($data1)
     public function findstudentinformation_article($user_id)
  {
   
    $q1=$this->db->select("*")
    ->where("user_id",$user_id)
    ->get("tbl_users_details");
    return $q1->result();
  }
  public function studentupdateprofile($id)
    {
        $q= $this->db
                ->where("user_id",$id)
                  ->update("tbl_users_details");
            return $q;
    }//public function updateprofile($id,$data)

  public function personalstudentprofile($user_id)
      {
        /* $user_id = $this->session->userdata("user_id");*/
          $q=$this->db->select("*")
                      ->from('tbl_users_details')
                      ->where('tbl_users_details.user_id',$user_id)
                      ->get();
                       $result=$q->result();
                       return $result;
      }//public function searchproject($id)
   
    public function viewstudentresume($user_id)
    {
            // $user_id = $this->session->userdata("id");
                $q=$this->db->select("*")
                            ->from('tbl_users_details')
                            ->join('tbl_student_resume','tbl_users_details.user_id=tbl_student_resume.user_id')
                            ->join('tbl_student_resume_skill','tbl_student_resume.student_resume_id=tbl_student_resume_skill.student_resume_id')
                              ->where('tbl_student_resume.user_id',$user_id)
                             ->group_by('tbl_student_resume.user_id');
                             $q=$this->db->get();
                             $result=$q->result();
                            return $result;
    }// public function searchstudentresumeeducation($user_id)
    }