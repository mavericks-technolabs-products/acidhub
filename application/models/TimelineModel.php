<?php
defined('BASEPATH') OR exit('No direct script access allowed');
       class TimelineModel extends CI_Model
         {

	   

            public function searchtimeline($id)
            {
                 $user_id = $this->session->userdata("user_id");
               $q=$this->db->select("*")
                                ->from('tbl_users_details')
                                ->join('tbl_user','tbl_users_details.user_id=tbl_user.user_id')
                                ->where("tbl_user.user_id",$id);
                                  $q=$this->db->get();
                                  $result=$q->row();
                                  return $result;
            }//public function searchproject($id)

             public function timelinesearchproject()
            {
               $user_id = $this->session->userdata("user_id");
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')
                            ->where('tbl_project.user_id',$user_id)
                             ->group_by('tbl_project.project_id');
                             $q=$this->db->get();
                             $result=$q->result();
                             return $result;
            }//public function searchproject($id)

            public function timelinesearchblog()
        {
            $user_id = $this->session->userdata("user_id");
              $q=$this->db->select("*")
                          ->from('tbl_blog')
                          
                          ->join('tbl_users_details','tbl_blog.user_id=tbl_users_details.user_id')
                          ->join('tbl_blog_image','tbl_blog.blog_id=tbl_blog_image.blog_id')
                          ->where('tbl_blog.user_id',$user_id)
                          ->group_by('tbl_blog.blog_id')
                          ->get();
                         /* print_r($this->db->last_query());
                          exit();*/
                             
                             $result=$q->result();
                             return $result;
        }//public function searchblog()

            public function timelinesearchjob()
        {
            $user_id = $this->session->userdata("user_id");
               
                       $q= $this->db->select("*")
                                ->from('tbl_users_details')
                                ->join('tbl_job','tbl_users_details.user_id=tbl_job.user_id')
                                ->where("tbl_job.user_id",$user_id)
                                ->where('tbl_job.status','active');
                                  $q=$this->db->get();
                                  $result=$q->result();
                                  return $result;
        }//public function searchjob()
        }
