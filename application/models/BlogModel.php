<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BlogModel extends CI_Model
  {
      public function blogaddcreate($data)
	 		{          
            return $this->db->insert("tbl_blog",$data);
	 		}// public function blogaddcreate($data)
      public function get_recent_blog_id()
      {
            # code...
             $q=$this->db->select_max('blog_id')
                         ->get('tbl_blog');
                          return $q->row();
      }// public function get_recent_blog_id()
      public function insert_blog_multi_img($data = array())
      {
              $insert = $this->db->insert_batch('tbl_blog_image',$data);
              return $insert?true:false;
      }// public function insert_blog_multi_img($data = array())

      public function searchblog()
      {
            $user_id = $this->session->userdata("user_id");
              $q=$this->db->select("*")
                          ->from('tbl_blog')
                          /*->where('tbl_blog.status !=','open')
                          ->where('tbl_blog.status !=','updated')*/
                          ->where('tbl_blog.status !=','decline')
                          ->join('tbl_users_details','tbl_blog.user_id=tbl_users_details.user_id')
                          ->join('tbl_blog_image','tbl_blog.blog_id=tbl_blog_image.blog_id')
                          ->where('tbl_blog.user_id',$user_id)
                          ->group_by('tbl_blog.blog_id')
                          ->get();
                         $result=$q->result();
                             return $result;
      }//public function searchblog()
      public function find_blog_one($blog_id)
      {
            # code...
            $q=$this->db->select("*")
                        ->where("blog_id",$blog_id)
                        ->get('tbl_blog');
                          return $q->row();
      }//public function find_blog_one($blog_id)

      public function blog_get_multi_images($value)
      {
            # code...
            $q = $this->db->where('blog_id', $value)
                          ->get("tbl_blog_image");
                     return $q->result();
      }//public function blog_get_multi_images($value)
      public function update_blog_multi_img($data)
      {
          $insert = $this->db->insert('tbl_blog_image',$data);
          return true;
      }//public function update_blog_multi_img($data)
      public function blog_get_image($blog_img_id)
      {
            # code...
          $q = $this->db->where('blog_img_id', $blog_img_id)
                        ->get("tbl_blog_image");
                        return $q->row();
      }// public function blog_get_image($blog_img_id)
      public function blogdeleteimage($blog_img_id)
      {
          $this->db->where('blog_img_id', $blog_img_id)
                   ->delete('tbl_blog_image');
      }//public function blogdeleteimage($blog_img_id)
      public function update_blog($id,$data)
      {
         
               $q= $this->db->set($data)
                            ->where("blog_id",$id)
                            -> update("tbl_blog",$data);
              if($q)
                {
                   return true;
                }// if($q)
      }//public function update_blog($id,$data)

      public function deleteblog($blog_id)
      {
          //print_r($this->input->post());
              $q= $this->db->delete('tbl_blog',['blog_id'=>$blog_id]);
                  return $q;
      }// public function deleteblog($blog_id)

      public function selectblog($id,$blog_id)
      {

                $q=$this->db->select("*")
                            ->from('tbl_blog')
                            ->join('tbl_blog_image','tbl_blog.blog_id=tbl_blog_image.blog_id')
                            ->where("tbl_blog.blog_id",$blog_id)
                              ->get();
                             return $q->result();
      }//public function selectblog($id,$blog_id)

      public function blog_get_article($value)
      {
            # code...
            $q=$this->db->select("*")
                        ->where('blog_id',$value)
                        ->get('tbl_blog');
                        return $q->row();
      }//   public function blog_get_article($value)   
             
      public function blog_find_blog($id)
      {
         $q=$this->db->query("select * from tbl_blog  where blog_id='$id'");
         return $q->row();
      }//public function blog_find_blog($id)

      public function createrequestbloglist()
      {
        $query = $this->db->select("*")
                            ->where('status','open')
                            ->from('tbl_blog')
                            ->get();
        return $query->result();
      }// public function createrequestbloglist()
       
       /******************** new blog 26nov******************/
      public function allblogspecific()
    {
          $q=$this->db->select("*")
                      ->from('tbl_blog')
                      ->join('tbl_users_details','tbl_blog.user_id=tbl_users_details.user_id')
                      ->join('tbl_blog_image','tbl_blog.blog_id=tbl_blog_image.blog_id')
                      ->group_by('tbl_blog.blog_id')
                      ->get();
                       $result=$q->result();
                       return $result;
    }// public function blogspecific()   
     
    public function allselectionblog($blog_id)
    {
             $q=$this->db->select("*")
                        ->from('tbl_blog')
                        ->join('tbl_users_details','tbl_users_details.user_id=tbl_blog.user_id')
                        ->join('tbl_blog_image','tbl_blog.blog_id=tbl_blog_image.blog_id')
                        ->where('tbl_blog.blog_id',$blog_id)
                        ->group_by('tbl_blog.blog_id')
                       ->get();
                         $result=$q->row();
                        return $result;
    }// public function selectblog()       
  }//class BlogModel extends CI_Model
