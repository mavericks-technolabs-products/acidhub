<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class LoginModel extends CI_Model
{
    public function insert_data($data,$data1)
    {
        $q=$this->db->insert("tbl_user",$data1);
        $id=$this->db->insert_id();
        $data['user_id']=$id;
        $data['user_unique_id']=$data1['type'].$id.$data1['firstname'];
         // $data['user_unique_id']=$data1['type'].$id;
        $q3=$this->db->insert('tbl_users_details',$data);
        return $q;
    }//public function insert_data($data,$data1)
          
    public function findcontact($contact)
    {
        $this->db->select('*');
        $this->db->from ("tbl_users_details");
        $this->db->where('contact', $contact);
            $q=$this->db->get();
        return $q->result();
    }// public function findcontact($contact)

    public function searchemail($email)
    {
        $this->db->select('*');
        $this->db->from ("tbl_users_details");
        $this->db->where('email', $email);
            $q=$this->db->get();
        return $q->result();
    }//  public function searchemail($email)

    public function profilecontact($user_id)
    {
           $this->db->select('*');
         $this->db->from ("tbl_user");
           $this->db->where('user_id', $user_id);
            $q=$this->db->get();
        /*  print_r($this->db->last_query());exit();*/
            return $q->result();
    }//  public function profilecontact($user_id)

    public function find_interior($user_id)
    {
          	$query = $this->db->get_where('tbl_users_details', array('user_id' => $user_id));
             /*    print_r($this->db->last_query());exit();*/
 			      return $query->row();
    }//public function find_interior($user_id)
    public function find_architecture($user_id)
    {
            $query = $this->db->get_where('tbl_users_details', array('user_id' => $user_id));
            return $query->row();
    }//  public function find_architecture($user_id)
    public function login($email,$password)
    {
          $this->db->select('*')
                    ->from ('tbl_user')
                    ->where('email',$email)
                    ->where('password',$password);
         
          $q=$this->db->get();
          return $q->result();
    }// public function login($email,$password)
    public function updateprofile($id,$data)
		{
        $q= $this->db
				        ->where("user_id",$id)
				          ->update("tbl_users_details",$data);
				    return $q;
    }//public function updateprofile($id,$data)

    public function search_interior($interior_id)
    {
      		$q=$this->db->select("*")
      		->where("interior_id",$interior_id)
      		->get("tbl_interior");
      		// print_r($this->db->last_query());exit();
      		return $q->result();
    }// public function search_interior($interior_id)

    public function delete($id)
    {
        //print_r($this->input->post());
        return $this->db->delete('tbl_interior',['id'=>$id]);
    }//public function delete($id)
    public function updatechangepassword($id,$data)
    {
          // print_r($data4);
          // exit();
            $q= $this->db
          ->where("user_id",$id)
          ->update("tbl_users_details",$data);
          /*print_r($this->db->last_query());
          exit();*/
          return $q;
    }// public function updatechangepassword($id,$data)
    public function profileselectcontact($contact)
    {
           $this->db->select('*');
         $this->db->from ("tbl_interior");
           $this->db->where('contact', $contact);
            $q=$this->db->get();
        /*  print_r($this->db->last_query());exit();*/
            return $q->result();
    }// public function profileselectcontact($contact)

                      /*  ************* Job **************/
  }//class LoginModel extends CI_Model
  