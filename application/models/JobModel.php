<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class JobModel extends CI_Model
  {
      public function jobaddcreate($data)
      {          
           return $this->db->insert("tbl_job",$data);
      }// public function jobaddcreate($data)

      public function searchjob()
      {
            $user_id = $this->session->userdata("user_id");
               
                       $q= $this->db->select("*")
                                ->from('tbl_users_details')
                                ->join('tbl_job','tbl_users_details.user_id=tbl_job.user_id')
                                ->where("tbl_job.user_id",$user_id);
                                //->where('tbl_job.status','active');
                                  $q=$this->db->get();
                                  $result=$q->result();
                                  return $result;
      }//public function searchjob()

      public function find_job($job_id)
      {
              $q=$this->db->select("*")
                          ->where("job_id",$job_id)
                          ->get("tbl_job");
                        return $q->result();
      }//public function find_job($job_id)

      public function updateq($id,$data)
      {
          
          $q= $this->db
                    ->where("job_id",$id)
                    ->update("tbl_job",$data);
                    return $q;
      }// public function updateq($id,$data)

      public function searchjobdetails($user_id,$job_id)
      {
            // $user_id = $this->session->userdata("id");
                $q=$this->db->select("*")
                                ->from('tbl_users_details')
                                ->join('tbl_job','tbl_users_details.user_id=tbl_job.user_id')
                                ->where("job_id",$job_id);
                                  $q=$this->db->get();
                                  $result=$q->result();
                                  return $result;
      }//public function searchjobdetails($user_id,$job_id)

      public function studentjobapplydetails($job_id)
      {
        $q = $this->db->select("*")
                      ->from('tbl_student_job_apply')
                      ->where('job_id',$job_id)
                      ->get();
                      return $q->result();
      }
      public function studentjob_applydetails($job_id)
      {

        $q = $this->db->select("*")
                      ->from('tbl_users_details')
                      ->where('user_id',$job_id)
                      ->get();
                     return $q->row();
      }
      public function jobapplydetails($job_id)
      {
        $q = $this->db->select("user_id")
                      ->from('tbl_student_job_apply')
                      ->where('job_id',$job_id)
                      ->get();

        return $q->result();
      }

      public function job_applydetails($job_id)
      {

        $q = $this->db->select("*")
                      ->from('tbl_users_details')
                      ->where('user_id',$job_id)
                      ->get();

        return $q->row();
      }


      public function deletejob($job_id)
      {
            //print_r($this->input->post());
             $q= $this->db->delete('tbl_job',['job_id'=>$job_id]);
            return $q;
            
      }//public function deletejob($job_id)
	        
	 		      /************** apply job**************/
      public function selectresume($id,$job_resume_id)
      {

                   $q=$this->db->select("*")
                    ->from('tbl_interiorjob')
                    ->join('tbl_interiorjob','.job_resume_id=tbl_interior.job_resume_id')
                    ->where("tbl_project.project_id",$project_id)
                      ->get();
                    return $q->result();
                                       
      }// public function selectresume($id,$job_resume_id)

      public function jobapply($data)
      {          
             
              return $this->db->insert("tbl_interior_resume",$data);
      }//  public function jobapply($data)
      public function get_recent_resume_id()
      {
                      # code...
                     $q=$this->db->select_max('job_resume_id')
                             //->where('id',$value)
                              ->get('tbl_interior_resume');
                       return $q->row();

      }//public function get_recent_resume_id()
      public function resumedelete($job_resume_id)
      {
                      //print_r($this->input->post());
                      return $this->db->delete('tbl_interior_resume',['job_resume_id'=>$job_resume_id]);
      }// public function resumedelete($job_resume_id)
      public function selectapplycontact($contact_no)
      {
                         $this->db->select('*');
                       $this->db->from ("tbl_interior_resume");
                         $this->db->where('contact_no', $contact_no);
                          $q=$this->db->get();
                       return $q->result();
      }//public function selectapplycontact($contact_no)
      public function alljobspecific()
    {
             $q=$this->db->select("*")
                         ->from('tbl_job')
                         ->join('tbl_users_details','tbl_job.user_id=tbl_users_details.user_id')
                         ->group_by('tbl_job.job_id')
                         ->get();
                          $result=$q->result();
                          return $result;
    }// public function jobspecific()
    public function allselectjob($job_id)
    {
            // $user_id = $this->session->userdata("id");
                $q=$this->db->select("*")
                                ->from('tbl_job')
                                ->join('tbl_users_details','tbl_users_details.user_id=tbl_job.user_id')
                                ->where('tbl_job.job_id',$job_id)
                                ->group_by('tbl_job.job_id')
                                  ->get();
                                 $result=$q->result();
                                  return $result;
    }// public function allselectjob()
}
