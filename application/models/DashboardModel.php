<?php
class DashboardModel extends CI_Model
{
  public function count_interior()
  {
    $query = $this->db
                ->select('*')
                ->from('tbl_user')
                ->where('type','Interior')
                ->get();
    return $query->num_rows();
  }


  public function active_count_interior()
  {
    $query = $this->db
                ->select('*')
                ->where('status','active')
                ->where('type','Interior')
                ->from('tbl_user')
                ->get();
    return $query->num_rows();
  }
  public function active_count_architecture()
  {
    $query = $this->db
                ->select('*')
                ->where('status','active')
                ->where('type','Architecture')
                ->from('tbl_user')
                ->get();
    return $query->num_rows();
  }
  
  public function count_architecture()
  {
    $query = $this->db
                ->select('*')
                ->from('tbl_user')
                ->where('type','Architecture')
                ->get();
    return $query->num_rows();
  }

  public function student_count()
  {
    $query = $this->db
                ->select('*')
                ->where('type','Student')
                ->from('tbl_user')
                ->get();
    return $query->num_rows();
  }

  public function active_student_count()
  {
    $query = $this->db
                ->select('*')
                ->where('status','active')
                ->where('type','Student')
                ->from('tbl_user')
                ->get();
    return $query->num_rows();
  }

  public function count_job_interior()
  {
    $query = $this->db
                ->select('*')
                ->from('tbl_job')
                ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                ->where('type','Interior')
                ->get();
    return $query->num_rows();
  }

  public function week_count_job_interior()
  {
    $query = $this->db
                ->select('*')
                ->where('created_date >=', date('Y-m-d', strtotime('-1 week')))
                ->from('tbl_job')
                ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                ->where('type','Interior')
                ->get();
    return $query->num_rows();
  }

  public function month_count_job_interior()
  {
    $query = $this->db
                ->select('*')
                ->where('created_date >=', date('Y-m-d', strtotime('-1 month')))
                ->from('tbl_job')
                ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                ->where('type','Interior')
                ->get();
    return $query->num_rows();
  }

  public function count_job_architecture()
  {
    $query = $this->db
                ->select('*')
                ->from('tbl_job')
                ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                ->where('type','Architecture')
                ->get();
    return $query->num_rows();
  }

  public function week_count_job_architecture()
  {
    $query = $this->db
                ->select('*')
                ->where('created_date >=', date('Y-m-d', strtotime('-1 week')))
                ->from('tbl_job')
                ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                ->where('type','Architecture')
                ->get();
    return $query->num_rows();
  }

  public function month_count_job_architecture()
  {
    $query = $this->db
                ->select('*')
                ->where('created_date >=', date('Y-m-d', strtotime('-1 month')))
                ->from('tbl_job')
                ->join('tbl_user', 'tbl_job.user_id = tbl_user.user_id')
                ->where('type','Architecture')
                ->get();
    return $query->num_rows();
  }

  public function user_list()
  {
    $query = $this->db
              ->select("*")
              ->where('status','open')
              ->from('tbl_user')
              ->get();
    return $query->result();
  }

  public function change_interior_status($id,$type)
  {
    return $this->db->set('status', $type)
                    ->where('user_id', $id)
                    ->update('tbl_user');
  }

  public function active_user_list()
  {
    $query = $this->db
              ->select("*")
              ->where('status !=','open')
              ->where('status !=','decline')
              ->from('tbl_user')
              ->get();
    return $query->result();
  }

  public function admin_change_interior_status($id,$type)
  {
    return $this->db->set('status', $type)
                    ->where('user_id', $id)
                    ->update('tbl_user');
  }

  public function architecture_list()
  {
    $query = $this->db
              ->select("*")
              ->where('status','open')
              ->from('tbl_architecture')
              ->get();
    return $query->result();
  }

  public function change_architecture_status($id,$type)
  {
    return $this->db->set('status', $type)
                    ->where('architecture_id', $id)
                    ->update('tbl_architecture');
  }

  public function active_architecture_list()
  {
    $query = $this->db
              ->select("*")
              ->where('status !=','open')
              ->where('status !=','decline')
              ->from('tbl_architecture')
              ->get();
    return $query->result();
  }

  public function admin_change_architecture_status($id,$type)
  {
    return $this->db->set('status', $type)
                    ->where('architecture_id', $id)
                    ->update('tbl_architecture');
  }

  public function decline_interior_list()
  {
    $query = $this->db
              ->select("*")
              ->where('status','decline')
              ->from('tbl_user')
              ->get();
    return $query->result();
  }

  public function admin_change_decline_interior_status($id,$type)
  {
    return $this->db->set('status', $type)
                    ->where('user_id', $id)
                    ->update('tbl_user');
  }

  public function decline_architecture_list()
  {
    $query = $this->db
              ->select("*")
              ->where('status','decline')
              ->from('tbl_architecture')
              ->get();
    return $query->result();
  }

  public function admin_change_decline_architecture_status($id,$type)
  {
    return $this->db->set('status', $type)
                    ->where('architecture_id', $id)
                    ->update('tbl_architecture');
  }

  public function adminlist()
  {
    $query = $this->db
              ->select("*")
              ->from('tbl_admin')
              ->get();
    return $query->result();
  }

  public function bloglist()
  {
    $query = $this->db
              ->select("*")
              ->from('tbl_admin_blog')
              ->get();
    return $query->result();
  }

  public function allbloglist()
  {
    $query = $this->db
              ->select("*")
              ->from('tbl_blog')
              ->get();
    return $query->result();
  }

  public function allfaq()
  {
    $query = $this->db
              ->select("*")
              ->from('tbl_faq')
              ->get();
    return $query->result();
  }

}