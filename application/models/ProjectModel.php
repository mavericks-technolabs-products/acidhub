<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ProjectModel extends CI_Model
  {
      public function projectaddcreate($data)
    	{          
            return $this->db->insert("tbl_project",$data);
    	}

      public function get_recent_project_id()
      {
         $q=$this->db->select_max('project_id')
                  ->get('tbl_project');
          return $q->row();
      }//public function get_recent_project_id()

      public function searchproject()
      {
         $user_id = $this->session->userdata("user_id");
          $q=$this->db->select("*")
                      ->from('tbl_project')
                      ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')
                      ->where('tbl_project.user_id',$user_id)
                      // ->where('tbl_project.status','active')
                       ->group_by('tbl_project.project_id');
                       $q=$this->db->get();
                      /* print_r($this->db->last_query());
                          exit();*/
                       $result=$q->result();
                       return $result;
      }//public function searchproject($id)

      public function project_get_image($project_img_id)
      {
          $q = $this->db->where('project_img_id', $project_img_id)
                          ->get("tbl_project_img");
              return $q->row();
      }//public function project_get_image($project_img_id)

     public function project_delete_image($project_img_id)
      {
          $q=$this->db->where('project_img_id', $project_img_id)
                   ->delete('tbl_project_img');
                  return $q;
      }//public function project_delete_image($project_img_id)

      public function deleteproject($project_id)
      {
         $q= $this->db->delete('tbl_project',['project_id'=>$project_id]);
         return $q;
      }//public function deleteproject($project_id)

      public function update_project_multi_img($data)
      {
          $insert = $this->db->insert('tbl_project_img',$data);
          return true;
      }
      public function insert_multi_img($data = array())
      {
              $insert = $this->db->insert_batch('tbl_project_img',$data);
              return $insert?true:false;
      }
      public function find_project_one($project_id)
      {
            # code...
            $q=$this->db->select("*")
                        ->where("project_id",$project_id)
                        ->get('tbl_project');
                        return $q->row();
      }
  
      public function find_project($project_id)
      {
              $q=$this->db->select("*")
                          ->from('tbl_project')
                          ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')
                          ->where("tbl_project.project_id",$project_id)
                          ->get();
                          /*print_r($this->db->last_query());
                          exit();*/
                          return $q->result();
      }
            

      public function project_find_project($id)
      {
         $q=$this->db->query("select * from tbl_project where project_id='$id'");
         return $q->row();
      }
      public function selectproject($id,$project_id)
      {
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')                
                            ->where("tbl_project.project_id",$project_id)
                              ->get();
                               /*print_r($this->db->last_query());
                            exit();*/
                            return $q->result();
       }

       public function project_get_multi_images($value)
        {
                # code...
            $q = $this->db->where('project_id', $value)
                          ->get("tbl_project_img");
                         return $q->result();
        }

        public function project_get_article($value)
        {
              # code...
              $q=$this->db->select("*")
                          ->where('project_id',$value)
                          ->get('tbl_project');
                             return $q->row();
        }

        public function update_project($id,$data)
        {
                $q= $this->db
                         ->set($data)
                         ->where("project_id",$id)
                         -> update("tbl_project",$data);
                  /*  print_r($this->db->last_query());
              exit();*/
                       if($q)
                       {
                          return true;
                       }
               
        }

 
   public function blog_insert_interior_project($id,$data)
   {
     $this->db->set("project_location",$data['project_location']);
     $this->db->set("user_file",$data['user_file']);
     //$this->db->set("b.additional_architecture",$data['additional_architecture']);
     $this->db->where("project_id",$id);
     $this->db->update("tbl_project");
     return true; 
    }
   public function update_gallery($id , Array $data)
   {
          return $this->db
                      ->where('title_id',$id)
                      ->update('gallery_desc',$data );
  }

   public function insert($data = array())
   {
      $insert = $this->db->insert_batch('new_gallery',$data);
      return $insert?true:false;
    }
    public function allinteriorsearchproject()
    {
               /*$user_id = $this->session->userdata("user_id");*/
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')
                             //->where('tbl_project.user_id')
                             //->where('tbl_project.status','active')
                             ->group_by('tbl_project.project_id');
                             $q=$this->db->get();
                             $result=$q->result();
                            /* print_r($this->db->last_query());
                             exit();*/
                             return $result;
    }// public function interiorsearchproject()

     public function acidselectproject($project_id)
      {
                $q=$this->db->select("*")
                            ->from('tbl_project')
                            ->join('tbl_project_img','tbl_project.project_id=tbl_project_img.project_id')                
                            ->where("tbl_project.project_id",$project_id)
                            ->get();
                               /*print_r($this->db->last_query());
                            exit();*/
                            return $q->result();
       }


           
	 		}
	 		
