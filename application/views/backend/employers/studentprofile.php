<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<style type="text/css">
    .swal-button
    {
  padding: 7px 19px;
  border-radius: 2px;
  background-color: #4962B3;
  font-size: 12px;
  border: 1px solid #3e549a;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}
.swal-footer {
  background-color: rgb(245, 248, 250);
  margin-top: 32px;
  border-top: 1px solid #E9EEF1;
  overflow: hidden;
}
.swal-overlay {
  background-color: #8abe51;
}
#blah
{
	height: 100px;
	width: 100px;
	border-radius: 50%;
}
</style>
<?php if ($this->session->flashdata('Welcome')): ?>
                            <script>
                                swal({
                                    title: "Welcome",
                                    text: "<?php echo $this->session->flashdata('Welcome'); ?>",
                                    icon: "success",
                                    timer: 1500,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
		<!-- ======================= End Navigation ===================== -->
			
			
		<!-- ======================= Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Profile Settings</h2>
					<p><a href="<?php echo base_url();?>studentprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Profile Settings</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->

		
		<!-- ================ Profile Settings ======================= -->
		<section>
			<div class="container">
				
				<div class="row">
					<!-- col-md-6 -->
					<div class="col-md-6 col-12">
						
						<form method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>backend/student/StudentController/studentupdateprofile/<?php echo $student_details->user_id;?>">
						 	<?php $imgpath = "uploads/student/".$student_details->user_file; ?>

								<center><img src="<?= $imgpath ?>" style= "height: 100px;width: 80px;"></center>
						<div class="col-md-12">
							<div class="row">
								<input type="file"  name="user_file"  class=" img-responsive"  onchange="readURL(this);"  value="<?php echo set_value('user_file'); ?>">
								<?php if($error=$this->session->flashdata('error')): ?>
		                  						<div class="alert alert-danger">
		                  						<?= $error;?></div>
		                  							<?php endif; ?>
								<img id="blah" src="#" alt="" />
							<?php echo form_hidden('user_file',$student_details->user_file);?>
							</div>
						</div>
						<div class="col-md-12">
						
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>First Name</label>
										<input type="text" name="firstname" class="form-control" placeholder="" maxlenght="10" onkeypress="return isCharacterKey(event)" value="<?=$student_details->firstname;?>" required >
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<label>Last Name</label>
										<input type="text" name="lastname" class="form-control" placeholder="" pattern="[A-Za-z]{1,10000}" maxlenght="12" onkeypress="return isCharacterKey(event)" onkeypress="return isCharacterKey(event)" value="<?=$student_details->lastname;?>">
									</div>
								</div>
							</div>
							
							
						</div>
						<div class="col-md-12">
						<div class="row">
										<div class="col-md-12">
									<div class="form-group">
								<label>Email</label>
								<input type="text" name="email" class="form-control"  onblur="validate(this);" placeholder="" value="<?=$student_details->email;?>">
								 <div id="msg_email" style="color:red"></div>
								 <?php echo form_error('email'); ?>
								</div>
									</div>
							</div>
						</div>

					
				      <div class="col-md-12">
						<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Contact</label>
								<input type="text" name="contact" class="form-control" placeholder="" maxlength="10" pattern="\d{10}" title="Please enter exactly 10 digits" keypress="contactno()" value="<?=$student_details->contact;?>">
							</div>
						</div>

						
						<div class="col-md-6">
                   			<p class="birth">Date of Birth</p>
                   				<lnput placeholder="text" type="text" /> 
                          		<input type="date"  class="form-control"  name="bday" value="<?=$student_details->bday;?>" ></lnput>
                   			</div>
						</div>
					</div>
				</div>

						
						
					<!-- col-md-6 -->
					<div class="col-md-6 col-12 padd-top-30">
						
						<!-- col-md-12 -->
						<div class="col-md-12">
							<div class="row">
							</div>
						</div>
					</div>
								
						<div class="col-md-12">
							<div class="row">
							</div>
						</div>
								
						<div class="col-md-12">
						</div>
					
					<div class="col-md-12 col-12 padd-top-20 text-center">
						<button type="submit" class="btn theme-btn btn-m">Submit</button>
					</div>
					</form>
				</div>
				
			</div>
		</section>


		
<script type="text/javascript">
     function readURL(input) 
     {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
        
	function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
		return false;
		return true;
	}
	function isCharacterKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
		return true;
		return false;
	}
        
	function contactno()
	{          
		$('#phone').keypress(function(e) 
		{
		var a = [];
		var k = e.which;

		for (i = 48; i < 58; i++)
		a.push(i);

		if (!(a.indexOf(k)>=0))
		e.preventDefault();
		});
	}
</script>
      
       <?php
if($error = $this->session->flashdata('studentprofileFeedback')) 
{
	
    ?>
    <script>

        swal("Oop's!", "<?php echo $this->session->flashdata('studentprofileFeedback'); ?>", "success")



    </script>
    <?php
}
elseif($error = $this->session->flashdata('error')) {
    ?>
    <script>
        swal("Oops!", "<?php echo $this->session->flashdata('error'); ?>", "error")
        

    </script>
    <?php
}
?>
<?php
if($error = $this->session->flashdata('studentdate')) 
{
	
    ?>
    <script>

        swal("Oop's!", "<?php echo $this->session->flashdata('studentdate'); ?>", "error")



    </script>
    <?php
}
elseif($error = $this->session->flashdata('dateerror')) {
    ?>
    <script>
        swal("Oops!", "<?php echo $this->session->flashdata('dateerror'); ?>", "error")
        

    </script>
    <?php
}
?>
<script>
	function validateEmail(email)
      {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }

    function validate()
    {
      var $result = $("#msg_email");
      var email = $("#email").val();
      $result.text("");

      if (validateEmail(email))
      {
        if ($result) 
        {
          var email=$("#email").val();
          var BASE_URL = "<?php echo base_url();?>";
          $.ajax(
          {
            url: BASE_URL+'LoginController/cheqmail',
            type: 'POST',
            data:  { 'email': email},
            dataType:'json',
            success: function(response) 
            {
              if (response == 'Success.')
              {
                $('#msg_email').html('<span style="color: green;">'+'Success.'+"</span>");
              }
              else if(response == 'Email Already Exist.')
              {
                $('#msg_email').html('<span style="color: red;">'+'Email already Exist.'+"</span>");
              }
              else
              {
                $('#msg_email').html('Please enter valid email.');
              }
            }
          });
        }
      }
      else
      {
        $result.text(email + " is not valid.");
        $result.css("color", "red");
      }
      return false;
    }
</script>
		<!-- ================ End Profile Settings ======================= -->
			
		<!-- ================= footer start ========================= -->
		