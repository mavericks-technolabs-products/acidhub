<style>
.container h3,h4{
  color:black;
}

* {
  box-sizing: border-box;
}

.row > .column {
  padding: 0 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 25%;
}
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img {
  margin-bottom: -4px;
}

.caption-container {
  text-align: center;
  background-color: black;
  padding: 2px 16px;
  color: white;
}

.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.thumbnail.row1 {
    width: 100%;
    height: 100%;
    min-height: 87px;
    min-width: 98px;
}
.post-topbar {
    float: left;
    width: 100%;
    padding: 23px 20px;
    background-color: #fff;
    border-top: 4px solid #e44d3a;
    border-left: 1px solid #e4e4e4;
    border-bottom: 1px solid #e4e4e4;
    border-right: 1px solid #e4e4e4;
    -webkit-box-shadow: 0 0 1px rgba(0,0,0,0.24);
    -moz-box-shadow: 0 0 1px rgba(0,0,0,0.24);
    -ms-box-shadow: 0 0 1px rgba(0,0,0,0.24);
    -o-box-shadow: 0 0 1px rgba(0,0,0,0.24);
    box-shadow: 0 0 1px rgba(0,0,0,0.24);
    margin-bottom: 20px;
    margin-top: 0px;
  
}
.user-picy {
    float: left;
    width: 50px;
    margin-bottom: 10px;
}
.post-st {
    float: right;
    margin-top: 5px;
}
.post-st ul li {
    display: inline-block;
    margin-right: 6px;
}
.post-st ul li a {
    color: #b2b2b2;
    font-size: 16px;
    display: inline-block;
    background-color: #e5e5e5;
    height: 40px;
    padding: 0 15px;
    line-height: 40px;
    font-weight: 500;
}
span.pull-right.vacancy.buttontime {
    margin-top: -34px;
}
.sidebarimg img.img-circle {
    border: 2px solid #ff7c39;
    padding: 2px;
    margin-right: 20px;
    width: 69px;
    height: 69px;
}
.abc {
    margin-top: -51px;
    float: right;
    margin-right: 26px;
}

* {
    box-sizing: border-box;
}

body {
    margin: 0;
    font-family: Arial;
}

/* The grid: Four equal columns that floats next to each other */
.column {
    float: left;
    width:20%;
    padding: 10px;
}

/* Style the images inside the grid */
.column img {
    opacity: 0.8; 
    cursor: pointer; 
}

.column img:hover {
    opacity: 1;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* The expanding image container */
.container abc {
    position: relative;
    display: none;
}

/* Expanding image text */
#imgtext {
    position: absolute;
    bottom: 15px;
    left: 15px;
    color: white;
    font-size: 20px;
}

/* Closable button inside the expanded image */
.closebtn {
    position: absolute;
    top: 10px;
    /*right: 15px;*/
    color: white;
    font-size: 35px;
    cursor: pointer;
}
button.btn1 {
    color: #000;
    /*background-color: snow;*/
    padding: 11px;
    border-left: -1;
    border: none;
    background-color:#fff;
    border-right:1px solid #e44d3a;
    margin-right:27px;
}
.widget-boxed {
    margin-top: 18%;
}
.widget-boxed.btnbox {
    margin-top: 51px;
    padding: 25px;
    height: 84px;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
}
.cover-buttons {
    width: 100%;
    max-width: 1000px;
    margin: 0 auto;
    text-align: center;
    font-size: 16px;
    line-height: 15px;
}
.effect.social-links {
    text-align: left;
    position: relative;
}
a:-webkit-any-link {
   
    cursor: pointer;
   
}
.effect.social-links p {
    
    text-align: center;
   
}
.post-topbar {
    float: none;
}
.mySlides {display:none;}
</style>
		
		<!-- ================ Job Detail Basic Information ======================= -->
		<section class="detail-section" style="background:url(<?php echo base_url();?>assets/frontend/img/slider-2.jpg);">
			<div class="overlay"></div>
			<div class="profile-cover-content">
				<div class="container-fluid btnlink">
					
					<div class="job-owner hidden-xs hidden-sm">
						<div class="job-owner-avater">
							<img src="<?php echo base_url();?>assets/frontend/img/c-2.png" class="img-responsive img-circle" alt="" />
						</div>

					</div>
				</div>
			</div>
		</section>
		

	<div class="container">
		<div class="row">
		  <div class="col-md-3 col-sm-3">
			<div class="sidebar">
								<!-- Start: Job Overview -->
							<div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-world"></i><i>Intro</i></h4>
								</div>
								<div class="widget-boxed-body">

									<div class="side-list no-border">
										<ul>
											<li><i class="ti-credit-card padd-r-10"></i><?=$ans->firstname;?></li>
											<li><i class="ti-world"></i>&nbsp;<?=$ans->email;?></li>
											<li><i class="ti-mobile padd-r-10"></i><?=$ans->contact;?></li>
											
											<li><i class="ti-shield padd-r-10"></i><?=$ans->type;?></li>
										</ul>
											<h5>Share Job</h5>
									</div>
								</div>
							</div>
									<!-- End: Job Overview -->
								
						<div class="widget-boxed">
							<div class="widget-boxed-header">
							<h4><i class="glyphicon glyphicon-picture"></i><i>Gallary</i></h4>
							</div>
						<div class="widget-boxed-body">
							<div class="container">
						 		<div class="row">
				  					
		 
				  					<div class="col-md-1">
				  						<div class="thumbnail row1">
				   							<a href="#"><img src="http://localhost/acid/assets/frontend/img/home1.jpg" style="width:100%;height:77px;min-width:86px;" onclick="openModal();currentSlide(1)" class="hover-shadow cursor"></a>
				   						</div>
				 		  			</div>

				  					<div class="col-md-1">
					   					<div class="thumbnail row1">
					   					<a href="#"><img src="http://localhost/acid/assets/frontend/img/home2.jpg" style="width:100%;height:77px;min-width:86px;" onclick="openModal();currentSlide(2)" class="hover-shadow cursor"></a>
					   					</div>
				 					</div>
				  				</div>

								<div class="row">
				  					<div class="col-md-1">
				  						<div class="thumbnail row1">
				   							<a href="#"><img src="http://localhost/acid/assets/frontend/img/home1.jpg" style="width:100%;height:77px;min-width:86px;" onclick="openModal();currentSlide(1)" class="hover-shadow cursor"></a>
				   						</div>
				 		  			</div>
				 					<div class="col-md-1">
				   						<div class="thumbnail row1">
				   			 				 <a href="#"><img src="http://localhost/acid/assets/frontend/img/home2.jpg" style="width:100%;height:77px;min-width:86px;" onclick="openModal();currentSlide(2)" class="hover-shadow cursor"></a>
				   		  				</div>
									</div>
				 				</div>
				 			</div>
						</div>
					</div>
									<!-- End: Opening hour -->
									
									
									
			 		<div id="myModal" class="modal">
			  					<span class="close cursor" onclick="closeModal()">×</span>
			  		<div class="modal-content">
			  						<div class="mySlides" style="display: block;">
			      							<div class="numbertext">1 / 8</div>
			      						<img src="http://localhost/acid/assets/frontend/img/home1.jpg" style="width:100%;height:600px;">
			    					</div>

			    			<div class="mySlides" style="display: none;">
			      					<div class="numbertext">2 / 8</div>
			       					<img src="http://localhost/acid/assets/frontend/img/home2.jpg" style="width:100%;height:600px;">
			    			</div>

			    			<div class="mySlides" style="display: none;">
			      				<div class="numbertext">3 / 8</div>
			       				<img src="http://localhost/acid/assets/images/home1.jpg" style="width:100%;height:600px;">
			    			</div>
			  					<a class="prev" onclick="plusSlides(-1)">?</a>
			   					 <a class="next" onclick="plusSlides(1)">?</a>

			    				<div class="caption-container">
			      					<p id="caption"></p>
			    				</div>

						<div class="column">
			      			<img class="demo cursor active" src="http://localhost/acid/assets/frontend/img/home1.jpg" style="width:100%;height:200px;" onclick="currentSlide(1)" alt="">
			    		</div>
			    		<div class="column">
			      			<img class="demo cursor" src="http://localhost/acid/assets/frontend/img/home2.jpg" style="width:100%;height:200px;" onclick="currentSlide(2)" alt="">
			    		</div>
			    		<div class="column">
			      		<img class="demo cursor" src="http://localhost/acid/assets/images//home1.jpg" style="width:100%;height:200px;" onclick="currentSlide(3)" alt="">
			    		</div>
			       </div>
			</div>
	</div> <!--end sideber-->
	</div><!--col-md-3 col-sm-3-->
		<div class="col-md-6 col-sm-6">
							<div class="widget-boxed btnbox">
								
					<div class="cover-buttons">
						<a href="<?php echo base_url();?>project-dashboard"><button type="button" class="btn1"><i>Projects</i></button></a>
						<a href="<?php echo base_url();?>blog-dashboard"><button type="button" class="btn1"><i>Blogs</i></button></a>
						<a href="<?php echo base_url();?>job-dashboard"><button type="button" class="btn1"><i>Jobs</i></button></a>
						<a href="<?php echo base_url();?>employerprofile"><button type="button" class="btn1"><i>Profile</i></button></a>
					</div>
							</div>
						 <div class="post-topbar">
								<!-- <p class="headerline">Execute all visual design stages from concept to </p> -->
									
											 
								<div class="detail-wrapper">
								 <div class="detail-wrapper-header">
									<h4><i>Project</i></h4>
								</div>
								 <?php
         					 foreach ($project as $value) 
         					 {
                       
          						?>
								 <div class="job-verticle-list ">
			              <div class="vertical-job-card">
			                <div class="vertical-job-header">
			                  <div class="vrt-job-cmp-logo">
			                    <a href="project-details/<?=$value->project_id;?>"><img src="<?php echo base_url();?>assets/frontend/img/c-2.png" class="img-responsive" alt="" /></a>
			                  </div>
			                  <h4><a href="project-details/<?=$value->project_id;?>"><?=$value->project_title;?></a></h4><h5><?=$value->project_name;?></h5>
			                 
			                  
			                </div>
			                <div class="vertical-job-body">
			                  <div class="row">
			                    <div class="col-md-8 col-sm-6">
			                      <ul class="can-skils">
			                     
			                        <li><strong>Location: </strong><?=$value->project_location;?></li>
			                          <li><strong>Sub-Region: </strong><?=$value->sub_region;?></li>
			                       
			                        <li><strong>Date: </strong><?=$value->created_date;?></li>
			                      </ul>
			                    </div>
			                    <div class="col-md-4 col-sm-6">
			                    	<?php $image =base_url().$value->user_file;?>
			              <img src="<?= $image;?>" style="height: 100px;width: 100px;">
			                    </div>
			              </div>
			              
			                </div>
			                
			              </div>
			            </div>
			        </div>
			             <?php } ?>
	            
							
							<div class="post-topbar">
									<div class="detail-wrapper">
								 <div class="detail-wrapper-header">
									<h4><i>Blog</i></h4>
								</div>
								<?php
         					 foreach ($blog as $value) 
         					 {
                       
          						?>
								
								<div class="job-verticle-list ">
			              <div class="vertical-job-card">
			                <div class="vertical-job-header">
			                  <div class="vrt-job-cmp-logo">
			                    <a href="blog-details/<?=$value->blog_id;?>"><img src="<?php echo base_url();?>assets/frontend/img/c-2.png" class="img-responsive" alt="" /></a>
			                  </div>
			                  <h4><a href="blog-details/<?=$value->blog_id;?>"><?=$value->blog_author_name;?></a></h4>
			                  
			                </div>
			                <div class="vertical-job-body">
			                  <div class="row">
			                    <div class="col-md-8 col-sm-6">
			                      <ul class="can-skils">
			                     
			                        <li><strong>Blog Title: </strong><?=$value->blog_title;?></li>
			                          <!-- <li><strong>Sub-Region: </strong><?=$blog->sub_region;?></li> -->
			                       
			                        <li><strong>Date: </strong><?=$value->created_date;?></li>
			                      </ul>
			                    </div>
			                    <div class="col-md-4 col-sm-6">
			                    	<?php $image =base_url()."uploads/blogs/".$value->blog_mult_img;?>
			              <img src="<?= $image;?>" style="height: 100px;width: 100px;">
			                    </div>
			              </div>
			              
			                </div>
			                
			              </div>
			            </div>
						
							</div>
					

						

						<?php } ?>
	            
							
							<div class="post-topbar">
								<div class="detail-wrapper">
								 <div class="detail-wrapper-header">
									<h4><i>Job</i></h4>
								</div>
								 <?php
         					 foreach ($job as $value) 
         					 {
                       
          						?>
						<div class="job-verticle-list ">
			              <div class="vertical-job-card">
			                <div class="vertical-job-header">
			                  <div class="vrt-job-cmp-logo">
			                    <a href="job-details/<?=$value->job_id;?>"><img src="<?php echo base_url();?>assets/frontend/img/c-2.png" class="img-responsive" alt="" /></a>
			                  </div>
			                  <h4><a href="job-details/<?=$value->job_id;?>"><?=$value->company_name;?></a></h4><h5><?=$value->job_title;?></h5>
			                 
			                  
			                </div>
			                <div class="vertical-job-body">
			                  <div class="row">
			                    <div class="col-md-8 col-sm-6">
			                      <ul class="can-skils">
			                     
			                        <li><strong>position: </strong><?=$value->job_position;?></li>
			                          <li><strong>Type: </strong><?=$value->job_type;?></li>
			                       
			                        <li><strong>Gender: </strong><?=$value->gender;?></li>
			                          <li><strong>Qualifiaction: </strong><?=$value->min_qualification;?></li>
			                            <li><strong>Skill: </strong><?=$value->skills;?></li>
			                      </ul>
			                    </div>

			              </div>
			              
			                </div>
			                
			              </div>
			            </div>
									
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
	   </div><!-- col-md-6 col-sm-6 -->
						
	<div class="col-md-3 col-sm-3">
					<div class="sidebar">
								<div class="widget-boxed">
										<div class="widget-boxed-header">
											<h4><i class="ti-user"></i><i>Profession</i></h4>
										</div>
									<div class="widget-boxed-body">
										<div class="side-list no-border">
											<ul class="sidebarimg">

												<li><i class=""></i><img src="<?php echo base_url();?>uploads/<?=$ans->user_file;?>" class="img-circle" alt="" /><?=$ans->firstname;?></li>

												<li><i class=""></i><img src="<?php echo base_url();?>assets/frontend/img/slider-2.jpg" onclick="" class="img-circle" alt="">aseee</li>
												
												
											</ul>
									   </div>
									</div>
								</div>
								
								 <div class="widget-boxed">
											<div class="widget-boxed-header">
												<h4><i class="ti-user"></i><i>Architecture</i></h4>
											</div>
									<div class="widget-boxed-body">
										<div class="side-list no-border">
											<ul class="sidebarimg">

												<li class=""><i class=""></i><img src="<?php echo base_url();?>assets/frontend/img/slider-2.jpg"  onclick="" class="img-circle" alt="">Sonal</li>
												<li><i class=""></i><img src="<?php echo base_url();?>assets/frontend/img/slider-2.jpg"  onclick="" class="img-circle" alt="">aseee</li>
											
												
											</ul>
										</div>
									</div>
								</div>
								 <div class="widget-boxed">
										<div class="widget-boxed-header">
											<h4><i class="ti-user"></i><i>Civil</i></h4>
										</div>
									<div class="widget-boxed-body">
										<div class="side-list no-border">
											<ul class="sidebarimg">

												<li><i class=""></i><img src="<?php echo base_url();?>assets/frontend/img/slider-2.jpg"  onclick="" class="img-circle" alt="">Sonal</li>
												<li><i class=""></i><img src="<?php echo base_url();?>assets/frontend/img/slider-2.jpg" onclick="" class="img-circle" alt="">aseee</li>
												
												
											</ul>
									   </div>
									</div>
								</div> 
								 <div class="widget-boxed">
										<div class="widget-boxed-header">
											<h4><i class="ti-user"></i><i>Developer</i></h4>
										</div>
									<div class="widget-boxed-body">
										<div class="side-list no-border">
											<ul class="sidebarimg">

												<li><i class=""></i><img src="<?php echo base_url();?>assets/frontend/img/slider-2.jpg"  onclick="" class="img-circle" alt="">Sonal</li>
												<li><i class=""></i><img src="<?php echo base_url();?>assets/frontend/img/slider-2.jpg" onclick="" class="img-circle" alt="">aseee</li>
												
												
											</ul>
									   </div>
									</div>
								</div> 
								
						<div id="myModal" class="modal">
	  									<span class="close cursor" onclick="closeModal()">&times;</span>
	  										<div class="modal-content">

	   										 <div class="mySlides">
	      										<div class="numbertext">1 / 8</div>
	     											 <img src="<?php echo base_url();?>assets/frontend/img/home1.jpg" style="width:100%;height:600px;">
	    									</div>

	    									<div class="mySlides">
	     										 <div class="numbertext">2 / 8</div>
	       											<img src="<?php echo base_url();?>assets/frontend/img/home2.jpg" style="width:100%;height:600px;">
	    									</div>

	    										<div class="mySlides">
	      												<div class="numbertext">3 / 8</div>
	      											 <img src="<?php echo base_url();?>assets/images/home1.jpg" style="width:100%;height:600px;">
	    										</div>
	  										<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
	    									<a class="next" onclick="plusSlides(1)">&#10095;</a>

	    								<div class="caption-container">
	      									<p id="caption"></p>
	    								</div>


	    						<div class="column">
	      							<img class="demo cursor" src="<?php echo base_url();?>assets/frontend/img/home1.jpg" style="width:100%;height:200px;" onclick="currentSlide(1)" alt="">
	    						</div>
	   						  <div class="column">
	     						 <img class="demo cursor" src="<?php echo base_url();?>assets/frontend/img/home2.jpg" style="width:100%;height:200px;" onclick="currentSlide(2)" alt="">
	    					 </div>
	    						<div class="column">
	     						 <img class="demo cursor" src="<?php echo base_url();?>assets/images//home1.jpg" style="width:100%;height:200px;" onclick="currentSlide(3)" alt="">
	    					   </div>
	    					 </div>
	 				</div>
								 
				</div>
	 </div>
						
					</div>
					<!-- End Row -->
					
	</div><!-- End first container -->
</section>

		
		
		<!-- ====================== End Job Overview ================ -->
		
		<!-- ================= footer start ========================= -->


<script>
function myFunction(imgs) {
    var expandImg = document.getElementById("expandedImg");
    var imgText = document.getElementById("imgtext");
    console.log(expandImg);
    expandImg.src = imgs.src;
    imgText.innerHTML = imgs.alt;
    expandImg.parentElement.style.display = "block";
}

var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 2000); // Change image every 2 seconds
}
</script>