
		<!-- ======================= End Navigation ===================== -->
			
			
		<!-- ======================= Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Manage Jobs</h2>
					<p><a href="<?php echo base_url();?>employerprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Manage jobs</p>
					<?php if($feedback=$this->session->flashdata('feedback')): ?>
                  <div class="alert alert-dismissible alert-danger">
                  <?= $feedback; ?>
                <?php endif; ?>
                </div>
                <?php if($error=$this->session->flashdata('error')): ?>
                  <div class="alert alert-dismissible alert-danger">
                  <?= $error; ?>
                  <?php endif; ?>
					<!-- <ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp"> -->
					<ul class="nav navbar-nav navbar-right">
					
						<li class="sign-up"><a class="btn-signup red-btn" href="create-job" style="margin-right: 21px; color: white;background-color: #ff7c39 !important;"><span  style="padding: 7px;"></span>Create Job</a></li>
					</ul>
				<!--  -->
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ================ Office Address ======================= -->
		<section>
			<div class="container">
				<div class="row">
				
					<!-- Single Job -->
					<?php
					foreach($ans as $key=>$value) 
					{?>
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type full-type"><?=$value->job_type;?></span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox">
									<i class="fa fa-check"></i>
								</label>
							</div>
							
							<div class="job-featured"></div>
							
							<div class="u-content">
								<div class="avatar box-100">
									<a href="job-details/<?=$value->job_id;?>">
										<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-4.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html"><?=$value->job_title;?></a></h5>
							<h5><?=$value->type;?></h5>
								<!-- <p class="text-muted"><?=$value->skills;?></p> -->
								<p class="text-muted"><?=$value->min_qualification;?></p>
								<p class="text-muted"><?=$value->job_experiance;?></p>
								
								<p class="text-muted"><?=$value->gender;?></p>
								
								<span class="theme-cl"><?=$value->package;?></span>
								<p class="text-muted"><?php $date = $value->created_date;
											$newdate = date('d F Y', strtotime(str_replace('/', '-', $date)));
											echo $newdate;
								?></p>
							</div>
							
							<div class="job-type-grid">
								<a href="job-details/<?=$value->job_id;?>" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>

						
				<?php } ?>
					
					
					
				</div>
				
				
			</div>
		</section>
		<!-- ================ End Office Address ======================= -->
			
		<!-- ================= footer start ========================= -->
		<?php
if($error = $this->session->flashdata('Feedback')) 
{
    ?>
    <script>

        swal("Good Job!", "<?php echo $this->session->flashdata('Feedback'); ?>", "success")



    </script>
    <?php
}
elseif($error = $this->session->flashdata('error')) {
    ?>
    <script>
        swal("Oops!", "<?php echo $this->session->flashdata('error'); ?>", "error")
        

    </script>
    <?php
}
?>
<?php
if($error = $this->session->flashdata('Delete')) 
{
    ?>
    <script>

        swal("Oop's!", "<?php echo $this->session->flashdata('Delete'); ?>", "error")



    </script>
    <?php
}
elseif($error = $this->session->flashdata('error')) {
    ?>
    <script>
        swal("Oops!", "<?php echo $this->session->flashdata('error'); ?>", "error")
        

    </script>
    <?php
}
?>