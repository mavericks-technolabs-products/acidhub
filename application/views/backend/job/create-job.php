
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Create Job</h2>
					<p><a href="<?php echo base_url();?>employerprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Create Job</p>
				</div>
			</div>
		</div>

		<section class="create-job">
			<div class="container">
				<!-- General Information -->
					<div class="box">
						<div class="box-header">
							<h4>General Information</h4>
						</div>
						<div class="box-body">
						<form method="POST" action="<?php echo base_url();?>backend/JobController/job_insert">
							<div class="row">
								
							    <div class="col-sm-4">
									<label>Company Name</label>
									<input type="text" name="company_name" class="form-control" value="<?php echo set_value('company_name')?>"  onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)"  required />
									<?php echo form_error('company_name'); ?>
								</div>
								<div class="col-sm-4">
									<label>Job Title</label>
									<input type="text" name="job_title" class="form-control"  value="<?php echo set_value('job_title')?>" required />
									<?php echo form_error('job_title'); ?>
								</div>
								
								<div class="col-sm-4 m-clear">
									<label>Position</label>
									<input type="text" name="job_position" class="form-control" value="<?php echo set_value('job_position')?>" required/>
									<?php echo form_error('job_position'); ?>
								</div>
								<div class="col-sm-4 m-clear">
									<label>Experience</label>
									<input type="text" name="job_experiance" class="form-control" value="<?php echo set_value('job_experiance')?>" required/>
									<?php echo form_error('job_experiance'); ?>
								</div>
								<div class="col-sm-4 m-clear">
									<label>No. Of Vacancy</label>
									<input type="text" name="no_of_vacancy" class="form-control"  value="<?php echo set_value('no_of_vacancy')?>" required/>
									<?php echo form_error('no_of_vacancy'); ?>
								</div>
								
								<div class="col-sm-4">
									<label>Package</label>
									<input type="text" name="package" class="form-control" value="<?php echo set_value('package')?>" required/>
									<?php echo form_error('package'); ?>
								</div>
								
								<div class="col-sm-4 m-clear">
									<label>Job Type</label>
									<select class="wide form-control" name="job_type" required />
										<option value="select">select...</option>>
										<option value="Full Time">Full Time</option>
										<option value="Part Time">Part Time</option>
										<option value="Freelancer">Freelancer</option>
										<option value="internship">Internship</option>
									</select>
								</div>
								
								<div class="col-sm-4 m-clear">
									<label>Gender</label>
									<select class="wide form-control" name="gender" required />
										<option value="select">select...</option>>
										<option value="Female">Female</option>
										<option value="Male">Male</option>

									</select>
								</div>
								
								<div class="col-sm-4 m-clear">
									<label>Min Qualification</label>
									<input type="text" class="form-control" name="min_qualification" value="<?php echo set_value('min_qualification')?>"required />
									<?php echo form_error('min_qualification'); ?>
								</div>


                                <div class="col-sm-4">
									<label>Skills</label>
									<input type="text" class="form-control" name="skills" value="<?php echo set_value('skills')?>" required />
									<?php echo form_error('skills'); ?>
								</div>
								
							
							
					
					
					
					<!-- Qualification & Instruction -->
					   <div class="row">
							  <div class="col-sm-12 m-clear">
							<label>Qualification & Instruction</label>
							<textarea name="job_description" id="my-info"  class="form-control" placeholder="Some texts about me" rows="4" cols="400" value="<?php echo set_value('job_description')?>" required/></textarea>
							<?php echo form_error('job_description  '); ?>
						</div>
					</div>
						
					</div>
				
					<div class="text-center">
						<button type="submit" class="btn btn-m theme-btn">Submit & Exit</button>
					</div>
		    </form>
			</div>
			</div>
			</div>
	</section>

<script type="text/javascript">
    function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
                return false;
                return true;
        }
    function isCharacterKey(evt)
		      {
		        var charCode = (evt.which) ? evt.which : event.keyCode
		        if (charCode==32 || (charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
		       		return true;
		        	return false;
		        
		      }
</script>
		