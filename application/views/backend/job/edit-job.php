
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Update Job</h2>
					<p><a href="<?php echo base_url();?>employerprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Update Job</p>
				</div>
			</div>
		</div>

		<section class="create-job">
			<div class="container">
				<!-- General Information -->
					<div class="box">
						<div class="box-header">
							<h4>General Information</h4>
						</div>
						<div class="box-body">
						<form method="POST" action="<?php echo base_url();?>backend/JobController/update_job/<?=$ans[0]->job_id?>">
							<div class="row">
								 <div class="col-sm-4">
									<label>Company Name</label>
									<input type="text" name="company_name" class="form-control"  onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" required value="<?php echo $ans[0]->company_name;?>">
								</div>
								<div class="col-sm-4">
									<label>Job Title</label>
									<input type="text" name="job_title" class="form-control" required value="<?php echo $ans[0]->job_title;?>">
								</div>
								<div class="col-sm-4 m-clear">
									<label>Position</label>
									<input type="text" name="job_position" class="form-control"   required value="<?php echo $ans[0]->job_position;?>">
								</div>
								<div class="col-sm-4 m-clear">
									<label>Experience</label>
									<input type="text" name="job_experiance" class="form-control" required value="<?php echo $ans[0]->job_experiance;?>">
									
								</div>
								<div class="col-sm-4 m-clear">
									<label>No. Of Vacancy</label>
									<input type="text" name="no_of_vacancy" class="form-control" required value="<?php echo $ans[0]->no_of_vacancy;?>">
								</div>
								<div class="col-sm-4">
									<label>Package</label>
									<input type="text" name="package" class="form-control" required value="<?php echo $ans[0]->package;?>">
								</div>
								<div class="col-sm-4 m-clear">
									<label>Job Type</label>
                            			<select class="wide form-control" name="job_type"   value="<?php echo $ans[0]->job_type;?>">
                                 		<option value="">Select...</option>
                               			<option value="Full Time"<?=$ans[0]->job_type=='Full Time'? ' selected="selected"' : '';?>>Full Time</option>
                                		<option value="Part Time"<?=$ans[0]->job_type=='Part Time' ? ' selected="selected"' : '';?>>Part Time</option>
                              			<option value="Freelancer"<?=$ans[0]->job_type=='Freelancer' ? ' selected="selected"' : '';?>>Freelancer</option>
                               			<option value="Internship"<?=$ans[0]->job_type=='Internship' ? ' selected="selected"' : '';?>>Internship</option>
                               			</select>
                                </div>
                                <div class="col-sm-4 m-clear">
									 <label>Gender</label>
                            		<select class="wide form-control" name="gender" value="<?php echo $ans[0]->gender;?>">
                                 	<option value="">Select...</option>
                               		<option value="Female"<?=$ans[0]->gender=='Female'? ' selected="selected"' : '';?>>Female</option>
                                	<option value="Male"<?=$ans[0]->gender=='Male' ? ' selected="selected"' : '';?>>Male</option>
                            		</select>
                        		</div>
                        		<div class="col-sm-4 m-clear">
									<label>Min Qualification</label>
									<input type="text" class="form-control" name="min_qualification" required value="<?php echo $ans[0]->min_qualification;?>">
								</div>
								<div class="col-sm-4">
									<label>Skills</label>
									<input type="text" class="form-control" name="skills" required value="<?php echo $ans[0]->skills;?>">
								</div>

							</div>
						</div>
					</div>	
					            <div class="row">
							        <div class="col-sm-12 m-clear">
										<label>Qualification & Instruction</label>
											<textarea name="job_description"  class="form-control" placeholder="Some texts about me" rows="4" cols="400" required value="<?php echo $ans[0]->job_description;?>"><?php echo $ans[0]->job_description;?></textarea>
									</div>
								</div>

								<div class="text-center">
									<button type="submit" class="btn btn-m theme-btn">Update & Exit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
								
<script>        
 function contactno()
 {          
    $('#phone').keypress(function(e) 
    {
        var a = [];
        var k = e.which;

        for (i = 48; i < 58; i++)
            a.push(i);

        if (!(a.indexOf(k)>=0))
            e.preventDefault();
    });
}
      
function isNumberKey(evt)
             {
                 var charCode = (evt.which) ? evt.which : event.keyCode
                 if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
                    return false;
                    return true;
             }
        function isCharacterKey(evt)
		      {
		        var charCode = (evt.which) ? evt.which : event.keyCode
		        if (charCode==32 || (charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
		       		return true;
		        	return false;
		        
		      }
</script>						
								
								
								
								
								
								
								
								
								
								
								
								
                                 
                               
                                           
									
								
								
								


                        
                        
								
								
							
							
					
					
					   
					
		    

		
       