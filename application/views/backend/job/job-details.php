<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<style type="text/css">
    .swal-button
    {
  padding: 7px 19px;
  border-radius: 2px;
  background-color: #4962B3;
  font-size: 12px;
  border: 1px solid #3e549a;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}
.swal-footer {
  background-color: rgb(245, 248, 250);
  margin-top: 32px;
  border-top: 1px solid #E9EEF1;
  overflow: hidden;
}
.swal-overlay {
  background-color: #8abe51;
}
</style>
<?php if ($this->session->flashdata('Apply Job Succeed')): ?>
                            <script>
                                swal({
                                    title: "Welcome",
                                    text: "<?php echo $this->session->flashdata('Welcome'); ?>",
                                    icon: "success",
                                    timer: 1500,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ================ Job Detail Basic Information ======================= -->
		<section class="detail-section" style="background:url(<?php echo base_url();?>assets/frontend/img/slider-2.jpg);">
			<div class="overlay"></div>
			<div class="profile-cover-content">
				<div class="container">
					<div class="cover-buttons">
						<ul>
						<li><div class="buttons medium button-plain "><i class="fa fa-phone"></i>+<?=$ans[0]->firm_contact;?></div></li>
						<li><div class="buttons medium button-plain "><i class="fa fa-map-marker"></i>#<?=$ans[0]->firm_address;?></div></li>
						<!-- <li><a href="#add-review" class="buttons theme-btn"><i class="fa fa-paper-plane"></i><span class="hidden-xs">Response</span></a></li> -->
						</ul>
					</div>
					<div class="job-owner hidden-xs hidden-sm">
						<div class="job-owner-avater">
							<img src="<?php echo base_url();?>assets/frontend/img/c-2.png" class="img-responsive img-circle" alt="" />
						</div>
						<div class="job-owner-detail">
							<h4><?=$ans[0]->company_name;?></h4>
							<!-- <span class="theme-cl">Google PVT</span>
 -->
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- ================ End Job Detail Basic Information ======================= -->
		
		<!-- ================ Start Job Overview ======================= -->
		<section>
			<div class="container">
				
				<!-- row -->
				<div class="row">
					<?php
					/*print_r($ans);
					exit();*/
					if($ans) 
					{?>
					<div class="col-md-8 col-sm-8">
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-body">
								<div class="job-title-bar">
									<!-- <h3><?=$ans[0]->company_name;?></h3> -->
									<h3><?=$ans[0]->job_title;?> <span class="mrg-l-5 job-tag bg-success-light "><?=$ans[0]->job_type;?>
									</span></h3>
									<span class="pull-right vacancy">
									 <ul class="nav navbar-nav navbar-right">
  								   	 <div class="dropdown">
				                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
				                    <span class="glyphicon glyphicon-option-horizontal"></span></button>
				                    <ul class="dropdown-menu">
     								 <li><a href="<?php echo base_url();?>edit-job/<?php echo $ans[0]->job_id?>">Edit</a></li>
     								  <?php 
                                         $job_id = $ans[0]->job_id; 
                                        ?> 
                                       <li><a id="<?php echo $ans[0]->job_id?>" onclick="delete_job(this.id)" href="javascript:void(0);">Delete</a></li>
     								<!--  <li><a href="#">JavaScript</a></li> -->
    								</ul>
  									</div>
								</ul>
							</span>
									<div>
										<p class="mrg-bot-0">
											<!-- <i class="ti-location-pin mrg-r-5"></i> -->
											<?=$ans[0]->firm_address;?>
										</p>
										
										<p><strong>Qualification</strong>:<?=$ans[0]->min_qualification;?></p>
										<p><strong>Roles</strong> :<?=$ans[0]->job_position;?></p>
										<p><strong>Package</strong> :<?=$ans[0]->package;?></p>
										<p><strong>Skill</strong> :<?=$ans[0]->skills;?></p>
										<p><strong>Gender</strong> :<?=$ans[0]->gender;?></p>
										
									</div>
								</div>
							</div>
						</div>

						
						<div class="detail-wrapper">
							<div class="detail-wrapper-header">
								<h4>Overview</h4>
							</div>
							<div class="detail-wrapper-body">
								<p><?=$ans[0]->job_description;?></p>
								<!-- <p><?=$value->job_description;?></p> -->
							</div>
						</div>
						<?php } ?>

						<!-- <div class="detail-wrapper">
							<div class="detail-wrapper-header">
								<h4>Overview</h4>
							</div>
							<div class="detail-wrapper-body">
								<table>
									<thead>
										<tr>
											<th>Sr no</th>
											<th>Student Name</th>
										</tr>
									</thead>
									<tbody>
										<?php  $job = $this->JobModel->jobapplydetails($job_id);
												$count = count($job);
												$a=1;
												for($i=0;$i<$count;$i++)
												{
													$job1 = $this->JobModel->job_applydetails($job[$i]->user_id);
													
											 ?>
											 <tr>
												<td><?=$a;?></td>
												<td><a href="<?php echo base_url();?>studentdatainformation/<?=$job1->user_id;?>"><?=$job1->firstname;?></a></td>
											</tr>
										<?php  $a++;} ?>
									</tbody>
								</table>
							</div>
						</div> -->
						
						
					</div>
					
					<div class="col-md-4 col-sm-4">
						<div class="sidebar">
						
							<!-- Start: Opening hour -->
							<!-- <div class="widget-boxed">
								<div class="widget-boxed-body">
									<a href="#" class="btn btn-m theme-btn full-width mrg-bot-10"><i class="fa fa-paper-plane"></i>Response</a>
									<a href="#" class="btn btn-m light-gray-btn full-width" onclick="myFunction()"><i class="fa fa-linkedin"></i>Apply for Job</a>
								</div>
							</div> -->
							<!-- End: Opening hour -->
							
							<!-- Start: Job Overview -->
							<div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-location-pin padd-r-10"></i>Location</h4>
								</div>
								<div class="widget-boxed-body">
									<div class="side-list no-border">
										<ul>
											<li><i class="ti-credit-card padd-r-10"></i><?=$ans[0]->package;?></li>
											
											<li><i class="ti-shield padd-r-10"></i><?=$ans[0]->job_experiance;?></li>
											<li><i class="ti-shield padd-r-10"></i><?=$ans[0]->email;?></li>
											<li><i class="ti-shield padd-r-10"></i><?=$ans[0]->website;?></li>
										</ul>
										
									</div>
								</div>
							</div>
							
							 
						</div>
					</div>
					
				</div>
				<!-- End Row -->
				
				<!-- row -->
				
				<!-- End Row -->
				
			</div>
		</section>
	
<script type="text/javascript">
var url="<?php echo base_url();?>";
   function delete_job(id)
      	{
      		/*alert('ABC');*/
	     swal(
	      {
	            title: "Are you sure?",
	            text: "Are you sure you want to delete?",
	            icon: "warning",
	            buttons: true,
	            dangerMode: true,
	      })
	     .then((willDelete) => 
	      {
	            if(willDelete)
	            {
	                window.location = url+"delete_job/"+id;
	                swal("Your file is deleted!",
	                {
	                    icon: "success",
	                });
	            }
	            else
		        {
		            swal("Your file is safe!");
		        }
	      });
	   }

// When the user clicks on div, open the popup
function myFunction() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
}

</script>    
		
		<!-- ====================== End Job Overview ================ -->
		
		<!-- ================= footer start ========================= -->
		