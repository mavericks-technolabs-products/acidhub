
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ======================= Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Manage Resume</h2>
					<p><a href="<?php echo base_url();?>employertimeline" title="Home">Home</a> <i class="ti-arrow-right"></i> Manage Resume</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ======================= Manage Resume ======================== -->
		<section class="create-company">
			<div class="container">
				
				<div class="table-responsive"> 
					<table class="table table-lg table-hover">
						<thead>
							<tr>
								<th>Title</th>
								<th>Email</th>
								<th>Location</th>
								<th>Posted</th>
								<th>Action</th>
							</tr>
						</thead>
						
						<tbody>
						   <?php
                				foreach ($ans as $key=>$value) 
					          {
					          ?>
							<tr>
								
								    <td>
									    <a href="">
									    <?= $value->Title?>
									    
									    </td>
									    <td>
									    <?= $value->Email ?>

									    </td>
									  <td>
									    <?= $value->Location ?>
									    </td>    
									    
									  <td>
									    <?= $value->Posted ?>
									    </td>
									</tr>
							
							        <td>
    
							    <a href="backend/jobController/edit_job/<?php echo $value->job_id?>"," class="cl-success mrg-5" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a>
							        <a href="backend/jobController/delete_job/<?php echo $value->job_id?>"," class="cl-danger mrg-5" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
							    </td>
								 <?php 
									}
									?>
             					 </tr>
            				</tbody>
          				</table>
							
						</tbody>
					</table>
					
					<!-- flexbox -->
					
					<!-- /.flexbox -->
			
				</div>
				
			</div>
		</section>
		
		<!-- ====================== End Manage Resume ================ -->
		
		
		<!-- ================= footer start ========================= -->
		