
		
		<!-- ======================= Start Page Title ===================== -->
		
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Create Project</h2>
					<p><a href="<?php echo base_url();?>employerprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Create Project</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ======================= Create Job ===================== -->

		<section class="create-job">

			<div class="container">
						
                
					<!-- General Information -->

					<div class="box">
						<div class="box-header">
							<h4>General Information</h4>
						</div>
						<div class="box-body">
								<?php  echo form_open(base_url().'insert-project',array('method'=>'POST','class'=>"form-horizontal",'enctype'=>"multipart/form-data"));?>
							
									<div class="row">
									    <div class="col-sm-4">
											<label>Project Title</label>
											<input type="text" name="project_title" placeholder="" class="form-control" value="<?php echo set_value('project_title')?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" required />
											<?php echo form_error('project_title'); ?>
										</div>
										<div class="col-sm-4">
											<label>Project Location</label>
											<input type="text" name="project_location" class="form-control" placeholder="" value="<?php echo set_value('project_location')?>" onkeypress="return isCharacterKey(event)" onkeypress="return isNumberKey(event)" required />
											<?php echo form_error('project_location'); ?>
										</div>
										<div class="col-sm-4">
											<label>Sub-Region</label>
											<input type="text" name="sub_region" class="form-control" placeholder="" value="<?php echo set_value('sub_region')?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" required />
											<?php echo form_error('sub_region'); ?>
										</div>
									</div>
								    <div class="row">
									    <div class="col-sm-6">
										      <label>Add some pics?:</label>
										      <input type="file" id="photos" name="user_file[]" accept="image/jpeg,image/gif,image/png,image/x-eps/Max file size 2Mb"  multiple/>
										      <?php if($error=$this->session->flashdata('error')): ?>
		                  						<div class="alert alert-danger">
		                  						<?= $error;?></div>
		                  							<?php endif; ?>
										      <div class="row">
			                                     <div class="col-md-12">	
											       <label>Image Gallery</label>
											       <div id="photos_clearing" class="clearing-thumb" height='200px' width='150px'>	
											       </div>
											     </div>
											  </div>
										</div>
									</div>	
							</div>
							</div>
							
							  <div class="row">
								  <div class="col-sm-12 m-clear">
									<label>Project Description</label>
									<textarea name="project_description" id="my-info" name="project_description" class="form-control" placeholder="" rows="4" cols="400" value="<?php echo set_value('project_description')?>" required/><?php echo set_value('project_description')?></textarea>
									<?php echo form_error('project_description', '<div class="errors">', '</div>');?>
								  </div>
							  </div>			
							</div>
					
					
					

					<div class="text-center">
						<button type="submit" class="btn btn-m theme-btn">Submit & Exit</button>
					</div>
					
				</form>
			</div>
		</div>
		</section>

		<!-- ====================== End Create Job ================ -->
		
		
		<!-- ================= footer start ========================= -->
	
 
<script  src="<?php echo base_url();?>assets/frontend/js/index.js"></script>
<script type="text/javascript">
        function isNumberKey(evt)
             {
                 var charCode = (evt.which) ? evt.which : event.keyCode
                 if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
                    return false;
                    return true;
             }
             function isCharacterKey(evt)
		      {
		        var charCode = (evt.which) ? evt.which : event.keyCode
		        if (charCode==32 || (charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
		       		return true;
		        	return false;
		        
		      }
          <?php
if($error = $this->session->flashdata('Feedback')) 
{
	
    ?>
    <script>

        swal("Oop's!", "<?php echo $this->session->flashdata('Feedback'); ?>", "success")



    </script>
    <?php
}
elseif($error = $this->session->flashdata('errors')) {
    ?>
    <script>
        swal("Oops!", "<?php echo $this->session->flashdata('errors'); ?>", "error")
        

    </script>
    <?php
}
?>
</script>
<script>
var control = document.getElementById("user_file");
control.addEventListener("change", function(event) {
    // When the control has changed, there are new files
    var files = control.files;
    for (var i = 0; i < files.length; i++) {
        console.log("Filename: " + files[i].name);
        console.log("Type: " + files[i].type);
        console.log("Size: " + files[i].size + " bytes");
		var blob = files[i]; // See step 1 above
		console.log(blob.type);
		var fileReader = new FileReader();
		fileReader.onloadend = function(event) 
		{
		var arr = (new Uint8Array(event.target.result)).subarray(0, 4);
		var header = "";
		for(var i = 0; i < arr.length; i++) 
		{
		     header += arr[i].toString(16);
		}
		switch (header) 
		{
			    case "89504e47":
						        type = "photos/png";
						        break;
			    case "47494638":
						        type = "photos/gif";
						        break;
			    case "ffd8ffe0":
			    case "ffd8ffe1":
			    case "ffd8ffe2":
			    case "ffd8ffe3":
			    case "ffd8ffe8":
		        type = "photos/jpeg";
		        break;
				default:
				type = "unknown"; 
			    swal(
				    {
                        title: "Failed!",
                        text: "Please Select Correct File Type!",
                        icon: "error",
                        buttons: true,
                    })
                .then((value) => 
                {
                    if (value)
                    {
                    	$("#photos").replaceWith('<input type="file" name="photos" id="photos" class="file-input" data-show-caption="false" data-show-upload="false">');
				    }
                });
	        			break;
		}
			    console.log(header);
		};
				fileReader.readAsArrayBuffer(blob);
		}
				}, false);
</script>