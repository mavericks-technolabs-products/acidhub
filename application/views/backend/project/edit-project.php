
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ======================= Start Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Update Project</h2>
					<p><a href="<?php echo base_url();?>employerprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Update Project</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ======================= Create Job ===================== -->

		<section class="create-job">

			<div class="container">
						
                
					<!-- General Information -->

					<div class="box">
						<div class="box-header">
							<h4>General Information</h4>
						</div>
						<div class="box-body">
							<!--  -->

						<form method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>backend/ProjectController/update_project/<?=$ans->project_id?>">
						    <?php $cnt=count($ans);?>

							<div class="row">
							 	<div class="col-sm-4">
									<label>Project Title</label>
									<input type="text" name="project_title" class="form-control"  
									placeholder="" onkeypress="return isCharacterKey(event)" onkeypress="return isNumberKey(event)" required value="<?php echo $ans->project_title;?>">
								</div>
								
								<div class="col-sm-4">
									<label>Project Location</label>
									<input type="project_location" name="project_location" class="form-control" placeholder="" pattern="[A-Za-z]{1,10000}" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" required value="<?php echo $ans->project_location;?>">
								</div>
								<div class="col-sm-4">
									<label>Sub-Region</label>
									<input type="text" name="sub_region" class="form-control" placeholder="" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" required value="<?php echo $ans->sub_region;?>">
								</div>
							</div>
								
							<div class="row">
							    <div class="col-sm-6">
							      	<label>Add some pics?:</label>
							      	<input type="file" id="photos" name="user_file[]"  accept="image/jpeg,image/gif,image/png,image/x-eps/Max file size 2Mb" multiple/>
							        <?php if($feed=$this->session->flashdata('feed')): ?>
		                  				<div class="alert alert-danger">
		                  					<?= $feed;?></div>
		                  					<?php endif; ?>
									<div class="row">
		                                <div class="col-md-12">	
									      	<label>Image Gallery</label>
									      		<div id="photos_clearing" class="clearing-thumb" height='200px' width='150px'>
									      		</div>
									    </div>
									</div>
								</div>
							</div>

                            <div class="row">
                                <div class="col-md-12">
									<?php foreach($user_file as $img) {?>
										<div class="col-md-3">
										<a href="<?php echo base_url();?>delete-image/<?=$ans->project_id?>/<?=$img->project_img_id?>" class="btn btn-danger btn-xs">
										 	<i class="fa fa-trash-o"></i></a>
										<?php
											echo "<img src='" . base_url().$img->user_file."'height='200px' width='150px'>";
										?>
										</div>
									<?php } ?>
								</div>
							</div>
							<br>
						</div>
					</div>
					<br>
							<div class="row">
							  	<div class="col-sm-12 m-clear">
								<label>Project Description</label>
								<textarea name="project_description" id="my-info" name="job_description" class="form-control" placeholder="" rows="4" cols="400" required value="<?php echo $ans->project_description;?>"><?php echo $ans->project_description;?></textarea>
							   </div>
							</div>
					
					
						<div class="text-center">
							<button type="submit" class="btn btn-m theme-btn">Update & Exit</button>
						</div>
					 
				</form>
			</div>
		</div>
	</section>

<script type="text/javascript">
        function readURL(input) 
     {
        if (input.files && input.files[0]) 
        {
        var reader = new FileReader();

        reader.onload = function (e) 
		{
        $('#blah')
        .attr('src', e.target.result)
        .width(150)
        .height(200);
    	 };
		reader.readAsDataURL(input.files[0]);
		}
	}
  
function isNumberKey(evt)
    {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
            return false;
            return true;
    }
function isCharacterKey(evt)
		      {
		        var charCode = (evt.which) ? evt.which : event.keyCode
		        if (charCode==32 || (charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
		       		return true;
		        	return false;
		        
		      }
</script>
<script>

var control = document.getElementById("user_file");
control.addEventListener("change", function(event) {
    // When the control has changed, there are new files
    var files = control.files;
    for (var i = 0; i < files.length; i++) {
        console.log("Filename: " + files[i].name);
        console.log("Type: " + files[i].type);
        console.log("Size: " + files[i].size + " bytes");

        var blob = files[i]; // See step 1 above
		console.log(blob.type);

		var fileReader = new FileReader();
		fileReader.onloadend = function(event) {
		  var arr = (new Uint8Array(event.target.result)).subarray(0, 4);
		  var header = "";
		  for(var i = 0; i < arr.length; i++) {
		     header += arr[i].toString(16);
		  }

		  switch (header) {
						    case "89504e47":
									        type = "photos/png";
									        break;
						    case "47494638":
									        type = "photos/gif";
									        break;
						    case "ffd8ffe0":
						    case "ffd8ffe1":
						    case "ffd8ffe2":
						    case "ffd8ffe3":
						    case "ffd8ffe8":
									        type = "photos/jpeg";
									        break;
						    		default:
									        type = "unknown"; // Or you can use the blob.type as fallback
									        swal(
				                                {
				                                    title: "Failed!",
				                                    text: "Please Select Correct File Type!",
				                                    icon: "error",
				                                    buttons: true,
				                                })
				                                .then((value) => 
				                                {
				                                                            
				                                    if (value)
				                                    {
				                                    	$("#photos").replaceWith('<input type="file" name="photos" id="photos" class="file-input" data-show-caption="false" data-show-upload="false">');
												         // $(".file-preview").hide();
												         // $(".fileinput-remove-button").hide();
				                                    }
				                                });
									        break;
						}



		  console.log(header);

		  // Check the file signature against known types

		};
		fileReader.readAsArrayBuffer(blob);

    }
}, false);

</script>
		
		<!-- ====================== End Create Job ================ -->
		
		
		<!-- ================= footer start ========================= -->
		