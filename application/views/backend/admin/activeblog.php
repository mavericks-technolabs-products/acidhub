<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Striped rows -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Striped rows</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<table class="table datatable-basic table-striped">
						<thead>
							<tr>
								<th>Sr No</th>
								<th>blog_title</th>
								<th>blog_author_name</th>
								<th>type</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
                            <?php if (count( $active_blog )):
                                $i=1;
                                foreach ($active_blog as $active_blogs ) :
                                     ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $active_blogs->blog_title ?></td>
                                    <td><?= $active_blogs->blog_author_name ?></td>
                                    <td><?= $active_blogs->type ?></td>
                                    <td><?= $active_blogs->status ?></td>
                                    <td>
                                    	<?php 
                                    		$id=$active_blogs->user_id;
                                    		$type_activate="activate";
                                    		$type_deactivate="deactivate";
                                    	?>
                                    	<?php if($active_blogs->status == "deactivate") { ?>
                                    	<a id="<?php echo $active_blogs->blog_id?>" href="javascript:void(0);" onclick="change_status(this.id,'<?=$type_activate?>')">
                                        	<span class="label label-success">Activate</span>
                                        </a>
                                    	<?php } else {?>
                                        <a id="<?php echo $active_blogs->blog_id?>" href="javascript:void(0);" onclick="change_status_deactivate(this.id,'<?=$type_deactivate?>')">
                                        	<span class="label label-danger">Deactivate</span>
                                        </a>
                                    <?php } ?>
                                    </td>
                               </tr>
                                <?php 
                                $i++;
                                endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="3">
                                            No Records Found.
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                                    
                        </tbody>
					</table>
					<script type="text/javascript">
                     	function change_status(id,type)
                     	{
                         	var url="<?php echo base_url();?>";
                         	swal(
                         	{
                             	title: "Are you sure?",
                             	text: "You really want to activate this account?",
                             	icon: "warning",
                             	buttons: true,
                             	dangerMode: true,
                         	})
                         	.then((willDelete) => 
                         	{                                                                    
                             	if (willDelete)
                             	{
                                 	window.location = url+"admin_change_blog_status/"+id+'/'+type;
                                 	swal("Your file is deleted!",
                                 	{
                                     	timer: 1500,
                                     	icon: "success",
                                 	});
                             	}
                             	else
                             	{
                                 	swal("Your file is safe!");
                             	}
                         	});
                     	}

                     	function change_status_deactivate(id,type)
                     	{
                         	var url="<?php echo base_url();?>";
                         	swal(
                         	{
                             	title: "Are you sure?",
                             	text: "You really want to deactivate this account?",
                             	icon: "warning",
                             	buttons: true,
                             	dangerMode: true,
                         	})
                         	.then((willDelete) => 
                         	{                                                                    
                             	if (willDelete)
                             	{
                                 	window.location = url+"admin_change_blog_status/"+id+'/'+type;
                                 	swal("Your file is deleted!",
                                 	{
                                     	timer: 1500,
                                     	icon: "success",
                                 	});
                             	}
                             	else
                             	{
                                 	swal("Your file is safe!");
                             	}
                         	});
                     	}
                    </script>
				</div>
				<!-- /striped rows -->
			</div>
		</div>
	</div>
