<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Striped rows -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Striped rows</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<table class="table datatable-basic table-striped">
						<thead>
							<tr>
								<th>Sr No</th>
                                <th>Experience</th>
                                <th>Branch</th>
                                <th>Skill</th>
                                 <th>location</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Actions</th>
							</tr>
						</thead>
						<tbody>
                            <?php if (count( $declined_professional )):
                                $i=1;
                                foreach ($declined_professional as $declined_professionals ) :
                                     ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                     <td><?= $declined_professional->total_experience ?></td>
                                   <td><?= $declined_professional->branch ?></td>
                                    <td><?= $declined_professional->key_skill ?></td>
                                    <td><?= $declined_professional->location ?></td>
                                    <td><?= $declined_professional->type ?></td>
                                    <td><?= $declined_professional->status ?></td>
                                    <td>
                                    	<a href="<?php echo base_url();?>declinedprofessionalstatus/<?=$declined_professional->professional_id?>/<?=$type="active"?>">
                                            <span class="label label-success">Accept</span>
                                        </a>
                                        <a href="<?php echo base_url();?>declinedprofessionalstatus/<?=$declined_professional->professional_id?>/<?=$type="decline"?>">
                                            <span class="label label-danger">Decline</span>
                                        </a>
                                    </td>
                               </tr>
                                <?php 
                                $i++;
                                endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="3">
                                            No Records Found.
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                                    
                        </tbody>
					</table>
					<script type="text/javascript">
                     	function change_status(id,type)
                     	{
                         	var url="<?php echo base_url();?>";
                         	swal(
                         	{
                             	title: "Are you sure?",
                             	text: "You really want to activate this account?",
                             	icon: "warning",
                             	buttons: true,
                             	dangerMode: true,
                         	})
                         	.then((willDelete) => 
                         	{                                                                    
                             	if (willDelete)
                             	{
                                 	window.location = url+"admin_change_blog_status/"+id+'/'+type;
                                 	swal("Your file is deleted!",
                                 	{
                                     	timer: 1500,
                                     	icon: "success",
                                 	});
                             	}
                             	else
                             	{
                                 	swal("Your file is safe!");
                             	}
                         	});
                     	}

                     	function change_status_deactivate(id,type)
                     	{
                         	var url="<?php echo base_url();?>";
                         	swal(
                         	{
                             	title: "Are you sure?",
                             	text: "You really want to deactivate this account?",
                             	icon: "warning",
                             	buttons: true,
                             	dangerMode: true,
                         	})
                         	.then((willDelete) => 
                         	{                                                                    
                             	if (willDelete)
                             	{
                                 	window.location = url+"admin_change_blog_status/"+id+'/'+type;
                                 	swal("Your file is deleted!",
                                 	{
                                     	timer: 1500,
                                     	icon: "success",
                                 	});
                             	}
                             	else
                             	{
                                 	swal("Your file is safe!");
                             	}
                         	});
                     	}
                    </script>
				</div>
				<!-- /striped rows -->
			</div>
		</div>
	</div>
