<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/admin/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/admin/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/admin/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/admin/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/admin/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/loaders/blockui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/ui/nicescroll.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/ui/drilldown.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/pages/dashboard.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse bg-indigo">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.html"><img src="<?php echo base_url();?>assets/admin/images/logo_light.png" alt=""></a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<p class="navbar-text"><span class="label bg-success-400">Online</span></p>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown language-switch">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/flags/gb.png" class="position-left" alt="">
						English
					</a>
				</li>

				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/placeholder.jpg" alt="">
						<span>Victoria</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
						<li><a href="<?php echo base_url();?>adminsignup"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Second navbar -->
	<div class="navbar navbar-default" id="navbar-second">
		<ul class="nav navbar-nav no-border visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="navbar-second-toggle">
			<ul class="nav navbar-nav navbar-nav-material">
				<li class="active"><a href="<?php echo base_url('dashboard');?>"><i class="icon-display4 position-left"></i> Dashboard</a></li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-make-group position-left"></i> Admin <span class="caret"></span>
					</a>
					<ul class="dropdown-menu width-200"><!-- 
						<li><a href="<?php echo base_url('adminsignup');?>">Add Admin</a></li> -->
						<li><a href="<?php echo base_url('alladmin');?>">All Admins</a></li>
						<li><a href="<?php echo base_url('admin_blog');?>">Blog</a></li>
						<li><a href="<?php echo base_url('faq');?>">FAQ's</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i> User <span class="caret"></span>
					</a>
					<ul class="dropdown-menu width-200">
						<li><a href="<?php echo base_url('activeusers');?>">Active Users</a></li>
						<li><a href="<?php echo base_url('declinedusers');?>">Declined Users</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i> Blog <span class="caret"></span>
					</a>
					<ul class="dropdown-menu width-200">
						<li><a href="<?php echo base_url('activeblogs');?>">All Blog</a></li>
						<li><a href="<?php echo base_url('editedblogs');?>">Edited Blog</a></li>
						<li><a href="<?php echo base_url('declinedblogs');?>">Declined Blogs</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i> Job <span class="caret"></span>
					</a>
					<ul class="dropdown-menu width-200">
						<li><a href="<?php echo base_url('activejobs');?>">All Job</a></li>
						<li><a href="<?php echo base_url('editedjobs');?>">Edited Job</a></li>
						<li><a href="<?php echo base_url('declinedjobs');?>">Declined Job</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i> Project <span class="caret"></span>
					</a>
					<ul class="dropdown-menu width-200">
						<li><a href="<?php echo base_url('activeprojects');?>">All Project</a></li>
						<li><a href="<?php echo base_url('editedprojects');?>">Edited Project</a></li>
						<li><a href="<?php echo base_url('declinedprojects');?>">Declined Project</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i> Education <span class="caret"></span>
					</a>
					<ul class="dropdown-menu width-200">
						<li><a href="<?php echo base_url('activeeducation');?>">All Education</a></li>
						<li><a href="<?php echo base_url('editededucation');?>">Edited Education</a></li>
						<li><a href="<?php echo base_url('declinededucation');?>">Declined Education</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i> Professional <span class="caret"></span>
					</a>
					<ul class="dropdown-menu width-200">
						<li><a href="<?php echo base_url('activeprofessional');?>">All Professional</a></li>
						<li><a href="<?php echo base_url('editedprofessional');?>">Edited Professional</a></li>
						<li><a href="<?php echo base_url('declinedprofessional');?>">Declined Professional</a></li>
					</ul>
				</li>
				<!-- <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i> Resume <span class="caret"></span>
					</a>
					<ul class="dropdown-menu width-200">
						<li><a href="<?php echo base_url('activeprofessional');?>">All Professional</a></li>
						<li><a href="<?php echo base_url('editedprofessional');?>">Edited Professional</a></li>
						<li><a href="<?php echo base_url('declinedprofessional');?>">Declined Professional</a></li>
					</ul>
				</li> -->
			
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-make-group position-left"></i> Requests <span class="caret"></span>
					</a>
					<ul class="dropdown-menu width-200">
						<li><a href="<?php echo base_url('userstatus');?>">User Requests</a></li>
						<li><a href="<?php echo base_url('blogstatus');?>">Blog Requests</a></li>
						<li><a href="<?php echo base_url('jobstatus');?>">Job Requests</a></li>
						<li><a href="<?php echo base_url('projectstatus');?>">Project Requests</a></li>
						<li><a href="<?php echo base_url('educationstatus');?>">Education Requests</a></li>
						<li><a href="<?php echo base_url('professionalstatus');?>">Professional Requests</a></li>
					</ul>
				</li>
			</ul>

			<ul class="nav navbar-nav navbar-nav-material navbar-right">
				<!-- <li>
					<a href="changelog.html">
						<i class="icon-history position-left"></i>
						Changelog
						<span class="label label-inline position-right bg-success-400">1.6</span>
					</a>
				</li> -->

				<!-- <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-cog3"></i>
						<span class="visible-xs-inline-block position-right">Share</span>
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li> -->
			</ul>
		</div>
	</div>
	<!-- /second navbar -->