<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<style type="text/css">
    .swal-button {
  padding: 7px 19px;
  color: #ffff;
  border-radius: 2px;
  background-color: #3f51b5;
  font-size: 12px;
  border: 1px solid #3e549a;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}
.swal-footer {
  background-color: rgb(245, 248, 250);
  margin-top: 32px;
  border-top: 1px solid #E9EEF1;
  overflow: hidden;
}
.swal-overlay {
  background-color: #3f51b5;
}
</style>
<?php if ($this->session->flashdata('error')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('error'); ?>",
                                    icon: "success",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
<div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">
                <!-- Striped rows -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">FAQ's</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                                    <div class="col-md-6 col-sm-6 col-6" style="margin-left: 19px;padding-bottom: 10px;">
                                     <div class="btn-group">
                                         <a href="<?php echo base_url('add_faq');?>" id="addRow" class="btn btn-info">
                                             Add New <i class="fa fa-plus" style="color: white;"></i>
                                        </a>
                                     </div>
                                 </div>
                             </div>

                    <table class="table datatable-basic table-striped">
                        <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Question</th>
                                <th>Answer</th><!-- 
                                <th>Status</th> -->
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count( $faq )):
                                $i=1;
                                                    
                                foreach ($faq as $faqs ) :?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $faqs->question ?></td>
                                    <td><?= $faqs->answer ?></td>
                                    <td>
                                        <a href="<?php echo base_url();?>faq_edit/<?=$faqs->faq_id?>">
                                        <span class="label label-info">Edit</span>
                                        </a>
                                        <a id="<?php echo $faqs->faq_id?>" href="javascript:void(0);" onclick="delete_faq(this.id)" ><span class="label label-danger">Delete</span></a>
                                    </td>
                               </tr>
                                <?php 
                                $i++;
                                endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="3">
                                            No Records Found.
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                                    
                        </tbody>
                    </table>
                    <script type="text/javascript">
                                    function delete_faq(id)
                                    {
                                        var url="<?php echo base_url();?>";
                                        swal(
                                        {
                                            title: "Are you sure?",
                                            text: "You really want to delete this FAQ?",
                                            icon: "warning",
                                            buttons: true,
                                            dangerMode: true,
                                        })
                                        .then((willDelete) => 
                                        {
                                                                    
                                            if (willDelete)
                                            {
                                                window.location = url+"deletefaq/"+id;
                                                swal("Your file is deleted!",
                                                {
                                                    timer: 1500,
                                                    icon: "success",
                                                });
                                            }
                                            else
                                            {
                                                swal("Your file is safe!");
                                            }
                                        });
                                    }
                    </script>
                </div>
                <!-- /striped rows -->
            </div>
        </div>
    </div>

