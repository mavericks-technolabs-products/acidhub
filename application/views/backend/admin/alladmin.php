					<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Striped rows -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Striped rows</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<div class="row">
                        <div class="col-md-6 col-sm-6 col-6" style="margin-left: 19px;padding-bottom: 10px;">
                            <div class="btn-group">
                                <a href="<?php echo base_url('adminsignup');?>" id="addRow" class="btn btn-info">
                                 Add New <i class="fa fa-plus" style="color: white;"></i>
                                </a>
                            </div>
                        </div>
                    </div>

					<table class="table datatable-basic table-striped">
						<thead>
							<tr>
								<th>Sr No</th>
								<th>Profile Picture</th>
								<th>First Name</th>
								<th>Email</th>
								<th>Contact No</th>
								<th>DOB</th><!-- 
								<th>Status</th>
								<th>Actions</th> -->
							</tr>
						</thead>
						<tbody>
                            <?php if (count( $admin )):
                                $i=1;
                                                    
                                foreach ($admin as $admins ) :?>
                                <tr>
                                    <td><?= $i; ?></td>                                    
                                    <td>
                                    	<?php  
                                            $base = base_url()."uploads/";
                                            $path = $base.$admins->user_image;
                                        ?>
                                        <img src="<?= $path?>" width="50" height="50">
                                    </td>
                                    <td><?= $admins->firstname ?></td>
                                    <td><?= $admins->email ?></td>
                                    <td><?= $admins->contact ?></td>
                                    <td><?= $admins->birth_date ?></td><!-- 
                                    <td>
                                        <a href="<?php echo base_url();?>admin_change_interior_status/<?=$admins->admin_id?>/<?=$type="deactivate"?>">
                                        	<span class="label label-danger">Deactivate</span>
                                        </a>
                                    </td> -->
                               </tr>
                                <?php 
                                $i++;
                                endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="3">
                                            No Records Found.
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                                    
                        </tbody>
					</table>
				</div>
				<!-- /striped rows -->
			</div>
		</div>
	</div>
