					<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Striped rows -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Striped rows</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<table class="table datatable-basic table-striped">
						<thead>
							<tr>
								<th>Sr No</th>
								<th>University Name</th>
								<th>Graduate</th>
								<!-- <th>Gender</th> -->
								<th>Type</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
                            <?php if (count( $education_status )):
                                $i=1;
                                                    
                                foreach ($education_status as $educations_status ) :?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $educations_status->my_university ?></td>
                                    <td><?= $educations_status->graduate ?></td>
                                   <!-- <td><?= $educations_status->gender ?></td>  -->
                                    <td><?= $educations_status->type ?></td>
                                    <td><?= $educations_status->status ?></td>
                                    <td>
                                    	<a href="<?php echo base_url();?>changeeducationstatus/<?=$educations_status->education_id?>/<?=$type="active"?>">
                                        	<span class="label label-success">Accept</span>
                                        </a>
                                        <a href="<?php echo base_url();?>changeeducationstatus/<?=$educations_status->education_id?>/<?=$type="decline"?>">
                                        	<span class="label label-danger">Decline</span>
                                        </a>
                                    </td>
                               </tr>
                                <?php 
                                $i++;
                                endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="3">
                                            No Records Found.
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                                    
                        </tbody>
					</table>
				</div>
				<!-- /striped rows -->
			</div>
		</div>
	</div>
