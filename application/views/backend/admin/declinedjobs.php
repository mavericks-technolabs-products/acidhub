<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Striped rows -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Striped rows</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<table class="table datatable-basic table-striped">
						<thead>
							<tr>
								<th>Sr No</th>
								<th>Company Name</th>
                                <th>Job Title</th>
								<th>Job Type</th>
								<th>Type</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
                            <?php if (count( $delined_job )):
                                $i=1;
                                foreach ($delined_job as $delined_jobs ) :
                                     ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $delined_jobs->company_name ?></td>
                                    <td><?= $delined_jobs->job_title ?></td>
                                    <td><?= $delined_jobs->job_type ?></td>
                                    <td><?= $delined_jobs->type ?></td>
                                    <td><?= $delined_jobs->status ?></td>
                                    <td>
                                    	<a href="<?php echo base_url();?>declinedjobstatus/<?=$delined_jobs->job_id?>/<?=$type="active"?>">
                                            <span class="label label-success">Accept</span>
                                        </a>
                                        <a href="<?php echo base_url();?>declinedjobstatus/<?=$delined_jobs->job_id?>/<?=$type="decline"?>">
                                            <span class="label label-danger">Decline</span>
                                        </a>
                                    </td>
                               </tr>
                                <?php 
                                $i++;
                                endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="3">
                                            No Records Found.
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                                    
                        </tbody>
					</table>
					<script type="text/javascript">
                     	function change_status(id,type)
                     	{
                         	var url="<?php echo base_url();?>";
                         	swal(
                         	{
                             	title: "Are you sure?",
                             	text: "You really want to activate this account?",
                             	icon: "warning",
                             	buttons: true,
                             	dangerMode: true,
                         	})
                         	.then((willDelete) => 
                         	{                                                                    
                             	if (willDelete)
                             	{
                                 	window.location = url+"adminchangejobstatus/"+id+'/'+type;
                                 	swal("Your file is deleted!",
                                 	{
                                     	timer: 1500,
                                     	icon: "success",
                                 	});
                             	}
                             	else
                             	{
                                 	swal("Your file is safe!");
                             	}
                         	});
                     	}

                     	function change_status_deactivate(id,type)
                     	{
                         	var url="<?php echo base_url();?>";
                         	swal(
                         	{
                             	title: "Are you sure?",
                             	text: "You really want to deactivate this account?",
                             	icon: "warning",
                             	buttons: true,
                             	dangerMode: true,
                         	})
                         	.then((willDelete) => 
                         	{                                                                    
                             	if (willDelete)
                             	{
                                 	window.location = url+"adminchangejobstatus/"+id+'/'+type;
                                 	swal("Your file is deleted!",
                                 	{
                                     	timer: 1500,
                                     	icon: "success",
                                 	});
                             	}
                             	else
                             	{
                                 	swal("Your file is safe!");
                             	}
                         	});
                     	}
                    </script>
				</div>
				<!-- /striped rows -->
			</div>
		</div>
	</div>
