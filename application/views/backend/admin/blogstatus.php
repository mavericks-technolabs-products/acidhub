					<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Striped rows -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Striped rows</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<table class="table datatable-basic table-striped">
						<thead>
							<tr>
								<th>Sr No</th>
								<th>Blog Title</th>
								<th>Blog Name</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
                            <?php if (count( $user_blog_status )):
                                $i=1;
                                                    
                                foreach ($user_blog_status as $users_blog_status ) :?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $users_blog_status->blog_title ?></td>
                                    <td><?= $users_blog_status->blog_author_name ?></td>
                                    <td><?= $users_blog_status->status ?></td>
                                    <td>
                                    	<a href="<?php echo base_url();?>changeblogstatus/<?=$users_blog_status->blog_id?>/<?=$type="active"?>">
                                        	<span class="label label-success">Accept</span>
                                        </a>
                                        <a href="<?php echo base_url();?>changeblogstatus/<?=$users_blog_status->blog_id?>/<?=$type="decline"?>">
                                        	<span class="label label-danger">Decline</span>
                                        </a>
                                    </td>
                               </tr>
                                <?php 
                                $i++;
                                endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="3">
                                            No Records Found.
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                                    
                        </tbody>
					</table>
				</div>
				<!-- /striped rows -->
			</div>
		</div>
	</div>
