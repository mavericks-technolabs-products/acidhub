					<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Striped rows -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Striped rows</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<table class="table datatable-basic table-striped">
						<thead>
							<tr>
								<th>Sr No</th>
								<th>Project Location</th>
								<th>Project Title</th>
								<th>Submitted By</th>
								<th>Type</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
                            <?php if (count( $project_status )):
                                $i=1;
                                                    
                                foreach ($project_status as $projects_status ) :?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $projects_status->project_location ?></td>
                                    <td><?= $projects_status->project_title ?></td>
                                    <td><?= $projects_status->project_name ?></td>
                                    <td><?= $projects_status->type ?></td>
                                    <td><?= $projects_status->status ?></td>
                                    <td>
                                    	<a href="<?php echo base_url();?>changeprojectstatus/<?=$projects_status->project_id?>/<?=$type="active"?>">
                                        	<span class="label label-success">Accept</span>
                                        </a>
                                        <a href="<?php echo base_url();?>changeprojectstatus/<?=$projects_status->project_id?>/<?=$type="decline"?>">
                                        	<span class="label label-danger">Decline</span>
                                        </a>
                                    </td>
                               </tr>
                                <?php 
                                $i++;
                                endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="3">
                                            No Records Found.
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                                    
                        </tbody>
					</table>
				</div>
				<!-- /striped rows -->
			</div>
		</div>
	</div>
