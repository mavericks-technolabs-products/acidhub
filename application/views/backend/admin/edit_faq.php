<div class="page-container">
<!-- Page content -->
  <div class="page-content">
  <!-- Main content -->
    <div class="content-wrapper">
     <!-- Registration form -->
      <div class="panel panel-flat">
        <div class="panel-heading">
          <div class="heading-elements">
            <ul class="icons-list">
              <li><a data-action="collapse"></a></li>
              <li><a data-action="reload"></a></li>
              <li><a data-action="close"></a></li>
            </ul>
          </div>
        </div>

        <div class="panel-body">
          <?php echo form_open('backend/admin/AdminController/edit_faq');?>
          <?php echo form_hidden('user_id',$this->session->userdata('user_id'));?>
          <?php echo form_hidden('faqid',$edit_faq->faq_id);?>
          <div class="row">
            <div class="col-md-12">
            <fieldset>
            <legend><i class="glyphicon glyphicon-user"></i> FAQ's Details</legend>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Question:</label>
                    <input type="text" name="question" placeholder="" onkeypress="return isCharacterKey(event)" class="form-control" value="<?= $edit_faq->question; ?>"/>
                    <?php echo form_error('question');?>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Answer:</label>
                <textarea rows="3" cols="3"  name="answer" class="form-control" placeholder="Enter your answer here"><?= $edit_faq->answer; ?></textarea>
                <?php echo form_error('answer');?>
            </div><br>
            </fieldset>
            </div>
          </div>

          <div class="text-right">
            <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i>
            </button>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>

  <script type="text/javascript">
      function isCharacterKey(evt)
      {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (!(charCode > 31 && (charCode < 48 || charCode > 57)))
        return false;
        return true;
      }

  </script> 