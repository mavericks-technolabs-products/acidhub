					<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Striped rows -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Striped rows</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<table class="table datatable-basic table-striped">
						<thead>
							<tr>
								<th>Sr No</th>
								<th>First Name</th>
								<th>Email</th>
								<th>DOB</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
                            <?php if (count( $architecture )):
                                $i=1;
                                                    
                                foreach ($architecture as $architectures ) :?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $architectures->firstname ?></td>
                                    <td><?= $architectures->email ?></td>
                                    <td><?= $architectures->bday ?></td>
                                    <td><?= $architectures->status ?></td>
                                    <td>
                                    	<?php if($architectures->status == "deactivate") { ?>
                                    	<a href="<?php echo base_url();?>admin_change_architecture_status/<?=$architectures->architecture_id?>/<?=$type="activate"?>">
                                        	<span class="label label-success">Activate</span>
                                        </a>
                                    	<?php } else {?>
                                        <a href="<?php echo base_url();?>admin_change_architecture_status/<?=$architectures->architecture_id?>/<?=$type="deactivate"?>">
                                        	<span class="label label-danger">Deactivate</span>
                                        </a>
                                    <?php } ?>
                                    </td>
                               </tr>
                                <?php 
                                $i++;
                                endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="3">
                                            No Records Found.
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                                    
                        </tbody>
					</table>
				</div>
				<!-- /striped rows -->
			</div>
		</div>
	</div>
