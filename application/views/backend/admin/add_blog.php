<div class="page-container">
<!-- Page content -->
  <div class="page-content">
  <!-- Main content -->
    <div class="content-wrapper">
     <!-- Registration form -->
      <div class="panel panel-flat">
        <div class="panel-heading">
          <div class="heading-elements">
            <ul class="icons-list">
              <li><a data-action="collapse"></a></li>
              <li><a data-action="reload"></a></li>
              <li><a data-action="close"></a></li>
            </ul>
          </div>
        </div>

        <div class="panel-body">
          <?php echo form_hidden('user_id',$this->session->userdata('user_id'));?>
          <?php echo form_open_multipart('backend/admin/AdminController/insert_blog');?>
          <div class="row">
            <div class="col-md-12">
            <fieldset>
            <legend><i class="glyphicon glyphicon-user"></i> Blog Details</legend>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Blog Title:</label>
                    <input type="text" name="blogtitle" placeholder="" onkeypress="return isCharacterKey(event)" class="form-control" required />
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Blog Content:</label>
                <textarea rows="3" cols="3"  name="blog_content" class="form-control" placeholder="Enter your message here"></textarea>
            </div><br>
            <div class="col-md-8">
              <div class="form-group">
                <label class="display-block">Choose Profile Picture:</label>
                <input name="userfile" type="file">
              </div>
            </div>
            </fieldset>
            </div>
          </div>

          <div class="text-right">
            <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i>
            </button>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>

  <script type="text/javascript">
      function isCharacterKey(evt)
      {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (!(charCode > 31 && (charCode < 48 || charCode > 57)))
        return false;
        return true;
      }

  </script> 