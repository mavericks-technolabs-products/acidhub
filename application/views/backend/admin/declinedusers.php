					<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Striped rows -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Declined Interiors</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<table class="table datatable-basic table-striped">
						<thead>
							<tr>
								<th>Sr No</th>
								<th>First Name</th>
								<th>Email</th>
								<th>DOB</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
                            <?php if (count( $decline_interior )):
                                $i=1;
                                                    
                                foreach ($decline_interior as $interiors ) :?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $interiors->firstname ?></td>
                                    <td><?= $interiors->email ?></td>
                                    <td><?= $interiors->type ?></td>
                                    <td><?= $interiors->status ?></td>
                                    <td>
                                    	<a href="<?php echo base_url();?>admin_change_decline_interior_status/<?=$interiors->user_id?>/<?=$type="active"?>">
                                        	<span class="label label-success">Accept</span>
                                        </a>
                                    </td>
                               </tr>
                                <?php 
                                $i++;
                                endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="3">
                                            No Records Found.
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                                    
                        </tbody>
					</table>
				</div>
				<!-- /striped rows -->
			</div>
		</div>
	</div>