<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<style type="text/css">
    .swal-button {
  padding: 7px 19px;
  color: #ffff;
  border-radius: 2px;
  background-color: #3f51b5;
  font-size: 12px;
  border: 1px solid #3e549a;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}
.swal-footer {
  background-color: rgb(245, 248, 250);
  margin-top: 32px;
  border-top: 1px solid #E9EEF1;
  overflow: hidden;
}
.swal-overlay {
  background-color: #3f51b5;
}
</style>
<?php if ($this->session->flashdata('welcome')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('welcome'); ?>",
                                    icon: "success",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
<!-- Dashboard content -->
				<div class="row">
					<div class="col-lg-12">
						<!-- Quick stats boxes -->
						<div class="row">
							<h3>Total Counts</h3>
						</div>
						<div class="row">
							<div class="col-lg-3">

								<!-- Members online -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
										</div>

										<h3 class="no-margin"><?php $count = $this->DashboardModel->student_count(); ?>
										 	<?php echo $count; ?></h3>
										Total Students
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>

							<div class="col-lg-3">

								<!-- Members online -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
										</div>

										<h3 class="no-margin"><?php $count = $this->DashboardModel->active_student_count(); ?>
										 	<?php echo $count; ?></h3>
										Active Students
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>

							<div class="col-lg-3">

								<!-- Current server load -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
											
										</div>

										<h3 class="no-margin"><?php $count = $this->DashboardModel->count_architecture(); ?>
										 	<?php echo $count; ?></h3>
										Total Architectures
									</div>

									<div id="server-load"></div>
								</div>
								<!-- /current server load -->

							</div>

							<div class="col-lg-3">

								<!-- Current server load -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
											
										</div>

										<h3 class="no-margin"><?php $count = $this->DashboardModel->active_count_architecture(); ?>
										 	<?php echo $count; ?></h3>
										Active Architectures
									</div>

									<div id="server-load"></div>
								</div>
								<!-- /current server load -->

							</div>

							<div class="col-lg-3">

								<!-- Today's revenue -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
											
					                	</div>

										<h3 class="no-margin" ><?php $count = $this->DashboardModel->count_interior(); ?>
										 	<?php echo $count; ?></h3>
										Total Interiors
									</div>

									<div id="today-revenue"></div>
								</div>
								<!-- /today's revenue -->

							</div>
							<div class="col-lg-3">

								<!-- Today's revenue -->
								<div class="panel bg-voilet-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
											
					                	</div>

										<h3 class="no-margin"><?php $count = $this->DashboardModel->active_count_interior(); ?>
										 	<?php echo $count; ?></h3>
										Active Interiors
									</div>

									<div id="today-revenue"></div>
								</div>
								<!-- /today's revenue -->

							</div>
						</div>
						<!-- /quick stats boxes -->


						<div class="row">
							<h3>Jobs</h3>
						</div>

						<div class="row">
							<h3>Total Jobs</h3>
						</div>

						<div class="row">
							<div class="col-lg-3">

								<!-- Members online -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
										</div>

										<h3 class="no-margin"><?php $count1 = $this->DashboardModel->count_job_interior();
											$count2 = $this->DashboardModel->count_job_architecture();
										 ?>
											<?php echo $count1+$count2; ?></h3>
										Total Posted Jobs
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>
						</div>

							<div class="row">
								<h3>Jobs For Interiors</h3>
							</div>

							<div class="col-lg-3">

								<!-- Current server load -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
											
										</div>

										<h3 class="no-margin"><?php $count1 = $this->DashboardModel->count_job_interior(); ?>
											<?php echo $count1; ?></h3>
										Total Jobs For Interiors
									</div>

									<div id="server-load"></div>
								</div>
								<!-- /current server load -->

							</div>

							
							
							<div class="col-lg-3">

								<!-- Today's revenue -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
											
					                	</div>

										<h3 class="no-margin"><?php $count1 = $this->DashboardModel->week_count_job_interior(); ?>
											<?php echo $count1; ?>
										</h3>
										Jobs Posted For Interiors In Last Week
									</div>

									<div id="today-revenue"></div>
								</div>
								<!-- /today's revenue -->

							</div>
							<div class="col-lg-3">

								<!-- Today's revenue -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
											
					                	</div>

										<h3 class="no-margin"><?php $count1 = $this->DashboardModel->month_count_job_interior(); ?>
											<?php echo $count1; ?></h3>
										Jobs Posted For Interiors In Last Month
									</div>

									<div id="today-revenue"></div>
								</div>
								<!-- /today's revenue -->

							</div>
						</div>
					</div>

						<div class="row">
							<h3>Jobs For Architectures</h3>
						</div>

						<div class="col-lg-3">

								<!-- Today's revenue -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
											
					                	</div>

										<h3 class="no-margin" ><?php $count2 = $this->DashboardModel->count_job_architecture(); ?>
											<?php echo $count2; ?></h3>
										Total Jobs For Architectures
									</div>

									<div id="today-revenue"></div>
								</div>
								<!-- /today's revenue -->
						</div>

						<div class="col-lg-3">

								<!-- Today's revenue -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
											
					                	</div>

										<h3 class="no-margin" ><?php $count2 = $this->DashboardModel->week_count_job_architecture(); ?>
											<?php echo $count2; ?></h3>
										Jobs Posted For Interiors In Last Week
									</div>

									<div id="today-revenue"></div>
								</div>
								<!-- /today's revenue -->
						</div>

						<div class="col-lg-3">

								<!-- Today's revenue -->
								<div class="panel bg-400">
									<div class="panel-body" style="text-align: center;">
										<div class="heading-elements">
											
					                	</div>

										<h3 class="no-margin" ><?php $count2 = $this->DashboardModel->month_count_job_architecture(); ?>
											<?php echo $count2; ?></h3>
										Jobs Posted For Interiors In Last Month
									</div>

									<div id="today-revenue"></div>
								</div>
								<!-- /today's revenue -->
						</div>


						

					</div>
				</div>
				<!-- /dashboard content -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
