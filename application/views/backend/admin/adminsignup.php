<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<style type="text/css">
    .swal-button {
  padding: 7px 19px;
  color: #ffff;
  border-radius: 2px;
  background-color: #3f51b5;
  font-size: 12px;
  border: 1px solid #3e549a;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}
.swal-footer {
  background-color: rgb(245, 248, 250);
  margin-top: 32px;
  border-top: 1px solid #E9EEF1;
  overflow: hidden;
}
.swal-overlay {
  background-color: #3f51b5;
}
</style>
<?php if ($this->session->flashdata('error')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('error'); ?>",
                                    icon: "success",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main content -->
      <div class="content-wrapper">

        <!-- Registration form -->
      
          <div class="panel panel-flat">
            <div class="panel-heading">
              <div class="heading-elements">
                <ul class="icons-list">
                          <li><a data-action="collapse"></a></li>
                          <li><a data-action="reload"></a></li>
                          <li><a data-action="close"></a></li>
                        </ul>
                      </div>
            </div>

            <div class="panel-body">
              <?php echo form_open_multipart('backend/admin/AdminController/admin_signup');?>  
              <form method="POST" action="<?php echo base_url();?>backend/admin/AdminController/adminregistration_insert">
              <div class="row">
              

                <div class="col-md-12">
                  <fieldset>
                            <legend><i class="glyphicon glyphicon-user"></i> Admin details</legend>

                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>First name:</label>
                          <input type="text" name="firstname" placeholder="" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" class="form-control" required />
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Last name:</label>
                          <input type="text" name="lastname" placeholder=""  onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" class="form-control" required/>
                        </div>
                      </div>
                  

                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Email:</label>
                          <input type="email" name="email" id="email" onblur="cheq_e_mail(this);" placeholder="" class="form-control" required />
                          <div id="msg_email" style="color:red"></div>
                          <?php echo form_error('email'); ?>
                        </div>
                      </div>
                      </div>
                      <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Password:</label>
                          <input type="password" name="password" id="password" placeholder="" class="form-control" required />
                          <?php echo form_error('password'); ?>
                        </div>
                      </div>
                      <div class="col-md-4">
                  <div class="form-group">
                    <label>Confirm Password</label>
                    <input class="form-control" type="password"  name="confirmpassword"  placeholder="Confirm Password" id="confirmpassword" onBlur="match_password()"/>
                  </div>
                  <div id="msg" style="color:red"></div>
                </div>
                    
                       

                        
                       <div class="col-md-4">
                        <div class="form-group">
                          <label>Address line:</label>
                          <input type="text" name="address" placeholder="address" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                          <div class="col-md-4">
                        <div class="form-group">
                          <label>City:</label>
                          <input type="text" name="city" placeholder="city"  onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" class="form-control" required />
                        </div>
                      </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>State/Province:</label>
                          <input type="text" name="state" placeholder="state"  onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" class="form-control" required />
                        </div>
                      </div>
                    
                      <div class="col-md-4">

                      <option value="country" disabled selected>Country</option>
                      <select class="form-control" id="country" name="country">
                        <option value="select">Select</option>
                        <<option value="India">India</option> 
                        <option value="Us">Us</option> 
                        <option value="Cape Verde">Cape Verde</option> 
                      </select>
                    </div>
                  </div>




                        <div class="row">
                      <div class="col-md-4">
                    <div class="form-group gender">
                    <label>Gender</label><br>
                    <label class="radio-inline" name="gender">
                      <input type="radio" name="optradio" value="Male" checked>Male
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="optradio" value="Female">Female
                    </label>
                  </div>
                  </div>
                  <div class="col-md-4">
                             <p class="birth">Date of Birth</p>
                              <lnput placeholder="text" type="text" /> 
                                  <input type="date"  class="form-control"  name="birth_date" ></lnput>
                              </div>
                              <div class="col-md-4">
                        <div class="form-group">
                          <label>Contact<span class="required" style="color: #e02222;"> * </span>:</label>
                         <input type="text" class="form-control" onblur="cheq_number(this);" name="contact" id="number" maxlength="10" onkeypress="return isNumberKey(event)" />
                         <div id="msg_contact" style="color:red"></div>
                        </div>
                      </div>
                </div><br>
                <div class="form-group">
                  <label class="display-block">Choose Profile Picture:</label>
                  <input name="userfile" type="file">
                </div>
                    <div class="row">
                    <div class="form-group">
                      <label>Additional message:</label>
                      <textarea rows="3" cols="3"  name="additional_massage" class="form-control" placeholder="Enter your message here"></textarea>
                    </div>
                    </div>
                   <div class="content">

        
          <!-- <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title">Summernote editor</h5>
              <div class="heading-elements">
                <ul class="icons-list">
                          <li><a data-action="collapse"></a></li>
                          <li><a data-action="reload"></a></li>
                          <li><a data-action="close"></a></li>
                        </ul>
                      </div>
            </div>

            <div class="panel-body">
              <div class="summernote">
          
          </div>
          </div></div> --><br>
                  </fieldset>
                </div>
              </div>

              <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
               <!--  <input type="submit" value="submit"> -->
              </div>
              </form>
            </div>
          </div>
        
        <!-- /2 columns form -->
        <!-- /registration form -->

      </div>
      <!-- /main content -->

    </div>
    <!-- /page content -->

  </div>

  <script type="text/javascript">
      function match_password()
      {
        var password=document.getElementById("password").value;
        var confirmpassword=document.getElementById("confirmpassword").value;
            if(password!=confirmpassword)
            {
              document.getElementById("msg").innerHTML =("Password does not match.");
              document.getElementById("confirmpassword").value='';
              //document.getElementById("password").value='';
              // alert("password does not match");
            }
           /* else
            {
              document.getElementById("msg").innerHTML =("password match");
            }*/
      }
             
     function isNumberKey(evt)
             {
                 var charCode = (evt.which) ? evt.which : event.keyCode
                 if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
                    return false;
                    return true;
             }
    function isCharacterKey(evt)
          {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode==32 || (charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
              return true;
              return false;
            
          }
      function cheq_number()
      {
          var number=$("#number").val();
          var BASE_URL = "<?php echo base_url();?>";
          $.ajax(
          {
              url: BASE_URL+'backend/admin/AdminController/cheq_number',
              type: 'POST',
              data:  { 'number': number},
              dataType:'json',
              success: function(response) 
              {
                if (response == 'Success.')
                {
                  $('#msg_contact').html('<span style="color: green;">'+'Success.'+"</span>");
                }
                else if(response == 'Contact Number Already Exist.')
                {
                  $('#msg_contact').html('<span style="color: red;">'+'Number already Exist.'+"</span>");
                }
                else
                {
                  $('#msg_contact').html('Please enter valid number.');
                }
              }
          });
      }

      function cheq_e_mail()
      {
          var email=$("#email").val();
          var BASE_URL = "<?php echo base_url();?>";
          $.ajax(
          {
              url: BASE_URL+'backend/admin/AdminController/cheq_email',
              type: 'POST',
              data:  { 'email': email},
              dataType:'json',
              success: function(response) 
              {
                if (response == 'Success.')
                {
                  $('#msg_email').html('<span style="color: green;">'+'Success.'+"</span>");
                }
                else if(response == 'Email Already Exist.')
                {
                  $('#msg_email').html('<span style="color: red;">'+'Email already Exist.'+"</span>");
                }
                else
                {
                  $('#msg_email').html('Please enter valid email.');
                }
              }
          });
      }

  </script>

       <script type="text/javascript">
      function match_password()
      {
        /* alert("hi");*/
        var password=document.getElementById("password").value;
      /*  alert(password);*/
        var confirmpassword=document.getElementById("confirmpassword").value;
       /* alert(confirmpassword);*/
            if(password!=confirmpassword)
            {
              document.getElementById("msg").innerHTML =("Password does not match.");
              document.getElementById("confirmpassword").value='';
              //document.getElementById("password").value='';
              // alert("password does not match");
            }
           /* else
            {
              document.getElementById("msg").innerHTML =("password match");
            }*/
      }
    </script> 