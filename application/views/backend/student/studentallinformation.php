
<style>
@import url("https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700');
@import url('https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700');
body{
    font-family: 'Open Sans', sans-serif;
}
*:hover{
    -webkit-transition: all 1s ease;
    transition: all 1s ease;
}
section{
    float:left;
    width:100%;
    background: #fff;  /* fallback for old browsers */
    padding:30px 0;
}
h1{float:left; width:100%; color:#232323; margin-bottom:30px; font-size: 14px;}
h1 span{font-family: 'Libre Baskerville', serif; display:block; font-size:45px; text-transform:none; margin-bottom:20px; margin-top:30px; font-weight:700}
h1 a{color:#131313; font-weight:bold;}

/*Profile Card 1*/
.profile-card-1 {
  font-family: 'Open Sans', Arial, sans-serif;
  position: relative;
  float: left;
  overflow: hidden;
  width: 100%;
  color: #ffffff;
  text-align: center;
  height:368px;
  border:none;
}
.profile-card-1 .background {
  width:100%;
  vertical-align: top;
  opacity: 0.9;
  -webkit-filter: blur(5px);
  filter: blur(5px);
   -webkit-transform: scale(1.8);
  transform: scale(2.8);
}
.profile-card-1 .card-content {
  width: 100%;
  padding: 15px 25px;
  position: absolute;
  left: 0;
  top: 50%;
}
.profile-card-1 .profile {
  border-radius: 50%;
  position: absolute;
  bottom: 50%;
  left: 50%;
  max-width: 100px;
  opacity: 1;
  box-shadow: 3px 3px 20px rgba(0, 0, 0, 0.5);
  border: 2px solid rgba(255, 255, 255, 1);
  -webkit-transform: translate(-50%, 0%);
  transform: translate(-50%, 0%);
}
.profile-card-1 h2 {
  margin: 0 0 5px;
  font-weight: 600;
  font-size:25px;
}
.profile-card-1 h2 small {
  display: block;
  font-size: 15px;
  margin-top:10px;
}
.profile-card-1 i {
  display: inline-block;
    font-size: 16px;
    color: #ffffff;
    text-align: center;
    border: 1px solid #fff;
    width: 30px;
    height: 30px;
    line-height: 30px;
    border-radius: 50%;
    margin:0 5px;
}
.profile-card-1 .icon-block{
    float:left;
    width:100%;
    margin-top:15px;
}
.profile-card-1 .icon-block a{
    text-decoration:none;
}
.profile-card-1 i:hover {
  background-color:#fff;
  color:#2E3434;
  text-decoration:none;
}

/*Profile card 2*/
.profile-card-2 .card-img-block{
    float:left;
    width:100%;
    height:150px;
    overflow:hidden;
}
.profile-card-2 .card-body{
    position:relative;
}
.profile-card-2 .profile {
  border-radius: 70%;
  position: absolute;
  top: 78px;
  left: 15%;
  max-width: 75px;
  border: 3px solid rgba(255, 255, 255, 1);
  -webkit-transform: translate(-50%, 0%);
  transform: translate(-50%, 0%);
}
.profile-card-2 h5{
    font-weight:600;
    color:#6ab04c;
}
.profile-card-2 .card-text{
    font-weight:300;
    font-size:15px;
}
.profile-card-2 .icon-block{
    float:left;
    width:100%;
}
.profile-card-2 .icon-block a{
    text-decoration:none;
}
.profile-card-2 i {
  display: inline-block;
    font-size: 16px;
    color: #6ab04c;
    text-align: center;
    border: 1px solid #6ab04c;
    width: 30px;
    height: 30px;
    line-height: 30px;
    border-radius: 50%;
    margin:0 5px;
}
.profile-card-2 i:hover {
  background-color:#6ab04c;
  color:#fff;
}

/*Profile Card 3*/
.profile-card-3 {
  font-family: 'Open Sans', Arial, sans-serif;
  position: relative;
  float: left;
  overflow: hidden;
  width: 100%;
  text-align: center;
  height:368px;
  border:none;
}
.profile-card-3 .background-block {
    float: left;
    width: 100%;
    height: 200px;
    overflow: hidden;
}
.profile-card-3 .background-block .background {
  width:100%;
  vertical-align: top;
  opacity: 0.9;
  -webkit-filter: blur(0.5px);
  filter: blur(0.5px);
   -webkit-transform: scale(1.8);
  transform: scale(2.8);
}
.profile-card-3 .card-content {
  width: 100%;
  padding: 15px 25px;
  color:#232323;
  float:left;
  background:#efefef;
  height:50%;
  border-radius:0 0 5px 5px;
  position: relative;
  /*z-index: 9999;*/
}
.profile-card-3 .card-content::before {
    content: '';
    background: #efefef;
    width: 120%;
    height: 100%;
    left: 11px;
    bottom: 51px;
    position: absolute;
    z-index: -1;
    transform: rotate(-13deg);
}
.profile-card-3 .profile {
  border-radius: 50%;
  position: absolute;
  bottom: 50%;
  left: 50%;
  max-width: 100px;
  opacity: 1;
  box-shadow: 3px 3px 20px rgba(0, 0, 0, 0.5);
  border: 2px solid rgba(255, 255, 255, 1);
  -webkit-transform: translate(-50%, 0%);
  transform: translate(-50%, 0%);
  z-index:99999;
}
.profile-card-3 h2 {
  margin: 0 0 5px;
  font-weight: 600;
  font-size:25px;
}
.profile-card-3 h2 small {
  display: block;
  font-size: 15px;
  margin-top:10px;
}
.profile-card-3 i {
  display: inline-block;
    font-size: 16px;
    color: #232323;
    text-align: center;
    border: 1px solid #232323;
    width: 30px;
    height: 30px;
    line-height: 30px;
    border-radius: 50%;
    margin:0 5px;
}
.profile-card-3 .icon-block{
    float:left;
    width:100%;
    margin-top:15px;
}
.profile-card-3 .icon-block a{
    text-decoration:none;
}
.profile-card-3 i:hover {
  background-color:#232323;
  color:#fff;
  text-decoration:none;
}


/*Profile card 4*/
.profile-card-4 .card-img-block{
    float:left;
    width:100%;
    height:150px;
    overflow:hidden;
}
.profile-card-4 .card-body{
    position:relative;
}
.profile-card-4 .profile {
    border-radius: 50%;
    position: absolute;
    top: -62px;
    left: 50%;
    width:100px;
    border: 3px solid rgba(255, 255, 255, 1);
    margin-left: -50px;
}
.profile-card-4 .card-img-block{
    position:relative;
}
.profile-card-4 .card-img-block > .info-box{
    position:absolute;
    background:rgba(217,11,225,0.6);
    width:100%;
    height:100%;
    color:#fff;
    padding:20px;
    text-align:center;
    font-size:14px;
   -webkit-transition: 1s ease;
    transition: 1s ease;
    opacity:0;
}
.profile-card-4 .card-img-block:hover > .info-box{
    opacity:1;
    -webkit-transition: all 1s ease;
    transition: all 1s ease;
}
.profile-card-4 h5{
    font-weight:600;
    color:#d90be1;
}
.profile-card-4 .card-text{
    font-weight:300;
    font-size:15px;
}
.profile-card-4 .icon-block{
    float:left;
    width:100%;
}
.profile-card-4 .icon-block a{
    text-decoration:none;
}
.profile-card-4 i {
  display: inline-block;
    font-size: 16px;
    color: #d90be1;
    text-align: center;
    border: 1px solid #d90be1;
    width: 30px;
    height: 30px;
    line-height: 30px;
    border-radius: 50%;
    margin:0 5px;
}
.profile-card-4 i:hover {
  background-color:#d90be1;
  color:#fff;
}

/*Profile Card 5*/
.profile-card-5{
    margin-top:20px;
}
.profile-card-5 .btn{
    border-radius:2px;
    text-transform:uppercase;
    font-size:12px;
    padding:7px 20px;
}
.profile-card-5 .card-img-block {
    width: 91%;
    margin: 0 auto;
    position: relative;
    top: -20px;
    
}
.profile-card-5 .card-img-block img{
    border-radius:5px;
    box-shadow:0 0 10px rgba(0,0,0,0.63);
}
.profile-card-5 h5{
    color:#4E5E30;
    font-weight:600;
}
.profile-card-5 p{
    font-size:14px;
    font-weight:300;
}
.profile-card-5 .btn-primary{
    background-color:#4E5E30;
    border-color:#4E5E30;
}
.col-md-12.add textarea.form-control.height-80 {
    width: 100%;
    max-width: 567px;
    margin: 0 auto;
}
.col-sm-12.personal textarea.form-control.height-80 {
    width: 100%;
    max-width: 567px;
    margin: 0 auto;
}
.col-sm-12.career textarea.form-control.height-80 {
    width: 100%;
    max-width: 567px;
    margin: 0 auto;
}
input#email {
    width: 100%;
    max-width: 800px;
    margin: 0 auto;
}
div#myModal11 {
    margin-left: 200px!important;
}
.anyClass {
  height:800px!important;
  overflow-y: scroll;
}
.studresume div#myModal16 {
    right: inherit;
    left: inherit;
}
</style>
<div class="page-title">
      <div class="container">
        <div class="page-caption">
          <h2>Profile</h2>
          <p><a href="<?php echo base_url();?>studentprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Profile</p>
          <?php if($feedback=$this->session->flashdata('feedback')): ?>
                  <div class="alert alert-dismissible alert-danger">
                  <?= $feedback; ?>
                <?php endif; ?>
                </div>
                <?php if($error=$this->session->flashdata('error')): ?>
                  <div class="alert alert-dismissible alert-danger gg">
                  <?= $error; ?>
                  <?php endif; ?>
         
        </div>
      </div>
    </div>
  </div>
  
      <section>
    <div class="container">
      <div class="row">
        <?php
          foreach($ans as $key=>$value) 
          {?>
          <div class="col-md-4">
          
            <div class="card profile-card-3">
                <div class="background-block">
                    <img src="<?php echo base_url();?>assets/frontend/img/sdg-cover-13.jpg" alt="profile-sample1" class="background"/>
                </div>
                <div class="profile-thumb-block">
                    <img src="<?php echo base_url();?>uploads/student/<?=$value->user_file;?>" alt="profile-image" class="profile"/>
                </div>
                <div class="card-content">
                    <h2><?=$value->firstname;?><small><?=$value->type;?></small></h2>
                      <h5><?=$value->email;?></h5>
                      <h4><?=$value->contact;?></h4>
                   
                </div>
                   <?php } ?>
                </div>
               
    
      </div>
       

            <div class="col-md-8 studresume">
               <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?=$ans[0]->user_id?>">View Resume</button>
           
                 <div class="modal anyClass" id="myModal<?=$value->user_id?>">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>backend/StudentController/studentupdate_resume/<?php echo $value->user_id;?> ">
                  
                  <?php
                 foreach($resume as $key=>$value) 
                  {?>
                <div class="modal-dialog">
                <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h3><i class="glyphicon glyphicon-user"></i>Resume Details</h3>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
      <div class="modal-body">

        <div class="row">
           

                <div class="col-md-12">
                 <fieldset>
                            

                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>First name:</label>
                         <input type="text" name="firstname"  placeholder="Firstname" maxlength="12" required value="<?php echo $resume[0]->firstname;?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" />
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Last name:</label>
                          <input type="text" name="lastname" placeholder="Lastname"  maxlength="12" required value="<?php echo $resume[0]->lastname;?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" />
                        </div>
                      </div>
                  
                        <div class="col-md-4">
                        <div class="form-group">
                          <label>Contact:</label>
                          <input type="contact" name="contact" id="contact" placeholder="Contact" maxlength="10" pattern="\d{10}" onblur="cheq_number(this);" title="Please enter exactly 10 digits"  required value="<?php echo $resume[0]->contact;?>" />
                          <?php echo form_error('contact'); ?>
                        </div>
                      </div>
                    </div>
                      
                     
                    <div class="row">
                      <div class="col-md-12 input#email">
                        <div class="form-group">
                          <label>Email:</label>
                          <input type="email" name="email" id="email"  placeholder="Email...." onblur="cheq_e_mail(this);" required value="<?=$resume[0]->email;?>" />
                          <div id="msg_email" style="color:red"></div>
                          <?php echo form_error('email'); ?>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Skill</label>
                            <input type="skills" name="skills" placeholder="Skills" required value="<?php echo $resume[0]->skills;?>" />
                        </div>
                      </div>
                 
                       <div class="col-md-4">
                        <div class="form-group">
                          <label>Qualification</label>
                            <input type="qualification" name="qualification" placeholder="Qualification" required value="<?php echo $resume[0]->qualification;?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" />
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Hobby</label>
                           <input type="text" name="hobby" class="" placeholder="Hobby" required value="<?php echo $resume[0]->hobby;?>"/>
                        </div>
                      </div>
                    </div>
                  
                     <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                         <label>Work Experience</label>
                          <input type="text" name="content"  placeholder="Work Experience" maxlength="12" required value="<?php echo $resume[0]->content;?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" />
                        </div>
                      </div>
                    
                    <div class="col-md-4">
                        <label>Date of Birth</label>
                          <input type="date"  name="created_date" required value="<?=$resume[0]->created_date;?>"></lnput>
                    </div>
                     
                    <div class="col-sm-4">
                      <div class="form-group">
                      <label>Gender</label>
                          <input type="text" name="gender"  placeholder="gender" maxlength="12" required value="<?php echo $resume[0]->gender;?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" />
                      </div>
                    </div>
                  </div>
                    <div class="row">
                      <div class="col-sm-4">
                      <div class="form-group">
                      <label>Tools</label>
                     <input type="text" name="skills"  placeholder="skills" maxlength="12" required value="<?php echo $resume[0]->skills;?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" />
                   </div>
                 </div>
                  <div class="col-sm-4">
                      <div class="form-group">
                      <label>Version</label>
                     <input type="text" name="version"  placeholder="version" maxlength="12" required value="<?php echo $resume[0]->version;?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" />
                   </div>
                 </div>
               </div>
                  
                   
                      <div class="row">
                      <div class="col-md-12 add">
                     <label>Address</label>
                    <input type="text" name="student_address" class="form-control" placeholder="Address" required  value="<?php echo $resume[0]->student_address;?>"/>
                      </div>
                      </div>
                     <div class="row">
                    <div class="col-sm-12 personal">
                       <div class="form-group">
                    <label>Personal Objectivities</label>
                    <textarea class="form-control height-80" name="personal_objectivities" placeholder="Personal Objectivities" required value="<?php echo $resume[0]->personal_objectivities;?>" /><?php echo $resume[0]->personal_objectivities;?></textarea>
                   </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-sm-12 career">
                     <div class="form-group">
                  <label>Career Objectivities</label>
                  <textarea class="form-control height-80" name="career_objectivities" placeholder="Career Objectivities" value="<?php echo $resume[0]->career_objectivities;?>" /><?php echo $resume[0]->career_objectivities;?></textarea>
                  </div>
                  </div>
                </div>
                     <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>City</label>
                           <input type="text" name="city" class="" placeholder="City" maxlength="12" required value="<?php echo $resume[0]->city;?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" />
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>State</label>
                           <input type="text" name="state" class="" placeholder="State" maxlength="12" required value="<?php echo $resume[0]->state;?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)"/>
                        </div>
                      </div>
                      
                    
                      <div class="col-md-4">
                        <div class="form-group">
                    <label>Country</label>
                         <input type="text" name="country" class="" placeholder="Country" maxlength="12" required value="<?php echo $resume[0]->country;?>" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)"/>
                      
                    </select>
                    </div>
                  </div>
                    </div>
                    <div class="row">
                  <div class="col-sm-12">
                     <div class="form-group">
                     <label>Upload Photos Here:</label>
               <!--    <input type="file"  id="upload_image" name="upload_image[]" class="file-input-ajax">  -->
                  <img src="<?php echo base_url();?>uploads/<?=$value->upload_image;?>" class="img-responsive" alt="" height="500px!important" width="100px" />
                 </div>
                      </div>
                  </div>
                    

                     
                
                   
                  
                  

              <!--     
               <div class="col-md-12 col-12 padd-top-10 text-right">
            <button type="submit" class="btn theme-btn btn-m">Submit</button>
          </div> -->

        <!-- Modal footer -->
               <!--  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div> -->
                      <?php } ?>
                      </fieldset>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
          </div>
        </div>
</div>
</div>
</section>
  <script>
   function contact(){          
            $('#phone').keypress(function(e) {
                var a = [];
                var k = e.which;

                for (i = 48; i < 58; i++)
                    a.push(i);

                if (!(a.indexOf(k)>=0))
                    e.preventDefault();
            });
        }
        function isCharacterKey(evt)
      {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
          return true;
          return false;
      }

      function isNumberKey(evt)
             {
                 var charCode = (evt.which) ? evt.which : event.keyCode
                 if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
                    return false;
                    return true;
             }
  </script>  
        