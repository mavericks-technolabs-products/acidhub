<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script>
<style>
/*button.btn.btn-m.theme-btn {
    margin-top: -18px;
}*/
#searchinput 
{
    width: 300px;
}
#searchclear 
{
    position:absolute;
    right:5px;
    top:0;
    bottom:0;
    height:14px;
    margin:auto;
    /*font-size:14px;*/
    cursor:pointer;
    color:#ccc;
}
.form-control 
{
    height: 40px;
    border: 1px solid #dde6ef;
    margin-bottom: 10px;
    box-shadow: none;
    border-radius: 0;
    background: #fbfdff;
    font-size: 15px;
    color: #6b7c8a;
    font-weight: 400;
}
div#fresher {
    margin-top: -13px;
}
</style>
<script type="text/javascript">
function check()
{
    var dropdown = document.getElementById("skills").value;
     if (dropdown == "Microsoft Excel" || dropdown == "Microsoft Power Point" || dropdown == "Microsoft Word" || dropdown == "System Application & Product" || dropdown == "Extended Three-Dimentional Analysis Of Building System" || dropdown == "Stoad Pro" || dropdown == "Anyasis Civil" || dropdown == "MS Project" || dropdown == "Primavera" || dropdown == "Maya" || dropdown == "Hand Drafting" || dropdown == "Free Hand Skeching" || dropdown == "Sketch Up 3D" || dropdown == "Auto CAD" || dropdown == "Rhinoceros-3D" || dropdown == "Lumion" || dropdown == "Middileware" || dropdown == "PNG CAD" ||dropdown == "Apache Hadoop" || dropdown == "ARCHICAD" || dropdown == "3D MAX" || dropdown == "X-Ray Rendering" || dropdown == "PhotoShop" ||dropdown == "InDesign" || dropdown == "Coral Drow" || dropdown == "CATIA")
     {
        document.getElementById("fresher").style.display ="block";
        var name = dropdown;
        var version = document.getElementById("selectexperience").value;
        var bar = { name };
        delete bar.name; // foo is gone
     }
     else
     {
        document.getElementById("fresher").style.display ="none";
     }
}
</script>
<body>
		
		<div class="page-title">
            <div class="container">
                <div class="page-caption">
                    <h2>Create Resume</h2>
                    <p><a href="<?php echo base_url();?>studentprofile" title="Home">Home</a><i class="ti-arrow-right"></i> Create Resume</p>
                    <ul class="nav navbar-nav navbar-right">
                    <li class="sign-up"><a class="btn-signup red-btn" href="<?PHP ECHO BASE_URL();?>studentresume-dashboard" style="margin-right: 21px;   padding: 12px 1px; color: white;background-color: #ff7c39 !important;"><span  style="padding: 7px;"></span>View Resume</a></li>
                 </ul>
                </div>
            </div>
        </div>
        <section>
            <div class="container">
                    <div class="box">
                        <div class="box-header">
                            <h4>Use</h4>
                        </div>
                        <div class="box-body">
                        	<form method="POST" action="<?php echo base_url();?>backend/student/StudentController/skillsadd_resume">
                        	<?php echo form_hidden("resume_id",$student_resume_id);?>
                           <div class="row">
                                <div class="col-sm-4">
                                    <label><h3>Tools</h3></label>
                                    <select name="skills" id="skills" class="form-control" required="required" onchange="check(this);" multiple/>
                                    <option value="select">select...</option>
                                    <option id="Excel">Microsoft Excel</option>
                                    <option name="Microsoft Power Point">Microsoft Power Point</option>
                                    <option id="Word">Microsoft Word</option>
                                    <option id="System Application">System Application & Product</option>
                                    <option id="Extended Three-Dimentional">Extended Three-Dimentional Analysis Of Building System</option>
                                    <option id="Stoad Pro">Stoad Pro</option>
                                    <option id="Anyasis Civil">Anyasis Civil</option>
                                    <option id="MS Project">MS Project</option>
                                    <option id="Primavera">Primavera</option>
                                    <option id="Maya">Maya</option>
                                    <option id="Hand Drafting">Hand Drafting</option>
                                    <option id="Free Hand Skeching">Free Hand Skeching</option>
                                    <option id="Sketch Up 3D">Sketch Up 3D</option>
                                    <option id="Auto CAD">Auto CAD</option>
                                    <option id="Rhinoceros-3D">Rhinoceros-3D</option>
                                    <option id="Lumion">Lumion</option>
                                    <option id="Middileware">Middileware</option>
                                    <option id="PNG CAD">PNG CAD</option>
                                    <option id="Apache Hadoop">Apache Hadoop</option>
                                    <option id="ARCHICAD ">ARCHICAD</option>
                                    <option id="3D MAX ">3D MAX</option>
                                    <option id="X-Ray Rendering">X-Ray Rendering</option>
                                    <option id="PhotoShop">PhotoShop</option>
                                    <option id="InDesign">InDesign</option>
                                    <option id="Coral Drow">Coral Drow</option>
                                    <option id="CATIA">CATIA</option>
                                    </select>

                                </div><br><br>
                                <div class="col-md-4">  
                                <div id="fresher"  style="display:none;">
                                    <label>Version</label>
                                        <input type="text" name="version"  class="form-control" ng-model="version" id="selectexperience" onblur="check(this)" required/>
                                </div>
                                </div>
                               
                        		<div class="col-sm-4 btn1">
                        			<button type="add" class="btn btn-m theme-btn">Add</button>
                        		</div>
                        		</form>
                		</div>
                	</div>
        		</div>                  
            	
           
					<table class="table table-bordered table-hover datatable-highlight">
						<thead >
							<tr>
								<th width="20%">Skills</th>
								<th width="20%">Version</th>
								<th width="20%">Action</th>
							</tr>
						</thead>
                        <tbody>

                        <?php if(isset($ans) )
						{
							foreach ($ans as $value):?>
							<tr>
								
								<td width="20%">
									<?= $value->skills ?></td>
								<td width="20%">
									<?= $value->version ?></td>
                                    
								<td>
							         <a class="btn btn-danger btn-xs" href="<?php echo base_url();?>backend/student/StudentController/deleteresume/<?=$value->resume_id?>/<?= $value->student_resume_id?>"><i class="glyphicon glyphicon-trash"></i></a>
                                </td>
							</tr>
							<?php  endforeach;  }?>
                    	    <?php if(empty($ans)) 
                    	    { ?>
                    		<tr>
                    		<td colspan="3">
                    		no record found
                    		</td>
                    		</tr>
                    	    <?php } ?>
	                       </tbody>
                            </table>
                            </div>
                 </section>

<script type="text/javascript">
$(document).ready(function(){
        
                $("#searchinput").keyup(function(){
                	console.info("in");
                    $("#searchclear").toggle(Boolean($(this).val()));

                });
                $("#searchclear").toggle(Boolean($("#searchinput").val()));
                $("#searchclear").click(function(){
                	console.info("in");
                    $("#searchinput").val('').focus();
                    $(this).hide();
                });
            });
            var url="<?php echo base_url();?>";
            function deleteresume(id)
        {
          /*alert('ABC');*/
          swal(
          {
              title: "Are you sure?",
              text: "Are you sure you want to delete?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => 
          {
              
              if(willDelete)
              {
                /*
                */
                  window.location = url+"deleteresume/"+id;
                  swal("Your file is deleted!",
                  {
                      icon: "success",
                  });
          }
          else
          {
              swal("Your file is safe!");
          }
          });
    }

</script>
    
			<?php
			if($error = $this->session->flashdata('delete')) {
			    ?>
			    <script>
			        swal({

			            title: 'Congrats !',
			            text: '<?php echo $this->session->flashdata('delete'); ?>',
			            timer: 4000

			        })

			    </script>
			<?php }?>

</body>
</html>