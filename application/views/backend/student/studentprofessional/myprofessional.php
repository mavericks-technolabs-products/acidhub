   <style type="text/css">
  .blog-avatar.text-center.blogimg img {
    max-width: 91px;
    border-radius: 50%;
    margin: 0 auto 5px;
    border: 4px solid rgba(255,255,255,1);
    box-shadow: 0 2px 10px 0 #d8dde6;
    -webkit-box-shadow: 0 2px 10px 0 #d8dde6;
    -moz-box-shadow: 0 2px 10px 0 #d8dde6;
    width: 100%;
    height: 100%;
    max-height: 100px;
} 
.blog-grid-box-img {
    height: 191px;
    max-height: 250px;
    overflow: hidden;
    display: flex;
    align-items: center;
}
</style>  
    <!-- ======================= End Navigation ===================== -->
    
    <!-- ======================= Page Title ===================== -->
    <div class="page-title">
      <div class="container">
        <div class="page-caption">
          <h2>Professional</h2>
          <p><a href="<?php echo base_url();?>studentprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Professional</p>
        </div>
      </div>
    </div>

    <!-- ======================= End Page Title ===================== -->
    
    <!-- ====================== Start Job Detail 2 ================ -->
     
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-4">
          <div class="widget-boxed">
                <div class="form-group">
              <div class="contact-img">
                <img src="<?php echo base_url();?>assets/frontend/img/avatar4.jpg" class="img-circle img-responsive" alt="">
              </div>
            </div>
            </div>
            
         
            
            <div class="widget-boxed padd-bot-0">
              <div class="widget-boxed-header br-0">
                <h4>Designation <a href="#designation" data-toggle="collapse"><i class="pull-right ti-plus" aria-hidden="true"></i></a></h4>
              </div>
              <div class="widget-boxed-body collapse" id="designation">
                <div class="side-list no-border">
                  <ul>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="a">
                        <label for="a"></label>
                      </span>
                      Web Designer
                      <span class="pull-right">102</span>
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="b">
                        <label for="b"></label>
                      </span>
                      Php Developer
                      <span class="pull-right">78</span>
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="c">
                        <label for="c"></label>
                      </span>
                      Project Manager
                      <span class="pull-right">12</span>
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="d">
                        <label for="d"></label>
                      </span>
                      Human Resource
                      <span class="pull-right">85</span>
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="e">
                        <label for="e"></label>
                      </span>
                      CMS Developer
                      <span class="pull-right">307</span>
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="f">
                        <label for="f"></label>
                      </span>
                      App Developer
                      <span class="pull-right">256</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            
            <div class="widget-boxed padd-bot-0">
              <div class="widget-boxed-header br-0">
                <h4>Experince <a href="#experince" data-toggle="collapse"><i class="pull-right ti-plus" aria-hidden="true"></i></a></h4>
              </div>
              <div class="widget-boxed-body collapse" id="experince">
                <div class="side-list no-border">
                  <ul>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="11">
                        <label for="11"></label>
                      </span>
                      1Year To 2Year
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="21">
                        <label for="21"></label>
                      </span>
                      2Year To 3Year
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="31">
                        <label for="31"></label>
                      </span>
                      3Year To 4Year
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="41">
                        <label for="41"></label>
                      </span>
                      4Year To 5Year
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="51">
                        <label for="51"></label>
                      </span>
                      5Year To 7Year
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="61">
                        <label for="61"></label>
                      </span>
                      7Year To 10Year
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            
            <div class="widget-boxed padd-bot-0">
              <div class="widget-boxed-header br-0">
                <h4>Qualification <a href="#qualification" data-toggle="collapse"><i class="pull-right ti-plus" aria-hidden="true"></i></a></h4>
              </div>
              <div class="widget-boxed-body collapse" id="qualification">
                <div class="side-list no-border">
                  <ul>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="12">
                        <label for="12"></label>
                      </span>
                      High School
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="22">
                        <label for="22"></label>
                      </span>
                      Intermediate
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="32">
                        <label for="32"></label>
                      </span>
                      Graduation
                    </li>
                    <li>
                      <span class="custom-checkbox">
                        <input type="checkbox" id="42">
                        <label for="42"></label>
                      </span>
                      Master Degree
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            
          </div>
          
          <!-- Start Job List -->
          <div class="col-md-9 col-sm-8">
            
            <div class="row mrg-bot-20">
              <div class="col-sm-5">
                <h4>Professional &amp; Vacancies</h4>
              </div>
              
              <div class="col-sm-7">
                
                <div class="fl-right">
                  <div class="search-wide">
                    <h5>Short By</h5>
                  </div>
                  
                  <div class="search-wide full">
                    <select class="wide form-control">
                      <option value="1">Most Recent</option>
                      <option value="2">Most Viewed</option>
                      <option value="4">Most Search</option>
                    </select>
                  </div>
                </div>
             </div>
            </div>
            <?php
              foreach ($ans as $key=>$value) {
            ?>

            <div class="job-verticle-list">
              <div class="vertical-job-card">
                <div class="vertical-job-header">
                  <div class="vrt-job-cmp-logo">
                    <a href="professional-details/<?=$value->professional_id;?>"><img src="<?php echo base_url();?>assets/frontend/img/c-1.png" class="img-responsive" alt="" /></a>
                  </div>
                  <h4><a href="professional-details/<?=$value->professional_id;?>"><?=$value->firstname;?></a></h4>
                </div>
                <div class="vertical-job-body">
                  <div class="row">
                    <div class="col-md-9 col-sm-8">
                      <ul class="can-skils">
                        <li><strong>Branch: </strong><?=$value->branch;?></li>
                        <li><strong>Skill: </strong><?=$value->key_skill;?></li>
                        <li><strong>Location: </strong><?=$value->location;?></li> 
                      </ul>
                    </div>
                    <div class="col-md-3 col-sm-4">
                      <div class="vrt-job-act">
                       <a href="allprofessional_details/<?=$value->professional_id;?>" title="" class="btn-job light-gray-btn">View Professional</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
      </div>
        <!-- End Row -->
    </div>
    </section>
 

<script type="text/javascript">
var url="<?php echo base_url();?>";
  function delete_professional(id)
  {
          /*alert('ABC');*/
          swal(
          {
              title: "Are you sure?",
              text: "Are you sure you want to delete?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => 
          {
              if(willDelete)
              {
                  window.location = url+"delete-professional/"+id;
                  swal("Your file is deleted!",
                  {
                      icon: "success",
                  });
          }
          else
          {
              swal("Your file is safe!");
          }
          });
  }
</script>    
<?php
if($error = $this->session->flashdata('Feedback')) 
{
    ?>
    <script>

        swal("Good Job!", "<?php echo $this->session->flashdata('Feedback'); ?>", "success")



    </script>
    <?php
}
else if($error = $this->session->flashdata('error')) {
    ?>
    <script>
        swal("Oops!", "<?php echo $this->session->flashdata('error'); ?>", "error")
        

    </script>
    <?php
}
?>
    <!-- ====================== End Job Detail 2 ================ -->   
    
    <!-- ================= footer start ========================= -->