<style type="text/css">
	i.fa.fa-edit.iconsize {
    font-size: 23px;
}
   i.fa.fa-trash-o.iconsize {
    font-size: 23px;
}
</style>
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ================ Job Detail Basic Information ======================= -->
		<section class="detail-section" style="background:url(<?php echo base_url();?>assets/frontend/img/slider-2.jpg);">
			<div class="overlay"></div>
			<div class="profile-cover-content">
				<div class="container">
					<div class="cover-buttons">
						<ul>
						<li><div class="buttons medium button-plain "><i class="fa fa-phone"></i>+<?=$ans[0]->contact;?></div></li>
						<li><a href="#add-review" class="buttons theme-btn"><i class="fa fa-paper-plane"></i><span class="hidden-xs">Response</span></a></li>
						</ul>
					</div>
					<div class="job-owner hidden-xs hidden-sm">
						<div class="job-owner-avater">
							<img src="<?php echo base_url();?>assets/frontend/img/c-2.png" class="img-responsive img-circle" alt="" />
						</div>
						<div class="job-owner-detail">
							<h4>Web Designing</h4>
							<span class="theme-cl">Google PVT</span>

						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- ================ End Job Detail Basic Information ======================= -->
		
		<!-- ================ Start Job Overview ======================= -->
		<section>
			<div class="container">
				
				<!-- row -->
				<div class="row">
					<?php
					if($ans) 
					{?>
					<div class="col-md-8 col-sm-8">
					   <div class="detail-wrapper">
							<div class="detail-wrapper-body">
								<div class="job-title-bar">
									<!-- <h3><?=$ans[0]->company_name;?></h3> -->
									
									<h3><?=$ans[0]->firstname;?> <span class="mrg-l-5 job-tag bg-success-light"><?=$ans[0]->type;?>
									</span></h3>
									<p><strong>Branch</strong> :<?=$ans[0]->branch;?></p>
									<p><strong>Skill</strong> :<?=$ans[0]->key_skill;?></p>
									<p><strong>location</strong> :<?=$ans[0]->location;?></p>
									<p><strong>Experience</strong> :<?=$ans[0]->total_experience;?></p>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
						
						
					<div class="col-md-4 col-sm-4">
						<div class="sidebar">
							<!-- <div class="widget-boxed">
								<div class="widget-boxed-body">
									<a href="#" class="btn btn-m theme-btn full-width mrg-bot-10"><i class="fa fa-paper-plane"></i>Response</a>
									<a href="#" class="btn btn-m light-gray-btn full-width"><i class="fa fa-linkedin"></i>Apply for Job</a>
								</div>
							</div>
							 -->
							<div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-location-pin padd-r-10"></i>Location</h4>
								</div>
								<div class="widget-boxed-body">
									<div class="side-list no-border">
										<ul>
											<li><i class="ti-credit-card padd-r-10"></i><?=$ans[0]->branch;?></li>
											<li><i class="ti-mobile padd-r-10"></i><?=$ans[0]->contact;?></li>
											<li><i class="ti-email padd-r-10"></i><?=$ans[0]->email;?></li>
											<li><i class="ti-shield padd-r-10"></i><?=$ans[0]->key_skill;?></li>
										</ul>
										<!-- <h5>Share Job</h5> -->
									</div>
								</div>
							</div>
							
							 
						</div>
					</div>
					
				</div>
				<!-- End Row -->
				
				<!-- row -->
				<!-- <div class="row">
					<div class="col-md-12">
						<h4 class="mrg-bot-20">More Jobs</h4>
					</div>
				</div> -->
				<!-- End Row -->
				
				<!-- row -->
				<!-- <div class="row">
			
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type full-type">Full Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox" checked>
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-1.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">Product Redesign</a></h5>
								<p class="text-muted">2708 Scenic Way, Sutter, IL 62373</p>
							</div>
							
							<div class="job-type-grid">
								<a href="employerprojectdetails" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div> -->
					
					<!-- Single Job -->
					<!-- <div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type full-type">Full Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox">
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-2.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">New Product Mockup</a></h5>
								<p class="text-muted">2708 Scenic Way, Sutter, IL 62373</p>
							</div>
							
							<div class="job-type-grid">
								<a href="employerprojectdetails" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					 -->
					<!-- Single Job -->
					<!-- <div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type part-type">Full Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox" checked>
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-3.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">Custom Php Developer</a></h5>
								<p class="text-muted">3765 C Street, Worcester</p>
							</div>
							
							<div class="job-type-grid">
								<a href="employerprojectdetails" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					 -->
					<!-- Single Job -->
					<!-- <div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type part-type">Part Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox">
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-4.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">Wordpress Developer</a></h5>
								<p class="text-muted">2719 Duff Avenue, Winooski</p>
							</div>
							
							<div class="job-type-grid">
								<a href="employerprojectdetails" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
				</div> -->
				<!-- End Row -->
				
			</div>
		</section>
	
<script type="text/javascript">

   
    	var url="<?php echo base_url();?>";
    	function delete_education(id)
      	{
      		/*alert('ABC');*/
	        swal(
	        {
	            title: "Are you sure?",
	            text: "Are you sure you want to delete?",
	            icon: "warning",
	            buttons: true,
	            dangerMode: true,
	        })
	        .then((willDelete) => 
	        {
	            
	            if(willDelete)
	            {
	            	/*
	            	*/
	                window.location = url+"delete-education/"+id;
	                swal("Your file is deleted!",
	                {
	                    icon: "success",
	                });
	        }
	        else
	        {
	            swal("Your file is safe!");
	        }
	        });
		}

   
</script>    
		
		<!-- ====================== End Job Overview ================ -->
		
		<!-- ================= footer start ========================= -->
		