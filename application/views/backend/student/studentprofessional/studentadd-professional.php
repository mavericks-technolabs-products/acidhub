
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ======================= Start Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Professional</h2>
					<p><a href="<?php echo base_url();?>studentprofile" title="Home">Home</a> <i class="ti-arrow-right"></i>Professional</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ======================= Create Job ===================== -->
		<section class="create-job">
			<div class="container">
				
					<!-- General Information -->
					<div class="box">
						<div class="box-header">
							<h4>Professional Information</h4>
						</div>
						<div class="box-body">
							<form method="POST" action="<?php echo base_url();?>backend/student/StudentController/professional_insert">
							<div class="row">
							
								<div class="col-sm-6">
									<label>Total Experience</label>
									<input type="text" name="total_experience" class="form-control">
								</div>
								
								<div class="col-sm-6">
									<label>Branch</label>
									<input type="text" name="branch" class="form-control"  onkeypress="return isCharacterKey(event)">
								</div>
								
								
								
								<div class="col-sm-6 m-clear">
									<label>Key Skill</label>
									<input type="text" name="key_skill" class="form-control">
								</div>
								
								
								
								<div class="col-sm-6 m-clear">
									<label>Location</label>
									<input type="text" name="location" onkeypress="return isCharacterKey(event)" onkeypress="return isNumberKey(event)" class="form-control">
								</div>
								
								
								
							</div>
						</div>
					</div>
					
					
					
					
					<div class="text-center">
						<button type="submit" class="btn btn-m theme-btn">Submit</button>
					</div>
					
				</form>
			</div>
		</section>
		
		<!-- ====================== End Create Job ================ -->
		
		
		<!-- ================= footer start ========================= -->
	<script type="text/javascript">
        function isNumberKey(evt)
             {
                 var charCode = (evt.which) ? evt.which : event.keyCode
                 if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
                    return false;
                    return true;
             }
             function isCharacterKey(evt)
      {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
       		return true;
        	return false;
        
      }
      function isCharacterKey(evt)
      {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
          return true;
          return false;
        
      }
        </script>	