<!DOCTYPE html>

<html>
	
<head>

    <title>Acid Hub - Job Portal Template</title>

    <!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/plugins/bootstrap/css/bootstrap.min.css">
	
	<!-- Bootstrap Select Option css -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/plugins/bootstrap/css/bootstrap-select.min.css">
	
    <!-- Icons -->
    <link href="<?php echo base_url();?>assets/frontend/plugins/icons/css/icons.css" rel="stylesheet">
    
    <!-- Animate -->
    <link href="<?php echo base_url();?>assets/frontend/plugins/animate/animate.css" rel="stylesheet">
    
    <!-- Bootsnav -->
    <link href="<?php echo base_url();?>assets/frontend/plugins/bootstrap/css/bootsnav.css" rel="stylesheet">
	
	<!-- Nice Select Option css -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/plugins/nice-select/css/nice-select.css">
	
	<!-- Aos Css -->
    <link href="<?php echo base_url();?>assets/frontend/plugins/aos-master/aos.css" rel="stylesheet">

	<!-- Slick Slider -->
    <link href="<?php echo base_url();?>assets/frontend/plugins/slick-slider/slick.css" rel="stylesheet">	
    
    <!-- Custom style -->
    <link href="<?php echo base_url();?>assets/frontend/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/frontend/css/responsiveness.css" rel="stylesheet">
	
	<!-- Custom Color -->
    <link href="<?php echo base_url();?>assets/frontend/css/skin/default.css" rel="stylesheet">

           <!--  ************* sweetalert***************** -->
          <!--  ********** scrll bar css ****************** -->
   <https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css>
              <!--  ********** scroll bar css end****************** -->
   	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" 
    crossorigin="anonymous"></script>

    <!--  *************sweetalert end ****************** -->
</head>
	
	<!-- ======================= Start Navigation ===================== -->
		<nav class="navbar navbar-default navbar-mobile navbar-fixed white no-background bootsnav">
			<div class="container">
			
				<!-- Start Logo Header Navigation -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
						<i class="fa fa-bars"></i>
					</button>
					<a class="navbar-brand" href="#">
						<img src="<?php echo base_url();?>assets/frontend/img/Logo-Illustrator.jpg" class="logo logo-display" alt="">
						<img src="<?php echo base_url();?>assets/frontend/img/Logo-Illustrator.jpg" class="logo logo-scrolled" alt="">
					</a>

				</div>
				<!-- End Logo Header Navigation -->

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="navbar-menu">
				
					<ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
				
						<li>
							<a href="<?php echo base_url();?>studentprofile" class="dropdown-toggle" data-toggle="dropdown">Profile</a>
						</li>
					
						<li class="dropdown">
								<a href="#"  class="dropdown-toggle" data-toggle="dropdown">Education</a>
							<ul class="dropdown-menu animated fadeOutUp">
								<li><a href="<?php echo base_url();?>myeducation_dashboard">All Education</a></li>
								<li><a href="<?php echo base_url();?>education-dashboard">My Education</a></li>
							</ul>
						</li>
							
						<li class="dropdown">
								<a href="#"  class="dropdown-toggle" data-toggle="dropdown">Professional</a>
							<ul class="dropdown-menu animated fadeOutUp">
								<li><a href="<?php echo base_url();?>studentprofessional_dashboard">All Professional</a></li>
								<li><a href="<?php echo base_url();?>professional-dashboard">My Professional</a></li>
							</ul>
						</li>
							
						<li>
							<a href="<?php echo base_url();?>student-resume" class="dropdown-toggle" data-toggle="dropdown">Resume</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>studentalljob" class="dropdown-toggle" data-toggle="dropdown">job</a>
						</li>
					</ul>
					
					<ul class="nav navbar-nav navbar-right">
						<li class="sign-up"><a class="btn-signup red-btn" href="<?php echo base_url();?>destroy_session"><span class="ti-briefcase"></span>Logout</a></li> 
					</ul>
				</div>
			</div>   
		</nav>