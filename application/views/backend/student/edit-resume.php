
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ======================= Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Create Resume</h2>
					<p><a href="<?php echo base_url();?>studentprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Create Resume</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ======================== Create Resume ====================== -->
		<section>
			<div class="container">
				
					<!-- General Information -->
					<div class="box">
						<div class="box-header">
							<h4>Personal Details</h4>
						</div>
						<div class="box-body">
							<form method="POST" action="<?php echo base_url();?>backend/student/StudentController/studentupdate_resume/<?=$ans[0]->student_resume_id?>">
							<div class="row">
							
								<div class="col-sm-4">
									<label>First Name</label>
									<input type="text" name="firstname" class="form-control" placeholder="firstname" onkeypress="return isCharacterKey(event)"  required value="<?php echo $ans[0]->firstname;?>"/>
								</div>
								
								<div class="col-sm-4">
									<label>Last Name</label>
									<input type="text" name="lastname" class="form-control" placeholder="lastname" onkeypress="return isCharacterKey(event)"  required value="<?php echo $ans[0]->lastname;?>"/>
								</div>
								
								<div class="col-sm-4">
									<label>Email</label>
									<input type="email" name="email" id="email" onblur="validate(this);" placeholder="" class="form-control" required value="<?=$ans[0]->email;?>"> 
					                          <div id="msg_email" style="color:red"></div>
					                          <?php echo form_error('email'); ?>
					                      </div>
								
								<div class="col-sm-4">
									<label>Contact</label>
									<input type="text" name="contact" id="number"  class="form-control" maxlength="10" pattern="\d{10}" onblur="cheq_number(this);" title="Please enter exactly 10 digits" required  value="<?php echo $ans[0]->contact;?>">
									
									<div id="msg_contact" style="color:red"></div>
								</div>
								
								
								<div class="col-sm-4">
									<label>Gender</label>
									<select class="wide form-control" name="gender" required value="<?php echo $ans[0]->gender;?>"/>
										<option value="select">select...</option>>
										<option value="Female"<?=$ans[0]->gender=='Female'? ' selected="selected"' : '';?>>Female</option>
                                	<option value="Male"<?=$ans[0]->gender=='Male' ? ' selected="selected"' : '';?>>Male</option>
									</select>
								</div>
								<div class="col-sm-4">
									<label>Skill</label>
									<input type="text" name="skills" class="form-control" placeholder="Skill" required value="<?php echo $ans[0]->skills;?>"/>
								</div>
								<div class="col-sm-4">
									<label>Qualification</label>
									<input type="text" name="qualification" class="form-control" placeholder="Qualification" required value="<?php echo $ans[0]->qualification;?>"/>
								</div>
								<div class="col-sm-4">
									<label>Experience</label>
									<select class="wide form-control" name="content" required value="<?php echo $ans[0]->content;?>"/>
										<option value="select">select...</option>>
										<option value="Yes!"<?=$ans[0]->content=='Yes!'? ' selected="selected"' : '';?>>Yes</option>
                 						<option value="No!"<?=$ans[0]->content=='No!'? ' selected="selected"' : '';?>>No</option>
									</select>
								</div>
								<div class="col-sm-4">
									<label>Hobby</label>
									<input type="text" name="hobby" class="form-control" placeholder="Hobby" required value="<?php echo $ans[0]->hobby;?>"/>
								</div>
								<div class="col-sm-12">
									<label>Personal Objectivities</label>
									<textarea class="form-control height-80" name="personal_objectivities" placeholder="Personal Objectivities" required value="<?php echo $ans[0]->personal_objectivities;?>"/><?php echo $ans[0]->personal_objectivities;?></textarea>
								</div>
								
								<div class="col-sm-12">
									<label>Career</label>
									<textarea class="form-control height-80" name="career_objectivities" placeholder="Career Objectivities" required value="<?php echo $ans[0]->career_objectivities;?>"/><?php echo $ans[0]->career_objectivities;?></textarea>
								</div>
								<div class="col-sm-4">
									<label>City</label>
									<input type="text" name="city" class="form-control" placeholder="City" onkeypress="return isCharacterKey(event)"  required value="<?php echo $ans[0]->city;?>"/>
								</div>
								<div class="col-sm-4">
									<label>State</label>
									<input type="text" name="state" class="form-control" placeholder="State" onkeypress="return isCharacterKey(event)"  required value="<?php echo $ans[0]->state;?>"/>
								</div>
								<div class="col-sm-4">
									<label>Country</label>
									<input type="text" name="country" class="form-control" placeholder="Country" onkeypress="return isCharacterKey(event)"  required value="<?php echo $ans[0]->country;?>"/>
								</div>
								 <div class="row">
									     <div class="col-lg-12">
										<div class="form-group">
								     <label>Upload Photos Here:</label>
									<input type="file"  id="upload_image" name="upload_image[]" class="file-input-ajax" multiple/>
									<img src="<?php echo base_url();?>uploads/<?=$ans[0]->upload_image;?>" class="img-responsive" alt="" height="500px!important" width="100px" />
								</div>
							</div>
							</div>
								
							</div>
						</div>
					</div>
					
					<!-- Personal Detail & Address -->
			
					
					
					
					<div class="text-center">
						<button type="submit" class="btn btn-m theme-btn">Submit</button>
					</div>
					
				</form>
			</div>
		</section>
		
		<!-- ====================== End create Resume ================ -->
		
		
		<!-- ================= footer start ========================= -->

		
		<script>
		$('#myTab a').click(function (e) {
			e.preventDefault()
			$(this).tab('show')
		})
		</script>
		
		<!-- Date Dropper Script-->
		<script>
			$('#reservation-date').dateDropper();
		</script>
		
		<script>
			$('#date-from').dateDropper();
		</script>
		
		<script>
			$('#date-to').dateDropper();
		</script>
		
		<script>
			$('#e-date-from').dateDropper();
		</script>
		
		<script>
			$('#e-date-to').dateDropper();
		</script>
		
		<script>
			AOS.init();
		</script>
			  
    </body>

<!-- Mirrored from themezhub.com/demo/updated-jobhill-template/job-hill-live-preview/create-resume.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Aug 2018 09:06:16 GMT -->
</html>
<script>
	function myFunction() {
  var input = document.createElement('input');
  input.type = 'text';
  input.name = 'pet[]';
  return input;
}

var form = document.getElementById('myForm');
document.getElementById('addPet').addEventListener('click', function(e) {
  form.appendChild(myFunction());
});
$("#searchclear").click(function(){
    $("#searchinput").val('');
});
function isCharacterKey(evt)
      {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
       		return true;
        	return false;
        
      }
      function contactno(){          
            $('#phone').keypress(function(e) {
                var a = [];
                var k = e.which;

                for (i = 48; i < 58; i++)
                    a.push(i);

                if (!(a.indexOf(k)>=0))
                    e.preventDefault();
            });
        }
      function validateEmail(email)
      {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }

    function validate()
    {
      var $result = $("#msg_email");
      var email = $("#email").val();
      $result.text("");

      if (validateEmail(email))
      {
        if ($result) 
        {
          var email=$("#email").val();
          var BASE_URL = "<?php echo base_url();?>";
          $.ajax(
          {
            url: BASE_URL+'StudentController/studentcheqmail',
            type: 'POST',
            data:  { 'email': email},
            dataType:'json',
            success: function(response) 
            {
              if (response == 'Success.')
              {
                $('#msg_email').html('<span style="color: green;">'+'Success.'+"</span>");
              }
              else if(response == 'Email Already Exist.')
              {
                $('#msg_email').html('<span style="color: red;">'+'Email already Exist.'+"</span>");
              }
              else
              {
                $('#msg_email').html('Please enter valid email.');
              }
            }
          });
        }
      }
      else
      {
        $result.text(email + " is not valid.");
        $result.css("color", "red");
      }
      return false;
    }
</script>