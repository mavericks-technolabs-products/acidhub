
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Create Blog</h2>
					<p><a href="<?php echo base_url()?>employerprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Create Blog</p>
				</div>
			</div>
		</div>

		<section class="create-job">
			<div class="container">
				
					
					<!-- General Information -->
					<div class="box">
						<div class="box-header">
							<h4>Blog Information</h4>
						</div>
						<div class="box-body">
							<?php  echo form_open('backend/blog/blogController/blog_insert',array('method'=>'POST','class'=>"form-horizontal",'enctype'=>"multipart/form-data"));?>
						
							<div class="row">
							
								<div class="col-sm-6">
									<label>Blog title</label>
									<input type="text" name="blog_title" class="form-control"
									onkeypress="return isNumberKey(event)"   required />
								</div>
								
							   </div>
							    <div class="row">
							    	<div class="col-sm-6">
							     	<label>Add some pics?:</label>
							      	<input type="file" id="photos" name="blog_mult_img[]" accept="image/jpeg,image/gif,image/png,image/x-eps" multiple/>
							    <div class="row">
                                    <div class="col-md-12">
							       		<label>Image Gallery</label>
										<ul id="photos_clearing" class="clearing-thumbs" calss="form-control" height='200px' width='150px' data-clearing>
							      		</ul>
							    	</div>
							  	</div>
							</div>
						</div>
								
							    <div class="row">
							       <div class="col-sm-12 m-clear">
										<label>Blog Description</label>
										<textarea name="blog_description" id="my-info" name="blog_description" class="form-control" placeholder="" rows="4" cols="400" required/></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					
					<!-- Qualification & Instruction -->
					 
				
									<div class="text-center">
										<button type="submit" class="btn btn-m theme-btn">Submit & Exit</button>
									</div>
		    					</form>
							</div>
						</div>
					</div>
	          </section>

<script  src="<?php echo base_url();?>assets/frontend/js/index.js"></script>
<script type="text/javascript">
    function isNumberKey(evt)
        {
                 var charCode = (evt.which) ? evt.which : event.keyCode
                 if (!(charCode > 31 && (charCode < 48 || charCode > 57)))
                    return false;
                    return true;
        }
</script>