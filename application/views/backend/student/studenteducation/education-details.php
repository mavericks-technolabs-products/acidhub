<style type="text/css">
	i.fa.fa-edit.iconsize {
    font-size: 23px;
}
   i.fa.fa-trash-o.iconsize {
    font-size: 23px;
}
</style>
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ================ Job Detail Basic Information ======================= -->
		<section class="detail-section" style="background:url(<?php echo base_url();?>assets/frontend/img/slider-2.jpg);">
			<div class="overlay"></div>
			<div class="profile-cover-content">
				<div class="container">
					<div class="cover-buttons">
						<ul>
						<li><div class="buttons medium button-plain "><i class="fa fa-phone"></i>+<?=$ans[0]->contact;?></div></li>
						<!-- <li><div class="buttons medium button-plain "><i class="fa fa-map-marker"></i>#<?=$ans[0]->firm_address;?></div></li> -->
						<li><a href="#add-review" class="buttons theme-btn"><i class="fa fa-paper-plane"></i><span class="hidden-xs">Response</span></a></li>
						<!-- <li><a href="#" data-job-id="74" data-nonce="01a769d424" class="buttons btn-outlined"><i class="fa fa-heart-o"></i><span class="hidden-xs">Bookmark</span> </a></li> -->
						</ul>
					</div>
					<div class="job-owner hidden-xs hidden-sm">
						<div class="job-owner-avater">
							<img src="<?php echo base_url();?>assets/frontend/img/c-2.png" class="img-responsive img-circle" alt="" />
						</div>
						<div class="job-owner-detail">
							<h4>Web Designing</h4>
							<span class="theme-cl">Google PVT</span>

						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- ================ End Job Detail Basic Information ======================= -->
		
		<!-- ================ Start Job Overview ======================= -->
		<section>
			<div class="container">
				
				<!-- row -->
				<div class="row">
					<?php
					/*print_r($ans);
					exit();*/
					if($ans) 
					{?>
					<div class="col-md-8 col-sm-8">
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-body">
								<div class="job-title-bar">
									<!-- <h3><?=$ans[0]->company_name;?></h3> -->
									
									<h3><?=$ans[0]->my_university;?> <span class="mrg-l-5 job-tag bg-success-light"><?=$ans[0]->type;?>
									</span></h3>
									 <ul class="nav navbar-nav navbar-right">
  								   	 <li><a href="<?php echo base_url();?>edit-education/<?php echo $ans[0]->education_id?>"><i class="fa fa-edit iconsize"></i></a></li>
     								  <?php 
                                         $education_id = $ans[0]->education_id; 
                                        /* print_education_idr($job_id);
                                         exit();*/
                                         // $user_file = $ans[0]->user_file;                             
                                           ?> 
                                       <li><a id="<?php echo $ans[0]->education_id?>" onclick="delete_education(this.id)" href="javascript:void(0);"><i class="fa fa-trash-o iconsize"></i></a></li>
									</ul>
									
										<p><strong>Company</strong> :<?=$ans[0]->company;?></p>
										<p><strong>Designation</strong> :<?=$ans[0]->designation;?></p>
										<p><strong>Content</strong> :<?=$ans[0]->content;?></p>
										<p><strong>Gender</strong> :<?=$ans[0]->gender;?></p>
										
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
						
		
					
					<div class="col-md-4 col-sm-4">
						<div class="sidebar">
						
							<!-- Start: Opening hour -->
							<!-- <div class="widget-boxed">
								<div class="widget-boxed-body">
									<a href="#" class="btn btn-m theme-btn full-width mrg-bot-10"><i class="fa fa-paper-plane"></i>Response</a>
									<a href="#" class="btn btn-m light-gray-btn full-width"><i class="fa fa-linkedin"></i>response</a>
								</div>
							</div> -->
							<!-- End: Opening hour -->
							
							<!-- Start: Job Overview -->
							<div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-location-pin padd-r-10"></i>Location</h4>
								</div>
								<div class="widget-boxed-body">
									<div class="side-list no-border">
										<ul>
											<li><i class="ti-credit-card padd-r-10"></i><?=$ans[0]->my_university;?></li>
											
											<li><i class="ti-mobile padd-r-10"></i><?=$ans[0]->contact;?></li>
											<li><i class="ti-email padd-r-10"></i><?=$ans[0]->email;?></li>
											
											<li><i class="ti-shield padd-r-10"></i><?=$ans[0]->company;?></li>
											
											<li><i class="ti-shield padd-r-10"></i><?=$ans[0]->graduate;?></li>
										</ul>
										<h5>Share Job</h5>
										
									</div>
								</div>
							</div>
							<!-- End: Job Overview -->
							
							
							 
						</div>
					</div>
					
				</div>
				<!-- End Row -->
				
				<!-- row -->
				<!-- <div class="row">
					<div class="col-md-12">
						<h4 class="mrg-bot-20">More Jobs</h4>
					</div>
				</div> -->
				<!-- End Row -->
				
				<!-- row -->
				
				<!-- End Row -->
				
			</div>
		</section>
	
<script type="text/javascript">
var url="<?php echo base_url();?>";
    function delete_education(id)
    {
      		/*alert('ABC');*/
    swal(
    {
        title: "Are you sure?",
        text: "Are you sure you want to delete?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
   .then((willDelete) => 
	{
	    if(willDelete)
	    {
	        window.location = url+"delete-education/"+id;
	        swal("Your file is deleted!",
	        {
	                    icon: "success",
	        });
	    }
	        else
	        {
	            swal("Your file is safe!");
	        }
	        });
	}

</script>    
		
		<!-- ====================== End Job Overview ================ -->
		
		<!-- ================= footer start ========================= -->
		