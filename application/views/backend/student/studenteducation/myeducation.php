
    <!-- ======================= End Navigation ===================== -->
      
      
    <!-- ======================= Page Title ===================== -->
    <div class="page-title">
      <div class="container">
        <div class="page-caption">
          <h2>Education</h2>
          <p><a href="<?php echo base_url();?>studentprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Education</p>
          <?php if($feedback=$this->session->flashdata('feedback')): ?>
                  <div class="alert alert-dismissible alert-danger">
                  <?= $feedback; ?>
                <?php endif; ?>
                </div>
                <?php if($error=$this->session->flashdata('error')): ?>
                  <div class="alert alert-dismissible alert-danger">
                  <?= $error; ?>
                  <?php endif; ?>
          <!-- <ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp"> -->
          <!-- <ul class="nav navbar-nav navbar-right">
          
            <li class="sign-up"><a class="btn-signup red-btn" href="studentadd-education" style="margin-right: 21px; color: white;background-color: #ff7c39 !important;"><span  style="padding: 1px;"></span>Create Education</a></li>
          </ul> -->
        <!--  -->
        </div>
      </div>
    </div>
    <!-- ======================= End Page Title ===================== -->
    
    <!-- ================ Office Address ======================= -->
    <section>
      <div class="container">
        <div class="row">
        
          <!-- Single Job -->
          <?php
          foreach($ans as $key=>$value) 
          {?>
          <div class="col-md-3 col-sm-6">
            <div class="grid-job-widget">
            
              <span class="job-type full-type"><?=$value->type;?></span>
              <div class="job-like">
                <label class="toggler toggler-danger">
                  <input type="checkbox">
                  <i class="fa fa-check"></i>
                </label>
              </div>
              
              <div class="job-featured"></div>
              
              <div class="u-content">
                <div class="avatar box-100">
                  <a href="education-details/<?=$value->education_id;?>">
                    <img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-4.png" alt="">
                  </a>
                </div>
                <h5><a href="employer-detail/html"></a><?=$value->company;?></h5>
              <h5></h5>
                <!-- <p class="text-muted"><?=$value->company;?></p> -->
                <p class="text-muted"><?=$value->designation;?></p>
                <p class="text-muted"><?=$value->content;?></p>
                <p class="text-muted"><?php $date = $value->created_date;
                        $newdate = date('d F Y', strtotime(str_replace('/', '-', $date)));
                        echo $newdate;
                        ?></p>
                <p class="text-muted"><?=$value->gender;?></p>
                <span class="theme-cl"></span>
                
              </div>
              
              <div class="job-type-grid">
                <a href="studenteducation_details/<?=$value->education_id;?>" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
              </div>
              
            </div>
          </div>

            
        <?php } ?>
          
          
          
        </div>
        
        
      </div>
    </section>
    <!-- ================ End Office Address ======================= -->
      
    <!-- ================= footer start ========================= -->
    <?php
if($error = $this->session->flashdata('Feedback')) 
{
    ?>
    <script>

        swal("Good Job!", "<?php echo $this->session->flashdata('Feedback'); ?>", "success")



    </script>
    <?php
}
elseif($error = $this->session->flashdata('error')) {
    ?>
    <script>
        swal("Oops!", "<?php echo $this->session->flashdata('error'); ?>", "error")
        

    </script>
    <?php
}
?>
<?php
if($error = $this->session->flashdata('Delete')) 
{
    ?>
    <script>

        swal("Good Job!", "<?php echo $this->session->flashdata('Delete'); ?>", "success")



    </script>
    <?php
}
elseif($error = $this->session->flashdata('error')) {
    ?>
    <script>
        swal("Oops!", "<?php echo $this->session->flashdata('error'); ?>", "error")
        

    </script>
    <?php
}
?>