
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ======================= Start Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
						<h2>Resume Detail</h2>
						<p><a href="<?php echo base_url()?>studentprofile" title="Home">Home</a><i class="ti-arrow-right"></i> Resume Detail</p>
						<ul class="nav navbar-nav navbar-right">
          				<li class="sign-up"><a class="btn-signup red-btn" href="<?php echo base_url()?>edit-resume" style="margin-right: 21px;   padding: 12px 1px; color: white;background-color: #ff7c39 !important;"><span  style="padding: 7px;"></span>Edit Resume</a></li>
          				</ul>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		
		<!-- ====================== Resume Detail ================ -->
		<section>
			<div class="container">
				<!-- row -->
				<div class="row">
					
					<div class="col-md-8 col-sm-8">
						<?php
					foreach($ans as $key=>$value) 
					{
						?>
						<div class="detail-wrapper">
							<div class="detail-wrapper-body">
							
								<div class="text-center mrg-bot-30">
									<img src="<?php echo base_url();?>uploads/student/<?=$value->user_file;?>" class="img-responsive" alt="" height="150px!important" width="70px" />
									
									<h4 class="meg-0"><?=$value->firstname;?></h4>
									<span><?=$value->type;?></span>
								</div>
								
								<div class="row">
									<div class="col-sm-4 mrg-bot-10">
										<i class="ti-location-pin padd-r-10"></i><?=$value->student_address;?>
									</div>
									<div class="col-sm-4 mrg-bot-10">
										<i class="ti-email padd-r-10"></i><?=$value->email;?>
									</div>
									<div class="col-sm-4 mrg-bot-10">
										<i class="ti-mobile padd-r-10"></i><?=$value->contact;?>
									</div>
									<div class="col-sm-4 mrg-bot-10">
										<i class="ti-credit-card padd-r-10"></i><?=$value->gender;?>
									</div>
									<div class="col-sm-4 mrg-bot-10">
										<i class="ti-shield padd-r-10"></i><?=$value->skills;?>
									</div>
									<div class="col-sm-4 mrg-bot-10">
										<i class="ti-shield padd-r-10"></i><?=$value->content;?>
									</div>
								</div>
								
							</div>

						</div>

						<div class="detail-wrapper">
							<div class="detail-wrapper-header">
								<h4>Personal Objectivies</h4>

							</div>
							<div class="detail-wrapper-body">
								<p><?=$value->personal_objectivities;?></p>
								
							</div>
						</div>
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-header">
								<h4>Career Objectivies</h4>

							</div>
							<div class="detail-wrapper-body">
								<p><?=$value->career_objectivities;?></p>
								
							</div>
						</div>
						<?php } ?>
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-header">
								<h4>Education</h4>
							</div>
							<div class="detail-wrapper-body">
							<?php
							foreach($resume_education as $key=>$value) 
								{
								?>
								<div class="edu-history info">
									<i></i>
									<div class="detail-info">
										<h3><?=$value->my_university;?></h3>
										<h3><?=$value->qualification;?></h3>
										<i><?=$value->education_from;?> - <?=$value->education_to;?></i>
										<p><?php $date = $value->created_date;
											$newdate = date('d F Y', strtotime(str_replace('/', '-', $date)));
											echo $newdate;?></center></p>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-header">
								<h4>Work & Experience</h4>
							</div>
							<div class="detail-wrapper-body">
							<?php
							foreach($resume_workeducation as $key=>$value) 
								{
								?>
								<div class="edu-history info">
									<i></i>
									<div class="detail-info">
										<h3><?=$value->company;?></h3>
										<i><?=$value->experience_from;?> - <?=$value->experience_to;?></i>
										<p><?=$value->designation;?></p>
										<p><?php $date = $value->created_date;
											$newdate = date('d F Y', strtotime(str_replace('/', '-', $date)));
											echo $newdate;
								?></center></p>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					
					<!-- Sidebar -->
					<div class="col-md-4 col-sm-4">
						<div class="sidebar">
						
						
							<div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-location-pin padd-r-10"></i>Location</h4>
								</div>
								<div class="widget-boxed-body">
									<div class="side-list no-border">
										<ul>
											<li><i class="ti-world padd-r-10"></i><?=$value->city;?></li>
											<li><i class="ti-mobile padd-r-10"></i>91 <?=$value->contact;?></li>
											<li><i class="ti-email padd-r-10"></i><?=$value->email;?></li>
											<li><i class="ti-pencil-alt padd-r-10"></i><?=$value->qualification;?></li>
											<li><i class="ti-shield padd-r-10"></i> <?=$value->content;?></li>
										</ul>
										
									</div>
								</div>
							</div>
							<!-- End: Job Overview -->
							
							<!-- Start: Opening hour -->
							<!-- <div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-headphone padd-r-10"></i>Contact Now</h4>
								</div>
								<div class="widget-boxed-body">
									<form>
										<input type="text" class="form-control" placeholder="Enter your Name *">
										<input type="text" class="form-control" placeholder="Email Address*">
										<input type="text" class="form-control" placeholder="Phone Number">
										<textarea class="form-control height-140" placeholder="Message should have more than 50 characters"></textarea>
										
										<span>You accepts our <a href="#" title="">Terms and Conditions</a></span>
									</form>
								</div>
							</div> -->
						</div>
					</div>
				</div>
				<div class="row">
				</div>
			</div>
		</section>
		
		<!-- ====================== End Resume Detail ================ -->
		
		
		<!-- ================= footer start ========================= -->
<script type="text/javascript">

   
    	var url="<?php echo base_url();?>";
    	function delete_resume(id)
      	{
      		/*alert('ABC');*/
	        swal(
	        {
	            title: "Are you sure?",
	            text: "Are you sure you want to delete?",
	            icon: "warning",
	            buttons: true,
	            dangerMode: true,
	        })
	        .then((willDelete) => 
	        {
	            
	            if(willDelete)
	            {
	            	/*
	            	*/
	                window.location = url+"delete_resume/"+id;
	                swal("Your file is deleted!",
	                {
	                    icon: "success",
	                });
	        }
	        else
	        {
	            swal("Your file is safe!");
	        }
	        });
		}

   
</script>    		