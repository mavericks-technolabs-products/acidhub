
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Create Blog</h2>
					<p><a href="<?php echo base_url();?>employerprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Create Blog</p>
				</div>
			</div>
		</div>

		<section class="create-job">
			<div class="container">
				
					
					<!-- General Information -->
					<div class="box">
						<div class="box-header">
							<h4>Blog Information</h4>
						</div>
						<div class="box-body">
							<?php  echo form_open('backend/blogController/blog_insert',array('method'=>'POST','class'=>"form-horizontal",'enctype'=>"multipart/form-data"));?>
						
							<div class="row">
								<div class="col-sm-6">
									<label>Blog Author Name</label>
									<input type="text" name="blog_author_name" class="form-control" value="<?php echo set_value('blog_author_name')?>" onkeypress="return isCharacterKey(event)"
									onkeypress="return isNumberKey(event)"   required />
									<?php echo form_error('blog_author_name'); ?>
								</div>
								<div class="col-sm-6">
									<label>Blog title</label>
									<input type="text" name="blog_title" class="form-control" value="<?php echo set_value('blog_title')?>" onkeypress="return isNumberKey(event)"   required />
									<?php echo form_error('blog_title'); ?>
								</div>
							</div>
							<div class="row">
							    <div class="col-sm-6">
							        <label>Add some pics?:</label>
							        <input type="file" id="photos" name="blog_mult_img[]" accept="image/jpeg,image/gif,image/png,image/x-eps" multiple/>
							        <?php if($error=$this->session->flashdata('error')): ?>
                  						<div class="alert alert-danger">
                  						<?= $error;?></div>
                  							<?php endif; ?>
							<div class="row">
                                <div class="col-md-12">
							        <label>Image Gallery</label>
									<ul id="photos_clearing" class="clearing-thumbs" calss="form-control" height='200px' width='150px' data-clearing>
							      </ul>
							    </div>
							</div>
						</div>
					</div>
								
							<div class="row">
							  	<div class="col-sm-12 m-clear">
								<label>Blog Description</label>
								<textarea name="blog_description" id="my-info" name="blog_description" class="form-control" placeholder="" rows="4" cols="400"  value="<?php echo set_value('blog_description')?>" required/><?php echo set_value('blog_description')?></textarea>
								<?php echo form_error('blog_description', '<div class="errors">', '</div>');?>
								</div>
							</div>
						</div>
					</div>
				</div>
					
					
				    <div class="text-center">
						<button type="submit" class="btn btn-m theme-btn">Submit & Exit</button>
					</div>
		    	</form>
			</div>
		</div>
	</div>
</section>

<script  src="<?php echo base_url();?>assets/frontend/js/index.js"></script>
<script type="text/javascript">
	function isNumberKey(evt)
     {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
            return false;
            return true;
     }
     function isCharacterKey(evt)
		      {
		        var charCode = (evt.which) ? evt.which : event.keyCode
		        if (charCode==32 || (charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
		       		return true;
		        	return false;
		        
		      }
</script>