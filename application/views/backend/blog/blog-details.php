<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
.mySlides 
{
	display:none;
}
</style>
		<!-- ======================= End Navigation ===================== -->
			
			
		<!-- ======================= Start Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Blog Detail</h2>
					<p><a href="<?php echo base_url();?>employerprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Blog Detail</p>

                  </div>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
			
		<!-- ===================== Blogs In Grid ===================== -->
		<section>
			<div class="container">
				<div class="row">
						<?php $cnt=count($ans);
						if($ans) 
						$name=$this->session->userdata('firstname');
						{?>
					<!-- =============== Blog Detail ================= -->
					<div class="col-md-8 col-sm-12">
						<article class="blog-news detail-wrapper">
							<div class="full-blog">
							<figure class="img-holder">
								<div id="myCarousel" class="carousel slide" data-ride="carousel">
   								 <ol class="carousel-indicators" >
		     					 <?php 
									$i=0;
									$cnt=count($ans);
									/*print_r($cnt);exit();*/
									for ($i=0; $i <$cnt ; $i++)
									{
										if ($i==0) 
										{
											?>
												<li data-target="#myCarousel" data-slide-to="<?=$i;?>" class="active"></li>
											<?php
										}
										else
										{
											?>
												<li data-target="#myCarousel" data-slide-to="<?=$i;?>"></li>
											<?php
										}
									} 
								?>
 								</ol>

	    					<div class="carousel-inner">
       						<?php $i=0;
							$cnt=count($ans);
							/*print_r($cnt);exit();*/
							for ($i=0; $i <$cnt ; $i++)
							{
								if ($i==0) 
								{
									$img=$ans[$i]->blog_mult_img;
									?>
			           			<div class="item active">
			       			 <img src="<?php echo base_url();?>uploads/blogs/<?=$img?>" alt="" style="width:100%; max-height:400px;">
			           			</div>
							<?php
								}
								else
								{
									$img=$ans[$i]->blog_mult_img;
									?>
			         <div class="item">
			        <img src="<?php echo base_url();?>uploads/blogs/<?=$img?>" alt="" style="width:100%; max-height:400px;">
			         </div>
    
				      <?php
				         }
												
						} 
					?>
                   </div>

    
			   	<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			      <span class="glyphicon glyphicon-chevron-left"></span>
			      <span class="sr-only">Previous</span>
			    </a>
			    <a class="right carousel-control" href="#myCarousel" data-slide="next">
			      <span class="glyphicon glyphicon-chevron-right"></span>
			      <span class="sr-only">Next</span>
			    </a>
					</div>
							<div class="blog-post-date theme-bg">
										<?php $date = $ans[0]->created_date;
											$newdate = date('d F Y', strtotime(str_replace('/', '-', $date)));
											echo $newdate;
								?>
									</div>
								</figure>
							</div>
								
								<!-- Blog Content -->
								<div class="full blog-content">
									<p><strong>Author Name</strong> :<?=$ans[0]->blog_author_name;?></p>
									<div class="post-meta">By: <a href="#" class="author theme-cl"><?=$ans[0]->blog_title;?></a> | 
									 <ul class="nav navbar-nav navbar-right">
									<li><a href="<?php echo base_url();?>edit-blog/<?php echo $ans[0]->blog_id?>"><i class="fa fa-edit"></i></a></li>
     								  <?php 
                                         $blog_id = $ans[0]->blog_id; 
                                       ?> 
                                       <li><a id="<?php echo $ans[0]->blog_id?>" onclick="delete_blog(this.id)" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a></li>
     								</div>
								<div>
									<a href="blog-details"></h3></a><br>
										<div class="blog-text">
										<p><?=$ans[0]->blog_description;?></p>
										</div>
									<?php } ?>
									<!-- Blog Share Option -->
								</div>
								<!-- Blog Content -->
							</div>
						</article>
					</div>
					<!-- /.col-md-8 -->
					
					<!-- ===================== Blog Sidebar ==================== -->
					<div class="col-md-4 col-sm-12">
						<div class="sidebar">
						
							<div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-check-box padd-r-10"></i>Recent Blogs</h4>
								</div>
								<div class="widget-boxed-body padd-top-5">
									<div class="side-list">
										<ul class="side-blog-list">
											<li>
												<a href="#">
													<div class="blog-list-img">
														<img src="<?php echo base_url();?>assets/frontend/img/image-3.jpg" class="img-responsive" alt="">
													</div>
												</a>
												<div class="blog-list-info">
													<h5><a href="#" title="blog">Freel Documentry</a></h5>
													<div class="blog-post-meta">
														<span class="updated">Nov 26, 2017</span> | <a href="#" rel="tag">Documentry</a>					
													</div>
												</div>
											</li>
											
											<li>
												<a href="#">
													<div class="blog-list-img">
														<img src="<?php echo base_url();?>assets/frontend/img/image-4.jpg" class="img-responsive" alt="">
													</div>
												</a>
												<div class="blog-list-info">
													<h5><a href="#" title="blog">Preez Food Rock</a></h5>
													<div class="blog-post-meta">
														<span class="updated">Oct 10, 2017</span><a href="#" rel="tag">Food</a>					
													</div>
												</div>
											</li>
											
											<li>
												<a href="#">
													<div class="blog-list-img">
														<img src="<?php echo base_url();?>assets/frontend/img/image-1.jpg" class="img-responsive" alt="">
													</div>
												</a>
												<div class="blog-list-info">
													<h5><a href="#" title="blog">Cricket Buzz High</a></h5>
													<div class="blog-post-meta">
														<span class="updated">Oct 07, 2017</span> | <a href="#" rel="tag">Sport</a>					
													</div>
												</div>
											</li>
											
											<li>
												<a href="#">
													<div class="blog-list-img">
														<img src="<?php echo base_url();?>assets/frontend/img/image-5.jpg" class="img-responsive" alt="">
													</div>
												</a>
												<div class="blog-list-info">
													<h5><a href="#" title="blog">Tour travel Tick</a></h5>
													<div class="blog-post-meta">
														<span class="updated">Sep 27, 2017</span> | <a href="#" rel="tag">Travel</a>					
													</div>
												</div>
											</li>

										</ul>
									</div>
								</div>
							</div>
							<!-- End: Recent Listing -->
							
						</div>
					</div>
					
				</div>
				
			</div>
		</section>
		<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 2000); // Change image every 2 seconds'
   
}
</script>
		<!-- ===================== End Blogs In Grid ===================== -->
			
		<!-- ================= footer start ========================= -->
<script type="text/javascript">
  var url="<?php echo base_url();?>";
    function delete_blog(id)
      {
      		/*alert('ABC');*/
        swal(
        {
            title: "Are you sure?",
            text: "Are you sure you want to delete?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => 
        {
	            
	        if(willDelete)
	         {
	            window.location = url+"delete_blog/"+id;
	            swal("Your file is deleted!",
	             {
	                    icon: "success",
	             });
	         }
	        else
	        {
	            swal("Your file is safe!");
	        }
	    });
	 }
</script>    