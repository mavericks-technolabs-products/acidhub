<style type="text/css">
  .blog-avatar.text-center.blogimg img {
    max-width: 91px;
    border-radius: 50%;
    margin: 0 auto 5px;
    border: 4px solid rgba(255,255,255,1);
    box-shadow: 0 2px 10px 0 #d8dde6;
    -webkit-box-shadow: 0 2px 10px 0 #d8dde6;
    -moz-box-shadow: 0 2px 10px 0 #d8dde6;
    width: 100%;
    height: 100%;
    max-height: 100px;
}	
.blog-grid-box-img {
    height: 191px;
    max-height: 250px;
    overflow: hidden;
    display: flex;
    align-items: center;
}
</style>
		
		<!-- ======================= End Navigation ===================== -->
		
		
		<!-- ======================= Start Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Blog</h2>
					<p><a href="<?php echo base_url();?>employerprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Blog</p>
					<?php if($feedback=$this->session->flashdata('feedback')): ?>
                  <div class="alert alert-dismissible alert-danger">
                  <?= $feedback; ?>
                <?php endif; ?>
                </div>
                <?php if($error=$this->session->flashdata('error')): ?>
                  <div class="alert alert-dismissible alert-danger gg">
                  <?= $error; ?>
                  <?php endif; ?>
					<ul class="nav navbar-nav navbar-right">
					
						<li class="sign-up"><a class="btn-signup red-btn" href="create-blog" style="margin-right: 21px; padding: 13px 7px; color: white;background-color: #ff7c39 !important;"><span  style="padding: 7px;"></span>Create Blog</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ===================== Blogs In Grid ===================== -->
		
		<section>
			<div class="container">
				<div class="row">
					<?php
					foreach($ans as $key=>$value) 
					{
						$name=$this->session->userdata('firstname');
				   ?>
					<div class="col-md-4 col-sm-6">
						<div class="blog-box blog-grid-box">
							<div class="blog-grid-box-img">
								<img src="<?php echo base_url();?>uploads/blogs/<?=$value->blog_mult_img;?>" height="200px! important;" width="450px" class="img-responsive" alt="" />
							</div>
							
							<div class="blog-grid-box-content">
								<div class="blog-avatar text-center blogimg">
									<img src="<?php echo base_url();?>uploads/<?=$value->user_file;?>" class="img-responsive" alt="" />
									
									<p><strong>By</strong> <span class="theme-cl"><?=$name
									;?></span></p>
									<span class="theme-cl"><?=$value->type;?></span>
								</div>
								<p><center><?php $date = $value->created_date;
											$newdate = date('d F Y', strtotime(str_replace('/', '-', $date)));
											echo $newdate;
								?></center></p>
								<p><center><?=$value->blog_author_name;?></center></p>
								<h4><center><?=$value->blog_title;?></center></h4>
								<p><center><?php $a=word_limiter($value->blog_description,30);
								echo $a;
								?></center></p>
							
								<a href="blog-details/<?=$value->blog_id;?>" class="theme-cl" title="Read More..">Continue...</a>
							</div>
							
						</div>
					</div>
						<?php } ?>
					
				</div>
			</div>
		</section>
	
		<!-- ===================== End Blogs In Grid ===================== -->

		<!-- ================= footer start ========================= -->
		<?php
if($error = $this->session->flashdata('Feedback')) 
{
    ?>
    <script>

        swal("Good Job!", "<?php echo $this->session->flashdata('Feedback'); ?>", "success")



    </script>
    <?php
}
elseif($error = $this->session->flashdata('error')) {
    ?>
    <script>
        swal("Oops!", "<?php echo $this->session->flashdata('error'); ?>", "error")
        

    </script>
    <?php
}
?>
<?php
if($error = $this->session->flashdata('Delete')) 
{
    ?>
    <script>

        swal("Good Job!", "<?php echo $this->session->flashdata('Delete'); ?>", "success")



    </script>
    <?php
}
elseif($error = $this->session->flashdata('error')) {
    ?>
    <script>
        swal("Oops!", "<?php echo $this->session->flashdata('error'); ?>", "error")
        

    </script>
    <?php
}
?>
