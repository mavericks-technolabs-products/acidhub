
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Update Blog</h2>
					<p><a href="<?php echo base_url();?>employerprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Update Blog</p>
				</div>
			</div>
		</div>

		<section class="create-job">
			<div class="container">
				
					
					<!-- General Information -->
					<div class="box">
						<div class="box-header">
							<h4>Blog Information</h4>
						</div>
						<div class="box-body">
							 <form method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>backend/BlogController/update_blog/<?=$ans->blog_id?>">

							 	  <?php $cnt=count($ans); ?>
						
							<div class="row">
							    <div class="col-sm-6">
									<label>Blog Author Name</label>
									<input type="text" name="blog_author_name" class="form-control"  onkeypress="return isNumberKey(event)" required value="<?php echo $ans->blog_author_name;?>">
								</div>
								<div class="col-sm-6">
									<label>Blog title</label>
									<input type="text" name="blog_title" class="form-control"  onkeypress="return isNumberKey(event)" required value="<?php echo $ans->blog_title;?>">
								</div>
							</div>
								
							  
							<div class="row">
							    <div class="col-sm-6">
							     	<label>Add some pics?:</label>
							      	<input type="file" id="photos" name="blog_mult_img[]" accept="image/jpeg,image/gif,image/png,image/x-eps" multiple/>
							      	<div class="row">
                                    	<div class="col-md-12">
							       			<label>Image Gallery</label>
											<ul id="photos_clearing" class="clearing-thumbs" calss="form-control" height='200px' width='150px' data-clearing>
							      			</ul>
							    		</div>
							  		</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
										<?php foreach($blog_mult_img as $img){?>
											<div class="col-md-3">
										 		<a href="<?php echo base_url();?>blog-delete-image/<?=$ans->blog_id?>/<?=$img->blog_img_id?>" 
										 			class="btn btn-danger btn-xs">
										 		<i class="fa fa-trash-o"></i></a>  
										<?php
												echo "<img src='" . base_url()."uploads/blogs/".$img->blog_mult_img."'height='200px' width='150px'>";
										  ?>
										</div>
										<?php } ?>
								</div>
							</div>
							<br>
								
						    <div class="row">
							  	<div class="col-sm-12 m-clear">
									<label>Blog Description</label>
									<textarea name="blog_description" id="my-info" name="blog_description" class="form-control" placeholder="" rows="4" cols="400" required value="<?php echo $ans->blog_description;?>"><?php echo $ans->blog_description;?></textarea>
								</div>
							</div>
						</div>
					</div>
					
						<div class="text-center">
							<button type="submit" class="btn btn-m theme-btn">Update & Exit</button>
						</div>
		   			 </form>
				</div>
			</div>
		</div>
	</section>
<script type="text/javascript">
    function readURL(input) 
     {
        if (input.files && input.files[0]) 
        {
        var reader = new FileReader();

        reader.onload = function (e) 
        {
        $('#blah')
        .attr('src', e.target.result)
        .width(150)
        .height(200);
        };

        reader.readAsDataURL(input.files[0]);
        }
      }
    function isNumberKey(evt)
     {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (!(charCode > 31 && (charCode < 48 || charCode > 57)))
            return false;
            return true;
     }
     function isCharacterKey(evt)
		      {
		        var charCode = (evt.which) ? evt.which : event.keyCode
		        if (charCode==32 || (charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
		       		return true;
		        	return false;
		        
		      }
</script>
 
		