
		<!-- ======================= End Navigation ===================== -->
			
			
		<!-- ======================= Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Manage jobs</h2>
					<p><a href="<?php echo base_url();?>home" title="Home">Home</a> <i class="ti-arrow-right"></i> Manage jobs</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ================ Office Address ======================= -->
		<section>
			<div class="container">
				<div class="row">
				<?php
					foreach($ans as $key=>$value) 
					{?>
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type full-type"><?=$value->job_type;?></span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox">
									<i class="fa fa-check"></i>
								</label>
							</div>
							
							<div class="job-featured"></div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="#">
										<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-6.png" alt="">
									</a>
								</div>
								<h5><a href="#"><?=$value->job_title;?></a></h5>
								<h5><?=$value->type;?></h5>
								<p class="text-muted"><?=$value->skills;?></p>
								<p class="text-muted"><?=$value->gender;?></p>
								<span class="theme-cl"><?=$value->package;?></span>
								<p class="text-muted"><?php $date = $value->created_date;
											$newdate = date('d F Y', strtotime(str_replace('/', '-', $date)));
											echo $newdate;
								?></p>
							</div>
							
							<div class="job-type-grid">
								<a href="jobdetails/<?=$value->job_id;?>" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					<?php } ?>
					<!-- Single Job -->
					
					
				</div>
				<!-- Row -->
				
				
				
			</div>
		</section>
		