<style type="text/css">
.grid-job-widget {
  padding: 13px!important;
                }
</style>
		<!-- ======================= End Navigation ===================== -->
			
			
		<!-- ======================= Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Interior Detail</h2>
					<p><a href="<?php echo base_url();?>home" title="Home">Home</a> <i class="ti-arrow-right"></i> Interior Detail</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ================ Employer Profile ======================= -->
		<section>
			<div class="container">
				
				<div class="col-md-3 col-sm-3">
					<div class="emp-pic">
						<img src="<?php echo base_url();?>uploads/<?=$ans->user_file;?>" class="img-responsive" alt="" />
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4">
					<div class="emp-des">
						<h3><?=$ans->firstname;?></h3>
						<span class="theme-cl"><?=$ans->contact;?></span>
						<p><?=$ans->firm_address;?> </p>
					</div>
					
				</div>
				
				<div class="col-md-5 col-sm-5">
					<div class="emp-detail">
						<ul>
							<li><span class="cl-danger"><?php  
							$id=$ans->user_id;
							$count = $this->AcidModel->job_count($id); ?>
										 	<?php echo $count; ?></span>Jobs</li>
							<li><span class="cl-success"><?php  
							$id=$ans->user_id;
							$count = $this->AcidModel->active_job_count($id); ?>
							<?php echo $count; ?></span>Active Jobs</li>
							<!-- <li><span class="cl-primary">120</span>Followers</li> -->
						</ul>
					</div>
				</div>
				
			</div>
		</section>
		<!-- ================ End Employer Profile ======================= -->
		
		<!-- ================ Employers Jobs ======================= -->
		<section class="padd-top-20">
			<div class="container">
			
				<div class="row">
					
					<div class="col-md-12 col-sm-12">
						<div class="short">
							
						</div>
					</div>					
				</div>
				<div class="row">
						 <?php $cnt=count($ans);
					           /*print_r($cnt);
					           exit();*/
					           ?>
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						<div class="job-like">
						</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="#">
										<img src="<?php echo base_url();?>uploads/<?=$ans->user_file;?>" class="img-responsive" alt="" style="border-radius: 50%;"/>
									</a>
								</div>
								<h5><a href="#"><?=$ans->firstname;?></a></h5>
								<p class="text-muted"><?=$ans->email;?></p>
							    <p class="text-muted"><?=$ans->gender;?></p>
								<p class="text-muted"><?=$ans->firm_address;?></p>
							</div>
							
							<div class="job-type-grid">
								<a href="<?php echo base_url();?>interior-project/<?=$ans->user_id;?>" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							<?php  ?>
						</div>
					</div>
					
					<!-- Single Job -->
				</div>
			</div>
		</section>
		