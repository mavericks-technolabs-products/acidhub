
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ======================= Start Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption text-center">
					<h2>Job In Grid</h2>
					<p><a href="<?php echo base_url();?>home" title="Home">Home</a> <i class="ti-arrow-right"></i> Job Layout One</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ======================= Search Filter ===================== -->
		<section class="padd-0 ">
			<div class="container">
				<div class="col-md-12">
					<form>
						<fieldset class="search-form">
						
							<div class="col-md-4 col-sm-4 padd-0">
								<input type="text" class="form-control" placeholder="Skills, Designation, Companies" />
							</div>
								
							<div class="col-md-3 col-sm-3 padd-0">
								<select class="wide form-control">
									<option data-display="Location">All Country</option>
									<option value="1">Allahabad</option>
									<option value="2">India</option>
									<option value="3" disabled>Australia</option>
									<option value="4">United State</option>
								</select>
							</div>
								
							<div class="col-md-3 col-sm-3 padd-0">
								<select class="wide form-control">
									<option data-display="Category">Show All</option>
									<option value="1">Web Design</option>
									<option value="2">Accountant</option>
									<option value="3" disabled>Marketting</option>
									<option value="4">Farmer</option>
								</select>
							</div>
								
							<div class="col-md-2 col-sm-2 padd-0 m-clear">
								<button type="submit" class="btn theme-btn full-width height-50 radius-0">FIND JOB</button>
							</div>
								
						</fieldset>
					</form>
				</div>
			</div>
		</section>
		<!-- ======================= Search Filter ===================== -->
		
		<!-- ====================== Start Job Detail 2 ================ -->
		<section class="padd-top-20">
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-sm-5">
						<h4>98 Jobs & Vacancies</h4>
					</div>
					<div class="col-sm-7">
						
						<div class="fl-right">
							<div class="search-wide">
								<h5>Short By</h5>
							</div>
							
							<div class="search-wide full">
								<select class="wide form-control">
									<option value="1">Most Recent</option>
									<option value="2">Most Viewed</option>
									<option value="4">Most Search</option>
								</select>
							</div>
							
							<div class="search-wide full">
								<select class="wide form-control">
									<option>10 Per Page</option>
									<option value="1">20 Per Page</option>
									<option value="2">30 Per Page</option>
									<option value="4">50 Per Page</option>
								</select>
							</div>
						</div>
						
					</div>
				</div>
				<!-- End Row -->
				
				<!-- row -->
				<div class="row mrg-top-20">
					
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type full-type">Full Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox" checked>
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-1.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">Product Redesign</a></h5>
								<p class="text-muted">2708 Scenic Way, Sutter, IL 62373</p>
							</div>
							
							<div class="job-type-grid">
								<a href="job-detail.html" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type full-type">Full Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox">
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-2.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">New Product Mockup</a></h5>
								<p class="text-muted">2708 Scenic Way, Sutter, IL 62373</p>
							</div>
							
							<div class="job-type-grid">
								<a href="job-detail.html" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type part-type">Full Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox" checked>
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-3.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">Custom Php Developer</a></h5>
								<p class="text-muted">3765 C Street, Worcester</p>
							</div>
							
							<div class="job-type-grid">
								<a href="job-detail.html" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type part-type">Part Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox">
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-4.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">Wordpress Developer</a></h5>
								<p class="text-muted">2719 Duff Avenue, Winooski</p>
							</div>
							
							<div class="job-type-grid">
								<a href="job-detail.html" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type internship-type">Internship</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox" checked>
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-5.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">Web Maintenence</a></h5>
								<p class="text-muted">2708 Scenic Way, Sutter, IL 62373</p>
							</div>
							
							<div class="job-type-grid">
								<a href="job-detail.html" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type part-type">Part Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox">
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-6.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">Photoshop Designer</a></h5>
								<p class="text-muted">2865 Emma Street, Lubbock</p>
							</div>
							
							<div class="job-type-grid">
								<a href="job-detail.html" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type full-type">Full Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox">
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-7.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">HTML5 & CSS3 Coder</a></h5>
								<p class="text-muted">2719 Burnside Avenue, Logan</p>
							</div>
							
							<div class="job-type-grid">
								<a href="job-detail.html" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type part-type">Part Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox" checked>
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-8.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">.Net Developer</a></h5>
								<p class="text-muted">3815 Forest Drive, Alexandria</p>
							</div>
							
							<div class="job-type-grid">
								<a href="job-detail.html" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					
				</div>
				<!-- End Row -->
				
			</div>
		</section>
		
		<!-- ====================== End Job Detail 2 ================ -->
		
		
		<!-- ================= footer start ========================= -->
		