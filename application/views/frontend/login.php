<div class="page-title">
	<div class="container">
		<div class="page-caption">
			<h2>Login</h2>
			<a href="<?php echo base_url();?>home" title="Home">Home</a> <i class="ti-arrow-right"></i> Login
			 <div class="alert pull-right">
				
				<?php if($feed=$this->session->flashdata('feed')): ?>
          		<div class="alert alert-dismissible alert-success">
              		<?= $feed; ?>
           			<?php endif; ?>
        		</div>
        	</div>
		</div>
	</div>
</div>	


<!-- <img src="<?php echo base_url();?>assets/img/reset.jpeg"> -->

			<div class="modal-dialog">
				<div class="modal-content" id="myModalLabel1">
					<div class="modal-body">
						<div class="text-center"><img src="<?php echo base_url();?>assets/frontend/img/logo.png" class="img-responsive" alt=""></div>
						
						<!-- Nav tabs -->
						<ul class="nav nav-tabs nav-advance theme-bg" role="tablist">
							<li class="nav-item active">
								<a class="nav-link" data-toggle="tab" href="#employer" role="tab">
								<i class="ti-user"></i> Employer</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#candidate" role="tab">
								<i class="ti-user"></i> Candidate</a>
							</li>
						</ul>
						<!-- Nav tabs -->
							
						<!-- Tab panels -->
						<div class="tab-content">
						
							<!-- Employer Panel 1-->
							<div class="tab-pane fade in show active" id="employer" role="tabpanel">
							  <form method="POST" action="<?php echo base_url();?>LoginController/login_valid">
								<?php if($error=$this->session->flashdata('login_failed')): ?>
                  						<div class="alert alert-danger">
                  						<?= $error;?></div>
                  							<?php endif; ?>

								<div class="form-group">
										<label>Email</label>
										<input type="text" name="email" class="form-control" placeholder="User Name" >
										<?php echo form_error('email'); ?>
								</div>
									
								<div class="form-group">
										<label>Password</label>
										<input type="password" name="password" class="form-control" placeholder="*********">
										<?php echo form_error('password'); ?>
								</div>
									
								<div class="form-group">
										<span class="custom-checkbox">
											<input type="checkbox" id="44">
											<label for="44"></label>Remember me
										</span>
										<a href="" title="Forget" class="fl-right">Forgot Password?</a>
								</div>
								<div class="form-group text-center">
										<button type ="submit" class="btn theme-btn full-width btn-m">LogIn </button>
								</div>
 							</form>
								
								<div class="log-option"><span>OR</span></div>
								
								<div class="form-group text-center">
										<a href="signup" class="btn theme-btn full-width btn-m">Create New Account </a>
								</div>
					
							</div>
				
						<!-- Tab panels -->
					</div>
				</div>
			</div>
		</div>   
<script> 
swal(
    {
        title: "Good job!",
        text: "Your account has been successfully!",
        icon: "success",
        buttons: true,
    })
    .then((value) => 
    {
                                
        if (value)
        {
        	window.location.href = <?php echo base_url();?>login;
        }
    });     
</script>  
<?php
if($error = $this->session->flashdata('Feed')) 
{
    ?>
    <script>

        swal("Good Job!", "<?php echo $this->session->flashdata('Feed'); ?>", "success")



    </script>
    <?php
}
elseif($error = $this->session->flashdata('error')) {
    ?>
    <script>
        swal("Oops!", "<?php echo $this->session->flashdata('error'); ?>", "error")
        

    </script>
    <?php
} 
elseif($notice = $this->session->flashdata('notice')) {
    ?>
    <script>
        swal({
  icon: "warning",
  title: 'Oops...',
  text: "<?php echo $this->session->flashdata('notice'); ?>"
})

    </script>
    <?php
}
?>