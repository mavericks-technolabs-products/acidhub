<style type="text/css">
	.buttons {
    font-size: 13px;
    font-weight: 400;
    padding: 11px 12px;
    display: inline-block;
    overflow: hidden;
    border-radius: 1px;
    line-height: 16px;
    transition: all .2s ease;
    max-width: 100% !important;
}
.card-img-bottom:hover
{
	opacity: 0.7;
}
.pqr 
{
    padding: 5px;
    width: 100%;
}

.pqr:hover {
    box-shadow: 0 0 10px 5px rgba(0, 140, 186, 0.5);
}


.mySlides {display:none;}
</style>
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ======================= Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Project</h2>
					<p><a href="<?php echo base_url();?>employerprofile" title="Home">Home</a> <i class="ti-arrow-right"></i> Project</p>

					<div class="cover-buttons">
						<ul>
						<li><div class="buttons medium button-plain "></i></div></li>
						<li><div class="buttons medium button-plain "></i></div></li>
						<li><a href="#add-review" class="buttons theme-btn"><i class="fa fa-paper-plane"></i><span class="hidden-xs"></span>Response</span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	
		<!-- ======================= End Page Title ===================== -->
		
		
		<!-- ====================== Start Job Detail 2 ================ -->
		<section>
			<div class="container">
				<!-- row -->
				<div class="row">
					 <?php $cnt=count($ans);
					           /*print_r($cnt);
					           exit();*/
					           ?>
					<div class="col-md-8 col-sm-8">
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-body">
							
								<div class="text-center mrg-bot-10">
									<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    							<ol class="carousel-indicators" >
		     					 <?php 
									$i=0;
									$cnt=count($ans);
									
									for ($i=0; $i <$cnt ; $i++)
									{
										if ($i==0) 
										{
											?>
												
												 <li data-target="#myCarousel" data-slide-to="<?=$i;?>" class="active"></li>
											<?php
										}
										else
										{
											?>
												
												  <li data-target="#myCarousel" data-slide-to="<?=$i;?>"></li>
											<?php
										}
										
									} 
									?>
 								</ol>
    
					      <div class="carousel-inner">
					      	  <?php 
							$i=0;
							$cnt=count($ans);
							/*print_r($cnt);exit();*/
							for ($i=0; $i <$cnt ; $i++)
							{
								if ($i==0) 
								{
									$img=$ans[$i]->user_file;
									?>
			           <div class="item active">
			       			 <img src="<?php echo base_url();?><?=$img?>" alt="Los Angeles" style="width:50%; height:300px!important;">
			           </div>
							<?php
								}
								else
								{
									$img=$ans[$i]->user_file;
									?>
			         <div class="item">
			        <img src="<?php echo base_url();?><?=$img?>" alt="Chicago" style="width:50%; height:300px!important;">
			         </div>
    
				      <?php
				         }
												
								} 
											?>
					    </div>

					    <!-- Left and right controls -->
					    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
					      <span class="glyphicon glyphicon-chevron-left"></span>
					      <span class="sr-only">Previous</span>
					    </a>
					    <a class="right carousel-control" href="#myCarousel" data-slide="next">
					      <span class="glyphicon glyphicon-chevron-right"></span>
					      <span class="sr-only">Next</span>
					    </a>
					</div>
					</div>
								
								
							</div>
						</div>
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-header">
								<h4>Overview</h4>
							</div>
							<div class="detail-wrapper-body">
								<p><?=$ans[0]->project_description;?></p>
							</div>
						</div>
						
						
					</div>
					
					<!-- Sidebar -->
					<div class="col-md-4 col-sm-4">
						<div class="sidebar">
						
					
							<!-- Start: Job Overview -->
							<div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-location-pin padd-r-10"></i>Location</h4>
								</div>
								<div class="widget-boxed-body">
									<div class="side-list no-border">
										<ul>
											<li><i class="ti-location-pin mrg-r-5"></i><?=$ans[0]->project_location;?></li>
										    <li><i class="ti-shield padd-r-10"></i><?=$ans[0]->sub_region;?></li>
										</ul>
									</div>
								</div>
							</div>
							<!-- End: Job Overview -->
						</div>
					</div>
					<!-- End Sidebar -->
				</div>
				<!-- End Row -->
				
			</div>
		</section>
		<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 2000); // Change image every 2 seconds'
   
}
</script>
		<!-- ====================== End Job Detail 2 ================ -->
		
		
		<!-- ================= footer start ========================= -->
		