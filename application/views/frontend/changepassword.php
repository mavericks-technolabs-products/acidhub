 <div class="page-title">
	<div class="container">
		<div class="page-caption">
			<h2>Login</h2>
			<a href="<?php echo base_url();?>home" title="Home">Home</a> <i class="ti-arrow-right"></i> Login
			 <div class="alert pull-right">
				<!-- <strong>Success!</strong> Your Account Has Been Created Successfully! -->
				<?php if($feed=$this->session->flashdata('feed')): ?>
          		<div class="alert alert-dismissible alert-success">
              		<?= $feed; ?>
           			<?php endif; ?>
        		</div>
        	</div>
		</div>
	</div>
</div>	


<!-- <img src="<?php echo base_url();?>assets/img/reset.jpeg"> -->

			<div class="modal-dialog">
				<div class="modal-content" id="myModalLabel1">
					<div class="modal-body">
						<div class="text-center"><img src="<?php echo base_url();?>assets/frontend/img/logo.png" class="img-responsive" alt=""></div>
						
						<!-- Nav tabs -->
						<ul class="nav nav-tabs nav-advance theme-bg" role="tablist">
							<li class="nav-item active">
								<a class="nav-link" data-toggle="tab" href="#employer" role="tab">
								<i class="ti-user"></i> Employer</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#candidate" role="tab">
								<i class="ti-user"></i> Candidate</a>
							</li>
						</ul>
						<!-- Nav tabs -->
							
						<!-- Tab panels -->
						<div class="tab-content">
						
							<!-- Employer Panel 1-->
							<div class="tab-pane fade in show active" id="employer" role="tabpanel">
							  <form method="POST" action="<?php echo base_url();?>LoginController/updatechangepassword_query">

								<div class="row">
									<div class="form-group">
										<label>New password</label>
										 <input class="form-control input-group-lg" type="password" name="password" title="Enter password" placeholder="New password" id="password"/>
									</div>
								</div>
								
								<div class="row">	
									<div class="form-group">
										<label>Confirm password</label>
										  <input class="form-control input-group-lg" type="Confirm Password" name="confirmpassword" title="Enter Confirm  password" placeholder="Confirm password" id="confirmpassword" onBlur="match_password()"/>
									</div>
								</div>
									
								  <div id="msg"></div>
                    				<p><a href="#">Forgot Password?</a></p>
                    				<button class="btn btn-primary">Update Password</button>
                  					</form>
                  				</div>
                  			</div>
 						</div>
					</div>
				</div>
		
 <script type="text/javascript">
      function match_password()
      {
        // alert("hi");
        var password=document.getElementById("password").value;
        //alert(password);
        var confirmpassword=document.getElementById("confirmpassword").value;
         //alert(confirmpassword);
            if(password!=confirmpassword)
            {
              document.getElementById("msg").innerHTML =("password does not match");
              document.getElementById("confirmpassword").value='';
              // alert("password does not match");
            }
            else
            {
              document.getElementById("msg").innerHTML =("password match");
            }
      }
    </script>   
