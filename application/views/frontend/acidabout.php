
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ======================= Start Freelancer Banner ===================== -->
		<section class="freelancer banner" style="background:url(<?php echo base_url();?>assets/frontend/img/banner.jpg);">
			<div class="container">
				<div class="col-md-7 col-sm-7">				
					<div class="caption">
						<h2>Get it done with a <br>freelancer</h2>
						<p>Grow your business with the top freelancing website.</p>
						<div class="input-group">
							<input type="text" class="form-control no-br" placeholder="What type of work do you need?">
							<span class="input-group-btn">
								<button type="button" class="btn banner-btn">Get Started</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- ======================= End Banner ===================== -->
		
		
		
		<!-- =================== Newsletter ==================== -->
		<section class="newsletter" style="background-image:url(<?php echo base_url();?>assets/frontend/img/trans-bg.png);">
			<div class="container">
				<div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
					<div class="newsletter-box text-center">
						<div class="input-group">
							<span class="input-group-addon"><span class="ti-email theme-cl"></span></span>
							<input type="text" class="form-control" placeholder="Enter your Email..">
						</div>
						<a href="<?php echo base_url();?>login"><button type="submit" class="btn theme-btn btn-radius btn-m">Login</button></a>
					</div>
				</div>
			</div>
		</section>
		<!-- =================== End Newsletter ==================== -->
		
		
		