
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ======================= Start Banner ===================== -->
		<div class="main-banner" style="background-image:url(<?php echo base_url();?>assets/frontend/img/slider-2.jpg);">
			<div class="container">
				<div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1">
				
					<div class="caption text-center cl-white">
						<h2>Find Your Career. You Deserve it.</h2>
						<!-- <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias</p> -->
					</div>
					
					<!-- <form>
						<fieldset class="home-form-1 icon-field">
						
							<div class="col-md-4 col-sm-4 padd-0">
								<div class="form-group">
									<div class="icon-addon addon-lg">
										<input type="text" class="form-control br-1" placeholder="Skills, Designation, Companies" />
										<label class="fa fa-pencil"></label>
									</div>
								</div>
								
							</div>
								
							<div class="col-md-3 col-sm-3 padd-0">
								<div class="form-group">
									<div class="icon-addon addon-lg">
										<select class="wide form-control br-1">
											<option data-display="Location">All Country</option>
											<option value="1">Allahabad</option>
											<option value="2">India</option>
											<option value="3" disabled>Australia</option>
											<option value="4">United State</option>
										</select>
										<label class="fa fa-map-marker"></label>
									</div>
								</div>
							</div>
								
							<div class="col-md-3 col-sm-3 padd-0">
								<div class="form-group">
									<div class="icon-addon addon-lg">
										<select class="wide form-control">
											<option data-display="Category">Show All</option>
											<option value="1">Web Design</option>
											<option value="2">Accountant</option>
											<option value="3" disabled>Marketting</option>
											<option value="4">Farmer</option>
										</select>
										<label class="fa fa-crosshairs"></label>
									</div>
								</div>
							</div>
								
							<div class="col-md-2 col-sm-2 padd-0">
								<button type="submit" class="btn theme-btn cl-white seub-btn">FIND JOB</button>
							</div>
								
						</fieldset>
					</form> -->
					
					<div class="text-center">
						<!-- <div class="choose-opt">
							<div class="choose-opt-box"><span>OR</span></div>
						</div> -->
						<a href="<?php echo base_url();?>login" class="btn theme-btn btn-m mrg-5"><span class="ti-upload padd-r-5"></span>Upload Your CV</a>
						<a href="<?php echo base_url();?>login" class="btn btn-hiring btn-m mrg-5"><span class="ti-briefcase padd-r-5"></span>Post a Job for Free</a>
					</div>
					
				</div>
			</div>
		</div>
		<!-- ======================= End Banner ===================== -->
		
		<!-- ====================== Advance Features ================= -->
		<section>
			<div class="container">
				
				<div class="row" data-aos="fade-up">
					<div class="col-md-12">
						<div class="heading">
							<h2>Advance Features</h2>
							<p>Post A Job To Tell Us About Your Project. We'll Quickly Match You With The Right Freelancers.</p>
						</div>
					</div>
				</div>
				
				<div class="row">
					
					<!-- Single Features -->
					<div class="col-md-4 col-sm-4">
						<div class="features-box">
							<i class="theme-cl ti-headphone-alt" aria-hidden="true"></i>
							<h4>24x7 Fully Support</h4>
							<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						</div>
					</div>
					
					<!-- Single Features -->
					<div class="col-md-4 col-sm-4">
						<div class="features-box">
							<i class="theme-cl ti-briefcase" aria-hidden="true"></i>
							<h4>Accountants & consultants</h4>
							<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						</div>
					</div>
					
					<!-- Single Features -->
					<div class="col-md-4 col-sm-4">
						<div class="features-box">
							<i class="theme-cl ti-paint-bucket" aria-hidden="true"></i>
							<h4>Designers & Creatives</h4>
							<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						</div>
					</div>
					
				</div>
				
			</div>
		</section>
		
		<!-- ================= Employer & Candidate Register Area ========================= -->
		<section class="padd-0">
			<div class="container-fluid padd-0">
			
				<div class="col-md-6 col-sm-6 padd-0">
					<div class="half-box employer-box text-center" style="background-image:url(<?php echo base_url();?>assets/frontend/img/employer.jpg);">
						<h2>Join With Acid Hub</h2>
						<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						<a href="signup" class="btn theme-btn btn-radius btn-m">Create Account</a>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 padd-0">
					<div class="half-box candidate-box text-center" style="background-image:url(<?php echo base_url();?>assets/frontend/img/candidate.jpg);">
						<h2>Join With Acid Hub</h2>
						<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						<a href="signup" class="btn light-btn btn-radius btn-m">Create Account</a>
					</div>
				</div>
				
			</div>
		</section>
		<!-- ================= Employer & Candidate Register Area ========================= -->
		
		<!-- ======================= All Jobs ==================================== -->
		
		<!-- ====================== End All jobs ============================ -->
		
		<!-- ====================== Tag Section ============================ -->
		<section>
			<div class="container">
			
				<div class="row" data-aos="fade-up">
					<div class="col-md-12">
						<div class="heading">
							<h2>Latest Employers</h2>
							<p>Each month, more than 7 million Jobhunt turn to website in their search for work, making over<br>160,000 applications every day.</p>
						</div>
					</div>
				</div>
				
				<div class="row" data-aos="fade-up">
					<div class="employer-slide">
					
						<!-- Single Employer -->
						<div class="employer-widget">
							<div class="u-content">
								<div class="avatar box-80">
									<a href="interior"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-2.png" alt=""></a>
								</div>
								<h5><a href="<?php echo base_url();?>interior">Google Inc</a></h5>
								<p class="text-muted">2708 Scenic Way, Sutter, IL 62373</p>
							</div>
							<div class="job-type-grid">
								<a href="<?php echo base_url();?>interior" class="employer-browse-btn btn-radius br-light">view</a>
							</div>
						</div>
						
						<!-- Single Employer -->
						<div class="employer-widget">
							<div class="u-content">
								<div class="avatar box-80">
									<a href="<?php echo base_url();?>interior"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-1.png" alt=""></a>
								</div>
								<h5><a href="<?php echo base_url();?>interior">Apple</a></h5>
								<p class="text-muted">2708 Scenic Way, Sutter, IL 62373</p>
							</div>
							<div class="job-type-grid">
								<a href="<?php echo base_url();?>interior" class="employer-browse-btn btn-radius br-light">view</a>
							</div>
						</div>
						
						<!-- Single Employer -->
						<div class="employer-widget">
							<div class="u-content">
								<div class="avatar box-80">
									<a href="interior"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-3.png" alt=""></a>
								</div>
								<h5><a href="<?php echo base_url();?>interior">Core PHP</a></h5>
								<p class="text-muted">3765 C Street, Worcester</p>
							</div>
							<div class="job-type-grid">
								<a href="<?php echo base_url();?>interior" class="employer-browse-btn btn-radius br-light">view</a>
							</div>
						</div>
						
						<!-- Single Employer -->
						<div class="employer-widget">
							<div class="u-content">
								<div class="avatar box-80">
									<a href="<?php echo base_url();?>interior"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-4.png" alt=""></a>
								</div>
								<h5><a href="<?php echo base_url();?>interior">Indiso Ltd</a></h5>
								<p class="text-muted">2719 Duff Avenue, Winooski</p>
							</div>
							<div class="job-type-grid">
								<a href="<?php echo base_url();?>interior" class="employer-browse-btn btn-radius br-light">view</a>
							</div>
						</div>
						
						<!-- Single Employer -->
						<div class="employer-widget">
							<div class="u-content">
								<div class="avatar box-80">
									<a href="<?php echo base_url();?>interior"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-5.png" alt=""></a>
								</div>
								<h5><a href="<?php echo base_url();?>interior">Likvi Soft</a></h5>
								<p class="text-muted">2708 Scenic Way, Sutter, IL 62373</p>
							</div>
							<div class="job-type-grid">
								<a href="<?php echo base_url();?>interior" class="employer-browse-btn btn-radius br-light">view</a>
							</div>
						</div>
						
						<!-- Single Employer -->
						<div class="employer-widget">
							<div class="u-content">
								<div class="avatar box-80">
									<a href="<?php echo base_url();?>interior"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-6.png" alt=""></a>
								</div>
								<h5><a href="<?php echo base_url();?>interior">Divargo Pvt</a></h5>
								<p class="text-muted">2865 Emma Street, Lubbock</p>
							</div>
							<div class="job-type-grid">
								<a href="<?php echo base_url();?>interior" class="employer-browse-btn btn-radius br-light">view</a>
							</div>
						</div>
						
						<!-- Single Employer -->
						<div class="employer-widget">
							<div class="u-content">
								<div class="avatar box-80">
									<a href="<?php echo base_url();?>interior"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-7.png" alt=""></a>
								</div>
								<h5><a href="<?php echo base_url();?>interior">MicroSoft Ltd</a></h5>
								<p class="text-muted">2719 Burnside Avenue, Logan</p>
							</div>
							<div class="job-type-grid">
								<a href="<?php echo base_url();?>interior" class="employer-browse-btn btn-radius br-light">view</a>
							</div>
						</div>
						
						<!-- Single Employer -->
						<div class="employer-widget">
							<div class="u-content">
								<div class="avatar box-80">
									<a href="<?php echo base_url();?>interior"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-8.png" alt=""></a>
								</div>
								<h5><a href="<?php echo base_url();?>interior">Sivtrixi Inc</a></h5>
								<p class="text-muted">3815 Forest Drive, Alexandria</p>
							</div>
							<div class="job-type-grid">
								<a href="<?php echo base_url();?>interior" class="employer-browse-btn btn-radius br-light">view</a>
							</div>
						</div>
						
						<!-- Single Employer -->
						<div class="employer-widget">
							<div class="u-content">
								<div class="avatar box-80">
									<a href="<?php echo base_url();?>interior"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/img/c-9.png" alt=""></a>
								</div>
								<h5><a href="<?php echo base_url();?>interior">Google Inc</a></h5>
								<p class="text-muted">2708 Scenic Way, Sutter, IL 62373</p>
							</div>
							<div class="job-type-grid">
								<a href="<?php echo base_url();?>interior" class="employer-browse-btn btn-radius br-light">view</a>
							</div>
						</div>
						
					</div>
				</div>
				
			</div>
		</section>
		<!-- =================== End Tag Section ==================== -->
		
		<!-- ================ Opening job Section ============================== -->
		<section class="tag-sec" style="background:url(<?php echo base_url();?>assets/frontend/img/slider-01.jpg);">
			<div class="container">
				<div class="col-md-10 col-md-offset-1">
					<div class="tag-content">
						<img src="<?php echo base_url();?>assets/frontend/img/s-logo.png" class="img-responsive" alt="" />
						<h2>Hello favourite day</h2>
						<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa</p>
						<a href="#" class="btn theme-btn btn-radius" title="">EXPLORE US<i class="ti-shift-right"></i></a>
					</div>
				</div>
			</div>
		</section>
		<!-- ================ End Opening job Section ============================== -->
		
		<!-- =================== Newsletter ==================== -->
		<section class="newsletter" style="background-image:url(<?php echo base_url();?>assets/frontend/img/trans-bg.png);">
			<div class="container">
				<div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
					<div class="newsletter-box text-center">
						<!-- <div class="input-group">
							<span class="input-group-addon"><span class="ti-email theme-cl"></span></span>
							<input type="text" class="form-control" placeholder="Enter your Email..">
						</div> -->
						<button type="button" class="btn theme-btn btn-radius btn-m">subscribe Me!</button>
					</div>
				</div>
			</div>
		</section>
		<!-- =================== End Newsletter ==================== -->
		
		
		<!-- ================= footer start ========================= -->
		