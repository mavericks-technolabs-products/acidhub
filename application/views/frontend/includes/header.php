<!DOCTYPE html>

<!--[if gt IE 8]><!-->
<html class="no-js" lang="zxx">
	
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="robots" content="index,follow">
	<title>ACID Vishwa - Job Portal Template</title>

    <!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/plugins/bootstrap/css/bootstrap.min.css">
	
	<!-- Bootstrap Select Option css -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/plugins/bootstrap/css/bootstrap-select.min.css">
	
    <!-- Icons -->
    <link href="<?php echo base_url();?>assets/frontend/plugins/icons/css/icons.css" rel="stylesheet">
    
    <!-- Animate -->
    <link href="<?php echo base_url();?>assets/frontend/plugins/animate/animate.css" rel="stylesheet">
    
    <!-- Bootsnav -->
    <link href="<?php echo base_url();?>assets/frontend/plugins/bootstrap/css/bootsnav.css" rel="stylesheet">
	
	<!-- Nice Select Option css -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/plugins/nice-select/css/nice-select.css">
	
	<!-- Aos Css -->
    <link href="<?php echo base_url();?>assets/frontend/plugins/aos-master/aos.css" rel="stylesheet">

	<!-- Slick Slider -->
    <link href="<?php echo base_url();?>assets/frontend/plugins/slick-slider/slick.css" rel="stylesheet">	
    
    <!-- Custom style -->
    <link href="<?php echo base_url();?>assets/frontend/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/frontend/css/responsiveness.css" rel="stylesheet">
	
	<!-- Custom Color -->
    <link href="<?php echo base_url();?>assets/frontend/css/skin/default.css" rel="stylesheet">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
              <!--   ************ end file marquee tag ********* -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" 
    crossorigin="anonymous"></script>
 	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.clearing.js'></script>
	<script src='http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>
	<script  src="<?php echo base_url();?>assets/frontend/js/index.js"></script>
	</head>
<body>
		
		<!-- ======================= Start Navigation ===================== -->
		<nav class="navbar navbar-default navbar-mobile navbar-fixed white no-background bootsnav">
			<div class="container">
			
				<!-- Start Logo Header Navigation -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
						<i class="fa fa-bars"></i>
					</button>
					<a class="navbar-brand" href="#">
						<img src="<?php echo base_url();?>assets/frontend/img/Logo-Illustrator.jpg" class="logo logo-display" alt="">
						<img src="<?php echo base_url();?>assets/frontend/img/Logo-Illustrator.jpg" class="logo logo-scrolled" alt="">
				</div>
				
				<div class="collapse navbar-collapse" id="navbar-menu">
					<ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
					    <li>
							<a href="<?php echo base_url();?>home" >Home</a>
						</li>
					
						<li>
							<a href="<?php echo base_url();?>about" >About</a>
						</li>
						
						<li class="dropdown">
							<a href="<?php echo base_url();?>professional" class="dropdown-toggle" data-toggle="dropdown">Professionals</a>
								<ul class="dropdown-menu animated fadeOutUp">
									<li><a href="<?php echo base_url();?>interior">Interior</a></li>
									<li><a href="<?php echo base_url();?>architecture">Architecture</a></li>
									<li><a href="<?php echo base_url();?>civil">Civil</a></li>
									<li><a href="<?php echo base_url();?>developer">Developer</a></li>
								</ul>
						</li>
						
						<li>
							<a href="<?php echo base_url();?>blog">Blogs</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>job">Jobs</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>contact">Contact</a>
						</li>
					</ul>
					
					<ul class="nav navbar-nav navbar-right">
						<li class="br-right"><a href="javascript:void(0)"  data-toggle="modal" data-target="#signin"><i class="login-icon ti-user"></i>Login</a></li>
						<li class="sign-up"><a class="btn-signup red-btn" href="<?php echo base_url();?>signup"><span class="ti-briefcase"></span>Create Account</a></li> 
					</ul>
						
				</div>
				<!-- /.navbar-collapse -->
			</div>   
		</nav>