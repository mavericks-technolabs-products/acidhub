<style>
li.nav-item.active.tabcenter {
    width: 100%;
    max-width: 250px;
    margin: 0 auto;
    text-align: center;
    float: inherit;

}
  #blah
{
	height: 100px;
	width: 100px;
	border-radius: 50%;
}
.blog-avatar.text-center img {
    width: 100%;
    border-radius: 50%;
    margin: 0 auto 5px;
    border: 4px solid rgba(255,255,255,1);
    box-shadow: 0 2px 10px 0 #d8dde6;
    -webkit-box-shadow: 0 2px 10px 0 #d8dde6;
    min-height: 88px;
    max-width: 28%;
}
.contact-footer a.col-half {
    
    text-align: left;
}
</style>
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ======================= Start Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Developer</h2>
					<p><a href="<?php echo base_url();?>home" title="Home">Home</a> <i class="ti-arrow-right"></i> Developer</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ======================= Start All Employee ===================== -->
		<section>
			<div class="container">
			
				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav-advance theme-bg" role="tablist">
					<li class="nav-item active tabcenter">
						<a class="nav-link" data-toggle="tab" href="#recent" role="tab">
						Recent</a>
					</li>
					
				</ul>
					<!-- Nav tabs -->
					<!-- Tab panels -->
				<div class="tab-content">
					<?php
					foreach($ans as $key=>$value) 
						
					{
						?>
					<!--Panel 1-->
					<div class="tab-pane fade in show active" id="recent" role="tabpanel">
					
						<!-- Single Employee List -->
						<div class="col-md-4 col-sm-6 mrg-bot-30">
							<div class="contact-box">
							
								<div class="flexbox mrg-l-10">
									<label class="toggler toggler-danger">
										<input type="checkbox">
										<i class="fa fa-heart"></i>
									</label>
								</div>
							
								<div class="blog-grid-box-content">
								<div class="blog-avatar text-center blogimg">
									<img src="<?php echo base_url();?>uploads/<?=$value->user_file;?>" class="img-responsive" alt="" />
								</div>
							</div>
								
								<div class="contact-caption">
									<h4 class="font-16 font-midium"><?=$value->firstname;?></h4>
									 <span><?=$value->type;?></span> 
								</div>
								
								<div class="contact-footer">
									<a href="developerdetails/<?=$value->user_details_id;?>" class="left-br col-half">
										<span class="con-profile"><i class="ti-eye"></i>Quick View</span>
									</a>
								</div>
								
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
				<!-- Tab panels -->
				
			</div>
		</section>
		
		<!-- ====================== End All Employee ================ -->
		
		
		<!-- ================= footer start ========================= -->
	<script type="text/javascript">
         function readURL(input) {
            if (input.files && input.files[0]) 
            {
                var reader = new FileReader();
				reader.onload = function (e) 
                {
                    $('#blah')
                        .attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        </script>	