<style type="text/css">
.grid-job-widget 
{
  padding: 13px!important;
}

  .blog-avatar.text-center.blogimg img {
    max-width: 91px;
    border-radius: 50%;
    margin: 0 auto 5px;
    border: 4px solid rgba(255,255,255,1);
    box-shadow: 0 2px 10px 0 #d8dde6;
    -webkit-box-shadow: 0 2px 10px 0 #d8dde6;
    -moz-box-shadow: 0 2px 10px 0 #d8dde6;
    width: 100%;
    height: 100%;
    max-height: 85px;
}	
.blog-grid-box-img {
    height: 191px;
    max-height: 250px;
    overflow: hidden;
    display: flex;
    align-items: center;
}

</style>
		<!-- ======================= End Navigation ===================== -->
			
			
		<!-- ======================= Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Architecture Detail</h2>
					<p><a href="<?php echo base_url();?>home" title="Home">Home</a> <i class="ti-arrow-right"></i>Architecture  Detail</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ================ Employer Profile ======================= -->
		<section>
			<div class="container">
				
				<div class="col-md-3 col-sm-3">
					<div class="emp-pic">
						<img src="<?php echo base_url();?>uploads/<?=$ans->user_file;?>" class="img-responsive" alt="" />
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4">
					<div class="emp-des">
						<h3><?=$ans->firstname;?></h3>
						<span class="theme-cl"><?=$ans->contact;?></span>
						<p><?=$ans->firm_address;?> </p>
					</div>
				</div>
				
				<div class="col-md-5 col-sm-5">
					<div class="emp-detail">
						<ul>
							<li><span class="cl-danger"><?php  
							$id=$ans->user_id;
							$count = $this->AcidModel->job_count($id); ?>
							<?php echo $count; ?></span>Jobs</li>
							<li><span class="cl-success"><?php  
							$id=$ans->user_id;
							$count = $this->AcidModel->active_job_count($id); ?>
							<?php echo $count; ?></span>Active Jobs</li>
						
						</ul>
					</div>
				</div>
				
			</div>
		</section>
		<!-- ================ End Employer Profile ======================= -->
		
		<!-- ================ Employers Jobs ======================= -->
		<section class="padd-top-20">
			<div class="container">
			
				<div class="row">
					
					<div class="col-md-12 col-sm-12">
						<div class="short">
							<div class="custom dropdown">
							  <button class="btn light-btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							   All Jobs
								<span class="caret caret-search"></span>
							  </button>
							  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<li><a href="#">Full Time </a></li>
								<li><a href="#">Part Time </a></li>
								<li><a href="#">Freelancer</a></li>
								<li><a href="#">Internship</a></li>
							  </ul>
							</div>
						</div>
					</div>					
				</div>
				<div class="row">
						 <?php $cnt=count($ans);
					        ?>
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						<br>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="#">
										<img src="<?php echo base_url();?>uploads/<?=$ans->user_file;?>" class="img-circle img-responsive" alt="" style="border-radius: 50%;" />
									</a>
								</div>
								<h5><a href="employer-detail/html"><?=$ans->firstname;?></a></h5>
								<p class="text-muted"><?=$ans->email;?></p>
								<p class="text-muted"><?=$ans->gender;?></p>
								<p class="text-muted"><?=$ans->firm_address;?></p>
							</div>
							
							<div class="job-type-grid">
								<a href="<?php echo base_url();?>architecture-project/<?=$ans->user_id;?>" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							<?php  ?>
						</div>

					</div>
					
					<!-- Single Job -->
					
					
				</div>
			</div>
		</section>
	