 <style type="text/css">
  .blog-avatar.text-center.blogimg img {
    max-width: 91px;
    border-radius: 50%;
    margin: 0 auto 5px;
    border: 4px solid rgba(255,255,255,1);
    box-shadow: 0 2px 10px 0 #d8dde6;
    -webkit-box-shadow: 0 2px 10px 0 #d8dde6;
    -moz-box-shadow: 0 2px 10px 0 #d8dde6;
    width: 100%;
    height: 100%;
    max-height: 100px;
} 
.blog-grid-box-img {
    height: 191px;
    max-height: 250px;
    overflow: hidden;
    display: flex;
    align-items: center;
}
</style>  
    			<!-- **********************************  start *************************** -->

		<div class="main-banner" style="background-image:url(<?php echo base_url();?>assets/frontend/img/slider-1.jpg);">
			<div class="container">
				<div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1">
				
					<div class="caption text-center cl-white">
						<h2>Find Your Career. You Deserve it.</h2>
						<!-- <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias</p> -->

						<a href="<?php echo base_url();?>login" class="btn theme-btn btn-m mrg-5"><span class="ti-upload padd-r-5"></span>Upload Your CV</a>
						<a href="<?php echo base_url();?>login" class="btn btn-hiring btn-m mrg-5"><span class="ti-briefcase padd-r-5"></span>Post a Job for Free</a>
					</div>
				</div>
			</div>
		</div>
		<!-- ======================= End Banner ===================== -->
		
		<!-- ====================== How It Work ================= -->
      
		<section class="how-it-works">
		
			<div class="container">
				<div class="row" data-aos="fade-up">
					<div class="col-md-12">
						<div class="heading">
							<h2>How It Works?</h2>
							<p>Post A Job To Tell Us About Your Project. We'll Quickly Match You With The Right Freelancers.</style></p>
						</div>
					</div>
				</div>
				
				<div class="row">
				
					<div class="col-md-4 col-sm-4">
						<div class="work-process">
							<span class="process-icon bg-danger-light">
								<i class="ti-user"></i>
								<span class="process-count bg-danger cl-white">1</span>
							</span>
							<h4  style="color:#ff7c39 !important">Create An Account</h4>
							<p>Post a job to tell us about your project. We'll quickly match you with the right freelancers.</p>
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4">
						<div class="work-process step-2">
							<span class="process-icon bg-success-light">
								<i class="ti-pencil-alt"></i>
								<span class="process-count bg-success cl-white">2</span>
							</span>
							<h4  style="color:#ff7c39 !important">Find & Hire</h4>
							<p>Post a job to tell us about your project. We'll quickly match you with the right freelancers.</p>
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4">
						<div class="work-process step-3">
							<span class="process-icon bg-purple-light">
								<i class="ti-thumb-up"></i>
								<span class="process-count bg-purple cl-white">3</span>
							</span>
							<h4  style="color:#ff7c39 !important">Start Work</h4>
							<p>Post a job to tell us about your project. We'll quickly match you with the right freelancers.</p>
						</div>
					</div>
					
				</div>
				</div>
			
		</section>

		
		<!-- ================= Category start ========================= -->
		<section class="gray-bg">

			<div class="container">
			
				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav-advance theme-bg" role="tablist">
					<li class="nav-item active">
						<a class="nav-link" data-toggle="tab" href="#recent" role="tab">
						Recent Jobs</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#featured" role="tab">
						Featured Jobs</a>
					</li>
				</ul>
				
				<div class="tab-content">
					
					<!-- Recent Job -->
					<div class="tab-pane fade in show active" id="recent" role="tabpanel">
						<div class="row">
		                  <?php
					foreach($ans as $key=>$value) 
					// 	var_dump($ans);
					// exit();
						
					{
						?>
							<!-- Single Job -->
							<div class="col-md-3 col-sm-6">
								<div class="grid-job-widget">
								
									
									
									<div class="u-content">
										 <div class="form-group">
               							 <div class="contact-img">
                						<img src="<?php echo base_url();?>uploads/<?=$value->user_file;?>" class="img-responsive" alt="" />
                					
						              </div>
						            </div>
										<h5><a href="<?php echo base_url();?>homedetails"><?=$value->type;?></a></h5>
										<p class="text-muted"><?=$value->job_title;?></p>
									</div>
									
									<div class="job-type-grid">
										<a href="<?php echo base_url();?>jobdetails/<?=$value->job_id;?>" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
									</div>
									
								</div>
							</div>

					<?php } ?>
							
							
						</div>
					
					</div>
					
				
					

				</div>
				
			</div>
		</section>
		
		<!-- ================= Job start ========================= -->
	
		<!-- ====================== Tag Section ============================ -->
		<section class="tag-sec" style="background:url(<?php echo base_url();?>assets/frontend/img/slider-01.jpg);">
			<div class="container">
				<div class="col-md-10 col-md-offset-1">
					<div class="tag-content">
						<img src="<?php echo base_url();?>assets/frontend/img/s-logo.png" class="img-responsive" alt="" />
						<h2>Hello favourite day</h2>
						<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa</p>
						<a href="#" class="btn theme-btn btn-radius" title="">EXPLORE US<i class="ti-shift-right"></i></a>
					</div>
				</div>
			</div>
		</section>
		<!-- =================== End Tag Section ==================== -->
		
		<!-- ========================= Pricing Section ========================= -->
		
	
		
		<!-- =================== Newsletter ==================== -->
		<section class="newsletter" style="background-image:url(<?php echo base_url();?>assets/frontend/img/trans-bg.png);">
			<div class="container">
				<div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
					<div class="newsletter-box text-center">
						<!-- <div class="input-group">
							<span class="input-group-addon"><span class="ti-email theme-cl"></span></span>
							<input type="text" class="form-control" placeholder="Enter your Email..">
						</div> -->
						<button type="button" class="btn theme-btn btn-radius btn-m">subscribe Me!</button>
					</div>
				</div>
			</div>
		</section>
		<!-- =================== End Newsletter ====================