<style type="text/css">
  .blog-avatar.text-center img {
    max-width: 90px;
    border-radius: 50%;
    margin: 0 auto 5px;
    border: 4px solid rgba(255,255,255,1);
    box-shadow: 0 2px 10px 0 #d8dde6;
    -webkit-box-shadow: 0 2px 10px 0 #d8dde6;
    -moz-box-shadow: 0 2px 10px 0 #d8dde6;
}
	
.blog-grid-box-img {
    height: 191px;
    max-height: 250px;
    overflow: hidden;
    display: flex;

    align-items: center;

}
.blog-box.blog-grid-box.box1{
    height: 100%;
    min-height: 600px;
           

</style>

		<!-- ======================= End Navigation ===================== -->
		
		
		<!-- ======================= Start Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Blog</h2>
					<p><a href="<?php echo base_url();?>home" title="Home">Home</a> <i class="ti-arrow-right"></i> Blog</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
		
		<!-- ===================== Blogs In Grid ===================== -->
		<section>
			<div class="container">

				<div class="row">
					<?php
					foreach($ans as $key=>$value) 
					{
					?>
					<div class="col-md-4 col-sm-6 aa">

						<div class="blog-box blog-grid-box box1">
							<div class="blog-grid-box-img">
								<img src="<?php echo base_url();?>uploads/blogs/<?=$value->blog_mult_img;?>" height="200px! important;" width="450px" class="img-responsive" alt="" />
							</div>
							
							<div class="blog-grid-box-content">
								<div class="blog-avatar text-center">
									<img src="<?php echo base_url();?>uploads/<?=$value->user_file;?>" class="" alt="" />
									<p><strong>By</strong> <span class="theme-cl"><?=$value->type;?></span></p>
								</div>
								<p><center><?php $date = $value->created_date;
											$newdate = date('d F Y', strtotime(str_replace('/', '-', $date)));
											echo $newdate;
								?></center></p>
								<p><center><?=$value->blog_author_name;?></center></p>
								<h4><center><?=$value->blog_title;?></center></h4>
								<p><center><?php $a=word_limiter($value->blog_description,30);
								echo $a;
								?></center></p>
								<a href="blogdetails/<?=$value->blog_id;?>" class="theme-cl" title="Read More..">Continue...</a>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
		<!-- ===================== End Blogs In Grid ===================== -->

		<!-- ================= footer start ========================= -->
		