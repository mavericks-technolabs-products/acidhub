
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ================ Job Detail Basic Information ======================= -->
		<section class="detail-section" style="background:url(<?php echo base_url();?>assets/frontend/img/slider-2.jpg);">
			<div class="overlay"></div>
			<div class="profile-cover-content">
				<div class="container">
					<div class="cover-buttons">
						<ul>
						<li><div class="buttons medium button-plain "><i class="fa fa-phone"></i>+91 528 578 5458</div></li>
						<li><div class="buttons medium button-plain "><i class="fa fa-map-marker"></i>#2852, SCO 20 Chandigarh</div></li>
						<li><a href="#add-review" class="buttons theme-btn"><i class="fa fa-paper-plane"></i><span class="hidden-xs">Apply Now</span></a></li>
						<li><a href="#" data-job-id="74" data-nonce="01a769d424" class="buttons btn-outlined"><i class="fa fa-heart-o"></i><span class="hidden-xs">Bookmark</span> </a></li>
						</ul>
					</div>
					<div class="job-owner hidden-xs hidden-sm">
						<div class="job-owner-avater">
							<img src="<?php echo base_url();?>assets/frontend/img/c-2.png" class="img-responsive img-circle" alt="" />
						</div>
						<div class="job-owner-detail">
							<h4>Web Designing</h4>
							<span class="theme-cl">Google PVT</span>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- ================ End Job Detail Basic Information ======================= -->
		
		<!-- ================ Start Job Overview ======================= -->
		<section>
			<div class="container">
				
				<!-- row -->
				<div class="row">
					
					<div class="col-md-8 col-sm-8">
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-body">
								<div class="job-title-bar">
									<h3>Web Design <span class="mrg-l-5 job-tag bg-success-light">Full Time</span></h3>
									<div>
										<p class="mrg-bot-0">
											<i class="ti-location-pin mrg-r-5"></i>
											2726 Shinn Street, New York
										</p>
										
										<p><strong>Roles</strong> : UX/UI Designer, Web Designer, Graphic Designer</p>
									</div>
								</div>
							</div>
						</div>
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-header">
								<h4>Overview</h4>
							</div>
							<div class="detail-wrapper-body">
								<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
								<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
							</div>
						</div>
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-header">
								<h4>Responsibilities</h4>
							</div>
							<div class="detail-wrapper-body">
								<ul class="detail-list">
									<li>Execute all visual design stages from concept to final hand-off to engineering</li>
									<li>Conceptualize original website design ideas that bring simplicity and user friendliness to complex roadblocks</li>
									<li>Create wireframes, storyboards, user flows, process flows and site maps to communicate interaction and design ideas</li>
									<li>Present and defend designs and key deliverables to peers and executive level stakeholders</li>
									<li>Establish and promote design guidelines, best practices and standards</li>
								</ul>
							</div>
						</div>
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-header">
								<h4>Requirements</h4>
							</div>
							<div class="detail-wrapper-body">
								<ul class="detail-list">
									<li>Proven work experienceas a web designer</li>
									<li>Demonstrable graphic design skills with a strong portfolio</li>
									<li>Proficiency in HTML, CSS and JavaScript for rapid prototyping</li>
									<li>Experience working in an Agile/Scrum development process</li>
									<li>Excellent visual design skills with sensitivity to user-system interaction</li>
									<li>Ability to solve problems creatively and effectively</li>
									<li>Up-to-date with the latest Web trends, techniques and technologies</li>
									<li>BS/MS in Human-Computer Interaction, Interaction Design or a Visual Arts subject</li>
								</ul>
							</div>
						</div>
						
					</div>
					
					<div class="col-md-4 col-sm-4">
						<div class="sidebar">
						
							<!-- Start: Opening hour -->
							<div class="widget-boxed">
								<div class="widget-boxed-body">
									<a href="#" class="btn btn-m theme-btn full-width mrg-bot-10"><i class="fa fa-paper-plane"></i>Apply For Job</a>
									<a href="#" class="btn btn-m light-gray-btn full-width"><i class="fa fa-linkedin"></i>Apply with Linkedin</a>
								</div>
							</div>
							<!-- End: Opening hour -->
							
							<!-- Start: Job Overview -->
							<div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-location-pin padd-r-10"></i>Location</h4>
								</div>
								<div class="widget-boxed-body">
									<div class="side-list no-border">
										<ul>
											<li><i class="ti-credit-card padd-r-10"></i>Package: 20K To 50K/Month</li>
											<li><i class="ti-world padd-r-10"></i>https://www.jobhill.com</li>
											<li><i class="ti-mobile padd-r-10"></i>91 234 567 8765</li>
											<li><i class="ti-email padd-r-10"></i>suppoer@listinghub.com</li>
											<li><i class="ti-pencil-alt padd-r-10"></i>Bachelor Degree</li>
											<li><i class="ti-shield padd-r-10"></i>3 Year Exp.</li>
										</ul>
										<h5>Share Job</h5>
										<ul class="side-list-inline no-border social-side">
											<li><a href="#"><i class="fa fa-facebook theme-cl"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus theme-cl"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter theme-cl"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin theme-cl"></i></a></li>
											<li><a href="#"><i class="fa fa-pinterest theme-cl"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<!-- End: Job Overview -->
							
							<!-- Start: Opening hour -->
							<div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-time padd-r-10"></i>Opening Hours</h4>
								</div>
								<div class="widget-boxed-body">
									<div class="side-list">
										<ul>
											<li>Monday <span>9 AM - 5 PM</span></li>
											<li>Tuesday <span>9 AM - 5 PM</span></li>
											<li>Wednesday <span>9 AM - 5 PM</span></li>
											<li>Thursday <span>9 AM - 5 PM</span></li>
											<li>Friday <span>9 AM - 5 PM</span></li>
											<li>Saturday <span>9 AM - 3 PM</span></li>
											<li>Sunday <span>Closed</span></li>
										</ul>
									</div>
								</div>
							</div>
							<!-- End: Opening hour -->
							 
						</div>
					</div>
					
				</div>
				<!-- End Row -->
				
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<h4 class="mrg-bot-20">More Jobs</h4>
					</div>
				</div>
				<!-- End Row -->
				
				<!-- row -->
				<div class="row">
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type full-type">Full Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox" checked>
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-1.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">Product Redesign</a></h5>
								<p class="text-muted">2708 Scenic Way, Sutter, IL 62373</p>
							</div>
							
							<div class="job-type-grid">
								<a href="#" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type full-type">Full Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox">
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-2.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">New Product Mockup</a></h5>
								<p class="text-muted">2708 Scenic Way, Sutter, IL 62373</p>
							</div>
							
							<div class="job-type-grid">
								<a href="#" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type part-type">Full Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox" checked>
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-3.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">Custom Php Developer</a></h5>
								<p class="text-muted">3765 C Street, Worcester</p>
							</div>
							
							<div class="job-type-grid">
								<a href="#" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
					
					<!-- Single Job -->
					<div class="col-md-3 col-sm-6">
						<div class="grid-job-widget">
						
							<span class="job-type part-type">Part Time</span>
							<div class="job-like">
								<label class="toggler toggler-danger">
									<input type="checkbox">
									<i class="fa fa-heart"></i>
								</label>
							</div>
							
							<div class="u-content">
								<div class="avatar box-80">
									<a href="employer-detail/html">
										<img class="img-responsive" src="assets/img/c-4.png" alt="">
									</a>
								</div>
								<h5><a href="employer-detail/html">Wordpress Developer</a></h5>
								<p class="text-muted">2719 Duff Avenue, Winooski</p>
							</div>
							
							<div class="job-type-grid">
								<a href="#" class="btn job-browse-btn btn-radius br-light">Browse Now</a>
							</div>
							
						</div>
					</div>
				</div>
				<!-- End Row -->
				
			</div>
		</section>
		
		<!-- ====================== End Job Overview ================ -->
		
		<!-- ================= footer start ========================= -->
		