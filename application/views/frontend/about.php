  <br><br>
	
		<section class="how-it-works">
			<div class="container">
				
				<div class="row" data-aos="fade-up">
					<div class="col-md-12">
						<div class="heading">
							<h2>How It Works?</h2>
							<p>Post A Job To Tell Us About Your Project. We'll Quickly Match You With The Right Freelancers.</p>
						</div>
					</div>
				</div>
				
				<div class="row">
				
					<div class="col-md-4 col-sm-4">
						<div class="work-process">
							<span class="process-icon bg-danger-light">
								<i class="ti-user"></i>
								<span class="process-count bg-danger cl-white">1</span>
							</span>
							<h4>Create An Account</h4>
							<p>Post a job to tell us about your project. We'll quickly match you with the right freelancers.</p>
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4">
						<div class="work-process step-2">
							<span class="process-icon bg-success-light">
								<i class="ti-pencil-alt"></i>
								<span class="process-count bg-success cl-white">2</span>
							</span>
							<h4>Find & Hire</h4>
							<p>Post a job to tell us about your project. We'll quickly match you with the right freelancers.</p>
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4">
						<div class="work-process step-3">
							<span class="process-icon bg-purple-light">
								<i class="ti-thumb-up"></i>
								<span class="process-count bg-purple cl-white">3</span>
							</span>
							<h4>Start Work</h4>
							<p>Post a job to tell us about your project. We'll quickly match you with the right freelancers.</p>
						</div>
					</div>
					
				</div>
				
			</div>
		</section>
		
		<!-- ================= Employer & Candidate Register Area ========================= -->
		<section class="padd-0">
			<div class="container-fluid padd-0">
			
				<div class="col-md-6 col-sm-6 padd-0">
					<div class="half-box employer-box text-center" style="background-image:url(<?php echo base_url();?>assets/frontend/img/employer.jpg);">
						<h2>Join With Acid Hill</h2>
						<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						<a href="<?php echo base_url();?>signup" class="btn theme-btn btn-radius btn-m">Create Account</a>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 padd-0">
					<div class="half-box candidate-box text-center" style="background-image:url(<?php echo base_url();?>assets/frontend/img/candidate.jpg);">
						<h2>Join With Acid Hill</h2>
						<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						<a href="<?php echo base_url();?>signup" class="btn light-btn btn-radius btn-m">Create Account</a>
					</div>
				</div>
				
			</div>
		</section>
		<!-- ================= Employer & Candidate Register Area ========================= -->
		
		<!-- ======================== All features ======================= -->
		<section>
			<div class="container">
				
				<div class="row" data-aos="fade-up">
					<div class="col-md-12">
						<div class="heading">
							<h2>Best features By Acidhill</h2>
							<p>Each month, more than 7 million Jobhunt turn to website in their search for work, making over<br>160,000 applications every day.</p>
						</div>
					</div>
				</div>
				
				<div class="row">
					
					<!-- Single Features -->
					<div class="col-md-4 col-sm-4">
						<div class="features-box">
							<i class="theme-cl ti-desktop" aria-hidden="true"></i>
							<h4>Web developers</h4>
							<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						</div>
					</div>
					
					<!-- Single Features -->
					<div class="col-md-4 col-sm-4">
						<div class="features-box">
							<i class="theme-cl ti-mobile" aria-hidden="true"></i>
							<h4>Mobile developers</h4>
							<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						</div>
					</div>
					
					<!-- Single Features -->
					<div class="col-md-4 col-sm-4">
						<div class="features-box">
							<i class="theme-cl ti-paint-bucket" aria-hidden="true"></i>
							<h4>Designers & Creatives</h4>
							<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						</div>
					</div>
					
					<!-- Single Features -->
					<div class="col-md-4 col-sm-4">
						<div class="features-box">
							<i class="theme-cl ti-ruler-pencil" aria-hidden="true"></i>
							<h4>Content Writers</h4>
							<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						</div>
					</div>
					
					<!-- Single Features -->
					<div class="col-md-4 col-sm-4">
						<div class="features-box">
							<i class="theme-cl ti-headphone-alt" aria-hidden="true"></i>
							<h4>Virtual assistants</h4>
							<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						</div>
					</div>
					
					<!-- Single Features -->
					<div class="col-md-4 col-sm-4">
						<div class="features-box">
							<i class="theme-cl ti-bar-chart-alt" aria-hidden="true"></i>
							<h4>Sales & marketing experts</h4>
							<p>At Vero Eos Et Accusamus Et Iusto Odio Dignissimos Ducimus Qui Blanditiis Praesentium Voluptatum Deleniti Atque.</p>
						</div>
					</div>
					
				</div>
				
			</div>
		</section>
		<!-- ======================== All features ======================= -->
		
		<!-- ====================== Tag Section ============================ -->
		<section class="tag-sec" style="background:url(<?php echo base_url();?>assets/frontend/img/slider-01.jpg);">
			<div class="container">
				<div class="col-md-10 col-md-offset-1">
					<div class="tag-content">
						<img src="<?php echo base_url();?>assets/frontend/img/s-logo.png" class="img-responsive" alt="" />
						<h2>Hello favourite day</h2>
						<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa</p>
						<a href="#" class="btn theme-btn btn-radius" title="">EXPLORE US<i class="ti-shift-right"></i></a>
					</div>
				</div>
			</div>
		</section>
		<!-- =================== End Tag Section ==================== -->
		
		<!-- ========================= Testimonial Start ====================== -->
		<section>
			<div class="container">
				
				<div class="row" data-aos="fade-up">
					<div class="col-md-12">
						<div class="heading">
							<h2>What Client Say's</h2>
							<p>Each month, more than 7 million Jobhunt turn to website in their search for work, making over<br>160,000 applications every day.</p>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div id="testimonial-slide" class="testimonial-carousel">
						
						<!-- Single Testimonial -->
						<div class="testimonial-detail">
							<div class="pic">
								<img src="<?php echo base_url();?>assets/frontend/img/client-1.jpg" alt="" class="avatar avatar-xl">
							</div>
							<h3 class="user-testimonial-title">Sagar Singh</h3>
							<p class="user-description">
								At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi.
							</p>
							<div class="client-rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
						</div>
						
						<!-- Single Testimonial -->
						<div class="testimonial-detail">
							<div class="pic">
								<img src="<?php echo base_url();?>assets/frontend/img/client-2.jpg" alt="" class="avatar avatar-xl">
							</div>
							<h3 class="user-testimonial-title">Alixa Chive</h3>
							<p class="user-description">
								At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi.
							</p>
							<div class="client-rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
						</div>
						
						<!-- Single Testimonial -->
						<div class="testimonial-detail">
							<div class="pic">
								<img src="<?php echo base_url();?>assets/frontend/img/client-3.jpg" alt="" class="avatar avatar-xl">
							</div>
							<h3 class="user-testimonial-title">Surveer Singh</h3>
							<p class="user-description">
								At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi.
							</p>
							<div class="client-rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
						</div>
						
						<!-- Single Testimonial -->
						<div class="testimonial-detail">
							<div class="pic">
								<img src="<?php echo base_url();?>assets/frontend/img/client-4.jpg" alt="" class="avatar avatar-xl">
							</div>
							<h3 class="user-testimonial-title">Alita Dixit</h3>
							<p class="user-description">
								At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi.
							</p>
							<div class="client-rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</section>
		<!-- =================== End testimonial ==================== -->
		
		<!-- =================== Newsletter ==================== -->
		<section class="newsletter" style="background-image:url(<?php echo base_url();?>assets/frontend/img/trans-bg.png);">
			<div class="container">
				<div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
					<div class="newsletter-box text-center">
						<!-- <div class="input-group">
							<span class="input-group-addon"><span class="ti-email theme-cl"></span></span>
							<input type="text" class="form-control" placeholder="Enter your Email..">
						</div> -->
						<button type="button" class="btn theme-btn btn-radius btn-m">subscribe Me!</button>
					</div>
				</div>
			</div>
		</section>
		<!-- =================== End Newsletter ==================== -->
		
		<!-- ================= footer start ========================= -->
		