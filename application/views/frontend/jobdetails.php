
		<!-- ======================= End Navigation ===================== -->
		
		<!-- ================ Job Detail Basic Information ======================= -->
		<section class="detail-section" style="background:url(<?php echo base_url();?>assets/frontend/img/slider-2.jpg);">
			<div class="overlay"></div>
			<div class="profile-cover-content">
				<div class="container">
					<div class="cover-buttons">
						<ul>
						<li><div class="buttons medium button-plain "><i class="fa fa-phone"></i><?=$ans[0]->firm_contact;?></div></li>
						<li><div class="buttons medium button-plain "><i class="fa fa-map-marker"></i>#<?=$ans[0]->firm_address;?></div></li>
						<li><a href="login" class="buttons theme-btn"><i class="fa fa-paper-plane"></i><span class="hidden-xs">Response</span></a></li>
						
						</ul>
					</div>
					<div class="job-owner hidden-xs hidden-sm">
						<div class="job-owner-avater">
							<img src="<?php echo base_url();?>assets/frontend/img/c-2.png" class="img-responsive img-circle" alt="" />
						</div>
						<div class="job-owner-detail">
							<h4><?=$ans[0]->company_name;?></h4>
							
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- ================ End Job Detail Basic Information ======================= -->
		
		<!-- ================ Start Job Overview ======================= -->
		<section>
			<div class="container">
				
				<!-- row -->
				<div class="row">
					<?php $cnt=count($ans);
					/*print_r($ans);
					exit();*/
					if($ans) 
						
					{?>
					<div class="col-md-8 col-sm-8">
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-body">
								
								<div class="job-title-bar">
									<h3><?=$ans[0]->job_title;?> <span class="mrg-l-5 job-tag bg-success-light"><?=$ans[0]->job_type;?></span></h3>
									<div>
										<p class="mrg-bot-0">
											<!-- <i class="ti-location-pin mrg-r-5"></i> -->
											<?=$ans[0]->firm_address;?>
										</p>
										
										
										<p><strong>Qualification</strong>:<?=$ans[0]->min_qualification;?></p>
										<p><strong>Roles</strong> :<?=$ans[0]->job_position;?></p>
										<p><strong>Package</strong> :<?=$ans[0]->package;?></p>
										<p><strong>Skill</strong> :<?=$ans[0]->skills;?></p>
										<p><strong>Gender</strong> :<?=$ans[0]->gender;?></p>
									</div>
								</div>
							</div>
						</div>
						
						<div class="detail-wrapper">
							<div class="detail-wrapper-header">
								<h4>Overview</h4>
							</div>
							<div class="detail-wrapper-body">
								<p><?=$ans[0]->job_description;?></p>
							</div>
						</div>
						<?php } ?>
						
						
					</div>
					
					<div class="col-md-4 col-sm-4">
						<div class="sidebar">
						
							<!-- Start: Opening hour -->
							<div class="widget-boxed">
								<div class="widget-boxed-body">
									<a href="<?php echo base_url();?>login" class="btn btn-m theme-btn full-width mrg-bot-10"><i class="fa fa-paper-plane"></i>Apply For Job</a>
									<!-- <a href="#" class="btn btn-m light-gray-btn full-width">Similar Jobs</a> -->
								</div>
							</div>
							<!-- End: Opening hour -->
							
							<!-- Start: Job Overview -->
							<div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-location-pin padd-r-10"></i>Location</h4>
								</div>
								<div class="widget-boxed-body">
									<div class="side-list no-border">
										<ul>
											<li><i class="ti-credit-card padd-r-10"></i>Package: <?=$ans[0]->package;?></li>
											<li><i class="ti-world padd-r-10"></i><?=$ans[0]->contact;?></li>
											<li><i class="ti-mobile padd-r-10"></i>91 234 567 8765</li>
											<li><i class="ti-email padd-r-10"></i><?=$ans[0]->email;?></li>
											
											
										</ul>
										
									</div>
								</div>
							</div>
							
							 
						</div>
					</div>
					
				</div>
				<!-- End Row -->
				
				
				
			</div>
		</section>
		
		<!-- ====================== End Job Overview ================ -->
		
		<!-- ================= footer start ========================= -->
