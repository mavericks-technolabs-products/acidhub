
		<!-- ======================= End Navigation ===================== -->
			
			
		<!-- ======================= Start Page Title ===================== -->
		<div class="page-title">
			<div class="container">
				<div class="page-caption">
					<h2>Blog Detail</h2>
					<p><a href="home" title="Home">Home</a> <i class="ti-arrow-right"></i> Blog Detail</p>
				</div>
			</div>
		</div>
		<!-- ======================= End Page Title ===================== -->
			
		<!-- ===================== Blogs In Grid ===================== -->
		<section>
			<div class="container">
			
				<div class="row">
					<?php $cnt=count($ans);
					if($ans) 
					{?>
					<!-- =============== Blog Detail ================= -->
					<div class="col-md-8 col-sm-12">
						
						<!-- /.Article -->
						<article class="blog-news detail-wrapper">
							<div class="full-blog">
							
								<!-- Featured Image -->
								<figure class="img-holder">
									<a href="blogdetail"><img src="<?php echo base_url();?>uploads/blogs/<?=$ans->blog_mult_img;?>" height="200px! important;" width="750px" class="img-responsive" alt="" /></a>
									<div class="blog-post-date theme-bg">
										<?php $date = $ans->created_date;
											$newdate = date('d F Y', strtotime(str_replace('/', '-', $date)));
											echo $newdate;
								?>
									</div>
								</figure>
								
								<!-- Blog Content -->
								<div class="full blog-content">
									<p><strong>Author Name</strong> :<?=$ans->blog_author_name;?></p>
									
									<div class="post-meta">By: <a href="#" class="author theme-cl"><?=$ans->blog_title;?></a> | </div>
									<a href="blog-detail.html"><h3></h3></a>
									<div class="blog-text">
										<h3>Overview:</h3>
										<p><?=$ans->blog_description;?></p>
									</div>
								</div>
								<!-- Blog Content -->
								<?php } ?>
							</div>
						</article>
					</div>
					<!-- /.col-md-8 -->
					
					<!-- ===================== Blog Sidebar ==================== -->
					<div class="col-md-4 col-sm-12">
						<div class="sidebar">
						
							<!-- Search Bar -->
							<div class="widget-boxed">
								<div class="widget-boxed-header border-0">
									<h4><i class="ti-search padd-r-10"></i>Search Here</h4>
								</div>
								
								<div class="widget-boxed-body padd-top-5">
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Search&hellip;">
										<span class="input-group-btn">
											<button type="button" class="btn height-50 theme-btn">Go</button>
										</span>
									</div>
								</div>
							</div>
			
							
							
							<!-- Start: Recent Listing -->
							<div class="widget-boxed">
								<div class="widget-boxed-header">
									<h4><i class="ti-check-box padd-r-10"></i>Recent Blogs</h4>
								</div>
								<div class="widget-boxed-body padd-top-5">
									<div class="side-list">
										<ul class="side-blog-list">
											<li>
												<a href="#">
													<div class="blog-list-img">
														<img src="<?php echo base_url();?>assets/frontend/img/image-3.jpg" class="img-responsive" alt="">
													</div>
												</a>
												<div class="blog-list-info">
													<h5><a href="#" title="blog">Freel Documentry</a></h5>
													<div class="blog-post-meta">
														<span class="updated">Nov 26, 2017</span> | <a href="#" rel="tag">Documentry</a>					
													</div>
												</div>
											</li>
											
											<li>
												<a href="#">
													<div class="blog-list-img">
														<img src="<?php echo base_url();?>assets/frontend/img/image-4.jpg" class="img-responsive" alt="">
													</div>
												</a>
												<div class="blog-list-info">
													<h5><a href="#" title="blog">Preez Food Rock</a></h5>
													<div class="blog-post-meta">
														<span class="updated">Oct 10, 2017</span> | <a href="#" rel="tag">Food</a>					
													</div>
												</div>
											</li>
											
											<li>
												<a href="#">
													<div class="blog-list-img">
														<img src="<?php echo base_url();?>assets/frontend/img/image-1.jpg" class="img-responsive" alt="">
													</div>
												</a>
												<div class="blog-list-info">
													<h5><a href="#" title="blog">Cricket Buzz High</a></h5>
													<div class="blog-post-meta">
														<span class="updated">Oct 07, 2017</span> | <a href="#" rel="tag">Sport</a>					
													</div>
												</div>
											</li>
											
											<li>
												<a href="#">
													<div class="blog-list-img">
														<img src="<?php echo base_url();?>assets/frontend/img/image-5.jpg" class="img-responsive" alt="">
													</div>
												</a>
												<div class="blog-list-info">
													<h5><a href="#" title="blog">Tour travel Tick</a></h5>
													<div class="blog-post-meta">
														<span class="updated">Sep 27, 2017</span> | <a href="#" rel="tag">Travel</a>					
													</div>
												</div>
											</li>

										</ul>
									</div>
								</div>
							</div>
							<!-- End: Recent Listing -->
							
						</div>
					</div>
					
				</div>
				
			</div>
		</section>
		<!-- ===================== End Blogs In Grid ===================== -->
			
		<!-- ================= footer start ========================= -->
	